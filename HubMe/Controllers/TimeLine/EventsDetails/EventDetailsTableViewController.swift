//
//  EventDetailsTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/3/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class EventDetailsTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var acceptBtnStatus: RoundedButton!
    @IBOutlet weak var rejectBtnStatus: RoundedButton!
    @IBOutlet weak var maybeBtnStatus: RoundedButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
