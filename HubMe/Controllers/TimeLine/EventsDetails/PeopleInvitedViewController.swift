//
//  PeopleInvitedViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/2/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

class PeopleInvitedViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var attendes :[OptionalAttendee] = []
    fileprivate var plazzaProfilePresenter: PlazzaProfilePresenter?

    override func viewDidLoad() {
        
    }
    
    @objc public static func create() -> PeopleInvitedViewController {
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! PeopleInvitedViewController
    }

}
extension PeopleInvitedViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "peopleInvitedCell") as! PeopleInvitedTableViewCell
        cell.inviteeName.text = attendes[indexPath.row].name ?? attendes[indexPath.row].email
        cell.inviteeTitle.text =  attendes[indexPath.row].status == nil ? attendeeState.required.rawValue : attendes[indexPath.row].status
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        plazzaProfilePresenter = PlazzaProfilePresenter(self)
        plazzaProfilePresenter?.getPlazzaProfile(email: attendes[indexPath.row].email ?? "")
        loading.startAnimating()
        tableView.allowsSelection = false


    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendes.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
extension PeopleInvitedViewController : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
        
        loading.stopAnimating()
        tableView.allowsSelection = true

        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
        self.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }
    func setError(error: NSError, url: String) {
        loading.stopAnimating()
        tableView.allowsSelection = true

        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        
    }
    func setErrorReponse(msg: String?) {
        loading.stopAnimating()
        tableView.allowsSelection = true

        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
        
    }
}
