//
//  File.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/2/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import RHPlaceholder
//var MyObservationContext = 0

class EventDetailsViewController: UIViewController  {
//    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var eventSubject: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var organizerName: UILabel!
    @IBOutlet weak var organizerEmail: UILabel!
    @IBOutlet weak var OrganizerImg: UIImageView!
    @IBOutlet weak var eventLoc: UILabel!
    @IBOutlet weak var notesLb: UILabel!
    @IBOutlet weak var attendessViewWidth: NSLayoutConstraint!
    @IBOutlet weak var locationViewWidth: NSLayoutConstraint!
    @IBOutlet weak var numberOfInvitees: UILabel!
    @IBOutlet weak var eventDesc: UIWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var acceptStatusBtn: RoundedButton!
    
    @IBOutlet weak var maybeStatusBtn: RoundedButton!
    @IBOutlet weak var rejectStatusBtn: RoundedButton!
    @IBOutlet weak var arrowImg: UIImageView!
    fileprivate var eventDetailsPresenter:EventDetailsPresenter?
    @IBOutlet weak var locationView: RoundRectView!
    @IBOutlet weak var attendeesView: RoundRectView!
    private let placeholderMarker = Placeholder()
    fileprivate var  eventBody = ""
    fileprivate var  eventDetails:TimelineEventDTO?
    @IBOutlet weak var webviewHeightConstraint: NSLayoutConstraint!
    fileprivate var plazzaProfilePresenter: PlazzaProfilePresenter?

//    var observing = false

    var eventId:String?
    var channelId:String?
    var eventSelectedDate:Date?
    
    override func viewDidLoad() {
        eventDetailsPresenter = EventDetailsPresenter(self)
        eventDetailsPresenter?.getEventDetails(appointmentId: eventId ?? "", userChannelId: channelId ?? "")
        registerPlaceholderViews()
        // Start animation
        placeholderMarker.startAnimation()
        eventDesc.delegate = self
    }
  
    private func registerPlaceholderViews() {
        let viewElements: [UIView] = [
            eventSubject,
            eventDate,
            eventTime,
            organizerName,
            organizerEmail,
            OrganizerImg,
            eventLoc,
            numberOfInvitees,
            eventDesc,
            maybeStatusBtn,
            rejectStatusBtn,
            acceptStatusBtn
        ]
        placeholderMarker.register(viewElements)
    }
    @objc private func removePhaceholder() {
        placeholderMarker.remove()
    }
    @objc public static func create() -> EventDetailsViewController {
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! EventDetailsViewController
    }
    @IBAction func showOrganizerProfileAction(_ sender: Any) {
        
//        plazzaProfilePresenter = PlazzaProfilePresenter(self)
//        plazzaProfilePresenter?.getPlazzaProfile(email: organizerEmail.text ?? "")
//        loading.startAnimating()

        
    }
    @IBAction func peopleInvitedViewAction(_ sender: Any) {
        
        if numberOfInvitees.text?.trim() != "No Attendees".localized()
        {
            let vc = PeopleInvitedViewController.create()
            
            var allAttenees = (eventDetails?.requiredAttendees) ?? []

            if eventDetails?.optionalAttendees != nil
            {
                
                for (_,attendee) in (eventDetails?.optionalAttendees?.enumerated())!
                {
                    allAttenees.append(OptionalAttendee(email: attendee.email ?? "", name: attendee.name ?? "", status: attendeeState.optional.rawValue))
                }
                vc.attendes = allAttenees
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            Helper.showFloatAlert(title: "you don't have attendees".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
    }

}
extension EventDetailsViewController : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
     }
    
}
extension EventDetailsViewController: EventDetailsPresenterView
{
    func getEventDetails(response: TimelineEventDTO?) {
//        loading.stopAnimating()
        self.eventDetails = response
        self.removePhaceholder()
        eventSubject.text = response?.subject == "" ? "No Subject".localized() : response?.subject
        eventDate.text = Helper.convertDatetoString(date: eventSelectedDate ?? Date(), formate: "EEE d,MMM yyyy")
        
        eventTime.text = Helper.convertDatetoString(date: Date(milliseconds: Int(response?.start ?? 0)), formate: "hh:mm a") + "-" +  Helper.convertDatetoString(date: Date(milliseconds: Int(response?.end ?? 0)), formate: "hh:mm a")
        
        organizerEmail.text = (response?.organizer ?? "")
        organizerName.text = (response?.organizer ?? "").components(separatedBy: "@")[0]
        
        eventLoc.text =  response?.location ?? "" == "" ? "No Location".localized() : response?.location
        
        if  eventLoc.text == "No Location".localized()
        {
            locationViewWidth.constant = 160.0
            locationView.cornerRadius = 20

            
        }
        else
        {
            var locationWidth = 60.0 + (eventLoc.text?.width(withConstrainedHeight: 42.0, font: UIFont.systemFont(ofSize: 16.0)))!
            if locationWidth > screenWidth * 0.9
            {
                locationWidth = screenWidth * 0.9
            }
            locationViewWidth.constant = locationWidth
//            locationView.cornerRadius = locationWidth/8
        }
        
        let numberofAttendees = (response?.requiredAttendees?.count ?? 0)  + (response?.optionalAttendees?.count ?? 0)
        
        if numberofAttendees == 0
        {
            numberOfInvitees.text = "No Attendees".localized()
            attendessViewWidth.constant = 160.0
            attendeesView.cornerRadius = 20
            arrowImg.isHidden = true


        }
        else
        {
            numberOfInvitees.text = String(describing: numberofAttendees)
            attendessViewWidth.constant = 80.0
            arrowImg.isHidden = false
//            attendeesView.cornerRadius = 10

        }
        eventBody = response?.body ?? ""
        
        if eventBody == ""
        {
            eventDesc.isHidden = true
            notesLb.isHidden = true
        }
        else
        {
            eventDesc.loadHTMLString(eventBody , baseURL: nil)
        }
//        self.webviewHeightConstraint.constant = eventDesc.scrollView.contentSize.height
        let status = meetingStatus(rawValue: response?.myResponse?.lowercased() ?? "")
        
        switch status
        {
        case .accept? , .organizer?:
            
            acceptStatusBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
            acceptStatusBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            rejectStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            rejectStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            maybeStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            maybeStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            
            break
            
        case .tentative?:
            
            acceptStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            acceptStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            rejectStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            rejectStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            maybeStatusBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
            maybeStatusBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            
            break
            
        case .decline?:
            
            acceptStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            acceptStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            rejectStatusBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
            rejectStatusBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            maybeStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            maybeStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            
            break
            
        default:
            
            acceptStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            acceptStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            rejectStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            rejectStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            maybeStatusBtn.backgroundColor = #colorLiteral(red: 0.9254901961, green: 0.9254901961, blue: 0.9254901961, alpha: 1)
            maybeStatusBtn.setTitleColor(#colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), for: .normal)
            
            break
        }
//        self.tableView.reloadData()
        self.view.updateConstraintsIfNeeded()
    }
    
    func setError(error: NSError, url: String) {
        loading.stopAnimating()
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        
    }
    func setErrorReponse(msg: String?) {
        loading.stopAnimating()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
        
    }
}
extension EventDetailsViewController : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
        
        loading.stopAnimating()
        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
        self.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }
   
}
