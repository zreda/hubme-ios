//
//  TimeLineViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/19/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import DatePickerDialog

class TimeLineViewController: UIViewController {
    @IBOutlet weak var selecteDateLb: UILabel!
    
    fileprivate var timelinePresenter:TimeLinePresenter?
    fileprivate var eventsTimeLine:[TimeLineEvent] = []
    fileprivate var selectedDate:Date?
    fileprivate var userChannelIds:[Int] = []
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    @IBOutlet weak var noEventsLb: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if selectedDate == nil
        {
            userChannelIds = []
            selecteDateLb.text = Helper.convertDatetoString(date: Date(), formate: "EEE d,MMM yyyy")
            selectedDate = Date()
            timelinePresenter = TimeLinePresenter(self)
            userChannelsPresenter = UserChannelsPresenter(self)
            userChannelsPresenter?.getFilteredUserChannels()
            loading.startAnimating()
            getTimeLinebyDate(startDate: String(describing: Date().startOfDay.currentTimeMillis()), endDate: String(describing: Date().endOfDay.currentTimeMillis()))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        selectedDate = nil
    }
    func getTimeLinebyDate(startDate:String,endDate:String)
    {
        noEventsLb.isHidden = true
        timelinePresenter?.getTimeLine(requestModel: TimeLineRequestModel(startDate: startDate, endDate: endDate, userChannelIds: userChannelIds))
        
    }
   
    func setEventsTimeLine(events: [TimeLineAppointments]?)
    {
        var startTimeEventsArray:[TimeLine] = []
        eventsTimeLine = []
        for (_,item) in (events?.enumerated())!
        {
            let index = geteventIndex(timeLine: startTimeEventsArray, startTime: item.start ?? 0)
            if index == nil
            {
                var events:[TimeLineAppointments] = []
                events.append(item)
                startTimeEventsArray.append(TimeLine(startTime: item.start ?? 0, events: events))
            }
            else
            {
                var events:[TimeLineAppointments] = startTimeEventsArray[index ?? 0].events ?? []
                events.append(item)
                startTimeEventsArray[index ?? 0].events = events
            }
            
        }
        
        
        for (_,item) in startTimeEventsArray.enumerated()
        {
            eventsTimeLine.append(TimeLineEvent(startTime:item.startTime ?? 0, events: item.events ?? []))
        }
        tableView.reloadData()
        
    }
    
    func geteventIndex(timeLine:[TimeLine],startTime:Double) -> Int?
    {
        for (i,item) in timeLine.enumerated()
        {
            if item.startTime == startTime
            {
                return i
            }
        }
        return nil
    }
    
    @IBAction func prevDateBtnAction(_ sender: Any) {
        
        selecteDateLb.text =  Helper.convertDatetoString(date: Calendar.current.date(byAdding: .day, value: -1, to: selectedDate ?? Date())!, formate: "EEE d,MMM yyyy")
        selectedDate = Calendar.current.date(byAdding: .day, value: -1, to: selectedDate ?? Date())!
        
        getTimeLinebyDate(startDate: String(describing: (selectedDate?.startOfDay.currentTimeMillis())!), endDate: String(describing: (selectedDate?.endOfDay.currentTimeMillis())!))
        
    }
    @IBAction func nextDateBtnAction(_ sender: Any) {
        selecteDateLb.text =  Helper.convertDatetoString(date: Calendar.current.date(byAdding: .day, value: 1, to: selectedDate ?? Date())!, formate: "EEE d,MMM yyyy")
        selectedDate = Calendar.current.date(byAdding: .day, value: 1, to: selectedDate ?? Date())!
        getTimeLinebyDate(startDate: String(describing: (selectedDate?.startOfDay.currentTimeMillis())!), endDate: String(describing: (selectedDate?.endOfDay.currentTimeMillis())!))
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func event1TapAction(_ sender: MyTapGesture) {
        let eventDetailsVC = EventDetailsViewController.create()
        eventDetailsVC.channelId = eventsTimeLine[sender.index].events?[sender.eventIndex].userChannelId
        eventDetailsVC.eventId = eventsTimeLine[sender.index].events?[sender.eventIndex].id
        eventDetailsVC.eventSelectedDate = selectedDate

        self.navigationController?.pushViewController(eventDetailsVC, animated: true)
        
        
    }
    @IBAction func event2TapAction(_ sender: MyTapGesture) {
        let eventDetailsVC = EventDetailsViewController.create()
        eventDetailsVC.channelId = eventsTimeLine[sender.index].events?[sender.eventIndex].userChannelId
        eventDetailsVC.eventId = eventsTimeLine[sender.index].events?[sender.eventIndex].id
        eventDetailsVC.eventSelectedDate = selectedDate

        self.navigationController?.pushViewController(eventDetailsVC, animated: true)

    }
    @IBAction func calendarBtnAction(_ sender: Any) {
        
        DatePickerDialog().show("Choose date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                
                self.selecteDateLb.text = Helper.convertDatetoString(date: dt , formate: "EEE d,MMM yyyy")
                self.selectedDate = dt
                self.getTimeLinebyDate(startDate: String(describing: dt.startOfDay.currentTimeMillis()), endDate: String(describing: dt.endOfDay.currentTimeMillis()))
            }
        }
        
    }
    
    
    @IBAction func filterBtnAction(_ sender: Any) {
        
        let filterVC = TimeLineFilterViewController.create()
        filterVC.delegate = self
        filterVC.selectedUserChannels = userChannelIds
        filterVC.selectedDate = selectedDate
        self.navigationController?.pushViewController(filterVC, animated: true)
//        self.present(filterVC, animated: true, completion: nil)

        
    }
    @objc func moreEventsBtnAction(sender:UIButton)
    {
        let moreEventsVC = MoreTimeLineViewController.create()
        moreEventsVC.eventsTimeLine = eventsTimeLine[sender.tag]
        moreEventsVC.selectedDate = selectedDate
        moreEventsVC.delegate = self
        self.navigationController?.pushViewController(moreEventsVC, animated: true)
    }
    
}
extension TimeLineViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let event1tap = MyTapGesture(target: self, action: #selector(self.event1TapAction(_:)))
        event1tap.index = indexPath.row
        
        let event2tap = MyTapGesture(target: self, action: #selector(self.event2TapAction(_:)))
        event2tap.index = indexPath.row

        let cell = tableView.dequeueReusableCell(withIdentifier: "timeLine") as! TimeLineViewTableViewCell
        cell.event1View.isHidden = false
        cell.event2View.isHidden = false
        cell.eventsMoreView.isHidden = false
        
    
        if eventsTimeLine[indexPath.row].events?.count ?? 0 >= 1
        {
            event1tap.eventIndex = 0
            cell.startTimeLb.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].startTime  ?? 0)), formate: "hh:mm")
            cell.timeMode.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].startTime ?? 0)), formate: "a")
            cell.eventStatusView.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine[indexPath.row].events?[0].myResponse?.lowercased())!) ?? .unknown).1

            cell.event1View.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine[indexPath.row].events?[0].myResponse?.lowercased())!) ?? .unknown).0
            cell.event1View.borderColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine[indexPath.row].events?[0].myResponse?.lowercased())!) ?? .unknown).1
            if eventsTimeLine[indexPath.row].events?[0].subject != "" && eventsTimeLine[indexPath.row].events?[0].subject != nil
            {
                cell.event1Title.text = eventsTimeLine[indexPath.row].events?[0].subject
            }
            else
            {
                cell.event1Title.text = "No subject".localized()
                
            }
            cell.event1Time.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].events?[0].start ?? 0)), formate: "hh:mm a") + "-" +  Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].events?[0].end ?? 0)), formate: "hh:mm a")
            
            if eventsTimeLine[indexPath.row].events?[0].channelName?.lowercased() == HubMeConstants.Channels.exchangeChannel.lowercased()
            {
                cell.event1ChannelImg.image = #imageLiteral(resourceName: "channel_exchange_color")
            }
            else {
                cell.event1ChannelImg.image = #imageLiteral(resourceName: "channel_gmail_color")
            }
            if eventsTimeLine[indexPath.row].events?[0].location != "" && eventsTimeLine[indexPath.row].events?[0].location != nil
            {
                cell.event1Location.text = eventsTimeLine[indexPath.row].events?[0].location
            }
            else
            {
                cell.event1Location.text = "No Location".localized()
            }
            
            if eventsTimeLine[indexPath.row].events?.count == 1
            {
                cell.event2View.isHidden = true
                cell.eventsMoreView.isHidden = true
            }
            else if eventsTimeLine[indexPath.row].events?.count ?? 0 >= 2
            {
                event2tap.eventIndex = 1

                cell.event2View.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine[indexPath.row].events?[1].myResponse?.lowercased())!) ?? .unknown).0
                cell.event2View.borderColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine[indexPath.row].events?[1].myResponse?.lowercased())!) ?? .unknown).1
                if eventsTimeLine[indexPath.row].events?[1].subject != "" && eventsTimeLine[indexPath.row].events?[1].subject != nil
                {
                    cell.event2Title.text = eventsTimeLine[indexPath.row].events?[1].subject
                }
                else
                {
                    cell.event2Title.text = "No subject".localized()
                    
                }
                if eventsTimeLine[indexPath.row].events?[1].channelName?.lowercased() == HubMeConstants.Channels.exchangeChannel.lowercased()
                {
                    cell.event2ChannelImg.image = #imageLiteral(resourceName: "channel_exchange_color")
                }
                else {
                    cell.event2ChannelImg.image = #imageLiteral(resourceName: "channel_gmail_color")
                }
                cell.event2Time.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].events?[1].start ?? 0)), formate: "hh:mm a") + "-" +  Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine[indexPath.row].events?[1].end ?? 0)), formate: "hh:mm a")
                
                if eventsTimeLine[indexPath.row].events?[1].location != "" && eventsTimeLine[indexPath.row].events?[1].location != nil
                {
                    cell.event2Location.text = eventsTimeLine[indexPath.row].events?[1].location
                }
                else
                {
                    cell.event2Location.text = "No Location".localized()
                    
                }
                
                if eventsTimeLine[indexPath.row].events?.count == 2
                {
                    cell.eventsMoreView.isHidden = true
                }
                else if eventsTimeLine[indexPath.row].events?.count ?? 0 > 2
                {
                cell.event2View.isHidden = true
                cell.eventsMoreView.isHidden = false
                
                cell.numExtraEvents.text = "+ \(String(describing:(eventsTimeLine[indexPath.row].events?.count ?? 0) - 2)) events"
                cell.extraEventsBtn.tag = indexPath.row
                cell.extraEventsBtn.addTarget(self, action: #selector(TimeLineViewController.moreEventsBtnAction), for: .touchUpInside)
                }

            }
            
        }
        
        cell.event1View.addGestureRecognizer(event1tap)
        cell.event2View.addGestureRecognizer(event2tap)

        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsTimeLine.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
}
extension TimeLineViewController : TimeLinePresenterView
{
    func getUserTimeLine(response: [TimeLineAppointments]?) {
        loading.stopAnimating()
        if response?.count == 0
        {
            noEventsLb.isHidden = false
            eventsTimeLine = []
            self.tableView.reloadData()
        }
        else
        {
            noEventsLb.isHidden = true
            setEventsTimeLine(events: response)
        }
    }
    func setErrorReponse(msg: String?) {
        loading.stopAnimating()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    func setError(error: NSError, url: String) {
        loading.stopAnimating()
        noEventsLb.isHidden = false
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        eventsTimeLine = []
        self.tableView.reloadData()

        
    }
}

extension TimeLineViewController : getEventsTimeLine
{
    func getSelectedDateTimeLine(date: Date) {
        loading.startAnimating()
        selectedDate = date
        selecteDateLb.text = Helper.convertDatetoString(date: date, formate: "EEE d,MMM yyyy")
        getTimeLinebyDate(startDate: String(describing: date.startOfDay.currentTimeMillis()), endDate: String(describing: date.endOfDay.currentTimeMillis()))
        
    }
}

extension TimeLineViewController : TimelineFilter
{
    func applyTimeLineFilter(selectedUserChannels: [Int],selectedDate:Date) {
        self.userChannelIds = selectedUserChannels
        self.selectedDate = selectedDate

        getTimeLinebyDate(startDate: String(describing: selectedDate.startOfDay.currentTimeMillis()), endDate: String(describing: selectedDate.endOfDay.currentTimeMillis()))
    }
}
extension TimeLineViewController : ChannelsPresenterView
{
    
    func setUserChannelsResult(response: [UserChannelsItem]?)
    {
     
        (response?.count ?? 0) > 0 ? (filterBtn.isEnabled = true) : (filterBtn.isEnabled = false)
        
    }
    
}
