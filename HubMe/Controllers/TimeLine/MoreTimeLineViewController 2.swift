//
//  MoreTimeLineViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/16/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

protocol getEventsTimeLine {
    func getSelectedDateTimeLine( date: Date)
}

class MoreTimeLineViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startTimeLb: UILabel!
    var eventsTimeLine:TimeLineEvent?
    var selectedDate:Date?
    var delegate: getEventsTimeLine?

    override func viewDidLoad() {
        startTimeLb.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine?.startTime  ?? 0)), formate: "hh:mm a")

    }
    
    @objc public static func create() -> MoreTimeLineViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! MoreTimeLineViewController
    }
    override func viewWillDisappear(_ animated: Bool) {
        delegate?.getSelectedDateTimeLine(date: selectedDate ?? Date())
    }
}
extension MoreTimeLineViewController:UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timeLine") as! TimeLineViewTableViewCell
        let view = UIView()
        view.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = view
        cell.event1View.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine?.events?[indexPath.row].myResponse?.lowercased())!) ?? .unknown).0
        cell.event1View.borderColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsTimeLine?.events?[indexPath.row].myResponse?.lowercased())!) ?? .unknown).1
        if eventsTimeLine?.events?[indexPath.row].subject != "" && eventsTimeLine?.events?[indexPath.row].subject != nil
        {
            cell.event1Title.text = eventsTimeLine?.events?[indexPath.row].subject
        }
        else
        {
            cell.event1Title.text = "No subject".localized()
            
        }
        cell.event1Time.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine?.events?[indexPath.row].start ?? 0)), formate: "hh:mm a") + "-" +  Helper.convertDatetoString(date: Date(milliseconds: Int(eventsTimeLine?.events?[indexPath.row].end ?? 0)), formate: "hh:mm a")
        
        if eventsTimeLine?.events?[indexPath.row].location != "" && eventsTimeLine?.events?[indexPath.row].location != nil
        {
            cell.event1Location.text = eventsTimeLine?.events?[indexPath.row].location
        }
        else
        {
            cell.event1Location.text = "No Location".localized()
        }
        if eventsTimeLine?.events?[indexPath.row].channelName?.lowercased() == HubMeConstants.Channels.exchangeChannel.lowercased()
        {
            cell.event1ChannelImg.image = #imageLiteral(resourceName: "channel_exchange_color")
        }
        else {
            cell.event1ChannelImg.image = #imageLiteral(resourceName: "channel_gmail_color")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventDetailsVC = EventDetailsViewController.create()
        eventDetailsVC.channelId = eventsTimeLine?.events?[indexPath.row].userChannelId
        eventDetailsVC.eventId = eventsTimeLine?.events?[indexPath.row].id
        
        self.navigationController?.pushViewController(eventDetailsVC, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eventsTimeLine?.events?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

