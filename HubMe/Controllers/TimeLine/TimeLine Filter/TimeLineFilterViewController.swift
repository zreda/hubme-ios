//
//  TimeLineFilterViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/22/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SDWebImage

protocol TimelineFilter {
    func applyTimeLineFilter(selectedUserChannels:[Int],selectedDate:Date)
}
class TimeLineFilterViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    var selectedUserChannels:[Int] = []
    var delegate: TimelineFilter?
    var accountItems:[GroupedChannel] =  []
    var selectedDate:Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        let nib = UINib(nibName: "FilterViewHeader", bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: "filterViewHeader")
//        self.navigationController?.navigationBar.backItem?.title = "Back".localized()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getFilteredUserChannels()
        
    }
    
    @objc public static func create() -> TimeLineFilterViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! TimeLineFilterViewController
    }
    @IBAction func backBtnAction(_ sender: Any) {
        var selectCount:Int = 0
        
        for (_,item) in accountItems.enumerated()
        {
            if item.isSelectAll == .deSelectAll
                
                //|| item.isSelectAll == .multiple
            {
                selectCount += 1
            }
        }
        
        if selectCount == accountItems.count
        {
            Helper.showFloatAlert(title: "Please Select at least one account".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

        }
        else
        {
            delegate?.applyTimeLineFilter(selectedUserChannels: selectedUserChannels, selectedDate: selectedDate ?? Date())
            self.navigationController?.popViewController(animated: true)
        }
       
       
    }
    
    @IBAction func applyFilterBtnAction(_ sender: Any) {
        
        selectedUserChannels = []
        
        for (_,item) in accountItems.enumerated()
        {
            for (_,account) in (item.accounts?.enumerated())!
            {
                if account.isChecked ?? false
                {
                    selectedUserChannels.append(account.id!)
                }
//                else
//                {
//                    selectedUserChannels.append(0)
//                }
            }
        }
        
        let allItemsEqual = selectedUserChannels.dropLast().allSatisfy { $0 == selectedUserChannels.last }
        if selectedUserChannels.count == 0
        {
            Helper.showFloatAlert(title: "Please Select at least one account".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else
        {
            delegate?.applyTimeLineFilter(selectedUserChannels: selectedUserChannels, selectedDate: selectedDate ?? Date())
            self.navigationController?.popViewController(animated: true)

        }
        
    }
    
    @IBAction func cancelFilterBtnAction(_ sender: Any)
    {
        delegate?.applyTimeLineFilter(selectedUserChannels: selectedUserChannels, selectedDate: selectedDate ?? Date())
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkSelectAllStatus(accountItem:GroupedChannel) -> (Bool,Int)
    {
        var selectStatus = 0
        
        for (_,item) in (accountItem.accounts?.enumerated())!
        {
            if item.isChecked ?? false
            {
                selectStatus += 1
            }
        }
        
        
        return (selectStatus == accountItem.accounts?.count ? true : false,selectStatus)
    }
    
    func isFoundSelected(channelId:Int) -> Bool
    {
        for (_,item) in selectedUserChannels.enumerated()
        {
            if channelId == item
            {
                return true
            }
        }
        return false
    }
    
    @objc func checkBtnTapped(sender:CheckBox)
    {
        let cell = tableView.cellForRow(at: sender.index) as! TimelineFilterTableViewCell
        
        if (cell.checkBtn != nil) {
            accountItems[sender.index.section].accounts?[sender.index.row].isChecked = !(accountItems[sender.index.section].accounts?[sender.index.row].isChecked ?? false)
        }
        
        if checkSelectAllStatus(accountItem: accountItems[sender.index.section]).0
        {
            accountItems[sender.index.section].isSelectAll = .selectAll
            setCheckStatus(isSelectAll: .selectAll, accounts:  accountItems[sender.index.section].accounts!)
            
        }
        else
        {
            accountItems[sender.index.section].isSelectAll = .multiple
        }
        tableView.reloadRows(at: [sender.index], with: .none)
        tableView.reloadSections([sender.index.section], with: .none)
        tableView.deselectRow(at: sender.index, animated: true)
    }
    
    func setCheckStatus(isSelectAll:SelectStatus ,  accounts:[UserChannelsItem])
    {
        switch isSelectAll
        {
        case .selectAll:
            accounts.forEach { $0.isChecked = true }
            break
        case .deSelectAll:
            accounts.forEach { $0.isChecked = false }
            break
        default:
            print("multiple")
        }
    }
}
extension TimeLineFilterViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filter_cell", for: indexPath) as! TimelineFilterTableViewCell
        
        switch accountItems[indexPath.section].isSelectAll
        {
        case .selectAll:
            accountItems[indexPath.section].accounts?[indexPath.row].isChecked = true
            break
        case .deSelectAll:
            accountItems[indexPath.section].accounts?[indexPath.row].isChecked = false
            break
        default:
            print("None")
        }
    
        cell.userMail.text = accountItems[indexPath.section].accounts?[indexPath.row].channelUsername
        cell.userName.text = accountItems[indexPath.section].accounts?[indexPath.row].channelUsername
        cell.userImg.sd_setImage(with: URL(string:  (HubMeConstants().main_url + "/ios" + (accountItems[indexPath.section].accounts?[indexPath.row].icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        cell.checkBtn.isChecked = accountItems[indexPath.section].accounts?[indexPath.row].isChecked ?? false
        cell.checkBtn.index = indexPath
        cell.checkBtn.addTarget(self, action: #selector(checkBtnTapped), for: UIControlEvents.touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "filterViewHeader") as! FilterViewHeader
        header.channelImg.sd_setImage(with: URL(string:  (HubMeConstants().main_url + "/ios" + (accountItems[section].accounts?[0].icon ?? "") )), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        
        switch (accountItems[section].isSelectAll)
        {
       
        case .selectAll:
            header.selectAllBtn.isChecked = true
            break
        case .deSelectAll:
            header.selectAllBtn.isChecked = false
            break
        default:
            header.selectAllBtn.isChecked = false
            print("None")
        }
        //        header.selectAllBtn.isChecked = accountItems[section].isSelectAll ?? false
        header.channelName.text = accountItems[section].groupName?.capitalizingFirstLetter()
        header.selectAllBtn.tag = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! TimelineFilterTableViewCell
        
        if (cell.checkBtn != nil) {
            accountItems[indexPath.section].accounts?[indexPath.row].isChecked = !cell.checkBtn.isChecked
        }
        
        if checkSelectAllStatus(accountItem: accountItems[indexPath.section]).0
        {
            accountItems[indexPath.section].isSelectAll = .selectAll
            
            setCheckStatus(isSelectAll: .selectAll, accounts:  accountItems[indexPath.section].accounts!)
        }
        else
        {
            accountItems[indexPath.section].isSelectAll = .multiple
        }
        
        tableView.reloadSections([indexPath.section], with: .none)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountItems[section].accounts?.count  ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return accountItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
}

extension TimeLineFilterViewController : SelectAllChannels
{
    func selectAllChannels(selectAll: SelectStatus , section :Int) {
        accountItems[section].isSelectAll = selectAll
        setCheckStatus(isSelectAll: selectAll, accounts:  accountItems[section].accounts!)
        tableView.reloadData()
    }
}
extension TimeLineFilterViewController : ChannelsPresenterView
{
    
    func setUserChannelsResult(response: [UserChannelsItem]?) {
        accountItems = []
        
        for(_,item) in Helper.groupChannels(channels: response ?? []).enumerated()
        {
            for (index,account) in (item.accounts?.enumerated())!
            {
                if selectedUserChannels.count > 0
                {
                    if isFoundSelected(channelId: account.id ?? 0)
                    {
                        item.accounts?[index].isChecked = true
                        
                    }
                }
                else
                {
                    selectedUserChannels = []
                    item.accounts?[index].isChecked = true
                }
            }
            if checkSelectAllStatus(accountItem: item).0
            {
                item.isSelectAll = .selectAll
                setCheckStatus(isSelectAll: .selectAll, accounts:  item.accounts!)
            }
            else
            {
                if checkSelectAllStatus(accountItem: item).1 < item.accounts?.count ?? 0 && checkSelectAllStatus(accountItem: item).1 != 0
                {
                    item.isSelectAll = .multiple
                }
                else
                {
                    item.isSelectAll = .deSelectAll
                    setCheckStatus(isSelectAll: .selectAll, accounts:  item.accounts!)
                }
            }
            
            accountItems.append(item)
            
        }
        
        tableView.reloadData()
        loading.stopAnimating()
        
    }
    
    func setError(error: NSError, url: String)
    {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        loading.stopAnimating()
        
    }
    
    
}
