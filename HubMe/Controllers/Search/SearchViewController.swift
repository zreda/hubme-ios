//
//  SearchViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/26/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var noSearchFound: UILabel!
    fileprivate var searchPresenter: SearchPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        searchPresenter = SearchPresenter(self)
        self.searchBar.delegate = self
        self.hideKeyboard()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false

    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @objc public static func create() -> SearchViewController {
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SearchViewController
    }

}
extension SearchViewController :  UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if  (searchBar.text?.trim().isEmpty ?? false) && searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" && searchBar.text?.trim() == ""
            
        {
            Helper.showFloatAlert(title: "search text empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            
        else
        {
            self.dismissKeyboard()

            if !loading.isAnimating
            {
                loading.startAnimating()
                noSearchFound.isHidden = true
                searchPresenter?.getSearchResults(searchText: searchBar.text?.trim() ?? "", itemsPerPage: String(describing:10), pageIndex: String(describing:0))
            }
        }
        
    }
}
extension SearchViewController : SearchPresenterView
{
    
    
    func getSearchResults(response: FeedsResponse?) {
        loading.stopAnimating()

        if response?.results?.count ?? 0 > 0
        {
            let feedsVC = FeedsTableViewController.create()
            feedsVC.type = .search
            feedsVC.feedList = response?.results ?? []
            feedsVC.searchText = searchBar.text ?? ""
            feedsVC.nextPageNumber = response?.nextPageIndex ?? ""
             
            self.navigationController?.pushViewController(feedsVC, animated: true)
        }
        else
        {
            noSearchFound.isHidden = false
        }
    }
    
    func setError(error: NSError, url: String) {
        loading.stopAnimating()
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)

    }
}
