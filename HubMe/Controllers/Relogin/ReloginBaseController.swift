//
//  ReloginBaseController.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/20/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import SafariServices
import LSDialogViewController
import StompClientLib

class ReloginBaseController : NSObject
    
{
    fileprivate var safariVC:SFSafariViewController?
    fileprivate var socketClient = StompClientLib()
    fileprivate var url = NSURL()
    fileprivate static let alert = SweetAlert()
    fileprivate var loginPresenter: LoginChannelPresenter?

    var controller:UIViewController?
    var channelName:String?
    var channelId:String?

    override init() {
        super.init()
        self.registerSocket()
        loginPresenter = LoginChannelPresenter(self)
    }
   

    func showReloginDialogue(accountName:String,channelName:String,channelId:String ,selectedAccount:UserChannelsItem)
    {
        if !ReloginBaseController.alert.isBeingPresented {
            
            ReloginBaseController.alert.titleLabel.font = UIFont.systemFont(ofSize: 10.0)
            _ = ReloginBaseController.alert.showAlert("account \(accountName) is Expired", subTitle: "Do you need Relogin ?".localized(), style: AlertStyle.none, buttonTitle:"Cancel".localized(), buttonColor:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , otherButtonTitle:  "OK".localized(), otherButtonColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) { (isOtherButton) -> Void in
                if !isOtherButton == true
                {
                    self.reLogin(channelName: channelName,channelId: channelId,selectedAccount: selectedAccount)
                }
            }
        }
       
    }
    
    func reLogin(channelName:String,channelId:String,selectedAccount:UserChannelsItem)
    {
        
        if (channelName.lowercased() == HubMeConstants.Channels.twitterChannel.lowercased())
        {
            self.channelId =  channelId
            self.channelName = channelName
            
            chooseFirstLoad(selectedConfig: selectedAccount.firstLoadConfig ?? FirstLoadConfig())
            
        }
        else if (channelName.lowercased() == HubMeConstants.Channels.gmailChannel.lowercased())
            
        {
            self.channelId =  channelId
            self.channelName = channelName
            
            showConfig(LSAnimationPattern.zoomInOut, firstConfig: selectedAccount.firstLoadConfig ?? FirstLoadConfig(criteria: "DATE", periods: HubMeConstants.periods))
            
        }
        else if (channelName.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() || channelName.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased())
        {
            let loginVC = AddMessageChannelViewController.create()
            loginVC.channel = selectedAccount
            loginVC.config = selectedAccount.firstLoadConfig ?? FirstLoadConfig(criteria: "DATE", periods: HubMeConstants.periods)
            loginVC.isExpired = true
            controller?.dismiss(animated: false, completion: nil)
            controller?.navigationController?.pushViewController(loginVC, animated: true)
            
        }
        else
        {
            let vc = LoginChannelViewController.create()
            vc.channel = selectedAccount
            vc.config =  selectedAccount.firstLoadConfig ?? FirstLoadConfig(criteria: "DATE", periods: HubMeConstants.periods)
//            vc.accountName = selectedAccount.channelUsername
            vc.email = selectedAccount.channelUsername
            vc.accountName = selectedAccount.accountName
            vc.isExpired = true
            
            if controller?.navigationController != nil
            {
                controller?.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                controller?.dismiss(animated: true, completion: {
                    self.controller?.navigationController?.pushViewController(vc, animated: true)
                })
            }        }
    }
    
    func showConfig(_ animationPattern: LSAnimationPattern ,firstConfig:FirstLoadConfig )
    {
        // to show the dialog
        let dialogViewController: ChooseChannelSynchronoizationViewController = ChooseChannelSynchronoizationViewController(nibName:"ChooseSynchronoizationPopupDialog", bundle: nil)
        dialogViewController.reloginDelegate = self
        dialogViewController.reloginController = controller!
        dialogViewController.config = firstConfig
        controller?.presentDialogViewController(dialogViewController, animationPattern: animationPattern, completion: { () -> Void in })
    }
    
    
    func chooseFirstLoad(selectedConfig:FirstLoadConfig)
    {
        var firstLoadFetchValue  = 200
        var auth = LoginRequestModel()
        
        if channelName?.lowercased() != HubMeConstants.Channels.twitterChannel.lowercased()
        {
            firstLoadFetchValue = selectedConfig.firstLoadFetchValue ?? 0
        }
        
        auth = LoginRequestModel(expired:true,channel_id: channelId ?? "", channel_name: channelName ?? "", firstLoadConfig: FirstLoadConfig(criteria: selectedConfig.criteria ?? "COUNT", firstLoadFetchValue: firstLoadFetchValue))
        self.loginPresenter = LoginChannelPresenter(self)
        
        self.loginPresenter?.getLoginSocial(auth: auth)
    }
    
    func dismssPeriodDialog()
    {
        controller?.dismissDialogViewController(LSAnimationPattern.zoomInOut)
        
    }
    @objc func safariLogin(notification: NSNotification) {
        // get the url from the auth callback
        safariVC?.dismiss(animated: true, completion: nil)
    }
}

extension ReloginBaseController : LoginChannelPresenterView
{
    func setErrorReponse(msg: String?)
    {
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    func setError(error: NSError, url: String) {
        controller?.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
    }
    
    func setLoginSocialResult(response: String, authUrl: String) {
        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            if channelName?.lowercased() == HubMeConstants.Channels.gmailChannel.lowercased()
            {
//                dismssPeriodDialog()
                let urlStr = authUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                
                if let authURL = NSURL(string: urlStr ?? "")
                {
                    
                    let safariVC = SFSafariViewController(url: authURL as URL)
                    safariVC.delegate = self
//                    UIApplication.shared.keyWindow?.rootViewController?.present(safariVC, animated: true, completion: nil)
//                    let top = UIApplication.shared.keyWindow?.rootViewController
                   controller?.present(safariVC, animated: true)
                    registerSocket()
                }
                
            }
            else
            {
                let webVC = AddSocialAccountViewController.create()
                webVC.channelName = channelName ?? ""
                webVC.authUrl = authUrl
                controller?.navigationController?.pushViewController(webVC, animated: true)
            }
        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
    }
    
}
extension ReloginBaseController  : SFSafariViewControllerDelegate
{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("listening ....")
        controller.dismiss(animated: false, completion: nil)
        controller.navigationController?.popViewController(animated: true)
        
    }
  
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true) { () -> Void in
            print("You just dismissed the login view.")
        }
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        print("didLoadSuccessfully: \(didLoadSuccessfully)")
        
    }
}

extension ReloginBaseController : StompClientLibDelegate
{
    func registerSocket(){
        
        let rabbitMQSec = Helper.getObjectDefault(key: HubMeConstants.userDefault.rabbitMQSecurity) as? RabbitmqConfig
        
        let baseURL = rabbitMQSec?.url
        url = NSURL(string: baseURL ?? "")!
        
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":rabbitMQSec?.username ?? "","passcode":rabbitMQSec?.password ?? "","host":"/"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
//        socketClient.autoDisconnect(time: 15)
        // Reconnect after 4 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        debugPrint("Socket is Disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        debugPrint("Socket will is Disconnected")
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, akaStringBody stringBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("JSON BODY : \(String(describing: jsonBody))")
        if let response = UserChannelResponse(JSON:  jsonBody as! [String: Any])
        {
            if response.message?.lowercased() == HubMeConstants.Messages.success.lowercased()
            {
                Helper.showFloatAlert(title: HubMeConstants.Messages.success.localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.controller?.navigationController?.popToRootViewController(animated: true)
                })
            }
            else
            {
                if response.message == "ACCOUNT_ALREADY_EXISTS".localized()
                {
                    Helper.showFloatAlert(title: "Account already exists".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: response.message ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.safariVC?.dismissDialogViewController()
                    //                    self.navigationController?.popViewController(animated: true)
                })
                
                
            }
        }
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        debugPrint("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        debugPrint("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        debugPrint("Server Ping")
    }
}

