//
//  AccountBottomSheetViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/28/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import FittedSheets
import SDWebImage


class AccountBottomSheetViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate var selectedIndex:Int?
    var accountsItem:[UserChannelsItem]?
    var selectedAccountItem: UserChannelsItem?
    var channelName:String?
    weak var delegate: RefreshAccountsDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.sheetViewController?.handleScrollView(self.tableView)
        getIndexOfSelectedAccount()
    }
    
    @objc public static func create() -> AccountBottomSheetViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AccountBottomSheetViewController
    }
    
    func getIndexOfSelectedAccount()
    {
        for (i,item) in (accountsItem?.enumerated())!
        {
            if selectedAccountItem?.channelUsername == item.channelUsername
            {
                selectedIndex = i
                return
            }
        }
        selectedIndex = 0
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
}

extension AccountBottomSheetViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header_cell", for: indexPath)
            return cell
        }
       
         if indexPath.row == tableView.numberOfRows(inSection: 0)-1 //  add account
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "manage_add_cell", for: indexPath) as UITableViewCell
            cell.imageView?.image = #imageLiteral(resourceName: "addAccount")
            cell.textLabel?.text = "Add new account".localized()
            
            if channelName?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() || channelName?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased()
            {
               cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            }
            else
            {
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

            }

            return cell

        }
        else if indexPath.row == tableView.numberOfRows(inSection: 0)-2 // manage account
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "manage_add_cell", for: indexPath) as UITableViewCell
            cell.imageView?.image = #imageLiteral(resourceName: "avatar")
            cell.textLabel?.text = "Manage my accounts".localized()
            return cell

        }
        
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "account_cell", for: indexPath) as! AccountTableViewCell
            cell.accountEmail.text = accountsItem?[indexPath.row-1].channelUsername ?? ""
            cell.userImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
                (accountsItem?[indexPath.row-1].icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            if selectedIndex == indexPath.row-1
            {
                cell.defaultImg.isHidden = false
            }
            else
            {
                cell.defaultImg.isHidden = true
            }

            return cell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (accountsItem?.count ?? 0) + 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row > 0 && indexPath.row < tableView.numberOfRows(inSection: 0)-1
        {
            return 80.0
        }
        else
        {
            return 60.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == tableView.numberOfRows(inSection: 0)-1 //  add account
        {
            if accountsItem?[0].name?.lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased() &&    accountsItem?[0].name?.lowercased() != HubMeConstants.Channels.slackChannel.lowercased()
            {
                delegate?.manageAccount(accountsItem: accountsItem ?? [], selectedAccountItem: selectedAccountItem ?? UserChannelsItem(), tag: HubMeConstants.ManageAccount.add.rawValue)

            }
        }
        else if indexPath.row == tableView.numberOfRows(inSection: 0)-2 // manage account
        {
            delegate?.manageAccount(accountsItem: accountsItem ?? [], selectedAccountItem: selectedAccountItem ?? UserChannelsItem(), tag: HubMeConstants.ManageAccount.manage.rawValue)
        }
        else
        {
            if accountsItem?.count ?? 0 > 1
            {
                delegate?.manageAccount(accountsItem: accountsItem ?? [], selectedAccountItem: accountsItem?[indexPath.row-1] ?? UserChannelsItem(), tag: "")
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
