//
//  ManageAccountViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/1/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

protocol RefreshAccountsDelegate: class {
    func refreshAccount(accountsItem: [UserChannelsItem]?,selectedAccountItem:UserChannelsItem?)
    func manageAccount(accountsItem: [UserChannelsItem]?,selectedAccountItem:UserChannelsItem?,tag:String)

}

class ManageAccountViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var selectedIndex:Int?
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var deletedIndex:Int?
    fileprivate let alert = SweetAlert()
    weak var delegate: RefreshAccountsDelegate?
    
    var accountsItem:[UserChannelsItem]?
    var selectedAccountItem: UserChannelsItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        userChannelsPresenter =  UserChannelsPresenter(self)
        getIndexOfSelectedAccount()
    }
    @objc public static func create() -> ManageAccountViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ManageAccountViewController
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        delegate?.refreshAccount(accountsItem: accountsItem, selectedAccountItem: selectedAccountItem)
    }
    func getIndexOfSelectedAccount()
    {
        for (i,item) in (accountsItem?.enumerated())!
        {
            if selectedAccountItem?.channelUsername == item.channelUsername
            {
                selectedIndex = i
                return
            }
        }
        selectedIndex = 0
        selectedAccountItem = accountsItem?[selectedIndex ?? 0]
    }
    @objc func deleteChannelBtnAction(sender:UIButton)
    {
        alert.titleLabel.font = UIFont.systemFont(ofSize: 10.0)
        _ = alert.showAlert("Remove account \(accountsItem?[sender.tag].channelUsername ?? "") ?", subTitle: "This account and any unsaved data will be removed from this applicationt.".localized(), style: AlertStyle.none, buttonTitle:"Cancel".localized(), buttonColor:#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) , otherButtonTitle:  "Delete".localized(), otherButtonColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) { (isOtherButton) -> Void in
            if !isOtherButton == true
            {

                self.loading.startAnimating()
                
                self.deletedIndex = sender.tag
                self.userChannelsPresenter?.deleteChannel(channel_id: String(describing:self.accountsItem?[sender.tag].id as! Int))
            }
            
        }
    }
}

extension ManageAccountViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ManageTableViewCell
        cell.accountEmailLb.text = accountsItem?[indexPath.row].channelUsername ?? ""
        cell.accountImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
            (accountsItem?[indexPath.row].icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(ManageAccountViewController.deleteChannelBtnAction), for: .touchUpInside)
        if selectedIndex == indexPath.row
        {
            cell.defaultImg.isHidden = false
        }
        else
        {
            cell.defaultImg.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accountsItem?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140.0
    }
    
}
extension ManageAccountViewController : ChannelsPresenterView
{
    func deleteChannel(response: String,  error :Bool) {
        
//        self.alert.showAlert("Cancelled!", subTitle: "Your account is safe".localized(), style: AlertStyle.error)
        self.loading.stopAnimating()

        if !error {
            //Remove object from array
            accountsItem?.remove(at: deletedIndex ?? 0)
            if accountsItem?.count == 0
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else
            {
                getIndexOfSelectedAccount()
//                selectedAccountItem = accountsItem?[0]
                
                self.tableView.reloadData()

            }
            
            Helper.showFloatAlert(title: String(format:"delete_channel_success".localized(),response), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
           
        }
            
        else
        {
            Helper.showFloatAlert(title: String(format:"delete_channel_error".localized(),response), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        
    }
    
    func setError(error: NSError,url:String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code )!, messge: error.description, url: url)
        loading.stopAnimating()
    }
}
