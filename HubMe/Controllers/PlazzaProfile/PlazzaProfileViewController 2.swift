//
//  PlazzaProfileTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/10/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class PlazzaProfileViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noProfileLb: UILabel!
    fileprivate var numberOfSections:Int = 0
    fileprivate var plazzaProfileArray:[[String]] = []
    fileprivate var sectionStringArray:[String] = []
    fileprivate var imgs:[UIImage] = [#imageLiteral(resourceName: "mail"),#imageLiteral(resourceName: "call"),#imageLiteral(resourceName: "pin"),#imageLiteral(resourceName: "plazza")]
    fileprivate var titles:[String] = ["Emails".localized(),"Phones".localized(),"Addresses".localized(),"Plazza".localized()]
    var profile:Profile?

    override func viewDidLoad() {
     
        super.viewDidLoad()
        //        registerPlaceholderViews()
        //        placeholderMarker.startAnimation()
        tableView.tableFooterView = UIView()
        initView()
        
       
    }
    
    func initView()
    {
    
        if profile?.emails?.count ?? 0 > 0
        {
            sectionStringArray.append(titles[0])
        }
        if profile?.phoneNumbers?.count ?? 0 > 0
        {
            sectionStringArray.append(titles[1])
        }
        if profile?.addresses?.count ?? 0 > 0
        {
            sectionStringArray.append(titles[2])
        }
        if (profile?.profileLink) != nil
        {
            sectionStringArray.append(titles[3])
        }
    }
    
    @objc public static func create() -> PlazzaProfileViewController {
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! PlazzaProfileViewController
    }
   
}
extension PlazzaProfileViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "header_cell") as! PlazzaProfileHeaderTableViewCell
            cell.plazzaImg.image =  Helper.convertBase64ToImage(imageString: profile?.avatar ?? "")
            cell.plazzaName.text = profile?.displayName ?? ""
            cell.plazzaTitle.text = profile?.title ?? ""
            cell.layoutIfNeeded()

            return cell
        }
       
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? PlazzaProfileTableViewCell
            var data:[String] = []
            if sectionStringArray[indexPath.row - 1] == titles[0]
            {
                cell?.sectionItem.0 = titles[0]
                cell?.sectionItem.1 = imgs[0]
                for (_,item) in (profile?.emails?.enumerated())!
                {
                    data.append(item.value ?? "")
                }
                cell?.dataArray = data
//                cell?.isUserInteractionEnabled = false
            }
            else if sectionStringArray[indexPath.row - 1] == titles[1]
            {
                cell?.sectionItem.0 = titles[1]
                cell?.sectionItem.1 = imgs[1]
                for (_,item) in (profile?.phoneNumbers?.enumerated())!
                {
                    data.append(item.value ?? "")
                }
                
                cell?.dataArray = data
//                cell?.isUserInteractionEnabled = false

            }
            else if sectionStringArray[indexPath.row - 1] == titles[2]
            {
                cell?.sectionItem.0 = titles[2]
                cell?.sectionItem.1 = imgs[2]
                for (_,item) in (profile?.addresses?.enumerated())!
                {
                    data.append("\(item.value?.streetAddress ?? "") ,\(item.value?.locality ?? "") , \(item.value?.postalCode ?? "") ,\(item.value?.country ?? "")")
                }
                cell?.dataArray = data
//                cell?.isUserInteractionEnabled = false

            }
            else if sectionStringArray[indexPath.row - 1] == titles[3]
            {
                cell?.sectionItem.0 = titles[3]
                cell?.sectionItem.1 = imgs[3]
                data.append(profile?.profileLink ?? "")
//                cell?.isUserInteractionEnabled = true

                cell?.dataArray = data
            }
            cell?.layoutIfNeeded()

        return cell ?? UITableViewCell()
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 110.0
        }
        else
        {
            return UITableViewAutomaticDimension
            
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension 
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  sectionStringArray.count + 1
    }
}
