//
//  ClientSideMenuViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/26/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class ClientSideMenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate let sideMenuTitle:[String] = ["Settings","Terms and conditions", "Feedback","Log out"]
    fileprivate let sideMenuImages = [#imageLiteral(resourceName: "ic_settings"),#imageLiteral(resourceName: "ic_terms_conditions"), #imageLiteral(resourceName: "feedback"),#imageLiteral(resourceName: "ic_logout")]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @objc public static func create() -> ClientSideMenuViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: "MenuNavigation")) as! ClientSideMenuViewController
    }
}

extension ClientSideMenuViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sidemenu_banner", for: indexPath) as! SideMenuHeaderTableViewCell
            let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
            
            cell.appVersionLb.text  = "Version \(versionNumber).\(buildNumber)"
            //            cell.appVersionLb.text  = Helper.getUserDefault(key: HubMeConstants.userDefault.latestVersion) as! String
            cell.usernameLb.text = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userName) as? String) ?? ""
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = sideMenuTitle[indexPath.row-1].localized()
            cell.imageView?.image = sideMenuImages[indexPath.row - 1]
            return cell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.sidemenu), object: nil, userInfo: ["sectionIndex":indexPath.row])
        //
        //        sideMenuController?.hideMenu()
        if indexPath.row == 1 // setting
        {
            self.navigationController?.pushViewController(SettingsViewController.create(), animated: true)
        }
        else if indexPath.row  == 2 // terms and conditions
        {
            self.navigationController?.pushViewController(TermsandAgreementViewController.create(), animated: true)
            
        }
        else if indexPath.row  == 3 // feedback
        {
            self.navigationController?.pushViewController(SendFeedbackViewController.create(), animated: true)
            
        }
        else if indexPath.row  == 4 // logout
        {
            Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userData)
            Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userName)
            let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
            
            let loginVC = mainSB.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController
            loginVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
            self.navigationController?.present(loginVC, animated: true, completion: nil)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 180
        }
        else
        {
            return 60.0
        }
    }
}
