//
//  MoreTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/9/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
//import GoogleSignIn

class MoreTableViewController: UITableViewController {

    @IBOutlet weak var versionLb: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        versionLb.text = "Version \(versionNumber).\(buildNumber)"

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if indexPath.row == 1 // terms and conditions
        {
            
            let vc = TermsandAgreementViewController.create()
            vc.file = "privacy"
            vc.headerTitle = "Terms and conditions".localized()
            self.navigationController?.pushViewController(vc, animated: true)

        }
       
        else if indexPath.row == 2 // send feedback
        {
            
                self.navigationController?.pushViewController(SendFeedbackViewController.create(), animated: true)
        }
        else if indexPath.row == 3 // logout
        {
            Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userData)
            Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userName)
//            GIDSignIn.sharedInstance().signOut()
            
            let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
            
            let loginVC = mainSB.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController
            self.navigationController?.present(loginVC, animated: true, completion: nil)
        }
    }

}
