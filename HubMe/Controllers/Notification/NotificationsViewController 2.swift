//
//  NotificationsViewViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController
import SafariServices
//import StompClientLib

class NotificationsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var noNotificationLb: UILabel!
    fileprivate var notificationPresenter:NotificationPresenter?
    fileprivate var loginPresenter: LoginChannelPresenter?
    fileprivate var userNotifications:[NotificationResponse] = []
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var registeredChannels:[GroupedChannel] = []
    fileprivate var selectedIndex:Int?
    fileprivate var channelId:String?
    fileprivate var channelName:String?
    fileprivate var safariVC:SFSafariViewController?
//    fileprivate var socketClient = StompClientLib()
    fileprivate var url = NSURL()
    //    fileprivate let alert = SweetAlert()
    fileprivate var relogin:ReloginBaseController?
    override func viewDidLoad() {
        super.viewDidLoad()
//        registerSocket()
        // Do any additional setup after loading the view.
//                  loading.startAnimating()
        notificationPresenter = NotificationPresenter(self)
        notificationPresenter?.getNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshNotifications(notification:)), name: Notification.Name(HubMeConstants.Notifications.refreshNotifications), object: nil)

    }
    
    @objc public static func create() -> NotificationsViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! NotificationsViewController
    }
    override func viewWillDisappear(_ animated: Bool) {
        relogin?.dismssPeriodDialog()
    }
    override func viewDidDisappear(_ animated: Bool) {
        relogin = ReloginBaseController()
        relogin?.controller = self
    }
    private func getSelectedAccount(groupedAccount: [GroupedChannel])->(GroupedChannel?,UserChannelsItem?)
    {
        var accounts:GroupedChannel?
        var selectAccount:UserChannelsItem?
        
        for (_,item) in groupedAccount.enumerated()
        {
            if item.groupName == userNotifications[selectedIndex ?? 0].channelName
            {
                accounts = item
                
                for (index,account) in (item.accounts?.enumerated())!
                {
                    if account.accountName == userNotifications[selectedIndex ?? 0].accountName || account.channelUsername == userNotifications[selectedIndex ?? 0].userName
                    {
                        selectAccount = item.accounts?[index]
                        break
                    }
                }
                break
            }
        }
        return (accounts,selectAccount)
    }
    
    func goToNewFeed(response: [UserChannelsItem]?)
    {
        if response?.count ?? 0 > 0  // make sure there is channels and get updated one
        {
            let feedsVC = FeedsTableViewController.create()
            feedsVC.type = .channel
            let groupedAccount = getSelectedAccount(groupedAccount: registeredChannels)
            feedsVC.accountsItem = groupedAccount.0 ?? GroupedChannel()
            feedsVC.selectedAccountItem =  groupedAccount.1
            self.navigationController?.pushViewController(feedsVC, animated: true)
        }
    }
    
    @objc func refreshNotifications(notification: Notification) {
        let newNotification = notification.object
        self.userNotifications.insert(newNotification as! NotificationResponse, at: 0)
        self.tableView.reloadData()
    }
}

extension NotificationsViewController : UITableViewDelegate , UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notification_cell") as! NotificationTableViewCell
        cell.configureCell(notification: userNotifications[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return userNotifications.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130.0
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        notificationPresenter?.readNotification(notificationId: String(describing:userNotifications[indexPath.row].id as! Int))
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getUserChannels()
        selectedIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
        tableView.allowsSelection = false
        
    }
}
extension NotificationsViewController : NotificationPresenterView
{
    func getUserNotifications(response: [NotificationResponse]?)
    {
        loading.stopAnimating()
        self.userNotifications = response ?? []
        if userNotifications.count == 0
        {
            noNotificationLb.isHidden = false
        }
        else
        {
            noNotificationLb.isHidden = true
            
        }
        self.tableView.reloadData()
        
    }
    func readNotificationSuccess(response: String)
    {
        if response.lowercased().localized() == "Success".lowercased().localized()
        {
            debugPrint("Notification Read Successfully")
            userNotifications[selectedIndex ?? 0].read = true
            self.tableView.reloadData()
        }
    }
    func setErrorReponse(msg: String?) {
        loading.stopAnimating()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    func setError(error: NSError, url: String) {
        loading.stopAnimating()
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
    }
}
extension NotificationsViewController : ChannelsPresenterView
{
    
    func setUserChannelsResult(response: [UserChannelsItem]?) {
        
        registeredChannels = Helper.groupChannels(channels: response ?? [])
        
        if userNotifications[selectedIndex ?? 0].type == HubMeConstants.NotificationType.new
        {
            goToNewFeed(response: response)
        }
        else
        {
            let groupedAccount = getSelectedAccount(groupedAccount: registeredChannels)
            
            let allChannels = HubMeConstants.allChannels
            
            let channel = allChannels.filter( {$0.groupName?.lowercased() ==  groupedAccount.1?.name?.lowercased() })
            
            groupedAccount.1?.firstLoadConfig = channel[0].firstConfig
            
            if (userNotifications[selectedIndex ?? 0].channelName ?? "").lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased() && (userNotifications[selectedIndex ?? 0].channelName ?? "").lowercased() != HubMeConstants.Channels.slackChannel.lowercased()
            {
                relogin = ReloginBaseController()
                relogin?.controller = self
//                relogin?.registerSocket()
                relogin?.showReloginDialogue(accountName: userNotifications[selectedIndex ?? 0].accountName ?? "", channelName: userNotifications[selectedIndex ?? 0].channelName ?? "", channelId: userNotifications[selectedIndex ?? 0].userChannelId ?? "", selectedAccount: groupedAccount.1 ?? UserChannelsItem())
                
            }
            tableView.allowsSelection = true

        }
    }
    
}
