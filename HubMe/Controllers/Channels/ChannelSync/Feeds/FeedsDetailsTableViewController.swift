//
//  FeedsDetailsTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController
import SDWebImage
import WebKit
import SVProgressHUD
enum HideState
{
    case collapse
    case expand
}
class FeedsDetailsTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var replyTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var sendReplyBtn: UIButton!

    fileprivate var feedPresenter: FeedsPresenter?
    fileprivate var replyPresenter: ReplyFeedPresenter?
    fileprivate var backBtn:UIBarButtonItem?
    fileprivate var relogin:ReloginBaseController?
    fileprivate var state:HideState = .collapse
    fileprivate var contentHeight = 0.0
    fileprivate var plazzaProfilePresenter: PlazzaProfilePresenter?

    fileprivate let expandAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont.systemFont(ofSize: 14),
        .foregroundColor: UIColor.lightGray,
        .underlineStyle: NSUnderlineStyle.styleSingle.rawValue]
    fileprivate var firstLb: String = ""

    var accountName:String?
    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    var errorFlag:Bool = true
    var type:userType?
    var feed: MessageItem?
    var isFinishedLoad:Bool = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Mail details".localized()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
//        self.dismissDialogViewController()
        loading.stopAnimating()
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        tableView.reloadData()
    }
    @objc public static func create() -> FeedsDetailsTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FeedsDetailsTableViewController
    }
    
    
    func initView()
    {
    
        backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action:  #selector(FeedsDetailsTableViewController.backAction))
        backBtn?.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backBtn
        fetchFeedDetails()
        
        if feed?.channel == HubMeConstants.Channels.exchangeChannel.lowercased() || feed?.channel == HubMeConstants.Channels.gmailChannel.lowercased()
        {
            replyView.isHidden = false
            commentView.isHidden = true
        }
        else if feed?.channel == HubMeConstants.Channels.plazzaChannel.lowercased() && feed?.options?.plazzaType?.lowercased() != HubMeConstants.MessageType.discussion.lowercased()
        {
            
            replyView.isHidden = true
            commentView.isHidden = false
        }
        else
        {
           replyView.isHidden = true
           commentView.isHidden = true
        }
        plazzaProfilePresenter = PlazzaProfilePresenter(self)

    }
    
    func fetchFeedDetails()
    {
        loading.startAnimating()
        feedPresenter = FeedsPresenter(self)
        feedPresenter?.getFeedDetails(msgId: (feed?.messageId) ?? "0", date:(feed?.date) ?? "0",userChannelId:(feed?.userChannelId) ?? "0")
        feedPresenter?.markFeedAsRead(feed: ReadFeedRequest(date: (feed?.date) ?? "", messageId: (feed?.messageId) ?? "", userChannelId: (feed?.userChannelId) ?? ""))
        
    }
    // actions
    @objc func backAction(){
        
        if replyTF.text != "" && replyTF.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.feedbackDetails = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
        
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func goback(_ go: Bool) {
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // manage keyboard hide / show height

    
    @objc func collapseBtnAction(_ sender: UIButton) {
        switch state
        {
        case .collapse:
//            sender.setTitle("Expand".localized(), for: .normal)
            state = .expand
            let cell = tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! RecipientTimeTableViewCell
            cell.numberOfRecipient.isHidden = true
            sender.isHidden = true
            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            self.tableView.beginUpdates()
             self.tableView.endUpdates()

            break
        case .expand:
//            sender.setTitle("Collapse".localized(), for: .normal)
            state = .collapse
            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)

            break
        }
    }
    // Buttons Actions 
   
    @IBAction func backBtnAction(_ sender: Any)
    {
        if replyTF.text != "" && replyTF.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.feedbackDetails = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            //            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func sendReplyAction(_ sender: Any) {
        
        if  (replyTF.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            replyPresenter = ReplyFeedPresenter (self)
            let requestModel = ComposeMessageRequestModel()
            requestModel.messageText = replyTF.text ?? ""
            requestModel.cc = []
            requestModel.to = []
            replyPresenter?.sendReply(msgId: (feed?.messageId) ?? "", date: feed?.date ?? "", userChannelId: feed?.userChannelId ?? "", requestModel: requestModel, msgType: ReplyType.reply)
            sendReplyBtn.isEnabled = false
            backBtn?.isEnabled = false
            loading.startAnimating()
        }
    }
    
    @IBAction func replyBtnAction(_ sender: Any) {
        let replyVC = ReplyViewController.create()
        replyVC.type = type
        replyVC.replyType = ReplyType.reply
        replyVC.msgItem = self.feed
        replyVC.groupedAccount = groupedAccount
        replyVC.channelItem = channelItem
         self.navigationController?.pushViewController(replyVC, animated: true)
    }
    
    
    @IBAction func replayAllBtnAction(_ sender: Any) {
        
        let replyVC = ReplyViewController.create()
        replyVC.type = type
        replyVC.replyType = ReplyType.replyAll
        replyVC.msgItem = self.feed
        replyVC.groupedAccount = groupedAccount
        replyVC.channelItem = channelItem
        self.navigationController?.pushViewController(replyVC, animated: true)
        
    }
    
    @objc func getPlazzaProfile(sender:UITapGestureRecognizer) {

        self.tableView.isUserInteractionEnabled = false
        plazzaProfilePresenter?.getPlazzaProfile(email: firstLb)
        
        SVProgressHUD.show()
    }

}

extension FeedsDetailsTableViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_header_cell") as! FeedHeaderTableViewCell
            cell.usernameLb.text = accountName ?? ""
//            cell.feedSubjectLb.text = feed?.title ?? ""

            if (feed?.iconUrl?.hasPrefix("http"))! {
                
                   cell.channelLogoImg.sd_setImage(with: URL(string: (feed?.iconUrl)! ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
                
                
            } else {
                   cell.channelLogoImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(feed?.iconUrl)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            }
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_time_cell") as! RecipientTimeTableViewCell
            cell.userImg.image = UIImage(named: "avatar_icon")
            
           
            if   feed?.channel == HubMeConstants.Channels.plazzaChannel.lowercased()
            {
                cell.firstRecipientLb.isHidden = true
                cell.showMoreBtn.isHidden = true
                cell.numberOfRecipient.isHidden = true

            }
            else
            {
                let tap = UITapGestureRecognizer(target: self, action: #selector(FeedsDetailsTableViewController.getPlazzaProfile))
                cell.firstRecipientLb.addGestureRecognizer(tap)
                cell.firstRecipientLb.isUserInteractionEnabled = true
                
                if feed?.options?.to?.count ?? 0 > 0
                {
                    if feed?.options?.to?.contains(where: {$0.email == accountName}) ?? false
                    {
                        cell.firstRecipientLb.text =  "to: me"
                        firstLb = accountName ?? ""
                    }
                    else
                    {
                        cell.firstRecipientLb.text =  "to: " + (feed?.options?.to?[0].email ?? "")
                        firstLb = feed?.options?.to?[0].email ?? ""

                    }
                }
                else if feed?.options?.cc?.count ?? 0 > 0
                {
                    if feed?.options?.cc?[0].email == accountName
                    {
                        cell.firstRecipientLb.text =  "cc: me"
                        firstLb = accountName ?? ""
                    }
                    else
                    {
                        cell.firstRecipientLb.text = "cc: " + (feed?.options?.cc?[0].email  ?? "")
                         firstLb = feed?.options?.cc?[0].email ?? ""
                    }
                }
                else if feed?.options?.bcc?.count ?? 0 > 0
                {
                    if feed?.options?.bcc?[0].email == accountName
                    {
                        cell.firstRecipientLb.text =  "bcc: me"
                        firstLb = accountName ?? ""
                    }
                    else
                    {
                        cell.firstRecipientLb.text = "bcc: " + (feed?.options?.bcc?[0].email ?? "")
                         firstLb = feed?.options?.bcc?[0].email ?? ""
                    }
                }
            }

            cell.senderLb.text = feed?.from ?? ""

            let attributeString = NSMutableAttributedString(string: "show".localized(),
                                                            attributes: expandAttributes)
            cell.showMoreBtn.setAttributedTitle(attributeString, for: .normal)
            
            cell.recipientTimeLb.text =  Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "dd/MM/yyyy") + " at " +  Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "h:mm a")
           
            cell.showMoreBtn.addTarget(self, action: #selector(FeedsDetailsTableViewController.collapseBtnAction), for: UIControlEvents.touchUpInside)
            cell.showMoreBtn.tag = indexPath.row
            let numberOfRecipient =  (feed?.options?.to?.filter { $0.email != accountName && $0.email != firstLb }.count ?? 0) + (feed?.options?.cc?.count ?? 0)!
                
                //+ (feed?.options?.cc?.count ?? 0)! + (feed?.options?.bcc?.count ?? 0)!
            if numberOfRecipient > 0 && state != .expand
            {
                cell.numberOfRecipient.text = String(describing : numberOfRecipient) + " more".localized()
                cell.showMoreBtn.isHidden = false

            }
            else
            {
                cell.numberOfRecipient.isHidden = true
                cell.showMoreBtn.isHidden = true
            }
            

            return cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recipient_cell") as! RecipientParentTableViewCell
            cell.view = self
            let toArr = feed?.options?.to?.filter { $0.email != accountName && $0.email != firstLb}
          
            cell.toRecipients = toArr ?? []
            cell.ccRecipients = feed?.options?.cc ?? []
            cell.bccRecipients = feed?.options?.bcc ?? []
            
            return cell
        }
            
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_content") as! FeedContentTableViewCell
            cell.feedContentWebView.tag = indexPath.row
            cell.feedContentWebView.scrollView.isMultipleTouchEnabled = false
//            cell.feedContentWebView.navigationDelegate = self
            cell.feedContentWebView.loadHTMLString( feed?.desc ?? "", baseURL: nil)
//            cell.feedContentWebView.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: CGFloat(contentHeight))
//            cell.feedContentWebView.scalesPageToFit = true
//            cell.feedContentWebView.contentMode = .scaleToFill


            return cell
           
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 80
        }
       
        else if indexPath.row == 3
        {
            //            return CGFloat(contentHeight )
            return 600.0
        }
            
        else
        {
            if indexPath.row == 2 && state == .collapse
            {
                return 0
            }
            
            return UITableViewAutomaticDimension 
        }
    }
//    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 80
        }
        else if indexPath.row == 3
        {
//            return CGFloat(contentHeight )
            return 600.0
        }
            
        else
        {
            if indexPath.row == 2 && state == .collapse
            {
                return 0
            }
            
            return UITableViewAutomaticDimension
        }
        
    }
    
    
}
extension FeedsDetailsTableViewController : WKNavigationDelegate
{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      
        if (webView.isLoading) ||  contentHeight > 8.0
        {
            return
        }
        loading.stopAnimating()

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
}
extension FeedsDetailsTableViewController : FeedsPresenterView
{
    func setFeedDetailsResult(response: FeedDetailResponse?) {
        feed?.desc = response?.text
        loading.stopAnimating()
        tableView.reloadData()
    }
    
    func setSuccess(response: String?) {
        debugPrint("Messge Read Successfully")
        
    }
    func setError(error: NSError,url: String) {
        loading.stopAnimating()
        SVProgressHUD.dismiss()
        backBtn?.isEnabled = true
        sendReplyBtn.isEnabled = true
        self.tableView.isUserInteractionEnabled = true

        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            relogin = ReloginBaseController()
            relogin?.controller = self
            relogin?.registerSocket()
            relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing:channelItem?.id ?? 0 ), selectedAccount: channelItem ?? UserChannelsItem())
        }
        else
        {
//            if (self.errorFlag) {
               if  feed?.channel == HubMeConstants.Channels.plazzaChannel.lowercased()
                {
                        Helper.showFloatAlert(title: "Reply in discussion is not implemented yet", subTitle: "", type: HubMeConstants.AlertType.AlertError)

                }
                else
               {
                    self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
                }
//                self.errorFlag = false
//            }
        }
        
    }
    
}



extension FeedsDetailsTableViewController : ReplyFeedPresenterView   {
    func setReplySuccess(response: String?) {
        debugPrint("Reply sent Successfully")
 
        loading.stopAnimating()
        Helper.showFloatAlert(title: response ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    func setErrorReponse(msg: String?,url: String) {
        loading.stopAnimating()
        SVProgressHUD.dismiss()
        sendReplyBtn.isEnabled = true
        backBtn?.isEnabled = true
        self.tableView.isUserInteractionEnabled = true
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    
}
extension FeedsDetailsTableViewController :  UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
}
extension FeedsDetailsTableViewController : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
        self.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }

}
