//
//  FeedsDetailsTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController
import SDWebImage


enum HideState
{
    case collapse
    case expand
}
class FeedsDetailsTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var replyTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var sendReplyBtn: UIButton!
  
    fileprivate var feedPresenter: FeedsPresenter?
    fileprivate var replyPresenter: ReplyFeedPresenter?
    fileprivate var backBtn:UIBarButtonItem?
    fileprivate var relogin:ReloginBaseController?
    fileprivate var state:HideState = .expand

    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    var errorFlag:Bool = true
    var type:userType?
    var feed: MessageItem?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = (feed?.channel?.capitalizingFirstLetter())! + " " +  "message".localized()
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        loading.stopAnimating()
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        tableView.reloadData()
    }
    @objc public static func create() -> FeedsDetailsTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FeedsDetailsTableViewController
    }
    
    
    func initView()
    {
    
        backBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action:  #selector(FeedsDetailsTableViewController.backAction))
        backBtn?.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = backBtn
        fetchFeedDetails()
        
        if feed?.channel == HubMeConstants.Channels.exchangeChannel.lowercased() || feed?.channel == HubMeConstants.Channels.gmailChannel.lowercased()
        {
            replyView.isHidden = false
            commentView.isHidden = true
        }
        else if  feed?.channel == HubMeConstants.Channels.twitterChannel.lowercased()
        {
            replyView.isHidden = true
            commentView.isHidden = false
        }

    }
    
    func fetchFeedDetails()
    {
        loading.startAnimating()
        feedPresenter = FeedsPresenter(self)
        feedPresenter?.getFeedDetails(msgId: (feed?.messageId) ?? "0", date:(feed?.date) ?? "0",userChannelId:(feed?.userChannelId) ?? "0")
        feedPresenter?.markFeedAsRead(feed: ReadFeedRequest(date: (feed?.date) ?? "", messageId: (feed?.messageId) ?? "", userChannelId: (feed?.userChannelId) ?? ""))
        
    }
    // actions
    @objc func backAction(){
        
        if replyTF.text != "" && replyTF.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.feedbackDetails = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func goback(_ go: Bool) {
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
        self.dismissDialogViewController()
    }
    
    // manage keyboard hide / show height
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    @objc func collapseBtnAction(_ sender: UIButton) {
        switch state
        {
        case .collapse:
            sender.setTitle("Expand".localized(), for: .normal)
            state = .expand
            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)

            break
        case .expand:
            sender.setTitle("Collapse".localized(), for: .normal)
            state = .collapse
            tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)

            break
        }
    }
    // Buttons Actions 
   
    @IBAction func backBtnAction(_ sender: Any)
    {
        if replyTF.text != "" && replyTF.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.feedbackDetails = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            //            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func sendReplyAction(_ sender: Any) {
        
        if  (replyTF.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            replyPresenter = ReplyFeedPresenter (self)
            let requestModel = ComposeMessageRequestModel()
            requestModel.messageText = replyTF.text ?? ""
            requestModel.cc = []
            requestModel.to = []
            replyPresenter?.sendReply(msgId: (feed?.messageId) ?? "", date: feed?.date ?? "", userChannelId: feed?.userChannelId ?? "", requestModel: requestModel, msgType: ReplyType.reply)
            sendReplyBtn.isEnabled = false
            backBtn?.isEnabled = false
            loading.startAnimating()
        }
    }
    
    @IBAction func replyBtnAction(_ sender: Any) {
        let replyVC = ReplyViewController.create()
        replyVC.type = type
        replyVC.replyType = ReplyType.reply
        replyVC.msgItem = self.feed
        replyVC.groupedAccount = groupedAccount
        replyVC.channelItem = channelItem
        self.navigationController?.present(replyVC, animated: true, completion: nil)
        
        //        self.navigationController?.pushViewController(replyVC, animated: true)
    }
    
    
    @IBAction func replayAllBtnAction(_ sender: Any) {
        
        let replyVC = ReplyViewController.create()
        replyVC.replyType = ReplyType.replyAll
        replyVC.msgItem = self.feed
        replyVC.groupedAccount = groupedAccount
        replyVC.channelItem = channelItem
        self.navigationController?.present(replyVC, animated: true, completion: nil)
        //        self.navigationController?.pushViewController(replyVC, animated: true)
        
    }

}

extension FeedsDetailsTableViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_header_cell") as! FeedHeaderTableViewCell
            
            cell.feedSubjectLb.text = feed?.title ?? ""

            if (feed?.iconUrl?.hasPrefix("http"))! {
                
                   cell.channelLogoImg.sd_setImage(with: URL(string: (feed?.iconUrl)! ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
                
            } else {
                   cell.channelLogoImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(feed?.iconUrl)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            }
            cell.senderLb.text = feed?.from ?? ""
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_time_cell") as! RecipientTimeTableViewCell
            
            cell.recipientTimeLb.text = "Sent".localized() +  Helper.getDayOfWeek(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "dd/MM/yyyy HH:mm"))! + " " + Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "dd/MM/yyyy HH:mm")
            
            if feed?.options?.plazzaTypeUrl != nil {
                cell.plazzaTypeImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios/"+(feed?.options?.plazzaTypeUrl ?? "")!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
                
            } else {
                cell.plazzaTypeWidthCons.constant = 0
                cell.plazzaTypeImg.isHidden = true
            }
            
            cell.collapseBtn.addTarget(self, action: #selector(FeedsDetailsTableViewController.collapseBtnAction), for: .touchUpInside)
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recipient_cell") as! RecipientParentTableViewCell
            cell.view = self
            cell.toRecipients = feed?.options?.to ?? []
            cell.ccRecipients = feed?.options?.cc ?? []
            cell.bccRecipients = feed?.options?.bcc ?? []
            

            
            return cell
        }
            
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_content") as! FeedContentTableViewCell
            cell.feedContentWebView.delegate = self
            cell.feedContentWebView.loadHTMLString( feed?.desc ?? "", baseURL: nil)
           
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3
        {
            return 300
        }
        
        else
        {
            if indexPath.row == 2 && state == .collapse
            {
                return 0
            }
            
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}
extension FeedsDetailsTableViewController : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if (webView.isLoading){
            return
        }
        loading.stopAnimating()

        
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
}
extension FeedsDetailsTableViewController : FeedsPresenterView
{
    func setFeedDetailsResult(response: FeedDetailResponse?) {
        feed?.desc = response?.text
        loading.stopAnimating()
        tableView.reloadData()
    }
    
    func setSuccess(response: String?) {
        debugPrint("Messge Read Successfully")
        
    }
    func setError(error: NSError,url: String) {
        loading.stopAnimating()
        
        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            relogin = ReloginBaseController()
            relogin?.controller = self
            relogin?.registerSocket()
            relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing:channelItem?.id ?? 0 ), selectedAccount: channelItem ?? UserChannelsItem())
        }
        else
        {
            if (self.errorFlag) {
                self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
                self.errorFlag = false
            }
        }
        
    }
    
}



extension FeedsDetailsTableViewController : ReplyFeedPresenterView   {
    func setReplySuccess(response: String?) {
        debugPrint("Reply sent Successfully")
        
        sendReplyBtn.isEnabled = false
        backBtn?.isEnabled = false
        
        loading.stopAnimating()
        Helper.showFloatAlert(title: response ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    func setErrorReponse(msg: String?,url: String) {
        loading.stopAnimating()
        sendReplyBtn.isEnabled = true
        backBtn?.isEnabled = true
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    
}
