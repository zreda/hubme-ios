//
//  MailListTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/22/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import LKPullToLoadMore
import ObjectMapper
import FittedSheets
import SDWebImage
import LSDialogViewController
import SafariServices
import ExpandableLabel


@objc protocol LastUpdateStaus {
    func setChannelStatus(channelItem: UserChannelsItem)
}
class FeedsTableViewController: UIViewController {
    
    @IBOutlet weak var mailsListTB: UITableView!
    @IBOutlet weak var pleasewait: UILabel!
    @IBOutlet weak var noFeedsFound: UILabel!
    
    @IBOutlet weak var noDataFound: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var newMsgBtn: UIBarButtonItem!
    @IBOutlet weak var accountImg: UIImageView!
    
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var teamNameLb: UILabel!
    @IBOutlet weak var accountMessageName: UILabel!
    @IBOutlet weak var headerHeightConst: NSLayoutConstraint!
    fileprivate var feedListPresenter: FeedsPresenter?
    fileprivate var feedList: [MessageItem] = []
    fileprivate var nextPageNumber: String!
    fileprivate var loadMoreControl: LKPullToLoadMore!
    fileprivate var pullRefresh: Bool = false
    fileprivate var accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
    fileprivate var loginPresenter: LoginChannelPresenter?
    fileprivate var safariVC:SFSafariViewController?
    fileprivate var socketClient = StompClientLib()
    fileprivate var topic = "/exchange/hubme/\((Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? "")"
    fileprivate var url = NSURL()
    fileprivate var states : Array<Bool>!
    fileprivate var relogin:ReloginBaseController?
    fileprivate var isShow:Bool = false
    
    var type: userType?
    var accountsItem: GroupedChannel = GroupedChannel()
    var selectedAccountItem: UserChannelsItem?
    var sheetController:SheetViewController?
    weak var delegate: LastUpdateStaus?
    var recentChannelItem: UserChannelsItem?
    var errorFlag:Bool = true
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(FeedsTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.orange
        
        return refreshControl
    }()
    
    override func viewDidLoad()
    {
        mailsListTB.register(UINib(nibName: "TweetCell", bundle: nil), forCellReuseIdentifier: "tweet_cell")
        mailsListTB.register(UINib(nibName: "FeedCell", bundle: nil), forCellReuseIdentifier: "feed_cell")
        mailsListTB.register(UINib(nibName: "HomeFeedCell", bundle: nil), forCellReuseIdentifier: "home_feed_cell")
        mailsListTB.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: "message_cell")
        mailsListTB.register(UINib(nibName: "HomeMessageCell", bundle: nil), forCellReuseIdentifier: "home_message_cell")
        
        mailsListTB?.tableFooterView = UIView(frame: CGRect.zero)
        self.mailsListTB.addSubview(self.refreshControl)
        if type == userType.channel
        {
            headerHeightConst.constant = 60.0
            relogin = ReloginBaseController()
            relogin?.controller = self
            relogin?.registerSocket()
        }
        else
        {
            headerHeightConst.constant = 0.0

        }
        initView()

        if selectedAccountItem?.firstLoadStatus == HubMeConstants.FeedsLoadStatus.expired && type ==  userType.channel && !isShow
        {
            let allChannels = HubMeConstants.allChannels

            let channel = allChannels.filter( {$0.groupId ==  accountsItem.groupId })

            selectedAccountItem?.firstLoadConfig = channel[0].firstConfig

            relogin?.showReloginDialogue(accountName: selectedAccountItem?.channelUsername ?? "", channelName: accountsItem.groupName ?? "", channelId: String(describing: selectedAccountItem?.id as! Int) , selectedAccount: selectedAccountItem ?? UserChannelsItem())
            isShow = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        dismssPeriodDialog()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if pleasewait.isHidden == true || noDataFound.isHidden == true || noFeedsFound.isHidden == true
        {
            loading.startAnimating()
        }
        if  accountsItem.groupName?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() || accountsItem.groupName?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased()
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        if  accountsItem.groupName?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() || accountsItem.groupName?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased()
        {
            accountName.isHidden = true
            accountMessageName.isHidden = false
            teamNameLb.isHidden = false
        }
        else
        {
            accountName.isHidden = false
            accountMessageName.isHidden = true
            teamNameLb.isHidden = true
            
        }
        initView()
        
        
    }
    
    func initView() {
        //        recentChannelItem =  channelItem
        //        self.navigationItem.title = channelItem?.name
        loadMoreControl = LKPullToLoadMore(imageHeight: 40, viewWidth: mailsListTB.frame.width, tableView: mailsListTB)
        loadMoreControl.enable(true)
        loadMoreControl.delegate = self
        loadMoreControl.resetPosition()
        
        if type == userType.channel && selectedAccountItem == nil
        {
            let backButton = UIBarButtonItem(title: "Channels".localized(), style: .plain, target: nil, action: nil)
            navigationController?.navigationItem.backBarButtonItem = backButton
            selectedAccountItem = accountsItem.accounts?[0] ?? UserChannelsItem()
        }
        else if type == userType.flow
        {
            self.navigationItem.rightBarButtonItem = nil
            let backButton = UIBarButtonItem(title: "Flows".localized(), style: .plain, target: nil, action: nil)
            navigationController?.navigationItem.backBarButtonItem = backButton
            nextPageNumber = "0"
            
        }
        
        if selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.slackChannel || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() {
            
            mailsListTB.rowHeight = UITableViewAutomaticDimension
            mailsListTB.estimatedRowHeight = 100
            
        }
        
        self.feedList = []
        mailsListTB.reloadData()
        nextPageNumber = "0"
        accountImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
            (selectedAccountItem?.icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        accountName.text = selectedAccountItem?.channelUsername
        accountMessageName.text = selectedAccountItem?.channelUsername
        teamNameLb.text = selectedAccountItem?.teamName
        feedListPresenter = FeedsPresenter(self)
        feedListPresenter?.getFeedResponse(type: type!, channelId: String(describing: selectedAccountItem?.id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
        
        loading.stopAnimating()
        noDataFound.isHidden = false
        pleasewait.isHidden = false
        
    }
 
    @IBAction func composeNewMessageBtnAction(_ sender: Any) {
        
        if accountsItem.accounts![0].name == HubMeConstants.Channels.twitterChannel // create new tweet
        {
            let destinationViewController = ComposeNewTweetViewController.create()
            destinationViewController.channelItem = selectedAccountItem
            destinationViewController.accountsItem = accountsItem.accounts
            destinationViewController.groupedAccount = accountsItem
            destinationViewController.delegate = self
            destinationViewController.type = type
            self.navigationController?.pushViewController(destinationViewController, animated: true)
            
        }
        else if accountsItem.accounts![0].name == HubMeConstants.Channels.plazzaChannel // plazza new post
        {
            let destinationViewController = ComposeNewPostPlazzaViewController.create()
            destinationViewController.channelItem = selectedAccountItem
//
            
            self.present(destinationViewController, animated: true, completion: nil)
        }
        else // compose new message
        {
            let destinationViewController = ComposeNewMessageViewController.create()
            destinationViewController.channelItem = selectedAccountItem
            destinationViewController.accountsItem = accountsItem.accounts
            destinationViewController.groupedAccount = accountsItem
            destinationViewController.delegate = self
            destinationViewController.type = type
//            self.navigationController?.pushViewController(destinationViewController, animated: true)
            self.present(destinationViewController, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func switchAccountBtnAction(_ sender: Any) {
        let accountsVC = AccountBottomSheetViewController.create()
        accountsVC.accountsItem = accountsItem.accounts
        accountsVC.selectedAccountItem = selectedAccountItem
        accountsVC.channelName = accountsItem.groupName
        accountsVC.delegate = self
        sheetController = SheetViewController(controller: accountsVC)
        sheetController?.ls_dialogBackgroundView?.isExclusiveTouch = true
        self.present(sheetController ?? SheetViewController(), animated: false, completion: nil)
    }
    @objc public static func create() -> FeedsTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FeedsTableViewController
    }
    
    
    func addNewAccount()
    {
        if (accountsItem.groupName == HubMeConstants.Channels.twitterChannel)
        {
            chooseFirstLoad(selectedConfig: accountsItem.firstConfig ?? FirstLoadConfig())
        }
        else if (accountsItem.groupName == HubMeConstants.Channels.gmailChannel)
            
        {
            showConfig(LSAnimationPattern.zoomInOut, firstConfig: accountsItem.firstConfig ?? FirstLoadConfig())
        }
        else {
            let vc = LoginChannelViewController.create()
            vc.channel = self.selectedAccountItem
            vc.config = accountsItem.firstConfig
            vc.channelId = String(describing:accountsItem.groupId!)
            vc.accountsItem = accountsItem.accounts
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    
    
    func chooseFirstLoad(selectedConfig:FirstLoadConfig)
    {
        
        var firstLoadFetchValue  = 200
        var auth = LoginRequestModel()
        let channelName = accountsItem.groupName ?? ""
        
        if channelName.lowercased() != HubMeConstants.Channels.twitterChannel.lowercased()
        {
            firstLoadFetchValue = selectedConfig.firstLoadFetchValue ?? 0
        }
        
        auth = LoginRequestModel(expired:false,channel_id:  String(describing: (accountsItem.groupId  ?? 0) as! Int) , channel_name: "", firstLoadConfig: FirstLoadConfig(criteria: selectedConfig.criteria ?? "COUNT", firstLoadFetchValue: firstLoadFetchValue))
        
        loginPresenter = LoginChannelPresenter(self)
        loginPresenter?.getLoginSocial(auth: auth)
        loading.startAnimating()
    }
    
    func showConfig(_ animationPattern: LSAnimationPattern ,firstConfig:FirstLoadConfig )
    {
        // to show the dialog
        let dialogViewController: ChooseChannelSynchronoizationViewController = ChooseChannelSynchronoizationViewController(nibName:"ChooseSynchronoizationPopupDialog", bundle: nil)
        dialogViewController.newAccountdelegate = self
        dialogViewController.config = firstConfig
        self.presentDialogViewController(dialogViewController, animationPattern: animationPattern, completion: { () -> Void in })
    }
    
    func dismssPeriodDialog()
    {
        self.dismissDialogViewController(LSAnimationPattern.zoomInOut)
        
    }
    @objc func safariLogin(notification: NSNotification) {
        // get the url from the auth callback
        //        let url = notification.object as! NSURL
        // Finally dismiss the Safari View Controller with:
        safariVC?.dismiss(animated: true, completion: nil)
    }
    
  
    
}
extension FeedsTableViewController : UITableViewDelegate , UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if  type == userType.flow // flows
        {
            if selectedAccountItem?.name!.lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased() && selectedAccountItem?.name!.lowercased() != HubMeConstants.Channels.slackChannel.lowercased()
            {
                let flowCell = tableView.dequeueReusableCell(withIdentifier: "home_feed_cell", for: indexPath) as! HomeFeedTableViewCell

                flowCell.configureCell(feed: feedList[indexPath.row])
                return flowCell
            }
            else
            {
                if let message_cell = tableView.dequeueReusableCell(withIdentifier: "home_message_cell", for: indexPath) as? MessageTableViewCell
                {
                    message_cell.configureCell(feed: feedList[indexPath.row])
                    message_cell.showMoreBtn.tag = indexPath.row
                    //                message_cell.delegate = self
                    message_cell.showMoreBtn.addTarget(self, action: #selector(moreBtnTapped), for: .touchUpInside)
                    return message_cell
                }
                return UITableViewCell()
            }

        }
        else // channels
        {

            if selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.twitterChannel.lowercased() || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.plazzaChannel.lowercased()
            {
                if let tweet_cell = mailsListTB.dequeueReusableCell(withIdentifier: "tweet_cell", for: indexPath) as? TweetTableViewCell
                {
                    tweet_cell.configureCell(feed: feedList[indexPath.row])
                    return tweet_cell
                }
                return UITableViewCell()

            }

            else if selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.slackChannel.lowercased()
            {

                if let message_cell = mailsListTB.dequeueReusableCell(withIdentifier: "message_cell", for: indexPath) as? MessageTableViewCell
                {
                    message_cell.configureCell(feed: feedList[indexPath.row])
                    message_cell.showMoreBtn.tag = indexPath.row
                    //                message_cell.delegate = self
                    message_cell.showMoreBtn.addTarget(self, action: #selector(moreBtnTapped), for: .touchUpInside)
                    return message_cell
                }
                return UITableViewCell()

            }
            else
            {
                if let feedCell = mailsListTB.dequeueReusableCell(withIdentifier: "feed_cell", for: indexPath) as? FeedTableViewCell
                {
                    feedCell.configureCell(feed: feedList[indexPath.row])
                    return feedCell
                }
                return UITableViewCell()
            }

        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return type == userType.flow ? feedList.count: feedList.count
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if  type == userType.flow  || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.slackChannel.lowercased(){
            
            return UITableViewAutomaticDimension
            //            if feedList[indexPath.row].isExpanded
            //            {
            //                return Helper.heightForText(text: feedList[indexPath.row].snippet ?? "", Font: UIFont.systemFont(ofSize: 17.0), Width: 200) + 60
            //            }
            //            else
            //            {
            //                return 100.0
            //            }
        }
            
        else if selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.twitterChannel || feedList[indexPath.row].options?.screenName != nil || selectedAccountItem?.name!.lowercased() == HubMeConstants.Channels.plazzaChannel.lowercased() {
            return 150.0
        }
            
            
        else {
            return 130
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if feedList[indexPath.row].channel?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() || feedList[indexPath.row].channel?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased()
        {
            
            for (i,item) in feedList.enumerated()
            {
                if i == indexPath.row
                {
                    feedList[indexPath.row].isExpanded = !feedList[indexPath.row].isExpanded
                }
                else
                {
                    item.isExpanded = false
                }
            }
            mailsListTB.reloadData()
            
        }
        else if feedList[indexPath.row].channel?.lowercased() != HubMeConstants.Channels.twitterChannel.lowercased()
        {
            if feedList[indexPath.row].options?.messageType == feedType.meeting.rawValue
            {
                let vc =  MeetingDetailsTableViewController.create()
                vc.feed = feedList[indexPath.row]
                vc.type = userType.channel
                vc.groupedAccount = accountsItem
                if type ==  userType.channel
                {
                    vc.channelItem = selectedAccountItem
                }
                else
                {
                    vc.channelItem = UserChannelsItem(id: Int(feedList[indexPath.row].userChannelId ?? "") ?? 0 , name: feedList[indexPath.row].channel ?? "", iconUrl: feedList[indexPath.row].iconUrl ?? "")
                }
                self.navigationController?.pushViewController(vc, animated: true)

            }
            else
            {
//                let vc =  FeedDetailsViewController.create()
                let vc =  FeedsDetailsTableViewController.create()
                vc.feed = feedList[indexPath.row]
                vc.type = type
                vc.groupedAccount = accountsItem
                if type ==  userType.channel
                {
                    vc.channelItem = selectedAccountItem
                }
                else
                {
                    vc.channelItem = UserChannelsItem(id: Int(feedList[indexPath.row].userChannelId ?? "") ?? 0 , name: feedList[indexPath.row].channel ?? "", iconUrl: feedList[indexPath.row].iconUrl ?? "")
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
          
        }
        else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TwitterDetailsTableViewController") as! TwitterViewController
            vc.feed = feedList[indexPath.row]
            vc.feedLogo = feedList[indexPath.row].iconUrl
            if type ==  userType.channel
            {
                vc.channelItem = selectedAccountItem
            }
            else
            {
                vc.channelItem = UserChannelsItem(id: Int(feedList[indexPath.row].userChannelId ?? "") ?? 0 , name: feedList[indexPath.row].channel ?? "", iconUrl: feedList[indexPath.row].iconUrl ?? "")
            }
            
            vc.groupedAccount = accountsItem
            vc.type = type
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        feedList[indexPath.row].isRead = true
        mailsListTB.deselectRow(at: indexPath, animated: true)
    }
}

extension FeedsTableViewController : LKPullToLoadMoreDelegate
{
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadMoreControl.scrollViewDidScroll(scrollView)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        loadMoreControl.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        
        if selectedAccountItem?.firstLoadStatus == HubMeConstants.FeedsLoadStatus.expired && type ==  userType.channel
        {
            
            let allChannels = HubMeConstants.allChannels
            
            let channel = allChannels.filter( {$0.groupId ==  accountsItem.groupId })
            
            selectedAccountItem?.firstLoadConfig = channel[0].firstConfig
            
            relogin?.showReloginDialogue(accountName: selectedAccountItem?.channelUsername ?? "", channelName: accountsItem.groupName ?? "", channelId: String(describing: selectedAccountItem?.id as! Int) , selectedAccount: selectedAccountItem ?? UserChannelsItem())
        }
        
        self.nextPageNumber = "0"
        feedListPresenter?.getFeedResponse(type: type!, channelId: String(describing: selectedAccountItem?.id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
        
        if !pleasewait.isHidden
        {
            loading.startAnimating()
        }
        else
        {
            pleasewait.isHidden = true
            noDataFound.isHidden = true 
            noFeedsFound.isHidden = true
            loading.stopAnimating()
        }
    }
    
    func loadMore() {
        
        if self.nextPageNumber != nil
        {
            if !pleasewait.isHidden
            {
                loading.startAnimating()
            }
            else
            {
                pleasewait.isHidden = true
                noDataFound.isHidden = true
                noFeedsFound.isHidden = true
                loading.stopAnimating()
            }
            loadMoreControl.enable(true)
            feedListPresenter?.getFeedResponse(type: type!, channelId: String(describing: selectedAccountItem?.id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
        }
        else
        {
            loadMoreControl.enable(false)
        }
        
    }
}
extension FeedsTableViewController:  FeedsPresenterView {
    
    func setReadForFeed(messageId: String, isRead: Bool) {
        
        mailsListTB.reloadData()
    }
    
    func setFeedResult(response:FeedsResponse?)
    {
        self.noDataFound.isHidden = true
        self.pleasewait.isHidden = true
        self.noFeedsFound.isHidden = true
        self.refreshControl.endRefreshing()
        self.loading.stopAnimating()
        pullRefresh = false
        
        var newsFeedList: [MessageItem] = []
        
        if let nextPage = response?.nextPageIndex
        {
            self.nextPageNumber = nextPage
        }
        else
        {
            self.nextPageNumber = nil
        }
        if response?.messages != nil {
            for (_,item) in (response?.messages?.enumerated())!
            {
                newsFeedList.append(item)
            }
        }
        
        if response?.error != nil {
            
            if (self.errorFlag) {
                Helper.showFloatAlert(title: (response?.error)!, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                self.errorFlag = false
            }
            
            if self.feedList.count != 0
            {
                self.noDataFound.isHidden = true
                self.pleasewait.isHidden = true
                self.noFeedsFound.isHidden = false
            }
        }
        
        if response?.messages != nil && response?.messages?.count == 0 && response?.error == nil && self.feedList.count == 0 {
            self.noDataFound.isHidden = true
            self.pleasewait.isHidden = true
            self.noFeedsFound.isHidden = false
            return
            
        }
        
        if  newsFeedList.count != 0 && self.feedList.count == 0 {
            self.feedList = newsFeedList
        } else {
            
            self.feedList = (self.feedListPresenter?.setNewFeeds(oldFeeds: (self.feedList), newFeeds: newsFeedList))!
            self.pullRefresh = false
        }
        if self.feedList.count == 0 {
            self.noDataFound.isHidden = false
            self.pleasewait.isHidden = false
            
        } else {
            self.pleasewait.isHidden = true
            self.noDataFound.isHidden = true
            self.noFeedsFound.isHidden = true
            
        }
        
        if type == userType.channel {
            self.feedList.sort {
                ($0.date ?? "")! > ($1.date ?? "")!
            }
        }
        
        states = [Bool](repeating: true, count: feedList.count)
        
        self.mailsListTB.reloadData()
        
    }
    
    func setError(error: NSError,url: String) {
        
        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            //            relogin = ReloginBaseController()
            //            relogin?.controller = self
            //            relogin?.registerSocket()
        }
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
        loading.stopAnimating()
    }
    
}
extension FeedsTableViewController : RefreshAccountsDelegate
{
    func refreshAccount(accountsItem: [UserChannelsItem]?, selectedAccountItem: UserChannelsItem?)
        
    {
        if self.accountsItem.accounts?.count != accountsItem?.count || self.selectedAccountItem?.channelUsername != selectedAccountItem?.channelUsername
        {
            
            self.selectedAccountItem = selectedAccountItem
            self.accountsItem.accounts = accountsItem
            nextPageNumber = "0"
            accountImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
                (selectedAccountItem?.icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            accountName.text = selectedAccountItem?.channelUsername
            accountMessageName.text = selectedAccountItem?.channelUsername
            teamNameLb.text = selectedAccountItem?.teamName
            feedListPresenter?.getFeedResponse(type: type!, channelId: String(describing: selectedAccountItem?.id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
            
            if !pleasewait.isHidden
            {
                loading.startAnimating()
            }
            else
            {
                pleasewait.isHidden = true
                noDataFound.isHidden = true
                noFeedsFound.isHidden = true
                loading.stopAnimating()
            }
        }
    }
    
    func manageAccount(accountsItem: [UserChannelsItem]?, selectedAccountItem: UserChannelsItem?, tag: String) {
        
        if  tag !=  ""
        {
            
            if tag ==  HubMeConstants.ManageAccount.add.rawValue
            {
                // switch to new account
                addNewAccount()
            }
            else if   tag ==  HubMeConstants.ManageAccount.manage.rawValue
            {
                let manageVC = ManageAccountViewController.create()
                manageVC.selectedAccountItem = selectedAccountItem
                manageVC.accountsItem = accountsItem
                manageVC.delegate = self
                self.navigationController?.pushViewController(manageVC, animated: true)
            }
        }
            
        else if selectedAccountItem?.channelUsername != self.selectedAccountItem?.channelUsername
        {
            feedList =  []
            mailsListTB.reloadData()
            self.selectedAccountItem = selectedAccountItem
            self.accountsItem.accounts = accountsItem
            nextPageNumber = "0"
            accountImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
                (selectedAccountItem?.icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            accountName.text = selectedAccountItem?.channelUsername
            
            accountMessageName.text = selectedAccountItem?.channelUsername
            teamNameLb.text = selectedAccountItem?.teamName
            
            feedListPresenter?.getFeedResponse(type: type!, channelId: String(describing: selectedAccountItem?.id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
            if !pleasewait.isHidden
            {
                loading.startAnimating()
            }
            else
            {
                pleasewait.isHidden = true
                noDataFound.isHidden = true
                noFeedsFound.isHidden = true
                loading.stopAnimating()
            }
            
        }
        self.sheetController?.closeSheet()
    }
}


extension FeedsTableViewController : LoginChannelPresenterView
{
    
    func setLoginSocialResult(response: String, authUrl: String) {
        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            if accountsItem.groupName?.lowercased() == HubMeConstants.Channels.gmailChannel.lowercased()
            {
                dismssPeriodDialog()
                let urlStr = authUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                
                if let authURL = NSURL(string: urlStr ?? "")
                {
                    
                    let safariVC = SFSafariViewController(url: authURL as URL)
                    safariVC.delegate = self
                    UIApplication.shared.keyWindow?.rootViewController?.present(safariVC, animated: true, completion: nil)
                    
                    //                    self.present(safariVC, animated: true, completion: nil)
                    //                    registerSocket()
                }
                
            }
            else
            {
                let webVC = AddSocialAccountViewController.create()
                webVC.channelName = accountsItem.groupName ?? ""
                webVC.authUrl = authUrl
                webVC.delegate = self
                webVC.accountsItem = accountsItem.accounts
                webVC.selectedAccountItem = selectedAccountItem
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        loading.stopAnimating()
    }
    
    
}


extension FeedsTableViewController : SFSafariViewControllerDelegate
{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("listening ....")
        controller.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true) { () -> Void in
            print("You just dismissed the login view.")
        }
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        print("didLoadSuccessfully: \(didLoadSuccessfully)")
        
    }
}


extension FeedsTableViewController : StompClientLibDelegate
{
    func registerSocket(){
        let baseURL = HubMeConstants.Url.rabbitmQ_url
        url = NSURL(string: baseURL)!
        
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":HubMeConstants.rabbirMQCredential.rabbitmQ_username,"passcode":HubMeConstants.rabbirMQCredential.rabbitmQ_password,"host":"/"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = self.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
        socketClient.autoDisconnect(time: 15)
        // Reconnect after 4 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 14.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket is Disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        print("Socket will is Disconnected")
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("JSON BODY : \(String(describing: jsonBody))")
        if let response = UserChannelResponse(JSON:  jsonBody as! [String: Any])
        {
            if response.message?.lowercased() == HubMeConstants.Messages.success.lowercased()
            {
                Helper.showFloatAlert(title: HubMeConstants.Messages.success.localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
            else
            {
                if response.message == "ACCOUNT_ALREADY_EXISTS"
                {
                    Helper.showFloatAlert(title: "Account already exists".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: response.message ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.safariVC?.dismissDialogViewController()
                    //                    self.navigationController?.popViewController(animated: true)
                })
                
                
            }
        }
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("Server Ping")
    }
}
extension FeedsTableViewController :NotSoGoodCellDelegate
{
    
    @objc func moreBtnTapped(sender:UIButton)
    {
        let cell = mailsListTB.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? MessageTableViewCell
        
        for (i,item) in feedList.enumerated()
        {
            if i == sender.tag
            {
                feedList[sender.tag].isExpanded = !feedList[sender.tag].isExpanded
            }
            else
            {
                item.isExpanded = false
            }
        }
        cell?.messageText.numberOfLines = feedList[sender.tag].isExpanded ? 0 : 2
        cell?.showMoreBtn.setTitle(feedList[sender.tag].isExpanded ? "Read less...".localized() : "Read more...".localized(), for: .normal)
        
        //        mailsListTB.beginUpdates()
        //        mailsListTB.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        mailsListTB.reloadData()
        //        mailsListTB.endUpdates()
        
    }
    // MARK: - my cell delegate
    func moreTapped(cell: MessageTableViewCell) {
        
        // this will "refresh" the row heights, without reloading
        //        mailsListTB.beginUpdates()
        //        mailsListTB.reloadData()
        //        mailsListTB.endUpdates()
        
        // do anything else you want because the switch was changed
        
    }
}
