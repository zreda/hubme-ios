//
//  MeetingDetailsViewController.swift
//  HubMe
//
//  Created by Ghonim on 11/20/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import WebKit
import LSDialogViewController
import SVProgressHUD

class MeetingDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var acceptBtn: RoundedButton!
    @IBOutlet weak var maybeBtn: RoundedButton!
    @IBOutlet weak var declineBtn: RoundedButton!
    
    fileprivate var feedPresenter: FeedsPresenter?
    fileprivate var relogin:ReloginBaseController?
    fileprivate var contentHeight = 0.0
    fileprivate var state:HideState = .collapse
    fileprivate var firstLb: String = ""
    fileprivate var plazzaProfilePresenter: PlazzaProfilePresenter?
    var accountName:String?
    var type:userType?
    var feed: MessageItem?
    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchFeedDetails()
        // Do any additional setup after loading the view.
        plazzaProfilePresenter = PlazzaProfilePresenter(self)

    }
    
    
    @objc public static func create() -> MeetingDetailsViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! MeetingDetailsViewController
    }
    override func viewWillDisappear(_ animated: Bool) {
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
        SVProgressHUD.dismiss()

        
    }
    
    func fetchFeedDetails()
    {
        loading.startAnimating()
        feedPresenter = FeedsPresenter(self)
        feedPresenter?.getFeedDetails(msgId: (feed?.messageId) ?? "0", date:(feed?.date) ?? "0",userChannelId:(feed?.userChannelId) ?? "0")
        feedPresenter?.markFeedAsRead(feed: ReadFeedRequest(date: (feed?.date) ?? "", messageId: (feed?.messageId) ?? "", userChannelId: (feed?.userChannelId) ?? ""))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = (feed?.channel?.capitalizingFirstLetter())! + " " +  "message".localized()
        //        self.navigationItem.title = feed?.title ?? ""
    }
    @objc func getPlazzaProfile(sender:UITapGestureRecognizer) {
        self.tableView.isUserInteractionEnabled = false
        plazzaProfilePresenter?.getPlazzaProfile(email: firstLb)
        SVProgressHUD.show()
    }
}

extension MeetingDetailsViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_time_cell") as! RecipientTimeTableViewCell
            cell.userImg.image = UIImage(named: "avatar_icon")
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(FeedsDetailsTableViewController.getPlazzaProfile))
            cell.firstRecipientLb.addGestureRecognizer(tap)
            cell.firstRecipientLb.isUserInteractionEnabled = true
            
            if feed?.options?.to?.count ?? 0 > 0
            {
                if feed?.options?.to?.contains(where: {$0.email == accountName}) ?? false
                {
                    cell.firstRecipientLb.text =  "to: me"
                    firstLb = accountName ?? firstLb
                }
                else
                {
                    cell.firstRecipientLb.text =  "to: " + (feed?.options?.to?[0].email ?? "")
                    firstLb = feed?.options?.to?[0].email ?? ""
                    
                }
            }
            else if feed?.options?.cc?.count ?? 0 > 0
            {
                if feed?.options?.cc?[0].email == accountName
                {
                    cell.firstRecipientLb.text =  "cc: me"
                    firstLb = accountName ?? ""
                }
                else
                {
                    cell.firstRecipientLb.text = "cc: " + (feed?.options?.cc?[0].email  ?? "")
                    firstLb = feed?.options?.cc?[0].email ?? ""
                }
            }
            else if feed?.options?.bcc?.count ?? 0 > 0
            {
                if feed?.options?.bcc?[0].email == accountName
                {
                    cell.firstRecipientLb.text =  "bcc: me"
                    firstLb = accountName ?? ""
                }
                else
                {
                    cell.firstRecipientLb.text = "bcc: " + (feed?.options?.bcc?[0].email ?? "")
                    firstLb = feed?.options?.bcc?[0].email ?? ""
                }
            }
            
            let numberOfRecipient =  (feed?.options?.to?.filter { $0.email != accountName && $0.email != firstLb }.count ?? 0) + (feed?.options?.cc?.count ?? 0)!

            if  numberOfRecipient > 0 && state != .expand
            {
                cell.showMoreBtn.isHidden = false
            }
            else
            {
                cell.showMoreBtn.isHidden = true
                
            }
            cell.senderLb.text = feed?.from ?? ""
            cell.recipientTimeLb.text =  Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "dd/MM/yyyy") + " at " +  Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "h:mm a")
            cell.showMoreBtn.addTarget(self, action: #selector(MeetingDetailsViewController.showMoreBtnAction), for: UIControlEvents.touchUpInside)
            cell.showMoreBtn.tag = indexPath.row
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recipient_cell") as! RecipientParentTableViewCell
            cell.meetingView = self
            
          let toArr = feed?.options?.to?.filter { $0.email != accountName && $0.email != firstLb}

            cell.toRecipients = toArr ?? []
            cell.ccRecipients = feed?.options?.cc ?? []
            cell.bccRecipients = feed?.options?.bcc ?? []
            
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "event_cell") as! MeetingTableViewCell
            cell.meeting_subject.text = feed?.title ?? ""
            if feed?.options?.meetingDetails?.location != "" && feed?.options?.meetingDetails?.location != nil
            {
                cell.location_img.isHidden = false
            }
            else
            {
                cell.location_img.isHidden = true
            }
            
            cell.meeting_location.text = feed?.options?.meetingDetails?.location  ?? ""
            
            if let occurance = feed?.options?.meetingDetails?.recurrence
            {
                if let endDate = occurance.endDate // have end date
                {
                    if let numberOfOccurrences = occurance.numberOfOccurrences // with ocurrance number
                    {
                        
                        cell.meeting_time.text = "\(occurance.recurrenceAsString ?? "") \(" effective ".localized()) + \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "dd, MMM YYYY")) \(" for ".localized()) \(String(describing: numberOfOccurrences)) \(" starts on ".localized()) + \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "HH:mm a"))"
                        
                    }
                    else
                    {
                        cell.meeting_time.text =  "\(occurance.recurrenceAsString ?? "")\(" effective ".localized()) \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "dd, MMM YYYY"))\(" until ".localized()) \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: endDate )), formate: "dd, MMM YYYY"))\(" starts on ".localized())\(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "HH:mm a"))\(" ends on ".localized()) \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: endDate )), formate: "HH:mm a"))"
                        
                    }
                }
                else // no end date
                {
                    cell.meeting_time.text = "\(occurance.recurrenceAsString ?? "") \(" effective ".localized())\(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "dd, MMM YYYY"))\(" starts on ".localized()) \(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "HH:mm a"))"
                }
                
            }
            else
            {
                cell.meeting_time.text = "From ".localized() + Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "dd, MMM YYYY HH:mm a") + "to ".localized() + Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.endDate as! Int)), formate: "dd, MMM YYYY HH:mm a")
                
            }
            cell.meeting_day.text = Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "dd")
            cell.meeting_month.text = Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: String(describing: feed?.options?.meetingDetails?.startDate as! Int)), formate: "MMM")
            return cell
            
        }
            
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feed_content") as! FeedContentTableViewCell
            cell.feedContentWebView.tag = indexPath.row
//            cell.feedContentWebView.navigationDelegate = self
            cell.feedContentWebView.loadHTMLString( feed?.desc ?? "", baseURL: nil)
            return cell
        }
        return UITableViewCell()
    }
    
    
    @IBAction func acceptBtnAction(_ sender: Any)
    {
        
        showMeetionOptionsDialogue(meetingAction: .accept)
    }
    
    @IBAction func maybeBtnAction(_ sender: Any)
    {
        showMeetionOptionsDialogue(meetingAction: .maybe)
        
    }
    
    @IBAction func declineBtnAction(_ sender: Any)
    {
        showMeetionOptionsDialogue(meetingAction: .decline)
        
    }
    func showMeetionOptionsDialogue(meetingAction:meetingActions)
    {
        // to show the dialog
        if ls_dialogViewController == nil
        {
            let dialogViewController: MeetingResponsesViewController = MeetingResponsesViewController(nibName:"MeetingReponsesView", bundle: nil)
            dialogViewController.delegate = self
            dialogViewController.action = meetingAction
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.fadeInOut, completion: { () -> Void in })
        }
        
    }
    
    func handelMeetingAction(action:meetingActions ,subAction:meetingSubActions)
    {
//        if ls_dialogViewController != nil
//        {
            self.dismissDialogViewController()
//        }
        let meetingRequest = MeetingActionRequestModel(action: action.rawValue, subAction: subAction.rawValue)
        switch subAction
        {
            
        case .noReponse , .sendNow :
            
            feedPresenter?.setMeetingAction(msgId: feed?.messageId ?? "", date: feed?.date ?? "", userChannelId: feed?.userChannelId ?? "", request: meetingRequest)
            loading.startAnimating()
            break
        case .responseBeforeSend :
            
            let replyVC = ReplyViewController.create()
            replyVC.type = type
            replyVC.replyType = ReplyType.reply
            replyVC.msgItem = self.feed
            replyVC.meetingModel = meetingRequest
            replyVC.groupedAccount = groupedAccount
            replyVC.channelItem = channelItem
            replyVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(replyVC, animated: true)
            break
            
        }
    }
    @objc func showMoreBtnAction(_ sender: UIButton) {
        switch state
        {
        case .collapse:
            state = .expand
            //            let cell = tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! RecipientTimeTableViewCell
            sender.isHidden = true
            tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
            tableView.beginUpdates()
            tableView.endUpdates()
            
            break
        case .expand:
            state = .collapse
            tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
            tableView.beginUpdates()
            tableView.endUpdates()
            break
        }
    }
    
}


extension MeetingDetailsViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 100
        }
        else if indexPath.row == 2
        {
            return 150.0
        }
        else if indexPath.row == 3
        {
//            return CGFloat(contentHeight )
            return 600.0
        }
            
        else
        {
            if indexPath.row == 1 && state == .collapse
            {
                return 0
            }
            
            return UITableViewAutomaticDimension
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 100
        }
        else if indexPath.row == 2
        {
            return 150.0
        }
        else if indexPath.row == 3
        {
//            return CGFloat(contentHeight )
            return 600.0
        }
            
        else
        {
            if indexPath.row == 1 && state == .collapse
            {
                return 0
            }
            
            return UITableViewAutomaticDimension
        }
    }
}

extension MeetingDetailsViewController : WKNavigationDelegate
{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        if (webView.isLoading) ||  contentHeight > 8.0
        {
            return
        }
        loading.stopAnimating()

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            
        }
    }
}

extension MeetingDetailsViewController : FeedsPresenterView
{
    func setFeedDetailsResult(response: FeedDetailResponse?) {
        loading.stopAnimating()
        feed?.desc = response?.text
        tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
        self.tableView.isUserInteractionEnabled = true
    }
    
    func setSuccess(response: String?) {
    
        debugPrint("Messge Read Successfully")
        
    }
    func setError(error: NSError,url: String) {
        loading.stopAnimating()
        SVProgressHUD.dismiss()
        self.tableView.isUserInteractionEnabled = true

        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            relogin = ReloginBaseController()
            relogin?.controller = self
            relogin?.registerSocket()
            relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing:channelItem?.id ?? 0 as! Int), selectedAccount: channelItem ?? UserChannelsItem())
        }
            
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
        
    }
    
    func setMeetingActionResult(response: String?) {
        
        loading.stopAnimating()
//        if ls_dialogViewController != nil
//        {
//            self.dismissDialogViewController()
//        }
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
extension MeetingDetailsViewController : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
        self.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }
    
    func setErrorReponse(msg: String?) {
        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
    }
}
