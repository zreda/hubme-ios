//
//  TwitterViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/10/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage
import LSDialogViewController

class TwitterViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var feed_from: UILabel!
    @IBOutlet weak var feed_subject: UILabel!
    @IBOutlet weak var feed_receipt_time: UILabel!
    @IBOutlet weak var feed_logo: UIImageView!
    @IBOutlet weak var feed_content: UIWebView!
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var replyConst: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var replyTF: DesignableUITextField!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var numberOfChar: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var replyPresenter: ReplyFeedPresenter?
    fileprivate var relogin:ReloginBaseController?
    fileprivate var feedPresenter: FeedsPresenter?
    
    var type:userType?
    var feedLogo: String?
    var feed: MessageItem?
    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    var  customBackButton:UIBarButtonItem?
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = (feed?.channel?.capitalizingFirstLetter())! + " " +  "message".localized()
        
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
    }
    func initView()
    {
        customBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action:  #selector(TwitterViewController.backAction))
        customBackButton?.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        
        replyTF.delegate = self
        // Do any additional setup after loading the view.
        feed_content.delegate = self
        feedPresenter = FeedsPresenter(self)
        feedPresenter?.getFeedDetails(msgId: (feed?.messageId) ?? "0", date:(feed?.date) ?? "0",userChannelId:(feed?.userChannelId) ?? "0")
        feedPresenter?.markFeedAsRead(feed: ReadFeedRequest(date: (feed?.date) ?? "", messageId: (feed?.messageId) ?? "", userChannelId: (feed?.userChannelId) ?? ""))
        
        feed_receipt_time.text = "Sent".localized() + Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: feed?.date), formate: "dd/MM/yyyy HH:mm")
        
        feed_logo.sd_setImage(with: URL(string: feedLogo ?? "" ), placeholderImage: #imageLiteral(resourceName: "channel_twitter_color"))
        
        if feed?.options?.screenName == "" || feed?.options?.screenName ==  nil {
            feed_subject.text = (feed?.title ?? "")!
        } else {
            feed_subject.text = "@"+(feed?.options?.screenName ?? "")!
            
        }
        feed_from.text = feed?.from
        
        if feed?.attachments?[0] != nil {
            
            feedImage.sd_setImage(with: URL(string: ((feed?.attachments?[0].url!)!)), placeholderImage: #imageLiteral(resourceName: "loading_image_error"))
        } else {
            feedImage.isHidden = true
        }
        
        feed_content.loadHTMLString(feed?.desc ?? "", baseURL: nil)
        
    }
    
    @IBAction func sendReplyBtnAction(_ sender: Any) {
        replyPresenter = ReplyFeedPresenter (self)
        if  !replyTF.text!.isEmpty && replyTF.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || replyTF.text == ""
        {
            Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            
            let requestModel = ComposeMessageRequestModel()
            requestModel.messageText = replyTF.text ?? ""
            requestModel.cc = []
            requestModel.to = []
            replyPresenter?.sendReply(msgId: (feed?.messageId) ?? "", date: feed?.date ?? "", userChannelId: feed?.userChannelId ?? "", requestModel: requestModel, msgType: ReplyType.reply)
            
            sendBtn.isEnabled = false
            customBackButton?.isEnabled = false
            self.dismissKeyboard()
            loading.startAnimating()
        }
        
    }
    
    @IBAction func replyEditingChanged(_ sender: Any) {
        
        numberOfChar.text =  "\(280 - (replyTF.text?.count ?? 0)!) character left"
    }
    @IBAction func replyBtnTapped(_ sender: Any) {
        replyConst.constant = 85.0
        replyView.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func backAction(){
        
        if replyTF.text != "" && replyTF.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.twitterDetails = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func goback(_ go: Bool) {
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 280
    }
}

extension TwitterViewController : FeedsPresenterView , UIWebViewDelegate
{
    func setFeedDetailsResult(response: FeedDetailResponse?) {
        
        
        feed?.desc = response?.text
        feed_content.loadHTMLString( feed?.desc ?? "", baseURL: nil)
        loading.stopAnimating()
        
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if (webView.isLoading){
            return
        }
        loading.stopAnimating()
        //        feed_content.loadHTMLString( feed?.desc ?? "", baseURL: nil)
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
    func setSuccess(response: String?) {
        loading.stopAnimating()
        debugPrint("Messge Read Successfully")
    }
    func setError(error: NSError,url: String) {
        loading.stopAnimating()
        sendBtn.isEnabled = true
        customBackButton?.isEnabled = true
        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            relogin = ReloginBaseController()
            relogin?.controller = self
            relogin?.registerSocket()
            relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing:channelItem?.id ?? 0 as! Int), selectedAccount: channelItem ?? UserChannelsItem())
        }
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
    }
    
}
extension TwitterViewController : ReplyFeedPresenterView {
    func setReplySuccess(response: String?) {
        debugPrint("Reply sent Successfully")
        
        loading.stopAnimating()
        Helper.showFloatAlert(title: response ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    func setErrorReponse(msg: String?,url: String) {
        loading.stopAnimating()
        sendBtn.isEnabled = true
        customBackButton?.isEnabled = true
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    
}
