//
//  ChooseChannelSynchronoizationViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/3/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import iOSDropDown

class ChooseChannelSynchronoizationViewController: UIViewController {

    @IBOutlet weak var periodTF: DropDown!
    @IBOutlet weak var backgroundView: UIView!
    var delegate: AddChannelViewController?
    var newAccountdelegate: FeedsTableViewController?
    var notificationDelegate: NotificationsViewController?
    var reloginDelegate: ReloginBaseController?
    var reloginController: UIViewController?

    var config:FirstLoadConfig?
    fileprivate var optionIds:[Int] = []
    fileprivate var optionArray:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.layer.cornerRadius = 10

        if let periods = config?.periods
        {
            for (_,item) in (periods.enumerated())
            {
                optionArray.append("Synchronize the last \(item) days".localized())
            }
        }
        optionArray.append("None")

        periodTF.optionArray = optionArray
        
         optionIds = config?.periods ?? []
        optionIds.append(0)
        periodTF.optionIds = optionIds

        // The the Closure returns Selected Index and String
        periodTF.didSelect{(selectedText , index ,id) in
            self.config?.firstLoadFetchValue = id
            print("Selected String: \(selectedText) \n index: \(index)")
        }
        
        periodTF.text = optionArray[0]
    }

    // close dialogView
    @IBAction func cancelBtnAction(_ sender: AnyObject) {
        self.delegate?.dismissDialogViewController()
        self.newAccountdelegate?.dismissDialogViewController()
        self.notificationDelegate?.dismissDialogViewController()
        self.reloginController?.dismissDialogViewController()


    }
    
    @IBAction func chooseBtnAction(_ sender: AnyObject) {
        if self.config?.firstLoadFetchValue == nil
        {
           self.config?.firstLoadFetchValue = optionIds[0]
        }
        self.delegate?.chooseFirstLoad(selectedConfig: config!)
        self.newAccountdelegate?.chooseFirstLoad(selectedConfig: config!)
//        self.notificationDelegate?.chooseFirstLoad(selectedConfig: config!)
        self.reloginDelegate?.chooseFirstLoad(selectedConfig: config!)
  
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
