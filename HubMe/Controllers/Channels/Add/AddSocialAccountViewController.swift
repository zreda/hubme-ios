//
//  AddSocialAccountViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/28/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SocketRocket
import ObjectMapper
import StompClientLib

class AddSocialAccountViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    weak var delegate: RefreshAccountsDelegate?
    fileprivate var socketClient = StompClientLib()
    fileprivate var url = NSURL()
    var channelName:String?
    var authUrl:String?
    var accountsItem:[UserChannelsItem]?
    var selectedAccountItem: UserChannelsItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add \(channelName ?? "") Channel".localized()
        webView.delegate = self
        registerSocket()
        if let url = URL(string: authUrl ?? "") {
            loading.startAnimating()
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        delegate?.refreshAccount(accountsItem: accountsItem, selectedAccountItem: selectedAccountItem)
    }
    
    @objc public static func create() -> AddSocialAccountViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddSocialAccountViewController
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
}
extension AddSocialAccountViewController : UIWebViewDelegate
    
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if (webView.isLoading){
            return
        }
        loading.stopAnimating()
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
}

extension AddSocialAccountViewController : StompClientLibDelegate
{
    func registerSocket(){
        let rabbitMQSec = Helper.getObjectDefault(key: HubMeConstants.userDefault.rabbitMQSecurity) as? RabbitmqConfig
        
        let baseURL = rabbitMQSec?.url
        url = NSURL(string: baseURL ?? "")!
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":rabbitMQSec?.username ?? "","passcode":rabbitMQSec?.password ?? "","host":"/"])
    }

    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
//        socketClient.autoDisconnect(time: 1)
        // Reconnect after 4 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket is Disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        print("Socket will is Disconnected")
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, akaStringBody stringBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("JSON BODY : \(String(describing: jsonBody))")
        if let response = UserChannelResponse(JSON:  jsonBody as! [String: Any])
        {
            if response.message?.lowercased() == HubMeConstants.Messages.success.lowercased()
            {
                Helper.showFloatAlert(title: HubMeConstants.Messages.success.localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
            else
            {
                if response.message == "ACCOUNT_ALREADY_EXISTS"
                {
                    Helper.showFloatAlert(title: "Account already exists".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: response.message ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.navigationController?.popViewController(animated: true)
                })
                
                
            }
        }
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("Server Ping")
    }
}
