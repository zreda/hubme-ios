//
//  AddMessageChannelViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/12/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SafariServices
import iOSDropDown
import StompClientLib

class AddMessageChannelViewController: UIViewController {
    
    @IBOutlet weak var channelLogoImg: UIImageView!
    @IBOutlet weak var channelNameLb: UILabel!
    @IBOutlet weak var channelNameTF: DesignableUITextField!
    @IBOutlet weak var teamNameTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var periodTF: DropDown!
    @IBOutlet weak var teamNameHeight: NSLayoutConstraint!
    @IBOutlet weak var teamNameLb: UILabel!
    @IBOutlet weak var periodTopConst: NSLayoutConstraint!
    fileprivate var loginPresenter: LoginChannelPresenter?
    fileprivate var safariVC:SFSafariViewController?
    fileprivate var socketClient = StompClientLib()
    fileprivate var url = NSURL()

    var config:FirstLoadConfig?
    var channel: UserChannelsItem?
    var isExpired = false
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
     //   self.navigationItem.title = "Add \(String(describing: (channel?.name) ?? "")) channel"
        self.navigationItem.title = "Add Channels"

    }
    override func viewWillDisappear(_ animated: Bool) {
    }
    func initUI()
    {
        if channel?.name?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased()
        {
            teamNameHeight.constant = 0
            periodTopConst.constant = 0
            teamNameLb.isHidden = true
            teamNameTF.isHidden = true
        }
        else
        {
            teamNameHeight.constant = 40
            periodTopConst.constant = 30
            teamNameLb.isHidden = false
            teamNameTF.isHidden = false
        }
        channelLogoImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "ios/" +
            (channel?.icon ?? "")!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        channelNameLb.text = channel?.name ?? ""
        // The list of array to display. Can be changed dynamically
        var  syncPeriod:[Int] = config?.periods ?? []
        
        var optionArray:[String] = []
        for (_,item) in syncPeriod.enumerated()
        {
            optionArray.append("Synchronize the last \(item) days")
        }
        optionArray.append("None")
        periodTF.optionArray = optionArray        //Its Id Values and its optional
        
        syncPeriod.append(0)
        periodTF.optionIds = syncPeriod
        periodTF.setBottomBorder(_color: #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1))
        periodTF.text = optionArray[0]
        periodTF.selectedRowColor = #colorLiteral(red: 0.9213546515, green: 0.9246771932, blue: 0.9278178811, alpha: 1)
        config?.firstLoadFetchValue = syncPeriod[0]
        // The the Closure returns Selected Index and String
        periodTF.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.config?.firstLoadFetchValue = id
        }
        
        
    }
    @objc public static func create() -> AddMessageChannelViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddMessageChannelViewController
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        
        if channelNameTF.text == ""  || channelNameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "channel_name_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            
        else if (teamNameTF.text == ""  || teamNameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ) && channel?.name?.lowercased() != HubMeConstants.Channels.slackChannel.lowercased()
        {
            Helper.showFloatAlert(title: "team_name_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            if channel?.name?.lowercased()  == HubMeConstants.Channels.mattermostChannel.lowercased()
            {
                Helper.showFloatAlert(title: "matter most cann't be login".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

            }
            else
            {
                let auth = LoginRequestModel(expired:isExpired,teamName: teamNameTF.text ?? "", subChannelName: channelNameTF.text!,firstLoadConfig:config!,channel_id: String(describing: channel?.id as! Int) )
                self.loginPresenter = LoginChannelPresenter(self)
                self.loginPresenter?.getLoginMessaging(auth: auth)
                self.loading.startAnimating()
            }
            
        }
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
extension AddMessageChannelViewController: LoginChannelPresenterView {
    
    func setLoginSocialResult(response: String,authUrl:String) {
        
        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            
        let urlStr = authUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            
            if let authURL = NSURL(string: urlStr ?? "")
            {
                
                let safariVC = SFSafariViewController(url: authURL as URL)
                safariVC.delegate = self
                UIApplication.shared.keyWindow?.rootViewController?.present(safariVC, animated: true, completion: nil)
                registerSocket()
            }
            
            
        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        loading.stopAnimating()
        
        
        
        
    }
    
    func setError(error: NSError, url: String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code )!, messge: error.description, url: url)
        loading.stopAnimating()
    }
    
    
    
}

extension AddMessageChannelViewController : SFSafariViewControllerDelegate
{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("listening ....")
        controller.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true) { () -> Void in
            print("You just dismissed the login view.")
        }
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        print("didLoadSuccessfully: \(didLoadSuccessfully)")
        
    }
}

extension AddMessageChannelViewController : StompClientLibDelegate
{
    func registerSocket(){
        let rabbitMQSec = Helper.getObjectDefault(key: HubMeConstants.userDefault.rabbitMQSecurity) as? RabbitmqConfig
        
        let baseURL = rabbitMQSec?.url
        url = NSURL(string: baseURL ?? "")!
        
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":rabbitMQSec?.username ?? "","passcode":rabbitMQSec?.password ?? "","host":"/"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
//        socketClient.autoDisconnect(time: 15)
        // Reconnect after 4 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket is Disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        print("Socket will is Disconnected")
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("JSON BODY : \(String(describing: jsonBody))")
   
        if let response = BaseResponse(JSON: jsonBody as? [String: Any] ?? ["":""])
        {
            if response.message?.lowercased().localized() == HubMeConstants.Messages.success.lowercased()
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    
                    Helper.showFloatAlert(title: HubMeConstants.Messages.success.localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        self.navigationController?.popToRootViewController(animated: true)
                    })
                    })
            }
            else
            {
                if response.message == "ACCOUNT_ALREADY_EXISTS".localized()
                {
                    Helper.showFloatAlert(title: "Account already exists".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: response.message ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.safariVC?.dismissDialogViewController()
                })
            }
        }
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, akaStringBody stringBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("Server Ping")
    }
}
