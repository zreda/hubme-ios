//
//  AddChannelViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/10/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
//import TwitterKit
import SDWebImage
//import GoogleSignIn
import LSDialogViewController
import SafariServices
//import StompClientLib

class AddChannelViewController: UIViewController {
    @IBOutlet weak var channelsTB: UITableView!
    @IBOutlet weak var noChannelsLB: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    fileprivate var allChannels: [UserChannelsItem] = []
    fileprivate var allChannelsPresenter: UserChannelsPresenter?
    fileprivate var loginPresenter: LoginChannelPresenter?
    fileprivate var channelId:String?
    fileprivate var channelName:String?
//    fileprivate var googleUser:GIDGoogleUser?
    fileprivate var safariVC:SFSafariViewController?
    fileprivate var socketClient = StompClientLib()
    fileprivate var url = NSURL()
    var groupUserList:[GroupedChannel] = []
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.title = "Choose a channel type".localized()
        
    }
    override func viewDidLoad() {
        
        initView()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        dismssPeriodDialog()
    }
    @objc public static func create() -> AddChannelViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddChannelViewController
    }
    
    func initView() {
       
        channelsTB?.tableFooterView = UIView(frame: CGRect.zero)
        let nib = UINib(nibName: "ChannelCell", bundle: nil)
        channelsTB.register(nib, forCellReuseIdentifier: "channel_cell")
        noChannelsLB.isHidden = true
        allChannelsPresenter = UserChannelsPresenter(self)
        allChannelsPresenter?.getAllChannels()
        loading.startAnimating()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.safariLogin(notification:)), name: Notification.Name(HubMeConstants.Notifications.CloseSafari), object: nil)
    }
    deinit{
   
        NotificationCenter.default.removeObserver(self)

    }
    func showConfig(_ animationPattern: LSAnimationPattern ,firstConfig:FirstLoadConfig )
    {
        // to show the dialog
        let dialogViewController: ChooseChannelSynchronoizationViewController = ChooseChannelSynchronoizationViewController(nibName:"ChooseSynchronoizationPopupDialog", bundle: nil)
        dialogViewController.delegate = self
        dialogViewController.config = firstConfig
        self.presentDialogViewController(dialogViewController, animationPattern: animationPattern, completion: { () -> Void in })
    }
    

    func chooseFirstLoad(selectedConfig:FirstLoadConfig)
    {
        var firstLoadFetchValue  = 200
        var auth = LoginRequestModel()
        
        if channelName?.lowercased() != HubMeConstants.Channels.twitterChannel.lowercased()
        {
            firstLoadFetchValue = selectedConfig.firstLoadFetchValue ?? 0
        }
        
        auth = LoginRequestModel(expired:false,channel_id: channelId ?? "", channel_name: "", firstLoadConfig: FirstLoadConfig(criteria: selectedConfig.criteria!, firstLoadFetchValue: firstLoadFetchValue))
        self.loginPresenter = LoginChannelPresenter(self)
        
        self.loginPresenter?.getLoginSocial(auth: auth)
        self.loading.startAnimating()
    }
    
    
    func dismssPeriodDialog()
    {
        self.dismissDialogViewController(LSAnimationPattern.zoomInOut)
        
    }
    @objc func safariLogin(notification: NSNotification) {
        // get the url from the auth callback
        safariVC?.dismiss(animated: true, completion: nil)
    }

}

extension AddChannelViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // configure cell data
        
        let channel_cell = channelsTB.dequeueReusableCell(withIdentifier: "channel_cell") as! ChannelTableViewCell
        channel_cell.msgReadingTime.isHidden = true
        
        channel_cell.configureCell(userChannel: allChannels[indexPath.row])
        
        return channel_cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (allChannels[indexPath.row].name == HubMeConstants.Channels.twitterChannel) {
            channelId = String(describing: self.allChannels[indexPath.row].id as! Int)
            
            channelName = allChannels[indexPath.row].name
            chooseFirstLoad(selectedConfig: allChannels[indexPath.row].firstLoadConfig ?? FirstLoadConfig() )
            
        }
        else if (allChannels[indexPath.row].name == HubMeConstants.Channels.gmailChannel)
            
        {
            channelId = String(describing: self.allChannels[indexPath.row].id as! Int)
            
            channelName = allChannels[indexPath.row].name
            showConfig(LSAnimationPattern.zoomInOut, firstConfig: allChannels[indexPath.row].firstLoadConfig ?? FirstLoadConfig())
            
        }
        else if (allChannels[indexPath.row].name?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() || allChannels[indexPath.row].name?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() )
            
        {
            let loginVC = AddMessageChannelViewController.create()
            loginVC.channel = allChannels[indexPath.row]
            loginVC.config = allChannels[indexPath.row].firstLoadConfig
            self.navigationController?.pushViewController(loginVC, animated: true)
            
        }
        else
        {
        
            let vc = LoginChannelViewController.create()
            vc.channel = allChannels[indexPath.row]
            vc.config = allChannels[indexPath.row].firstLoadConfig
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        channelsTB.deselectRow(at: indexPath, animated: true)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return allChannels.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60.0
    }
}


extension AddChannelViewController : LoginChannelPresenterView
{
    
    func setLoginSocialResult(response: String, authUrl: String) {
        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            if channelName?.lowercased() == HubMeConstants.Channels.gmailChannel.lowercased()
            {
                dismssPeriodDialog()
                let urlStr = authUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
                
                if let authURL = NSURL(string: urlStr ?? "")
                {
                    
                    let safariVC = SFSafariViewController(url: authURL as URL)
                    safariVC.delegate = self
                    UIApplication.shared.keyWindow?.rootViewController?.present(safariVC, animated: true, completion: nil)
                    registerSocket()
                }
                
            }
            else
            {
                let webVC = AddSocialAccountViewController.create()
                webVC.channelName = channelName ?? ""
                webVC.authUrl = authUrl
                self.navigationController?.pushViewController(webVC, animated: true)
            }
        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        loading.stopAnimating()
    }
    
    func setError(error: NSError, url: String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        
        loading.stopAnimating()
    }
}
extension AddChannelViewController : ChannelsPresenterView
{
    func getAllChannels(response: [UserChannelsItem]?) {
        
        loading.stopAnimating()
        
        self.allChannels = response!
        
        var newChannels:[UserChannelsItem] = []

        for (_,item) in allChannels.enumerated()
        {
            let results = groupUserList.filter
            {
                el in (el.groupName?.lowercased() == HubMeConstants.Channels.slackChannel.lowercased() && item.name?.lowercased()  ==  HubMeConstants.Channels.slackChannel.lowercased()) || (el.groupName?.lowercased() == HubMeConstants.Channels.mattermostChannel.lowercased() && item.name?.lowercased()  ==  HubMeConstants.Channels.mattermostChannel.lowercased())
            }
            if results.count == 0
            {
                newChannels.append(item)
            }

        }

        self.allChannels = newChannels
        
        if allChannels.count == 0 {
            noChannelsLB.isHidden = false
        } else {
            noChannelsLB.isHidden = true
            
        }
        self.channelsTB.reloadData()
    }
}
extension AddChannelViewController : SFSafariViewControllerDelegate
{
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        print("listening ....")
        controller.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true) { () -> Void in
            print("You just dismissed the login view.")
        }
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        print("didLoadSuccessfully: \(didLoadSuccessfully)")
        
    }
}
extension AddChannelViewController : StompClientLibDelegate
{
    func registerSocket(){
        let baseURL = HubMeConstants.Url.rabbitmQ_url
        url = NSURL(string: baseURL)!
        
        socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":HubMeConstants.rabbirMQCredential.rabbitmQ_username,"passcode":HubMeConstants.rabbirMQCredential.rabbitmQ_password,"host":"/"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.topic
        print("Socket is Connected : \(topic)")
        socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
//         socketClient.autoDisconnect(time: 15)
        // Reconnect after 4 sec
        socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        print("Socket is Disconnected")
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        print("Socket will is Disconnected")
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("JSON BODY : \(String(describing: jsonBody))")
        if let response = UserChannelResponse(JSON:  jsonBody as! [String: Any])
        {
            if response.message?.lowercased() == HubMeConstants.Messages.success.lowercased()
            {
                Helper.showFloatAlert(title: HubMeConstants.Messages.success.localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.navigationController?.popToRootViewController(animated: true)
                })
            }
            else
            {
                if response.message == "ACCOUNT_ALREADY_EXISTS"
                {
                    Helper.showFloatAlert(title: "Account already exists".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: response.message ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    self.safariVC?.dismissDialogViewController()
                    //                    self.navigationController?.popViewController(animated: true)
                })
                
                
            }
        }
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        print("DESTIONATION : \(destination)")
        print("String JSON BODY : \(String(describing: jsonBody))")
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        print("Receipt : \(receiptId)")
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        print("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        print("Server Ping")
    }
}

