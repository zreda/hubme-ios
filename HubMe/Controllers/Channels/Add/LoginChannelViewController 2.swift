//
//  LoginChannelViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/19/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage
import iOSDropDown
enum PasswordAppearance
{
    case show
    case hide
}
class LoginChannelViewController: UIViewController {
    
    @IBOutlet weak var emailTF: DesignableUITextField!
    @IBOutlet weak var passwordTF: DesignableUITextField!
    @IBOutlet weak var accountTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var accountHeightCons: NSLayoutConstraint!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var channelIcon: UIImageView!
    @IBOutlet weak var periodTF: DropDown!
    @IBOutlet weak var showPwdBtn: UIButton!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var trailingConst: NSLayoutConstraint!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    fileprivate var loginPresenter: LoginChannelPresenter?
    fileprivate var passwordAppearance:PasswordAppearance = .hide
    weak var delegate: RefreshAccountsDelegate?
    var config:FirstLoadConfig?
    var channel: UserChannelsItem!
    var accountName:String?
    var email:String?
    var isExpired = false

    var accountsItem:[UserChannelsItem]?
    var channelId:String?
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "Add \(String(describing: channel.name!)) channel"
    }
    override func viewDidLoad() {
//       hideKeyboard()
       initView()
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        delegate?.refreshAccount(accountsItem: accountsItem, selectedAccountItem: channel)
    }
    
    @objc public static func create() -> LoginChannelViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! LoginChannelViewController
    }
    
    func initView()
    {
        emailTF.delegate = self
        passwordTF.delegate = self
        accountTF.delegate = self
        accountTF.text = accountName ?? ""
        emailTF.text = email ?? ""
        var  syncPeriod:[Int] = config?.periods ?? []
        
        // The list of array to display. Can be changed dynamically
        var optionArray:[String] = []
        for (_,item) in syncPeriod.enumerated()
        {
            optionArray.append("Synchronize the last \(item) days")
        }
        optionArray.append("None")
        periodTF.optionArray = optionArray        //Its Id Values and its optional
        
        syncPeriod.append(0)
        periodTF.optionIds = syncPeriod
        periodTF.setBottomBorder()
        periodTF.text = optionArray[0]
        periodTF.selectedRowColor = #colorLiteral(red: 0.9213546515, green: 0.9246771932, blue: 0.9278178811, alpha: 1)
        config?.firstLoadFetchValue = syncPeriod[0]
        // The the Closure returns Selected Index and String
        periodTF.didSelect{(selectedText , index ,id) in
            print("Selected String: \(selectedText) \n index: \(index)")
            self.config?.firstLoadFetchValue = id
        }
        
        if channel.name == HubMeConstants.Channels.plazzaChannel {
            accountHeightCons.constant = 0.0
            
            // it's for test
            emailTF.placeholder = "Email"
            trailingConst.constant = 50
            verifyBtn.isHidden = false
            passwordTF.placeholder = "Verification code".localized()

//            emailTF.text = "aghonim.ext@orange.com"
//            passwordTF.text = ""
            
        }
        else
        {
            // it's for test
            accountTF.placeholder = "Username"
            emailTF.placeholder = "Email"
            trailingConst.constant = 30
            verifyBtn.isHidden = true
            
//            accountTF.text = "AD\\Hscv5540"
//            emailTF.text = "zreda.ext@orange.com"
//            passwordTF.text = "Welcome5540"

            
        }
        channelName.text = channel.name
        channelIcon.sd_setImage(with: URL(string: (HubMeConstants().main_url + "ios/" +
            channel.icon!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        loginPresenter = LoginChannelPresenter(self)
        
    }
    @IBAction func verifyBtnAction(_ sender: Any) {
        
        // call verify api
        
        if emailTF.text == ""  && emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "email_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if !Helper.isValidEmail(mail_address: emailTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        
        else
        {
            if channelId == nil
            {
                channelId = String(describing: channel.id!)
            }
            loading.startAnimating()
            loginPresenter?.verfiyPlazzaEmail(email: emailTF.text!, channelId: channelId!)
        }
    
    }
    @IBAction func showPasswordBtnAction(_ sender: Any) {
        switch passwordAppearance {
        case .hide:
            passwordAppearance = .show
            showPwdBtn.setImage(#imageLiteral(resourceName: "showPwd"), for: .normal)
            passwordTF.isSecureTextEntry = false
            break
        case .show:
            passwordAppearance = .hide
            showPwdBtn.setImage(#imageLiteral(resourceName: "notShowPwd"), for: .normal)
            passwordTF.isSecureTextEntry = true
            break
        }
    }
    @IBAction func confirmBtnTapped(_ sender: Any) {
        
        var auth: LoginRequestModel?
        
        if (accountHeightCons.constant != 0 && accountTF.text == "" && accountTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "") {
            
            Helper.showFloatAlert(title: NSLocalizedString("account_field_empty", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        } else if accountHeightCons.constant != 0 &&  !accountTF.text!.trimmingCharacters(in: .whitespacesAndNewlines).contains("\\")  {
            Helper.showFloatAlert(title: "domain_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if  accountHeightCons.constant != 0 &&  !Helper.isValidAccount(account: accountTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            Helper.showFloatAlert(title: "domain_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if emailTF.text == "" || passwordTF.text == "" {
            if emailTF.text == ""  && emailTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                Helper.showFloatAlert(title: "email_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            } else if passwordTF.text == "" {
                Helper.showFloatAlert(title: "password_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            
        } else if !Helper.isValidEmail(mail_address: emailTF.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        } else {
            
            if channelId == nil
            {
                channelId = String(describing: channel.id!)
            }
            if config?.firstLoadFetchValue == 0 // None
            {
                auth = LoginRequestModel(expired:isExpired ,email: emailTF.text!, password: passwordTF.text!,verificationCode:passwordTF.text!, account:accountTF.text!, channel_id: channelId!, channel_name: accountTF.text! , firstLoadConfig:nil)

            }
            else
            {
                auth = LoginRequestModel(expired:isExpired,email: emailTF.text!, password: passwordTF.text!,verificationCode:passwordTF.text!, account:accountTF.text!, channel_id: channelId!, channel_name: accountTF.text!,firstLoadConfig:config!)

            }
            loginPresenter = LoginChannelPresenter(self)
            loginPresenter?.getLoginResult(auth: auth!)
            loading.startAnimating()
            doneBtn.isEnabled = false
        }
        
    }
    
}

extension LoginChannelViewController: LoginChannelPresenterView {
    func setLoginResult(response: String) {
        
        loading.stopAnimating()
 
        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }

        doneBtn.isEnabled = true
    }
    
    
    
    func setError(error: NSError, url: String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code ?? 0)!, messge: error.description, url: url)
        loading.stopAnimating()
        doneBtn.isEnabled = true
    }
    
    func confirmationResponse(response: String) {
        loading.stopAnimating()

        if response.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized()
        {
            Helper.showFloatAlert(title: "code is sent , check your email".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)

        }
        else
        {
            Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        
        doneBtn.isEnabled = true
        
    }
    
}
extension LoginChannelViewController : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if channel.name == HubMeConstants.Channels.plazzaChannel
        {
            if textField == emailTF && emailTF.text == ""
            {
                emailTF.placeholder = "me@orange.com"
            }
            
        }
        else
        {
            if textField == accountTF && accountTF.text == ""
            {
                accountTF.placeholder = "AD\\abcd1234"
            }
            else if textField == emailTF  && emailTF.text == ""
            {
                emailTF.placeholder = "me@orange.com"
            }
            
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method


        if channel.name == HubMeConstants.Channels.plazzaChannel
        {
            if textField == emailTF && emailTF.text == ""
            {
                emailTF.placeholder = "Email"
            }
            
        }
        else
        {
            if textField == accountTF && accountTF.text == ""
            {
                accountTF.placeholder = "Username"
            }
           else if textField == emailTF && emailTF.text == ""
            {
                emailTF.placeholder = "Email"
            }
            
        }
        
        return true
    }

}
