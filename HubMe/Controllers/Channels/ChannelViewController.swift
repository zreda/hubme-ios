//
//  ChannelViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import ObjectMapper
import SideMenuSwift

class ChannelViewController: UIViewController {
    
    @IBOutlet weak var noChannelsLB: UILabel!
    @IBOutlet weak var userChannelsTB: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
        fileprivate var userChannelsList: [UserChannelsItem] = []
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var deletedChannelIndex: Int?
    fileprivate var accessToken = ""
    fileprivate var groupUserList:[GroupedChannel] = []
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ChannelViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.orange
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        
        initView()

    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        SideMenuController.preferences.basic.enablePanGesture = false

    }
    override func viewWillAppear(_ animated: Bool) {
//        userChannelsPresenter?.getUserChannelsStatus()
//        userChannelsTB.reloadData()
        groupUserList = []
        userChannelsTB.reloadData()
        userChannelsPresenter?.getUserChannels()
        self.navigationController?.navigationItem.backBarButtonItem?.title = "Channels".localized()
//        Crashlytics.sharedInstance().crash()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.refreshControl.endRefreshing()
    }
    
    func initView() {
        userChannelsPresenter = UserChannelsPresenter(self)

        userChannelsTB?.tableFooterView = UIView(frame: CGRect.zero)
        userChannelsTB.register(UINib(nibName: "ChannelCell", bundle: nil), forCellReuseIdentifier: "channel_cell")
        userChannelsTB.register(UINib(nibName: "AddNewChannelTableViewCell", bundle: nil), forCellReuseIdentifier: "add_channel_cell")

        noChannelsLB.isHidden = true
        accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        
        self.userChannelsTB.addSubview(self.refreshControl)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.cancelSocialAction(notification:)), name: Notification.Name(HubMeConstants.Notifications.socailChannelCancel), object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(getChannelFeeds), name: NSNotification.Name(rawValue: HubMeConstants.Notifications.getChannelFeeds), object: nil)

    }
    
    deinit {

        NotificationCenter.default.removeObserver(self)

    }
    @objc public static func create() -> ChannelViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ChannelViewController
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        
        userChannelsPresenter?.getUserChannels()
        
    }
    
    @objc func cancelSocialAction(notification: Notification) {
        
//        let addChannelVC = self.storyboard?.instantiateViewController(withIdentifier: "AddChannelViewController") as! AddChannelViewController
        let addChannelVC = AddChannelViewController.create()
        addChannelVC.groupUserList =  groupUserList

        self.navigationController?.pushViewController(addChannelVC, animated: false)
    }
    
 
//    @objc func getChannelFeeds(notification:NSNotification){
//
//        self.navigationController?.popToRootViewController(animated: false)
//    }
 
    
}

extension ChannelViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // configure cell data
        //        channel_cell.configureCell(userChannel: userChannelsList[indexPath.row])
        
        if indexPath.row < groupUserList.count
        {
            let channel_cell = userChannelsTB.dequeueReusableCell(withIdentifier: "channel_cell") as! ChannelTableViewCell
            
            channel_cell.channel_img.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
                (groupUserList[indexPath.row].accounts![0].icon ?? "")  ?? "")), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            channel_cell.channel_name.text = groupUserList[indexPath.row].groupName
            return channel_cell
            
        }
        else
            
        {
            let add_channel_cell = userChannelsTB.dequeueReusableCell(withIdentifier: "add_channel_cell") as! UITableViewCell
            return add_channel_cell
            
            
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row < groupUserList.count
        {
            let vc =  FeedsTableViewController.create()
            vc.type = .channel
            //        vc.channelItem = userChannelsList[indexPath.row]
            vc.accountsItem  = groupUserList[indexPath.row]
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            let addChannelVC = AddChannelViewController.create()
            addChannelVC.groupUserList =  groupUserList
            self.navigationController?.pushViewController(addChannelVC, animated: true)
        }
//        userChannelsTB.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        //        return userChannelsList.count
        return groupUserList.count + 1
    }
    
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            
//            let alertController = UIAlertController(title: "Delete Channel".localized(), message: String(format:"Are you Sure to delete channel".localized(),self.userChannelsList[indexPath.row].name!), preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
//            
//            let DestructiveAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.destructive) {
//                (result : UIAlertAction) -> Void in
//                print("Destructive")
//            }
//            
//            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
//            
//            let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertActionStyle.default) {
//                (result : UIAlertAction) -> Void in
//                
//                self.deletedChannelIndex = indexPath.row
//                self.userChannelsPresenter?.deleteChannel(channel_id: String(describing:self.userChannelsList[indexPath.row].id as! Int))
//            }
//            
//            alertController.addAction(DestructiveAction)
//            alertController.addAction(okAction)
//            self.present(alertController, animated: true, completion: nil)
//            
//        }
//    }
    
}

extension ChannelViewController:  ChannelsPresenterView
{
    
    func setUserChannelsResult(response: [UserChannelsItem]?) {
        
        if response?.count == 0 {
            noChannelsLB.isHidden = false
        } else {
            noChannelsLB.isHidden = true
            
        }
        userChannelsList = response!
        Helper.animateTable(table: self.userChannelsTB)
        groupUserList =  Helper.groupChannels(channels: userChannelsList)
        userChannelsTB.reloadData()
        self.refreshControl.endRefreshing()
        loading.stopAnimating()
        // userChannelsPresenter?.getUserChannelsStatus()
        userChannelsPresenter?.getRegisteredChannels()
    }
    
    func deleteChannel(response: String,  error :Bool) {
        
        if !error {
//            if userChannelsList[deletedChannelIndex!].name?.lowercased() == HubMeConstants.Channels.gmailChannel.lowercased()
//            {
//                GIDSignIn.sharedInstance()?.signOut()
//            }
            //Remove object from array
            userChannelsList.remove(at: deletedChannelIndex!)
            groupUserList = Helper.groupChannels(channels: userChannelsList)
            
            if userChannelsList.count == 0 {
                noChannelsLB.isHidden = false
            } else {
                noChannelsLB.isHidden = true
                
            }
            Helper.showFloatAlert(title: String(format:"delete_channel_success".localized(),response), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
            //Reload tableView
            self.userChannelsTB.reloadData()
            
        }
            
        else
        {
            Helper.showFloatAlert(title: String(format:"delete_channel_error".localized(),response), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        
        
    }
    func getAllChannels(response: [UserChannelsItem]?)
    {
        if (response?.count ?? 0) > 0
        {
            for (i,item) in groupUserList.enumerated()
            {
                if let foundItem =  response?.filter({$0.name!.lowercased() == item.groupName!.lowercased()})
                {
                    groupUserList[i].firstConfig = foundItem[0].firstLoadConfig
                }
            }
        }
    }
    
    func setError(error: NSError,url:String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code )!, messge: error.description, url: url)
        loading.stopAnimating()
    }
    
}
extension ChannelViewController: LastUpdateStaus {
    
    func setChannelStatus(channelItem: UserChannelsItem) {
        for (i, item) in userChannelsList.enumerated() {
            if channelItem.id == item.id {
                userChannelsList[i] = channelItem
                break
            }
        }
        userChannelsTB.reloadData()
    }
    
    func setUserChannelsStatus(response: [ChannelsStaus]?) {

        for (_, statusItem) in (response?.enumerated())! {

                if let index = self.userChannelsPresenter?.getIndexOfChannel(userChannelList: self.userChannelsList, id: Int(statusItem.userChannelId ?? "0")!)
                {
                    self.userChannelsList[index].date = statusItem.status
                }
        }

        userChannelsTB.reloadData()
    }
}
