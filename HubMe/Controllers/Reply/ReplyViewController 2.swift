//
//  ReplyViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/3/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SDWebImage
import WSTagsField
import LSDialogViewController
import UITextField_AutoSuggestion
import SVProgressHUD
class ReplyViewController: UITableViewController  {
    
    @IBOutlet weak var channelLogo: UIImageView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var toInputView: UIView!
    @IBOutlet weak var ccInputView: UIView!
    @IBOutlet weak var bccInputView: UIView!
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var reply: UITextView!
    @IBOutlet weak var oldMsg: UIWebView!
    @IBOutlet weak var sentLb: UILabel!
    @IBOutlet weak var oldSubject: UILabel!
    @IBOutlet weak var oldCC: UILabel!
    @IBOutlet weak var oldSender: UILabel!
    @IBOutlet weak var screenTitle: UILabel!
    fileprivate var replyPresenter: ReplyFeedPresenter?
    fileprivate var feedPresenter: FeedsPresenter?
    fileprivate var signaturePresenter:SignaturePresenter?
    fileprivate var senderHeight:CGFloat = 60.0
    fileprivate var ccHeight:CGFloat = 60.0
    fileprivate var bccHeight:CGFloat = 60.0
    fileprivate var replyButton:UIButton?
    fileprivate var backButton:UIButton?
    fileprivate var toTagField = WSTagsField()
    fileprivate var ccTagField = WSTagsField()
    fileprivate var bccTagField = WSTagsField()
    fileprivate var activeTextField:WSTagsField?
    fileprivate var to : [Bcc] = []
    fileprivate var cc : [Bcc] = []
    fileprivate var bcc : [Bcc] = []
    fileprivate var composeNewMessagePresenter:ComposeNewMessagePresenter?
    fileprivate var senders:[SendersModel] = []
    fileprivate var autoComplete: AutoComplete?
    fileprivate var currentTag: WSTagsField?
    fileprivate var isBeginEdit: Bool = false
    fileprivate var mySignarture :String?
    fileprivate var relogin:ReloginBaseController?
    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    var type:userType?
    var replyType:ReplyType?
    var msgItem:MessageItem?
    var meetingModel:MeetingActionRequestModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        oldMsg.delegate = self
        switch replyType
        {
        case .replyAll?:
            self.screenTitle.text = "Reply All".localized()
            self.navigationItem.title = "Reply All".localized()
            
            break
        case .reply?:
//          if type == 
            self.screenTitle.text = "Reply".localized()
            self.navigationItem.title = "Reply".localized()
            break
        case .comment?:
            self.navigationItem.title = ""
            break
        case .none:
            self.navigationItem.title = ""
            
        }
        oldMsg.loadHTMLString( msgItem?.desc ?? "", baseURL: nil)
        
        if msgItem?.supportedActions?.contains(HubMeConstants.SupportedActions.signature.rawValue) ?? false
        {
            signaturePresenter = SignaturePresenter(self)
            signaturePresenter?.getSignature()
        }
        composeNewMessagePresenter = ComposeNewMessagePresenter(self)
        NotificationCenter.default.addObserver(self, selector: #selector(ReplyViewController.addNewSender),name: NSNotification.Name(rawValue: HubMeConstants.Notifications.addSenderList),object: nil)
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard()
        SVProgressHUD.dismiss()
    }
    
    @objc private func addNewSender(notification: NSNotification){
        let sender = notification.object as! SendersModel
        print(sender.email ?? "")
        if currentTag == toTagField
        {
            
            to.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            toTagField.addTag(sender.name ?? "")
            
        }
        else if currentTag == ccTagField
        {
            
            cc.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            ccTagField.addTag(sender.name ?? "")
            
        }
        else if currentTag == bccTagField
        {
            bcc.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            bccTagField.addTag(sender.name ?? "")
            
        }
        
        if self.autoComplete != nil
        {
            for (_,view) in self.view.subviews.enumerated()
            {
                if  view is AutoComplete
                {
                    view.removeFromSuperview()
                    autoComplete = nil
                }
            }
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        loadData()
    }
    
    @objc public static func create() -> ReplyViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ReplyViewController
    }
    
    fileprivate  func initUI()
    {
        to = []
        cc = []
        bcc = []
        backButton = UIButton.init(type: .custom)
        replyButton  = UIButton.init(type: .custom)
        //        toTagField = WSTagsField()
        //        ccTagField = WSTagsField()
        //        bccTagField = WSTagsField()
        activeTextField = WSTagsField()
        initTags(field: toTagField)
        tagFieldEvents(tagField: toTagField)
        initTags(field: ccTagField)
        tagFieldEvents(tagField: ccTagField)
        initTags(field: bccTagField)
        tagFieldEvents(tagField: bccTagField)
        
        toTagField.delegate = self
        ccTagField.delegate = self
        bccTagField.delegate = self
        
        sentLb.text =  Helper.getDayOfWeek(Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: msgItem?.date), formate: "dd/MM/yyyy HH:mm"))! + " " + Helper.convertDatetoString(date: Helper.convertMillSecondStringtoDate(str: msgItem?.date), formate: "dd/MM/yyyy HH:mm")
        
        //set image for button
        backButton?.setImage(#imageLiteral(resourceName: "back") , for: UIControlState.normal)
        backButton?.setTitle("Back", for: .normal)
        backButton?.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5), for: UIControlState.normal)
        //add function for button
        backButton?.addTarget(self, action: #selector(ReplyViewController.backAction), for: UIControlEvents.touchUpInside)
        //set frame
        backButton?.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
        
        
        
        //set image for button
        replyButton?.setTitle("Send".localized(), for: .normal)
        //        replyButton.setImage(#imageLiteral(resourceName: "send-active"), for: UIControlState.normal)
        replyButton?.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5), for: UIControlState.normal)
        //add function for button
        replyButton?.addTarget(self, action: #selector(ReplyViewController.sendReply), for: UIControlEvents.touchUpInside)
        //set frame
        replyButton?.frame = CGRect(x: 0, y: 0, width: 53, height: 51)
        
        let leftButton = UIBarButtonItem(customView: backButton!)
        let rightButton = UIBarButtonItem(customView: replyButton!)
        
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.rightBarButtonItem = rightButton
        
    }
    
    fileprivate func initTags(field:WSTagsField)
    {
        if field == toTagField
        {
            field.frame = toInputView.bounds
            toInputView.addSubview(field)
        }
            
        else  if field == ccTagField
        {
            field.frame = ccInputView.bounds
            ccInputView.addSubview(field)
        }
            
        else // bcc
        {
            field.frame = bccInputView.bounds
            bccInputView.addSubview(field)
        }
        
        field.cornerRadius = 2.0
        field.spaceBetweenLines = 5
        field.spaceBetweenTags = 5
        field.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        field.placeholder = ""
        field.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        field.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        field.selectedColor = #colorLiteral(red: 0.9641233087, green: 0.5129789114, blue: 0, alpha: 1)
        
        field.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
        field.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) //old padding
        field.fieldTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        field.returnKeyType = .next
        field.textDelegate = self
    }
    
    @objc @IBAction func backAction(){
        
        if reply.text != "" && reply.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.replyDelegate = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            //            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func sendReply() {
        
        
        if toTagField.tags.count == 0 && ccTagField.tags.count == 0 && bccTagField.tags.count == 0 && toTagField.text == "" && ccTagField.text == "" && bccTagField.text == ""
        {
            Helper.showFloatAlert(title: "Message has no senders".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            
        else
        {
            if toTagField.text != ""
            {
                if  !toTagField.text!.isEmpty && toTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || toTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                    
                else if !Helper.isValidEmail(mail_address: toTagField.text!.condensedWhitespace)
                    
                {
                    Helper.showFloatAlert(title: "\(toTagField.text!) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else
                {
                    self.to.append(Bcc(email: toTagField.text!.condensedWhitespace, name: toTagField.text!.condensedWhitespace))
                    toTagField.addTag(toTagField.text ?? "")
                    
                    
                }
            }
            if ccTagField.text != ""
            {
                
                if  !ccTagField.text!.isEmpty && ccTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || ccTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else if !Helper.isValidEmail(mail_address: ccTagField.text?.condensedWhitespace ?? "")
                {
                    Helper.showFloatAlert(title: "\(ccTagField.text!) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else
                {
                    self.cc.append(Bcc(email: ccTagField.text?.condensedWhitespace ?? "", name: ccTagField.text!))
                    ccTagField.addTag(ccTagField.text ?? "")
                    
                }
            }
            
            if bccTagField.text != ""
            {
                if  !bccTagField.text!.isEmpty && bccTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || bccTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else if !Helper.isValidEmail(mail_address: bccTagField.text?.condensedWhitespace ?? "")
                {
                    Helper.showFloatAlert(title: "\(bccTagField.text!.condensedWhitespace) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                    
                }
                else
                {
                    self.bcc.append(Bcc(email: bccTagField.text?.condensedWhitespace ?? "", name: bccTagField.text!))
                    bccTagField.addTag(bccTagField.text ?? "")
                    
                }
            }
            
            let requestModel = ComposeMessageRequestModel()
            requestModel.messageText = reply.text ?? ""
            requestModel.cc = cc
            requestModel.to = to
            requestModel.bcc = bcc
            if msgItem?.options?.messageType == feedType.meeting.rawValue
            {
                feedPresenter = FeedsPresenter(self)
                meetingModel?.customResponse = CustomResponse(body: reply.text ?? "", to: to, cc: cc, bcc: bcc)
                feedPresenter?.setMeetingAction(msgId: msgItem?.messageId ?? "", date: msgItem?.date ?? "", userChannelId: msgItem?.userChannelId ?? "", request: meetingModel ?? MeetingActionRequestModel())
            }
            else
            {
                replyPresenter = ReplyFeedPresenter (self)
                replyPresenter?.sendReply(msgId: (msgItem?.messageId) ?? "", date: msgItem?.date ?? "", userChannelId: msgItem?.userChannelId ?? "", requestModel: requestModel, msgType: replyType!)
            }
            SVProgressHUD.show()
            replyButton?.isEnabled = false
            replyButton?.isUserInteractionEnabled = false
        }
        
    }
    func converBccToStringMails(arr:[Bcc]) ->String
    {
        var mail = ""
        for (_,item) in arr.enumerated()
        {
            mail.append("\(item.email?.condensedWhitespace ?? "") ;")
        }
        
        return mail
    }
    fileprivate func loadData()
    {
        oldSubject.text = msgItem?.title ?? ""
        oldSender.text = converBccToStringMails(arr: msgItem?.options?.to ?? [])
        oldCC.text = converBccToStringMails(arr: msgItem?.options?.cc ?? [])
        
        if let msgTitle = msgItem?.title
        {
            if  (msgTitle.starts(with: "RE")) ||  (msgTitle.starts(with: "FW") || (msgTitle.starts(with: "Re")) || (msgTitle.starts(with: "Fw")))
            {
                subject.text = msgItem?.title ?? ""
            }
            else
            {
                subject.text = "Re : \(String(describing: msgItem?.title ?? ""))"
            }
        }
        else
        {
            
            subject.text  = "Re :"
        }
        
        if (msgItem?.iconUrl?.hasPrefix("http"))! {
            
            channelLogo.sd_setImage(with: URL(string: (msgItem?.iconUrl)! ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            
        } else {
            channelLogo.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(msgItem?.iconUrl)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }
        
        channelName.text = msgItem?.channel?.capitalizingFirstLetter()
        
        
        to.append((msgItem?.options?.fromAddress)!)
        
        if replyType == ReplyType.replyAll{
            //            if ccArr?.count ?? 0 > 0
            //            {
            //                addSenders(view: ccTagField, arr: ccArr ?? [])
            //            }
            if msgItem?.options?.to != nil && (msgItem?.options?.to?.count)! > 0
            {
                if let toList = msgItem?.options?.to
                {
                    for (_,item) in (toList.enumerated())
                        
                    {
                        if !Helper.checkFound(str: item.email?.condensedWhitespace ?? "",array: to)
                        {
                            to.append(item)
                        }
                    }
                }
            }
            
            let ccArr = msgItem?.options?.cc ?? []
            
            for (_,item) in ccArr.enumerated()
                
            {
                if !Helper.checkFound(str: item.email?.condensedWhitespace ?? "",array: cc) && !(Helper.checkFound(str: msgItem?.options?.fromAddress?.name ?? "", array: ccArr))
                {
                    cc.append(item)
                }
            }
            
            
            if to.count >= 1
            {
                var indeces:[Bcc] = []
                for (_,item) in to.enumerated()
                {
                    if msgItem?.options?.accountAddress?.email == item.email && to.count > 1
                    {
                        //                        to.remove(at: i)
                        indeces.append(item)
                    }
                }
                
                for (_,item) in indeces.enumerated()
                {
                    for (j,toItem) in to.enumerated()
                    {
                        if toItem.email == item.email
                        {
                            to.remove(at: j)
                        }
                    }
                }
                indeces = []
                
                for (_,item) in cc.enumerated()
                {
                    if (msgItem?.options?.accountAddress?.email == item.email && to.count >= 1 )
                    {
                        indeces.append(item)
                    }
                }
                
                for (_,item) in indeces.enumerated()
                {
                    for (j,ccItem) in cc.enumerated()
                    {
                        if ccItem.email == item.email
                        {
                            cc.remove(at: j)
                        }
                    }
                }
                
            }
            addSenders(view: ccTagField, arr: cc)
            //            self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            
        }
        addSenders(view: toTagField, arr: to)
        self.tableView.reloadData()
        view.updateConstraints()
        
    }
    
    fileprivate func addSenders(view:WSTagsField , arr:[Bcc])
    {
        for (_,item) in  arr.enumerated()
        {
            view.addTag(item.name ?? "")
        }
        
    }
    
    fileprivate func tagFieldEvents(tagField:WSTagsField) {
        
        tagField.onVerifyTag = {tag, text in
            
            if !Helper.isValidEmail(mail_address: text.condensedWhitespace)
            {
                Helper.showFloatAlert(title: "\(text) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                return false
            }
            return true
        }
        
        tagField.onDidAddTag = { tagField, tag in
            print("onDidAddTag")
            if tagField == self.toTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.to)
                {
                    self.to.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))
                }
                
            }
            else if tagField == self.ccTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.cc)
                {
                    self.cc.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))
                    
                }
            }
            else if tagField == self.bccTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.bcc)
                {
                    self.bcc.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))
                    
                }
            }
        }
        
        tagField.onDidRemoveTag = { tagField, tag in
            print("onDidRemoveTag")
            
            if tagField == self.toTagField
            {
                let index = self.to.index{$0.name == tag.text}
                if index != nil
                {
                    self.to.remove(at: index!)
                }
            }
            else if tagField == self.ccTagField
            {
                let index = self.cc.index{$0.name == tag.text}
                
                if index != nil
                {
                    self.cc.remove(at: index!)
                }
            }
            else if tagField == self.bccTagField
            {
                let index = self.cc.index{$0.name == tag.text}
                
                if index != nil
                {
                    self.cc.remove(at: index!)
                }
            }
        }
        
        
        tagField.onDidChangeText = { _, text in
            print("onDidChangeText")
            
            if text?.trim() != "" && self.isBeginEdit && !(text?.trim().starts(with: ";") ?? false)!
            {
                self.currentTag = tagField
                self.composeNewMessagePresenter?.getSenders(sender: text?.trim() ?? "", channelType: "EMAIL")
            }
            
            
            if self.autoComplete != nil
            {
                for (_,view) in self.view.subviews.enumerated()
                {
                    if  view is AutoComplete
                    {
                        view.removeFromSuperview()
                        self.autoComplete = nil
                    }
                }
            }
        }
        
        
        tagField.onDidChangeHeightTo = { _, height in
            print("HeightTo \(height)")
            if tagField == self.toTagField
            {
                self.senderHeight = height + 10.0
                //                self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                
            }
            else if tagField == self.ccTagField
            {
                self.ccHeight = height + 10.0
                //                self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
                
            }
            else if tagField == self.bccTagField
            {
                self.bccHeight = height + 10.0
                //                self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
                
            }
            
            self.tableView.reloadData()
            
        }
        
        tagField.onDidSelectTagView = { _, tagView in
            print("Select \(tagView)")
        }
        
        tagField.onDidUnselectTagView = { _, tagView in
            print("Unselect \(tagView)")
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 // TO
        {
            return senderHeight
        }
            
        else if indexPath.row == 3 // CC
        {
            return ccHeight
        }
        else if indexPath.row == 4 // bcc
        {
            
            return bccHeight
        }
        else if indexPath.row == 5 // subject
        {
            return Helper.heightForText(text: oldSubject.text  ?? "", Font: UIFont.systemFont(ofSize: 10), Width: 200) + 40
            
        }
        else if indexPath.row == 6 //  new message
        {
            if mySignarture == ""
            {
                return 100.0
            }
            else
            {
                return  Helper.heightForText(text: mySignarture ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) < 60 ? 100 : Helper.heightForText(text: mySignarture ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) + 80
                
            }
        }
        else if indexPath.row == 7 || indexPath.row == 8 || indexPath.row == 9 || indexPath.row == 10
        {
            
            if indexPath.row == 7
            {
                return Helper.heightForText(text: oldSender.text ?? "", Font: UIFont.systemFont(ofSize: 16), Width: 200)
            }
                
            else if indexPath.row == 8
            {
                if msgItem?.options?.cc?.count == 0 || msgItem?.options?.cc == nil
                {
                    return 0.0
                }
                else
                {
                    return  Helper.heightForText(text: oldCC.text ?? "", Font: UIFont.systemFont(ofSize: 16), Width: 200)
                }
            }
            
            return 60.0
        }
        else if indexPath.row == 11
        {
            return 500.0
        }
        
        return 60.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

extension ReplyViewController : UIWebViewDelegate , UITextFieldDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        if (webView.isLoading){
            return
        }
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == toTagField {
            ccInputView.becomeFirstResponder()
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == toTagField
        {
            activeTextField = toTagField
        }
        else if textField == ccTagField
        {
            activeTextField = ccTagField
            
        }
        else // bcc
        {
            activeTextField = bccTagField
            
        }
        isBeginEdit = true
        print("begin editing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.autoComplete != nil
        {
            
            for (_,view) in self.view.subviews.enumerated()
            {
                if  view is AutoComplete
                {
                    view.removeFromSuperview()
                    autoComplete = nil
                }
            }
        }
    }
    
}
extension ReplyViewController : ReplyFeedPresenterView {
    
    func setReplySuccess(response: String?) {
        debugPrint("Reply sent Successfully")
        replyButton?.isEnabled = false
        backButton?.isEnabled = false
        Helper.showFloatAlert(title: response ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.dismiss(animated: true, completion: nil)
            
            //            self.navigationController?.popViewController(animated: true)
            SVProgressHUD.dismiss()
            
        })
        
    }
    
    func setError(error: NSError,url: String) {
        
        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            
            switch type {
                
            case .some(.flow):
                if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
                {
                    relogin = ReloginBaseController()
                    relogin?.controller = self
                    relogin?.registerSocket()
                    channelItem?.firstLoadConfig = FirstLoadConfig(criteria: "DATE", periods: HubMeConstants.periods)
                    relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing:channelItem?.id ?? 0 as! Int), selectedAccount: channelItem ?? UserChannelsItem())
                }
                else
                {
                    self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
                }
                
//              Helper.showFloatAlert(title: "Account is expired".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                break
            case .some(.channel):
                relogin = ReloginBaseController()
                relogin?.controller = self
                relogin?.registerSocket()
                let allChannels = HubMeConstants.allChannels
                
                let channel = allChannels.filter( {$0.groupId ==  channelItem?.channelId ?? 0 as! Int })
                
                channelItem?.firstLoadConfig = channel[0].firstConfig
                
                relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: msgItem?.channel ?? "", channelId: String(describing:channelItem?.id ?? 0 as! Int), selectedAccount: channelItem ?? UserChannelsItem())
                break
            case .none:
                print("None")
                break
                
            }
            
        }
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
        
        replyButton?.isEnabled = true
        replyButton?.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        
    }
    
    func setErrorReponse(msg: String?,url: String) {
        replyButton?.isEnabled = true
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        SVProgressHUD.dismiss()
    }
    
    func goback(_ go: Bool) {
        if go
        {
            //            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
            
        }
        self.dismissDialogViewController()
    }
}
extension ReplyViewController : SignaturePresenterView
{
    
    func setNewSignatureSuccess(response: String?)
    {
        let replaced = response?.replacingOccurrences(of: "\\n", with: "\n")
        
        mySignarture = "\n \n\(replaced ?? "")"
        reply.text = "\n \n\(replaced ?? "")"
        self.tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: UITableViewRowAnimation.none)
    }
}

extension ReplyViewController:ComposeNewMessagePresenterView
{
    func setErrorReponse(msg: String?) {
        replyButton?.isEnabled = true
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        SVProgressHUD.dismiss()
        
    }
    
    func composeMessageSuccess(response: String) {
        Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
            SVProgressHUD.dismiss()
            
        })
        
    }
    
    func getSendersSuccess(response: [SendersModel]?) {
        
        self.senders = response ?? []
        if senders.count > 0
        {
            
            if activeTextField == toTagField
            {
                let pos = 80 + to.count*12
                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.tableView.frame.size.height))
                autoComplete?.results = self.senders
                autoComplete?.wordChanged()
                
            }
            else if activeTextField == ccTagField
            {
                let pos = 80 + to.count*12 + cc.count*12
                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.tableView.frame.size.height))
                autoComplete?.results = self.senders
                autoComplete?.wordChanged()
                
                
            }
            else
            {
                let pos = 100 + to.count*12 + cc.count*12 + bcc.count*12
                
                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.tableView.frame.size.height))
                autoComplete?.results = self.senders
                autoComplete?.wordChanged()
            }
            
            
            if self.autoComplete != nil
            {
                if  !(self.view.subviews.last is AutoComplete)
                {
                    self.view.addSubview(autoComplete!)
                    self.view.setNeedsDisplay()
                }
            }
        }
        else
        {
            
            if self.autoComplete != nil
            {
                for (_,view) in self.view.subviews.enumerated()
                {
                    if  view is AutoComplete
                    {
                        view.removeFromSuperview()
                        autoComplete = nil
                    }
                }
            }
        }
        autoComplete?.tableView.reloadData()
    }
    
}
extension ReplyViewController : FeedsPresenterView
{
    func setMeetingActionResult(response: String?)
    {
        self.dismiss(animated: true, completion: nil)

    }
}
