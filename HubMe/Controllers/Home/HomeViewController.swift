//
//  HomeViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/19/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
//import Crashlytics
import LKPullToLoadMore
import SideMenuSwift
import StompClientLib

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var noFeedsFound: UILabel!
    @IBOutlet weak var notificationBtn: BadgeBarButtonItem!
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var userFlowPresenter: FlowListPresenter?
    fileprivate var feedListPresenter: FeedsPresenter?
    fileprivate var notificationPresenter:NotificationPresenter?
    fileprivate var manageVersion: ManageAppPresenter?

    fileprivate var registeredChannels:[GroupedChannel] = []
    fileprivate var flows: [FlowItem] = []
    fileprivate var nextPageNumber: String  = "0"
    fileprivate var loadMoreControl: LKPullToLoadMore!
    fileprivate var selectedFlow : Int?
    fileprivate var feedList: [MessageItem] = []
    fileprivate var url = NSURL()
    fileprivate var selectedIndex:Int?
    fileprivate var refresh:Bool = false
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(HomeViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.orange
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SideMenuController.preferences.basic.enablePanGesture = true

        dismissKeyboard()
//        registerSocket()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.register(UINib(nibName: "HomeFeedCell", bundle: nil), forCellReuseIdentifier: "home_feed_cell")
        
        tableView.register(UINib(nibName: "HomeMessageCell", bundle: nil), forCellReuseIdentifier: "home_message_cell")
        
        self.tableView.addSubview(self.refreshControl)
        
        manageVersion = ManageAppPresenter(self)
        manageVersion?.getAppVersion()

        NotificationCenter.default.addObserver(self, selector: #selector(self.addNewFlow(notification:)), name: Notification.Name(HubMeConstants.Notifications.addNewFlow), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateFeeds(notification:)), name: Notification.Name(HubMeConstants.Notifications.updateFeeds), object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(pushToController), name: NSNotification.Name(rawValue: HubMeConstants.Notifications.sidemenu), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(addNewChannels), name: NSNotification.Name(rawValue: HubMeConstants.Notifications.addNewChannel), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(getChannelFeeds), name: NSNotification.Name(rawValue: HubMeConstants.Notifications.getChannelFeeds), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshNotifications(notification:)), name: Notification.Name(HubMeConstants.Notifications.refreshNotifications), object: nil)
//        initView()

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        self.refreshControl.endRefreshing()
    }
    override func viewWillAppear(_ animated: Bool) {
        loading.startAnimating()
//        handleRefresh(_:)
        selectedFlow = 0
        refresh = true
        initView()
        updateBadgeNumber()
        notificationPresenter = NotificationPresenter(self)
        notificationPresenter?.getNotifications()
        tableView.reloadData()

        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")
//        Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.selectedFlowIndex)

    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        noFeedsFound.isHidden = true
        initView()
        
    }
    @objc func showMoreChannelsBtnAction(_sender:Any)
    {
        self.tabBarController?.selectedIndex = 0
    }
    @objc func showMoreFlowsBtnAction(_sender:Any)
    {
        self.tabBarController?.selectedIndex = 1
    }
    
    @objc func updateFeeds(notification: Notification) {
        let index = notification.object as! Int
        
        feedList = []
        nextPageNumber = "0"
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        selectedFlow = index
        
        self.loading.startAnimating()
        noFeedsFound.isHidden = true
        feedListPresenter = FeedsPresenter(self)
        if flows.count > 0
        {
            if index < flows.count
            {
                feedListPresenter?.getFeedResponse(type: .flow, channelId: String(describing: flows[index].id as! Int) , itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber)
            }
        }
        tableView.setContentOffset(.zero, animated: true)
        
    }
    
    func initView()
    {
        feedList = []
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        nextPageNumber = "0"
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getUserChannels()
        loadMoreControl = LKPullToLoadMore(imageHeight: 40, viewWidth: tableView.frame.width, tableView: tableView)
        
        loadMoreControl.enable(true)
        loadMoreControl.delegate = self
        loadMoreControl.resetPosition()
        noFeedsFound.isHidden = true
    }
    
    @objc func addNewFlow(notification: Notification)
    {
        //        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //        let tabBar =  appDelegate.tabBarHome
        let tabBar =  self.tabBarController
        if let navInTab:UINavigationController = tabBar?.viewControllers?[1] as? UINavigationController
        {
            navInTab.popToRootViewController(animated: true)
            navInTab.pushViewController(AddNewRuleTableViewController.create(), animated: true)
            tabBar?.selectedIndex = 1
        }
    }
    @objc func refreshNotifications(notification: Notification) {
        
        let accountExpired = notification.object
        if accountExpired != nil
        {
            var notificationNum = Helper.getUserDefault(key: HubMeConstants.userDefault.notificationBadge) as? Int ?? 0
            notificationNum  += 1
            Helper.saveUserDefault(key: HubMeConstants.userDefault.notificationBadge, value: notificationNum)
            updateBadgeNumber()
        }
    }
    
    @IBAction func notificationBtnAction(_ sender: Any) {
        
        notificationPresenter?.openNotification()
        self.navigationController?.pushViewController(NotificationsViewController.create(), animated: true)
        
        
    }
    @IBAction func menuBtnAction(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @objc func addNewChannels(notification:NSNotification){
        
        let tabBar =  self.tabBarController
        if  let navInTab:UINavigationController = tabBar?.viewControllers?[2] as? UINavigationController
        {
            navInTab.popToRootViewController(animated: true)
            let addChannelVC = AddChannelViewController.create()
            addChannelVC.groupUserList =  registeredChannels
            navInTab.pushViewController(addChannelVC, animated: true)
            tabBar?.selectedIndex = 2
        }
    }
    @objc func getChannelFeeds(notification:NSNotification){
        
        let feedsVC = FeedsTableViewController.create()
        feedsVC.type = .channel
        feedsVC.accountsItem = notification.userInfo?["accountsItem"] as! GroupedChannel
        let tabBar =  self.tabBarController
        if let navInTab = tabBar?.viewControllers?[2] as? UINavigationController
        {
            navInTab.popToRootViewController(animated: true)
            navInTab.pushViewController(feedsVC, animated: true)
            tabBar?.selectedIndex = 2
        }
        
    }
  
    func updateBadgeNumber()
    {
        let badgeNumber = Helper.getUserDefault(key: HubMeConstants.userDefault.notificationBadge) as? Int ?? 0
        notificationBtn.badgeNumber = badgeNumber
    }
    @objc func moreBtnTapped(sender:UIButton)
    {
        let cell = tableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? MessageTableViewCell
        for (i,item) in feedList.enumerated()
        {
            if i == sender.tag
            {
                feedList[sender.tag].isExpanded = !feedList[sender.tag].isExpanded
            }
            else
            {
                item.isExpanded = false
            }
        }
//        feedList[sender.tag-2].isExpanded = !feedList[sender.tag-2].isExpanded
        cell?.messageText.numberOfLines = feedList[sender.tag-2].isExpanded ? 0 : 2
        cell?.showMoreBtn.setTitle(feedList[sender.tag-2].isExpanded ? "Read less...".localized() : "Read more...".localized(), for: .normal)
        
        tableView.beginUpdates()
        tableView.reloadData()
//        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        tableView.endUpdates()
        
    }
    
    private func getSelectedAccount(groupedAccount: [GroupedChannel])->(GroupedChannel?,UserChannelsItem?)
    {
        var accounts:GroupedChannel?
        var selectAccount:UserChannelsItem?
        
        for (_,item) in groupedAccount.enumerated()
        {
            if selectedIndex ?? 0 < feedList.count - 1
            {
                if item.groupName?.lowercased() == feedList[selectedIndex ?? 0].channel?.lowercased()
                {
                    accounts = item
                    
                    for (index,account) in (item.accounts?.enumerated())!
                    {
                        if account.id == Int(feedList[selectedIndex ?? 0].userChannelId ?? "")
                        {
                            selectAccount = item.accounts?[index]
                            break
                        }
                    }
                    break
                }
            }
            
        }
        return (accounts,selectAccount)
    }
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "home_channel_cell", for: indexPath) as! HomeChannelTableViewCell
            cell.registeredChannels = registeredChannels
            if registeredChannels.count == 0
            {
                cell.showMoreBtn.isHidden = true
            }
            else
            {
                cell.showMoreBtn.isHidden = false
            }
            cell.showMoreBtn.addTarget(self, action: #selector(HomeViewController.showMoreChannelsBtnAction), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "home_flow_cell", for: indexPath) as! HomeFlowTableViewCell
            if refresh
            {
                cell.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
                cell.collectionView.contentOffset.x = 0

                refresh = false
            }
            if flows.count == 0
            {
                cell.showMoreBtn.isHidden = true
                cell.footerView.isHidden = false
            }
            else
            {
                cell.showMoreBtn.isHidden = false
                cell.footerView.isHidden = true

            }
            cell.flows = flows
            
            cell.showMoreBtn.addTarget(self, action: #selector(HomeViewController.showMoreFlowsBtnAction), for: .touchUpInside)
            
            return cell
        }
        else
        {
            if feedList.count > 0
            {
                if feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.slackChannel.lowercased() && feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased()
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "home_feed_cell", for: indexPath) as! HomeFeedTableViewCell
                    
                    cell.configureCell(feed: feedList[indexPath.row-2])
                    return cell
                }
                else
                {
                    
                    let message_cell = tableView.dequeueReusableCell(withIdentifier: "home_message_cell", for: indexPath) as! MessageTableViewCell
                    message_cell.configureCell(feed: feedList[indexPath.row-2])
                    message_cell.showMoreBtn.tag = indexPath.row-2
                    message_cell.showMoreBtn.addTarget(self, action: #selector(moreBtnTapped), for: .touchUpInside)
                    return message_cell
                }
            }
            return UITableViewCell()
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1
            
        {
            return 150.0
        }
            
        else
        {
            if feedList.count > 0
            {
                if feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.slackChannel.lowercased() && feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased()
                {
                    return 180.0
                }
                else
                {
                    return UITableViewAutomaticDimension
                    
                }
            }
            return 180.0

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2 + feedList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        if feedList.count > 0 && indexPath.row > 1  && feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.slackChannel.lowercased() && feedList[indexPath.row-2].channel?.lowercased() != HubMeConstants.Channels.mattermostChannel.lowercased()
        {
            if feedList[indexPath.row-2].channel != HubMeConstants.Channels.twitterChannel.lowercased()
            {
                if feedList[indexPath.row-2].options?.messageType == feedType.meeting.rawValue
                {
                    let vc =  MeetingDetailsViewController.create()
                    if registeredChannels.count > 0
                    {
                        vc.accountName = getSelectedAccount(groupedAccount: registeredChannels).1?.channelUsername
                    }
                    vc.feed = feedList[indexPath.row-2]
                    vc.type =  .flow
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                else
                {
//                    let detailVC =  FeedDetailsViewController.create()
                    let detailVC =  FeedsDetailsTableViewController.create()
                    detailVC.type = .flow
                    selectedIndex = indexPath.row-2
                    let groupedAccount = getSelectedAccount(groupedAccount: registeredChannels)
                    detailVC.groupedAccount = groupedAccount.0 ?? GroupedChannel()
                    detailVC.channelItem =  groupedAccount.1
                    detailVC.accountName = groupedAccount.1?.channelUsername

//                    vc.channelName = registeredChannels[indexPath.row-2].groupName
                    detailVC.feed = feedList[indexPath.row-2]
                    self.navigationController?.pushViewController(detailVC, animated: true)
                    //                self.present(vc, animated: true, completion: nil)
                }
                
            }
            else
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TwitterDetailsTableViewController") as! TwitterViewController
                vc.feed = feedList[indexPath.row-2]
//                vc.channelItem = feedList[indexPath.row-2]
                if registeredChannels.count > 0
                {
                    let groupedAccount = getSelectedAccount(groupedAccount: registeredChannels)
                    vc.groupedAccount = groupedAccount.0 ?? GroupedChannel()
                    vc.channelItem =  groupedAccount.1 ?? UserChannelsItem()
                }

                vc.feedLogo = ""
                self.navigationController?.pushViewController(vc, animated: true)
//                self.present(vc, animated: true, completion: nil)
                
//                UserChannelsItem(id: Int(feedList[indexPath.row-2].userChannelId), name: feedList[indexPath.row-2].channelUsername, iconUrl: feedList[indexPath.row-2].iconUrl)

            }
            
            feedList[indexPath.row-2].isRead = true
            
        }
        else
        {
            if feedList.count > 0
            {
                for (i,item) in feedList.enumerated()
                {
                    if i == indexPath.row-2
                    {
                        feedList[indexPath.row-2].isExpanded = !feedList[indexPath.row-2].isExpanded
                    }
                    else
                    {
                        item.isExpanded = false
                    }
                }
            }
            tableView.reloadData()
        }
//        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    @IBAction func searchBtnAction(_ sender: Any) {
        let vc = SearchViewController.create()
        vc.modalPresentationStyle = .fullScreen
//        self.navigationController?.present(vc, animated: false, completion: nil)
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
}

extension HomeViewController : FlowListPresenterView
{
    func setError(error: NSError, url: String)
    {
        if url != HubMeConstants.Url.get_app_version_url
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
            if feedList.count == 0 && flows.count > 0
            {
                self.noFeedsFound.isHidden = false
            }
            loading.stopAnimating()
            
        }
        
    }
    
    func setUFlowListResult(response: [FlowItem]?)
    {
        flows = response!
        tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableViewRowAnimation.none)
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.HomeFlow), object: flows, userInfo: ["Channels":registeredChannels.count])
        self.refreshControl.endRefreshing()
        loading.stopAnimating()
        
        
    }
    
}

extension HomeViewController : ChannelsPresenterView
{
    func setUserChannelsResult(response: [UserChannelsItem]?) {
        registeredChannels = Helper.groupChannels(channels: response ?? [])
        userChannelsPresenter?.getRegisteredChannels()
        if registeredChannels.count > 0
        {
            userFlowPresenter = FlowListPresenter(self)
            userFlowPresenter?.getUserFlows()
//            tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableViewRowAnimation.none)
        }
        else
        {
            self.flows = []
            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.HomeFlow), object: [], userInfo: ["Channels":registeredChannels.count])

        }
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.HomeChannels), object: registeredChannels, userInfo: nil)
        self.refreshControl.endRefreshing()
        loading.stopAnimating()
        tableView.reloadData()

        
    }
    
    func getAllChannels(response: [UserChannelsItem]?)
    {
        if (response?.count ?? 0) > 0
        {
            for (i,item) in registeredChannels.enumerated()
            {
                if let foundItem =  response?.filter({$0.name!.lowercased() == item.groupName!.lowercased()})
                {
                    registeredChannels[i].firstConfig = foundItem[0].firstLoadConfig
                }
            }
            
            HubMeConstants.allChannels = registeredChannels
            //            Helper.saveObjectDefault(key: HubMeConstants.userDefault.channelsSyncPeriods, value: registeredChannels)
        }
    }
}
extension HomeViewController : LKPullToLoadMoreDelegate
{
    func loadMore() {
        loading.startAnimating()
        loadMoreControl.enable(false)
        if flows.count > 0
        {
            feedListPresenter?.getFeedResponse(type: .flow, channelId: String(describing: flows[selectedFlow ?? 0].id as! Int), itemsPerPage: HubMeConstants().itemPerPage, pageIndex: nextPageNumber ?? "0")
        }
    }
    
    // MARK: - Scroll View
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        loadMoreControl.scrollViewDidScroll(scrollView)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        loadMoreControl.scrollViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }
}

extension HomeViewController : FeedsPresenterView
{
    func setFeedResult(response: FeedsResponse?) {
        loadMoreControl.enable(true)
        self.refreshControl.endRefreshing()
        self.loading.stopAnimating()
        var newsFeedList: [MessageItem] = []
        
        
        if let nextPage = response?.nextPageIndex
        {
            self.nextPageNumber = nextPage
        }
        else
        {
            self.nextPageNumber = "0"
        }
        if response?.messages != nil {
            for (_,item) in (response?.messages?.enumerated())!
            {
                newsFeedList.append(item)
            }
        }
        
        if response?.error != nil {
            
            if self.feedList.count != 0
            {
                self.noFeedsFound.isHidden = false
            }
        }
        
        if response?.messages != nil && response?.messages?.count == 0 && response?.error == nil && self.feedList.count == 0 {
            self.noFeedsFound.isHidden = false
            return
        }
        
        if  newsFeedList.count != 0 && self.feedList.count == 0 {
            self.feedList = newsFeedList
        } else {
            
            self.feedList = (self.feedListPresenter?.setNewFeeds(oldFeeds: (self.feedList), newFeeds: newsFeedList))!
        }
        if self.feedList.count == 0 {
            self.noFeedsFound.isHidden = false
            
            
        } else {
            self.noFeedsFound.isHidden = true
            
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension HomeViewController : NotificationPresenterView
{
    func getUserNotifications(response: [NotificationResponse]?)
    {
        if response?.count ?? 0 > 0
        {
            let openedNotifications = response?.filter( {$0.open == false })
            Helper.saveUserDefault(key: HubMeConstants.userDefault.notificationBadge, value: openedNotifications?.count ?? 0)
            updateBadgeNumber()
        }
    }
    
    func readNotificationSuccess(response: String)
    {
        if response.lowercased().localized() == HubMeConstants.Messages.success
        {
            debugPrint("Notification opened Successfully")
            
            Helper.saveUserDefault(key: HubMeConstants.userDefault.notificationBadge, value: 0)
            updateBadgeNumber()

            //            self.navigationController?.pushViewController(NotificationsViewController.create(), animated: true)
        }
    }
    func setErrorReponse(msg: String?) {
        loading.stopAnimating()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
    }
    
}
extension HomeViewController:ManageAppPresenterView
{
  
    func setAppVersion(response: AppVersionModel?)
    {
       let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
       let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
       
       if versionNumber+"."+buildNumber !=  response?.latestVersion
              {

                  if Int((versionNumber.components(separatedBy: "."))[0])! > Int((response?.minRequiredVersion?.components(separatedBy: "."))![0])! && (versionNumber+"."+buildNumber) != response?.latestVersion
                  {
                    let alert = UIAlertController(title: "New Version", message: "New version is availble \(response?.latestVersion!) please update your current version", preferredStyle: UIAlertControllerStyle.alert)
                      alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                      self.present(alert, animated: true, completion: nil)
                      
                  }
                  else if  Int((versionNumber.components(separatedBy: "."))[0])! < Int((response?.minRequiredVersion?.components(separatedBy: "."))![0])! // check major minimum version
                  {
                      
                      let alert = UIAlertController(title: "New Version", message: "You must upgrade your app to be able to enjoy using HubMe", preferredStyle: UIAlertControllerStyle.alert)
                      self.present(alert, animated: true, completion: nil)
                      self.view.isUserInteractionEnabled = false
                  }
              }
              
        
    }
}
