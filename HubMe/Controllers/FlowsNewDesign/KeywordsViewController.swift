//
//  KeywordsViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/20/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class KeywordsViewController: UIViewController {
    
    @IBOutlet weak var keywordTF: DesignableUITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectBtn: RoundedButton!
    var keywordsList: [String] = []
    var selectedKeywords: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    @objc public static func create() -> KeywordsViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! KeywordsViewController
    }
    
    @IBAction func addKeywordDidEndEditing(_ sender: Any) {
        keywordsListChanged()
        
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func selectBtnAction(_ sender: Any) {
        if selectedKeywords.count > 0
        {
//            let addNewRuleVC = AddNewRuleTableViewController.create()
            AddNewRuleTableViewController.selectedKeywords = selectedKeywords
//            self.navigationController?.pushViewController(addNewRuleVC, animated: true)
            self.navigationController?.popViewController(animated: true)

            // delegate with channels / senders / keywords
        }
    }
    
    @objc fileprivate func keywordsListChanged(sender: UIButton?=nil) {
        
        if sender == nil {
            
            if  !keywordTF.text!.isEmpty && keywordTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            {
                Helper.showFloatAlert(title: "blank_keyword".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if !Helper.isValidKeywords(text: keywordTF.text!.trim().condensedWhitespace)
            {
                Helper.showFloatAlert(title: NSLocalizedString("no_valid_keyword", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if keywordTF.text != "" && Helper.isValidKeywords(text: keywordTF.text!.trim().condensedWhitespace) {
                keywordsList.append(keywordTF.text!.trim().condensedWhitespace)
                tableView.reloadData()
                keywordTF.text = ""
                KeywordViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 2, newValue: keywordsList.count, checked: nil)
                
            }
            
            
        } else {
            keywordsList.remove(at: (sender?.tag)!)
            tableView.deleteRows(at: [IndexPath(row: (sender?.tag)!, section: 0)], with: UITableViewRowAnimation.automatic)
            tableView.reloadData()
            
            tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
            
            KeywordViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 2, newValue: keywordsList.count, checked: nil)
        }
        selectedKeywords = keywordsList
    }
}
extension KeywordsViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = keywordTF.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
extension KeywordsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "keyword_cell") as! ChannelAccountTableViewCell
        cell.accountName.text = keywordsList[indexPath.row]
//        cell.accountMail.isHidden = true
        cell.removeBtn.tag = indexPath.row
        cell.removeBtn.addTarget(self, action: #selector(keywordsListChanged), for: UIControlEvents.touchUpInside)
        return cell
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return keywordsList.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    
    
}
