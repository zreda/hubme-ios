
//
//  AddNewChanneldViewController.swift
//  HubMe
//
//  Created by Ghonim on 1/16/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class AddNewChanneldViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noChannelsLB: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var selectBtn: RoundedButton!
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var userChannels: [UserChannelsItem] = []
    fileprivate var groupedChannels:[GroupedChannel] = []
    var selectedChannels: [Channel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
//        selectedChannels = []
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getUserChannels()
        updateSelectBtn()
    }
    @objc public static func create() -> AddNewChanneldViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddNewChanneldViewController
    }
    
    func updateSelectBtn()
    {
        if selectedChannels.count == 0
        {
            selectBtn.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        }
        else
        {
            selectBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
        }
    }

    @IBAction func selectBtnAction(_ sender: Any) {
        
//        let addNewRuleVC = AddNewRuleTableViewController.create()
        AddNewRuleTableViewController.selectedChannels = selectedChannels
//        self.navigationController?.pushViewController(addNewRuleVC, animated: true)
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddNewChanneldViewController  : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "user_cell")  as! ChannelAccountTableViewCell
        switch groupedChannels[indexPath.section].isSelectAll
        {
        case .selectAll:
            groupedChannels[indexPath.section].accounts?[indexPath.row].isChecked = true
            break
        case .deSelectAll:
            groupedChannels[indexPath.section].accounts?[indexPath.row].isChecked = false
            break
        default:
            print("None")
        }
        let channel = groupedChannels[indexPath.section].accounts?[indexPath.row] ?? UserChannelsItem()

//        cell.accountImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios/" +
//        channel.icon!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        cell.accountMail.text = channel.channelUsername
        cell.accountName.text = channel.accountName
        cell.checkBtn.isChecked = groupedChannels[indexPath.section].accounts?[indexPath.row].isChecked ?? false
        cell.checkBtn.index = indexPath
        cell.checkBtn.addTarget(self, action: #selector(checkBtnTapped), for: UIControlEvents.touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "channel_cell") as! ChannelHeaderTableViewCell
    
        header.channelName.text = groupedChannels[section].groupName
        header.channelImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios/" +
            (groupedChannels[section].accounts?[0].icon ?? ""))), placeholderImage: #imageLiteral(resourceName: "loading_image"))

        
        switch (groupedChannels[section].isSelectAll)
        {
            
        case .selectAll:
            header.checkBtn.isChecked = true
            break
        case .deSelectAll:
            header.checkBtn.isChecked = false
            break
        default:
            header.checkBtn.isChecked = false
            print("None")
        }
        header.checkBtn.tag = section
        header.delegate = self

        return header
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ChannelAccountTableViewCell
        
        let id = groupedChannels[indexPath.section].accounts![indexPath.row].id!
        
        let name = groupedChannels[indexPath.section].accounts![indexPath.row].channelUsername!

        if (cell.checkBtn != nil) {
            groupedChannels[indexPath.section].accounts?[indexPath.row].isChecked = !cell.checkBtn.isChecked
            cell.checkBtn.isChecked = !cell.checkBtn.isChecked
        }
        
        if checkSelectAllStatus(accountItem: groupedChannels[indexPath.section]).0
        {
            groupedChannels[indexPath.section].isSelectAll = .selectAll

            setCheckStatus(isSelectAll: .selectAll, accounts:  groupedChannels[indexPath.section].accounts!)
        }
        else
        {
            groupedChannels[indexPath.section].isSelectAll = .multiple
        }
        
        
        if ( cell.checkBtn.isChecked) {
            
            if !isChannelFound(channelId: id) {
                selectedChannels.append(Channel(id: id, username: name))
            }
        } else {
            selectedChannels.remove(at: getIndexOfChannel(id: id)!)
            
        }
        
        updateSelectBtn()
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedChannels[section].accounts?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return groupedChannels.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
}

extension AddNewChanneldViewController : SelectAllChannels
{
    func selectAllChannels(selectAll: SelectStatus , section :Int) {
        if selectAll == .selectAll
        {
            for (_,item) in (groupedChannels[section].accounts?.enumerated())!
            {
                if !isChannelFound(channelId: item.id ?? 0) {
                    selectedChannels.append(Channel(id: item.id, username: item.channelUsername))
                }
            }
        }
        else if selectAll == .deSelectAll
        {
             selectedChannels = []
        }
        updateSelectBtn()
        groupedChannels[section].isSelectAll = selectAll
        setCheckStatus(isSelectAll: selectAll, accounts:  groupedChannels[section].accounts!)
        tableView.reloadData()
    }
}

extension AddNewChanneldViewController:  ChannelsPresenterView {

    func setCheckStatus(isSelectAll:SelectStatus ,  accounts:[UserChannelsItem])
    {
        switch isSelectAll
        {
        case .selectAll:
            accounts.forEach { $0.isChecked = true }
            break
        case .deSelectAll:
            accounts.forEach { $0.isChecked = false }
            break
        default:
            print("multiple")
        }
    }
    
    func checkSelectAllStatus(accountItem:GroupedChannel) -> (Bool,Int)
     {
         var selectStatus = 0
         
         for (_,item) in (accountItem.accounts?.enumerated())!
         {
             if item.isChecked ?? false
             {
                 selectStatus += 1
             }
      
         }
         
         updateSelectBtn()
         return (selectStatus == accountItem.accounts?.count ? true : false,selectStatus)
     }
     

    
    @objc func checkBtnTapped(_ sender: CheckBox) {

        let id = groupedChannels[sender.index.section].accounts![sender.index.row].id!
        
        let name = groupedChannels[sender.index.section].accounts![sender.index.row].channelUsername!

        let cell = tableView.cellForRow(at: sender.index) as! ChannelAccountTableViewCell

        if (sender.isChecked) {

            if !isChannelFound(channelId: id) {
                selectedChannels.append(Channel(id: id, username: name))
            }
        } else {
            selectedChannels.remove(at: getIndexOfChannel(id: id)!)

        }
        
        if (cell.checkBtn != nil) {
            groupedChannels[sender.index.section].accounts?[sender.index.row].isChecked = !(groupedChannels[sender.index.section].accounts?[sender.index.row].isChecked ?? false)
        }
        
        if checkSelectAllStatus(accountItem: groupedChannels[sender.index.section]).0
        {
            groupedChannels[sender.index.section].isSelectAll = .selectAll
            setCheckStatus(isSelectAll: .selectAll, accounts:  groupedChannels[sender.index.section].accounts!)
            
        }
        else
        {
            groupedChannels[sender.index.section].isSelectAll = .multiple
        }
        
//        tableView.reloadRows(at: [sender.index], with: .none)
//        tableView.reloadSections([sender.index.section], with: .none)
        updateSelectBtn()
        tableView.reloadData()
        tableView.deselectRow(at: sender.index, animated: true)


    }

    func isChannelFound(channelId: Int) -> Bool {
        for selected in (selectedChannels) {
            if channelId == selected.id {
                return true
            }
        }
        return false

    }

    func isChannelNameFound(channelId: Int) -> Bool {
        for selected in selectedChannels {
            if channelId == selected.id {
                return selected.name != ""
            }
        }
        return false

    }

    func getIndexOfChannel(id: Int) -> Int? {
        for (i, item) in  (selectedChannels.enumerated()) {
            if item.id == id {
                return i
            }
        }
        return nil
    }

    func setUserChannelsResult(response: [UserChannelsItem]?) {

        if response?.count == 0 {
            noChannelsLB.isHidden = false
        } else {
            noChannelsLB.isHidden = true
        }

        userChannels = response!
//        selectedChannels = []
        
//        if (AddNewRuleViewController.editRule != nil) && ((AddNewRuleViewController.editRule?.channels?.count)! > 0) {
//            selectedChannels = (AddNewRuleViewController.editRule?.channels)!
//
//        }

        if selectedChannels.count > 0
        {
            for (_,selectedChannel) in selectedChannels.enumerated()
            {
                for (i,userChannel) in userChannels.enumerated()
                {
                    if userChannel.id == selectedChannel.id
                    {
                        userChannels[i].isChecked = true
                        break
                    }
                }
            }
        }
        
        groupedChannels = Helper.groupChannels(channels: userChannels)
     
        tableView.reloadData()
        loading.stopAnimating()

    }

    func setError(error: NSError,url:String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        loading.stopAnimating()
    }

}
