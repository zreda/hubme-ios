//
//  AddNewRuleTableViewController.swift
//  HubMe
//
//  Created by Ghonim on 1/9/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class AddNewRuleTableViewController: UIViewController {
    
    static var selectedChannels: [Channel] = []
    static var selectedKeywords: [String] = []
    static var selectedSenders: [SendersModel] = []
    @IBOutlet weak var addRuleBtn: RoundedButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if AddNewRuleTableViewController.selectedChannels.count == 0
        {
            addRuleBtn.isEnabled = false
            addRuleBtn.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        }
        else
        {
            addRuleBtn.isEnabled = true
            addRuleBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)


        }
        self.tableView.reloadData()
    }
    @objc public static func create() -> AddNewRuleTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddNewRuleTableViewController
    }
    
    
    @IBAction func addRuleBtnAction(_ sender: Any) {
        
        self.navigationController?.pushViewController(RenameFlowViewController.create(), animated: true)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddNewRuleTableViewController : UITableViewDelegate
{
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return 100
        }
        return UITableViewAutomaticDimension
    }
}
extension AddNewRuleTableViewController : UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "newrule_header_cell") as! NewRuleHeaderTableViewCell
            
            if AddNewRuleTableViewController.selectedChannels.count != 0
            {
                cell.ruleNumberTxt.text = ""
                cell.ruleDescTxt.text = "Configure  your flow feeds based on the selected conditions,You have to add atleast one channel to create a rule".localized()
            }
                
            else
            {
                cell.ruleNumberTxt.text = "Rule 1"
                cell.ruleDescTxt.text = "Configure  your flow feeds based on the selected conditions".localized()

                
            }
            cell.layoutIfNeeded()
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "main_rule_cell") as! MainRuleTableViewCell
            cell.view = self

            
            if indexPath.row == 1 // add channel cell
            {
                cell.ruleImg = #imageLiteral(resourceName: "tab_channels_active")
                cell.ruleName = "Add channel".localized()
                cell.isRuleOptional = HubMeConstants.Optionality.required.rawValue.localized()
                cell.selectedChannelItems = AddNewRuleTableViewController.selectedChannels
                cell.ruleType = .channels
//                cell.tableView.reloadData()

            }
            else if indexPath.row == 2 // add sender
            {
                if AddNewRuleTableViewController.selectedChannels.count > 0
                {
                    cell.ruleImg = #imageLiteral(resourceName: "user-active")
                    cell.isEnabled = true
                }
                else
                {
                    cell.ruleImg = #imageLiteral(resourceName: "user-inactive")
                    cell.isEnabled = false


                }
                cell.ruleType = .senders
                cell.ruleName = "Select sender".localized()
                cell.isRuleOptional =  HubMeConstants.Optionality.optional.rawValue.localized()
                cell.selectedSenderItems = AddNewRuleTableViewController.selectedSenders
//                cell.tableView.reloadData()

            }
            else // add keyword
            {
                if AddNewRuleTableViewController.selectedChannels.count > 0
                {
                    cell.ruleImg = #imageLiteral(resourceName: "keyword-active")
                    cell.isEnabled = true

                }
                else
                {
                    cell.ruleImg = #imageLiteral(resourceName: "keyword-Icon")
                    cell.isEnabled = false
                }
                cell.ruleType = .keywords
                cell.ruleName = "Write a keyword".localized()
                cell.isRuleOptional = HubMeConstants.Optionality.optional.rawValue.localized()
                cell.selectedKeywordItems = AddNewRuleTableViewController.selectedKeywords
//                cell.tableView.reloadData()

            }
            cell.tableView.reloadData()

            cell.layoutIfNeeded()
            return cell
            
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
}
