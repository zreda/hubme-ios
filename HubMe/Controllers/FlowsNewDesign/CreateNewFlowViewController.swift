//
//  CreateNewFlowViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class CreateNewFlowViewController: UIViewController {

    @IBOutlet weak var flowNameTF: DesignableUITextField!
    @IBOutlet weak var tableView: UITableView!
    var rules:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @objc public static func create() -> CreateNewFlowViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! CreateNewFlowViewController
    }

    @IBAction func createFlowBtnAction(_ sender: Any) {
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
    }
    
}

extension CreateNewFlowViewController : UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 9
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add_new_rule_cell") as! NewRuleTableViewCell
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rule_cell") as! CreateRuleTableViewCell

           return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return rules.count+1
        return 10
    }
}
