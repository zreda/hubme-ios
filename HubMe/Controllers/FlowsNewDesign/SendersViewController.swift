//
//  SendersViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/20/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class SendersViewController: UIViewController {
    
    @IBOutlet weak var senderTF: DesignableUITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectBtn: RoundedButton!
    fileprivate var senders:[SendersModel] = []
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var flowPresenter: AddFlowPresenter?
    let dropDownTop = VPAutoComplete()
    var sendersList: [SendersModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        
    }
    
    @objc public static func create() -> SendersViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SendersViewController
    }
    
    func initView() {
        
//        selectedSenders = []
//        senders = []
        senderTF.delegate =  self
        senderTF.resignFirstResponder()
//        if AddNewRuleViewController.editRule?.senders?.count != 0 &&  AddNewRuleViewController.editRule != nil {
//            sendersList = (AddNewRuleViewController.editRule?.senders!)!
//            selectedSenders = (AddNewRuleViewController.editRule?.senders!)!
//            tableView.reloadData()
//        } else {
//            selectedSenders = []
//        }
//
        self.addDropDown()
        updateSelectBtn()
        tableView.reloadData()
    }
    
    @objc func dismissSenders() {
        dropDownTop.isShowView(is_show: false)
        
    }
    
    func addDropDown(){
        
        // For Top textField
        //        getSendersWithChannels(senders: senders).1
        dropDownTop.dataSource = getSendersWithChannels(senders: senders).0
        dropDownTop.imgDataSource = getSendersWithChannels(senders: senders).1
        dropDownTop.onTextField = senderTF
        dropDownTop.tableView?.reloadData()
        dropDownTop.onView = self.view
        dropDownTop.show { (str, index) in
            print("string : \(str) and Index : \(index)")
            self.sendersList.append(SendersModel(channelIconUrl: self.senders[index].channelIconUrl ?? "", name: self.senders[index].name ?? "",email:self.senders[index].email ?? "" ))
            
            self.senderTF.text = ""
            SenderViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 1, newValue: (self.sendersList.count), checked: nil)
            selectedSenders = (self.sendersList)
            
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        tableView.reloadData()
    }
    
    func updateSelectBtn()
    {
        if selectedSenders.count == 0
        {
            selectBtn.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        }
        else
        {
            selectBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
        }
    }
    
    @IBAction func addSenderDidChanged(_ sender: Any) {
        sendersListChanged()

    }
    @IBAction func senderEditChanged(_ sender: Any) {
        
         if senderTF.text?.removingWhitespaces() != ""
        {
            if senderTF.text != "" && Helper.isValidSenders(text: senderTF.text!) {
                
                flowPresenter = AddFlowPresenter(self)
                flowPresenter?.searchSender(query: senderTF.text!.removingWhitespaces())
                dropDownTop.isShowView(is_show: true)
                
            } else if !Helper.isValidSenders(text: senderTF.text!) {
                Helper.showFloatAlert(title: NSLocalizedString("no_valid_senders", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            
        }
        else
        {
            dismissSenders()
        }
    }
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func selectBtnAction(_ sender: Any) {
        
        if selectedSenders.count > 0
        {
//            let addNewRuleVC = AddNewRuleTableViewController.create()
            AddNewRuleTableViewController.selectedSenders = selectedSenders
//            self.navigationController?.pushViewController(addNewRuleVC, animated: true)
            self.navigationController?.popViewController(animated: true)
            // delegate with channels / senders / keywords
        }
    }
    
    @objc fileprivate func sendersListChanged(sender: UIButton?=nil) {

           if sender == nil {
               
               
               if  !senderTF.text!.isEmpty && senderTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
               {
                   Helper.showFloatAlert(title: "blank_sender".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
               }
               else if !Helper.isValidSenders(text: senderTF.text!) {
                   Helper.showFloatAlert(title: NSLocalizedString("no_valid_senders", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                   
               }
               else if senderTF.text != "" && Helper.isValidSenders(text: senderTF.text!) {
                   
                   sendersList.append(SendersModel(channelIconUrl: "", name: senderTF.text!))
                   tableView.reloadData()
                   SenderViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 1, newValue: sendersList.count, checked: nil)
                   senderTF.text = ""
                   dismissSenders()
                   
               }
       
           } else {
               sendersList.remove(at: (sender?.tag)!)
               tableView.deleteRows(at: [IndexPath(row: (sender?.tag)!, section: 0)], with: UITableViewRowAnimation.automatic)
               tableView.reloadData()

               tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)

           }
           selectedSenders = sendersList
            updateSelectBtn()
       }
}

extension SendersViewController : UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                      replacementString string: String) -> Bool {
           let maxLength = 30
           let currentString: NSString = senderTF.text! as NSString
           let newString: NSString =
               currentString.replacingCharacters(in: range, with: string) as NSString
           return newString.length <= maxLength
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
}

extension SendersViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sender_cell") as! ChannelAccountTableViewCell
        cell.accountName.text = sendersList[indexPath.row].name
        cell.accountMail.text = sendersList[indexPath.row].email

             if sendersList[indexPath.row].channelIconUrl != ""
             {
                 cell.accountImg?.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(sendersList[indexPath.row].channelIconUrl ?? "")!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
             }
//             else
//             {
//                 cell.accountImg.isHidden = true
//             }
             cell.removeBtn.tag = indexPath.row
             cell.removeBtn.addTarget(self, action: #selector(sendersListChanged), for: UIControlEvents.touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sendersList.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
extension SendersViewController : AddFlowPresenterView
{
      func getSearchResults(response: SearchModelResponse) {
            
            if let senders = response.response
            {
                
                self.senders = senders
                dropDownTop.dataSource = getSendersWithChannels(senders: senders).0
                dropDownTop.imgDataSource = getSendersWithChannels(senders: senders).1
                dropDownTop.tableView?.reloadData()
                dropDownTop.onTextField = senderTF
                dropDownTop.onView = self.view
    //            if self.senders.count == 0
    //            {
    //                Helper.showFloatAlert(title: "no_senders_found".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
    //            }
               
            }
            
        }
        
        func setError(error: NSError,url: String) {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)

        }

        
        func getSendersWithChannels(senders:[SendersModel])->([String],[String])
        {
            var senderNames:[String] = []
            var senderImgs:[String] = []
            for (_,item) in senders.enumerated()
            {
                senderNames.append(item.name ?? "")
                senderImgs.append(item.channelIconUrl ?? "")

            }
            return (senderNames,senderImgs)
        }
        
}
