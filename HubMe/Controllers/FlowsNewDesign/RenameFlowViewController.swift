//
//  RenameFlowViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class RenameFlowViewController: UIViewController {
    @IBOutlet weak var flowNameTF: DesignableUITextField!
    
    @IBOutlet weak var addBtn: RoundedButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        addBtn.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        addBtn.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    @objc public static func create() -> RenameFlowViewController {
           
           return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! RenameFlowViewController
       }

    @IBAction func flowNameChanged(_ sender: Any) {
        
        if flowNameTF.text != ""
        {
            addBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
            addBtn.isEnabled = true
        }
        else
        {
            addBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
            addBtn.backgroundColor = #colorLiteral(red: 0.6980392157, green: 0.6980392157, blue: 0.6980392157, alpha: 1)
        }
    }
   
    @IBAction func addBtnAction(_ sender: Any) {
        
        self.navigationController?.pushViewController(CreateNewFlowViewController.create(), animated: true)
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
