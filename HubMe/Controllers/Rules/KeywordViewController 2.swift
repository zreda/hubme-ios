//
//  KeywordViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/1/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit

var selectedKeywords: [String] = []

class KeywordViewController: UIViewController {

    @IBOutlet weak var addKeywordTF: DesignableUITextField!

    @IBOutlet weak var keywordsTB: UITableView!
    var keywordsList: [String] = []
    static var updateSegmentValueDelagte: UpdateSegmentCount?

    override func viewDidLoad() {
//        self.hideKeyboard()
        initView()

    }
    override func viewWillAppear(_ animated: Bool) {
        KeywordViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 2, newValue: keywordsList.count, checked: nil)

    }
    func initView() {
//        keywordsTB.estimatedRowHeight = 50.0
//        keywordsTB.rowHeight = UITableViewAutomaticDimension
        selectedKeywords = []
        addKeywordTF.delegate = self
        if AddNewRuleViewController.editRule?.keywords?.count != 0 && AddNewRuleViewController.editRule?.keywords != nil {
            keywordsList = (AddNewRuleViewController.editRule?.keywords!)!
            selectedKeywords = (AddNewRuleViewController.editRule?.keywords!)!
            keywordsTB.reloadData()
        } else {
            selectedKeywords = []
        }
        

    }
    override func viewDidLayoutSubviews() {
//        keywordsTB.frame = CGRect(x: keywordsTB.frame.origin.x, y: keywordsTB.frame.origin.y, width: keywordsTB.frame.size.width, height: keywordsTB.contentSize.height)
        keywordsTB.reloadData()
    }

    @IBAction func addKeywordDidChanged(_ sender: Any) {

        keywordsListChanged()
    }

    @objc fileprivate func keywordsListChanged(sender: UIButton?=nil) {

        if sender == nil {
            
             if  !addKeywordTF.text!.isEmpty && addKeywordTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            {
                Helper.showFloatAlert(title: "blank_keyword".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if !Helper.isValidKeywords(text: addKeywordTF.text!.trim().condensedWhitespace)
            {
                Helper.showFloatAlert(title: NSLocalizedString("no_valid_keyword", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if addKeywordTF.text != "" && Helper.isValidKeywords(text: addKeywordTF.text!.trim().condensedWhitespace) {
                keywordsList.append(addKeywordTF.text!.trim().condensedWhitespace)
                keywordsTB.reloadData()
                addKeywordTF.text = ""
                KeywordViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 2, newValue: keywordsList.count, checked: nil)
                
            }
            

        } else {
            keywordsList.remove(at: (sender?.tag)!)
            keywordsTB.deleteRows(at: [IndexPath(row: (sender?.tag)!, section: 0)], with: UITableViewRowAnimation.automatic)
            keywordsTB.reloadData()

            keywordsTB.frame = CGRect(x: keywordsTB.frame.origin.x, y: keywordsTB.frame.origin.y, width: keywordsTB.frame.size.width, height: keywordsTB.contentSize.height)

            KeywordViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 2, newValue: keywordsList.count, checked: nil)
        }
        selectedKeywords = keywordsList
    }

}
extension KeywordViewController: UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = keywordsTB.dequeueReusableCell(withIdentifier: "filter_cell") as! FlowFilterItemTableViewCell
        cell.filterItemName.text = keywordsList[indexPath.row]
        cell.deleteFilterBtn.tag = indexPath.row
        cell.deleteFilterBtn.addTarget(self, action: #selector(keywordsListChanged), for: UIControlEvents.touchUpInside)
        return cell

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return keywordsList.count
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = addKeywordTF.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }

}
