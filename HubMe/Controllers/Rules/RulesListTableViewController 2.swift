//
//  RulesListTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

var selectedChannels: [Channel] = []

class RulesListTableViewController: UIViewController {

    @IBOutlet weak var channelsTB: UITableView!
    @IBOutlet weak var noChannelsLB: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var userChannels: [UserChannelsItem] = []
    static var updateSegmentValueDelagte: UpdateSegmentCount?
    
    fileprivate var groupedChannels:[GroupedChannel] = []

    override func viewDidLoad() {

        selectedChannels = []
        AddNewRuleViewController.allChannels = []
         initView()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.hideKeyboard()
    }

    override func viewWillAppear(_ animated: Bool) {
   
        if hasDefualtChannels() {
            AddNewRuleViewController.editRule?.channels  = []
            selectedChannels = []
            
        } else {
            RulesListTableViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 0, newValue: (AddNewRuleViewController.editRule?.channels?.count)!, checked: nil)
        }
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getUserChannels()

    }

    override func viewDidLayoutSubviews() {
    }

    func initView() {
        channelsTB?.tableFooterView = UIView(frame: CGRect.zero)
        noChannelsLB.isHidden = true
        channelsTB.estimatedRowHeight = 60.0
        channelsTB.rowHeight = UITableViewAutomaticDimension
       
    }
    
    func hasDefualtChannels() -> Bool {

        if AddNewRuleViewController.editRule?.channels?.count != 0 && AddNewRuleViewController.editRule?.channels != nil {
            if AddNewRuleViewController.editRule?.channels![0].name == "" {
                return true
            }
        }
        return false
    }

}
extension RulesListTableViewController: UITableViewDelegate, UITableViewDataSource, ChannelsPresenterView {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = channelsTB.dequeueReusableCell(withIdentifier: "rule_cell", for: indexPath) as! RuleTableViewCell
        
        cell.configureCell(channel: groupedChannels[indexPath.section].accounts?[indexPath.row] ?? UserChannelsItem())

        // un grouped channels
//        cell.configureCell(channel: userChannels[indexPath.row])
        
        // default select all channels
//        if isChannelNameFound(channelId: self.userChannels[indexPath.row].id!) {
//            cell.checkBtn.isChecked = isChannelFound(channelId: self.userChannels[indexPath.row].id!)
//        }
        
//        if AddNewRuleViewController.editRule?.channels?.count != 0 && AddNewRuleViewController.editRule?.channels != nil {
//            if isChannelNameFound(channelId: self.userChannels[indexPath.row].id!) {
//                cell.checkBtn.isChecked = isChannelFound(channelId: self.userChannels[indexPath.row].id!)
//            }
//        }
        
        if AddNewRuleViewController.editRule?.channels?.count != 0 && AddNewRuleViewController.editRule?.channels != nil {
            if isChannelNameFound(channelId: groupedChannels[indexPath.section].accounts![indexPath.row].id!) {
                cell.checkBtn.isChecked = isChannelFound(channelId: groupedChannels[indexPath.section].accounts![indexPath.row].id!)
            }
            else
            {
                cell.checkBtn.isChecked = false
            }
        }
        else
        {
            cell.checkBtn.isChecked = false
        }
//      cell.checkBtn.tag = indexPath.row
        cell.checkBtn.index = indexPath
        cell.checkBtn.addTarget(self, action: #selector(checkBtnTapped), for: UIControlEvents.touchUpInside)
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedChannels[section].accounts?.count ?? 0
//        return userChannels.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return groupedChannels.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return groupedChannels[section].groupName
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return  UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        let cell = channelsTB.cellForRow(at: indexPath) as! RuleTableViewCell

        if (cell.checkBtn != nil) {
            cell.checkBtn.isChecked =  !cell.checkBtn.isChecked

        }
        checkBtnTapped(cell.checkBtn)

    }

    @objc func checkBtnTapped(_ sender: CheckBox) {

        let id = groupedChannels[sender.index.section].accounts![sender.index.row].id!
        
        let name = groupedChannels[sender.index.section].accounts![sender.index.row].channelUsername!

        if (sender.isChecked) {
        
            if !isChannelFound(channelId: id) {
                selectedChannels.append(Channel(id: id, username: name))
            }
         RulesListTableViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 0, newValue: 0, checked: true)
        } else {
            selectedChannels.remove(at: getIndexOfChannel(id: id)!)
        RulesListTableViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 0, newValue: 0, checked: false)

        }
        AddNewRuleViewController.editRule?.channels = selectedChannels

    }

    func isChannelFound(channelId: Int) -> Bool {
        for selected in (selectedChannels) {
            if channelId == selected.id {
                return true
            }
        }
        return false

    }

    func isChannelNameFound(channelId: Int) -> Bool {
        for selected in selectedChannels {
            if channelId == selected.id {
                return selected.name != ""
            }
        }
        return false

    }

    func getIndexOfChannel(id: Int) -> Int? {
        for (i, item) in  (selectedChannels.enumerated()) {
            if item.id == id {
                return i
            }
        }
        return nil
    }

    func setUserChannelsResult(response: [UserChannelsItem]?) {

        if response?.count == 0 {
            noChannelsLB.isHidden = false
        } else {
            noChannelsLB.isHidden = true
        }

        userChannels = response!
        selectedChannels = []
        
        if (AddNewRuleViewController.editRule != nil) && ((AddNewRuleViewController.editRule?.channels?.count)! > 0) {
            selectedChannels = (AddNewRuleViewController.editRule?.channels)!
            
        }
        
        // default select all channels
//        else {
//            
//            for (_,item) in userChannels.enumerated() {
//                AddNewRuleViewController.allChannels.append(Channel(id: item.id!, name: item.name))
//                selectedChannels.append(Channel(id: item.id!, name: item.name!))
//
//            }
//            if hasDefualtChannels()
//            {
//                RulesListTableViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 0, newValue: (AddNewRuleViewController.editRule?.channels?.count)!, checked: nil)
//            }
//            else
//            {
//                RulesListTableViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 0, newValue: selectedChannels.count, checked: nil)
//
//            }
//
//        }

        groupedChannels = Helper.groupChannels(channels: userChannels)
        channelsTB.reloadData()
        loading.stopAnimating()

    }

    func setError(error: NSError,url:String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        loading.stopAnimating()
    }

}
