//
//  AddNewRuleViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/27/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

@objc protocol FlowRule: NSObjectProtocol {
    @objc optional func setRule(rule: Rule)
    @objc optional func updateRule(channels: [Channel]?, keywords: [String]?, senders: [String]?)
}

protocol UpdateSegmentCount: NSObjectProtocol {
    func setUpdateValue(segmentNumber: Int, newValue: Int, checked: Bool?)
}

class AddNewRuleViewController: UIViewController {

    @IBOutlet weak var channelsContainer: UIView!
    @IBOutlet weak var sendersContainer: UIView!
    @IBOutlet weak var keywordsContainer: UIView!
    @IBOutlet weak var filterationSegment: UISegmentedControl!
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    static var flowRule: Rule?
    static var selectedRule: Int?
     var screenTitle: String?

    static var addFlowDelegate: FlowRule?
    static var editRule: Rule?
    static var allChannels: [Channel] = []

    override func viewWillAppear(_ animated: Bool) {
        if AddNewRuleViewController.editRule == nil {
            AddNewRuleViewController.flowRule = Rule()
            AddNewRuleViewController.editRule = Rule()

        }
        self.navigationItem.title = screenTitle

    }


    override func viewDidLoad() {
        channelsContainer.isHidden = false
        sendersContainer.isHidden = true
        keywordsContainer.isHidden = true
        
        
        RulesListTableViewController.updateSegmentValueDelagte = self
        SenderViewController.updateSegmentValueDelagte = self
        KeywordViewController.updateSegmentValueDelagte = self
        


    }
    @IBAction func helpBtnAction(_ sender: Any) {
        let vc = TermsandAgreementViewController.create()
        vc.file = "senderWork"
        vc.headerTitle = "Help with the flows".localized()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func filterationSegmentAction(_ sender: Any) {

        switch filterationSegment.selectedSegmentIndex {

        case 0:
            channelsContainer.isHidden = false
            sendersContainer.isHidden = true
            keywordsContainer.isHidden = true

        case 1:
            channelsContainer.isHidden = true
            sendersContainer.isHidden = false
            keywordsContainer.isHidden = true

        case 2:
            channelsContainer.isHidden = true
            sendersContainer.isHidden = true
            keywordsContainer.isHidden = false

        default:
            break
        }
    }

    @IBAction func addFlowAction(_ sender: Any) {

        AddNewRuleViewController.editRule?.channels = selectedChannels
        AddNewRuleViewController.editRule?.keywords = selectedKeywords
        AddNewRuleViewController.editRule?.senders = selectedSenders

        if (AddNewRuleViewController.editRule?.keywords?.count != 0 || AddNewRuleViewController.editRule?.senders?.count != 0 || AddNewRuleViewController.editRule?.channels?.count != 0) {
            
            if AddNewRuleViewController.editRule?.channels?.count == 0
            {
                Helper.showFloatAlert(title: NSLocalizedString("no_channel_selected", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)

            }
            else
            {
                AddNewRuleViewController.flowRule = AddNewRuleViewController.editRule
                AddNewRuleViewController.addFlowDelegate?.setRule!(rule: AddNewRuleViewController.flowRule!)
                self.navigationController?.popViewController(animated: true)

            }
        } else {
            Helper.showFloatAlert(title: NSLocalizedString("no_rules_found", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }

    }

}
extension AddNewRuleViewController: UpdateSegmentCount, FlowRule {

    func setUpdateValue(segmentNumber: Int, newValue: Int, checked: Bool? = nil) {
//        let currentValue = Int((filterationSegment.titleForSegment(at: segmentNumber)?.slice(from: "(", to: ")"))!)
//        var newSegmentValue: Int = newValue
//        if checked != nil {
//            if checked == true {
//                newSegmentValue = currentValue! + 1
//            } else {
//                newSegmentValue = currentValue! - 1  < 0 ? 0 :  currentValue! - 1 
//            }
//        }
//
//        let segTitle  = Helper.getStringUntill(myString: filterationSegment.titleForSegment(at: segmentNumber)!, character: "(")
//
//        filterationSegment.setTitle("\(segTitle)(\(newSegmentValue))", forSegmentAt: segmentNumber)

    }

    func updateRule(channels: [Channel]?, keywords: [String]?, senders: [String]?) {

        if channels?.count != 0 || keywords?.count != 0 || senders?.count != 0 {
            doneBtn.isEnabled = true
        } else {
           doneBtn.isEnabled = false
        }
    }

}
