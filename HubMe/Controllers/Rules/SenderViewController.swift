//
//  SenderViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/1/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

var selectedSenders: [SendersModel] = []

class SenderViewController: UIViewController  {

    @IBOutlet weak var sendersTB: UITableView!
    @IBOutlet weak var senderTF: UITextField!
    fileprivate var sendersList: [SendersModel] = []
    static var updateSegmentValueDelagte: UpdateSegmentCount?
    fileprivate var senders:[SendersModel] = []

    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var flowPresenter: AddFlowPresenter?
    let dropDownTop = VPAutoComplete()

    override func viewDidLoad() {
        initView()
        
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    func initView() {
        
        selectedSenders = []
        sendersList = []
        senders = []
        senderTF.delegate =  self
        senderTF.resignFirstResponder()
        if AddNewRuleViewController.editRule?.senders?.count != 0 &&  AddNewRuleViewController.editRule != nil {
            sendersList = (AddNewRuleViewController.editRule?.senders!)!
            selectedSenders = (AddNewRuleViewController.editRule?.senders!)!
            sendersTB.reloadData()
        } else {
            selectedKeywords = []
        }
        
        self.addDropDown()
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SenderViewController.dismissSenders))
//        view.addGestureRecognizer(tap)


    }
    
    @objc func dismissSenders() {
        dropDownTop.isShowView(is_show: false)

    }
    
    func addDropDown(){
        
        // For Top textField
//        getSendersWithChannels(senders: senders).1
        dropDownTop.dataSource = getSendersWithChannels(senders: senders).0
        dropDownTop.imgDataSource = getSendersWithChannels(senders: senders).1
        dropDownTop.onTextField = senderTF
        dropDownTop.tableView?.reloadData()
        dropDownTop.onView = self.view
        dropDownTop.show { (str, index) in
            print("string : \(str) and Index : \(index)")
            self.sendersList.append(SendersModel(channelIconUrl: self.senders[index].channelIconUrl ?? "", name: self.senders[index].name ?? ""))

            self.senderTF.text = ""
            SenderViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 1, newValue: (self.sendersList.count), checked: nil)
           selectedSenders = (self.sendersList)


        }
       
       
    }
    override func viewDidLayoutSubviews() {
        
        sendersTB.reloadData()
    }

    
    @IBAction func addSenderDidChanged(_ sender: Any) {

            sendersListChanged()
    }
    
    @IBAction func senderEditChanged(_ sender: Any) {
        
         if senderTF.text?.removingWhitespaces() != ""
        {
            if senderTF.text != "" && Helper.isValidSenders(text: senderTF.text!) {
                
                flowPresenter = AddFlowPresenter(self)
                flowPresenter?.searchSender(query: senderTF.text!.removingWhitespaces())
                dropDownTop.isShowView(is_show: true)
                
            } else if !Helper.isValidSenders(text: senderTF.text!) {
                Helper.showFloatAlert(title: NSLocalizedString("no_valid_senders", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            
        }
        else
        {
            dismissSenders()
        }


    }
    
    @objc fileprivate func sendersListChanged(sender: UIButton?=nil) {

        if sender == nil {
            
            
            if  !senderTF.text!.isEmpty && senderTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            {
                Helper.showFloatAlert(title: "blank_sender".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if !Helper.isValidSenders(text: senderTF.text!) {
                Helper.showFloatAlert(title: NSLocalizedString("no_valid_senders", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if senderTF.text != "" && Helper.isValidSenders(text: senderTF.text!) {
                
                sendersList.append(SendersModel(channelIconUrl: "", name: senderTF.text!))
                sendersTB.reloadData()
                SenderViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 1, newValue: sendersList.count, checked: nil)
                senderTF.text = ""
                dismissSenders()
                
            }
    
        } else {
            sendersList.remove(at: (sender?.tag)!)
            sendersTB.deleteRows(at: [IndexPath(row: (sender?.tag)!, section: 0)], with: UITableViewRowAnimation.automatic)
            sendersTB.reloadData()
            SenderViewController.updateSegmentValueDelagte?.setUpdateValue(segmentNumber: 1, newValue: sendersList.count, checked: nil)

            sendersTB.frame = CGRect(x: sendersTB.frame.origin.x, y: sendersTB.frame.origin.y, width: sendersTB.frame.size.width, height: sendersTB.contentSize.height)

        }
        selectedSenders = sendersList

    }

}

extension SenderViewController: UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate , AddFlowPresenterView{

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = sendersTB.dequeueReusableCell(withIdentifier: "filter_cell") as! FlowFilterItemTableViewCell
        cell.filterItemName.text = sendersList[indexPath.row].name
        if sendersList[indexPath.row].channelIconUrl != ""
        {
            cell.channel_icon?.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(sendersList[indexPath.row].channelIconUrl ?? "")!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }
        else
        {
            cell.channel_icon.isHidden = true
            cell.imgConst? .constant = 0 
        }
        cell.deleteFilterBtn.tag = indexPath.row
        cell.deleteFilterBtn.addTarget(self, action: #selector(sendersListChanged), for: UIControlEvents.touchUpInside)
        return cell

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return sendersList.count
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 30
        let currentString: NSString = senderTF.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func getSearchResults(response: SearchModelResponse) {
        
        if let senders = response.response
        {
            
            self.senders = senders
            dropDownTop.dataSource = getSendersWithChannels(senders: senders).0
            dropDownTop.imgDataSource = getSendersWithChannels(senders: senders).1
            dropDownTop.tableView?.reloadData()
            dropDownTop.onTextField = senderTF
            dropDownTop.onView = self.view
//            if self.senders.count == 0
//            {
//                Helper.showFloatAlert(title: "no_senders_found".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
//            }
           
        }
        
    }
    
    func setError(error: NSError,url: String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)

    }

    
    func getSendersWithChannels(senders:[SendersModel])->([String],[String])
    {
        var senderNames:[String] = []
        var senderImgs:[String] = []
        for (_,item) in senders.enumerated()
        {
            senderNames.append(item.name ?? "")
            senderImgs.append(item.channelIconUrl ?? "")

        }
        return (senderNames,senderImgs)
    }
    
}
