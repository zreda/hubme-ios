//
//  AddNewFlowViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/17/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController

class AddNewFlowViewController: UIViewController {
    
    @IBOutlet weak var flownameTF: DesignableUITextField!
    @IBOutlet weak var ruleTB: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    fileprivate var addNewFlowPresenter: AddFlowPresenter?
    fileprivate var senders: String = ""
    fileprivate var keywords: String = ""
    fileprivate var channel: String = ""
    fileprivate var channelAttributedString: NSMutableAttributedString = NSMutableAttributedString()
    fileprivate var sendersAttributedString: NSMutableAttributedString = NSMutableAttributedString()
    
    fileprivate var keywordsAttributedString: NSMutableAttributedString = NSMutableAttributedString()
    
    var isAddFlow: Bool = true
    var rulesList: FlowRequestModel?
    var flowId: Int?
    var flowItem: FlowItem?
    
    override func viewWillAppear(_ animated: Bool) {
        
        if !isAddFlow {
            self.navigationItem.title = "Edit flow".localized()
        } else {
            self.navigationItem.title = "Create flow".localized()
            
        }
        ruleTB.reloadData()
        
    }
    
    override func viewDidLoad() {
        flownameTF.delegate = self
        ruleTB.estimatedRowHeight = 60.0
        ruleTB.rowHeight = UITableViewAutomaticDimension
        rulesList = FlowRequestModel()
        
        if !isAddFlow {
            setFlowItem()
        }
        
        let customBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action:  #selector(SendFeedbackViewController.backAction))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        
    }
    
    public static func create() -> AddNewFlowViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! AddNewFlowViewController
    }
    
    override func viewDidLayoutSubviews() {
        //        super.updateViewConstraints()
        
        ruleTB.frame = CGRect(x: ruleTB.frame.origin.x, y: ruleTB.frame.origin.y, width: ruleTB.frame.size.width, height: ruleTB.frame.size.height)
        
    }
    
    
    @objc func backAction(){
        
        if flownameTF.text != "" || rulesList?.rules?.count != 0
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.flowDelegate = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
   
 
    @IBAction func doneBtnTapped(_ sender: Any) {
        
        if (flownameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "") {
            
            Helper.showFloatAlert(title: NSLocalizedString("empty_flow_name", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        } else if !(Helper.isValidFlowName(text: flownameTF.text!)) {
            Helper.showFloatAlert(title: NSLocalizedString("no_valid_flow_name", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        } else if (flownameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" && rulesList?.rules?.count == 0) {
            Helper.showFloatAlert(title: NSLocalizedString("no_rules_found", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        } else {
            self.rulesList?.name = flownameTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).condensedWhitespace
            addNewFlowPresenter = AddFlowPresenter(self)
            loading.isHidden = false
            
            if isAddFlow {
                loading.startAnimating()
                addNewFlowPresenter?.addUserFlow(flowRequest: rulesList!)
            } else {
                loading.startAnimating()
                addNewFlowPresenter?.updateFlow(updatedFlow: rulesList!)
                
            }
        }
        
    }
    
    func getFlowRule(rule: Rule) {
        
        if (rule.selectedChannels?.count)! > 0 {
            if (rule.selectedChannels?.count)! > 1 {
                channel = " from channels ".localized()
            } else {
                channel = " from channel ".localized()
                
            }
        } else {
            channel = ""
        }
        for (i, item) in (rule.channels?.enumerated())! {
            if item.name != "" {
                if i == (rule.channels?.count)! - 1 {
                    channel += item.channelUsername!
                } else {
                    channel += "\(item.channelUsername!), "
                }
            }
        }
        
        if (rule.channels?.count)! > 0 {
            channelAttributedString = NSMutableAttributedString(string: channel)
            
            if Helper.getDeviceLang() == HubMeConstants.languages.en
            {
                channelAttributedString.addAttribute(NSAttributedStringKey.foregroundColor , value: #colorLiteral(red: 0.2588235294, green: 0.4039215686, blue: 0.8235294118, alpha: 1), range: NSRange(
                    location: 14, length: channel.count-14))
            }
            else
            {
                channelAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: #colorLiteral(red: 0.2588235294, green: 0.4039215686, blue: 0.8235294118, alpha: 1), range: NSRange(
                    location: 10, length: channel.count-10))
                
            }
            
        } else {
            channelAttributedString = NSMutableAttributedString(string: "")
            
        }
        
        if (rule.keywords?.count)! > 0 {
            if (rule.keywords?.count)! > 1 {
                keywords = " including the keywords ".localized()
            } else {
                keywords = " including the keyword ".localized()
                
            }
        } else {
            keywords = ""
        }
        for (i, item) in (rule.keywords?.enumerated())! {
            if i == (rule.keywords?.count)! - 1 {
                keywords += item
            } else {
                keywords += "\(item), "
            }
            
        }
        
        if (rule.keywords?.count)! > 0 {
            
            keywordsAttributedString = NSMutableAttributedString(string: keywords)
            keywordsAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: Helper.hexStringToUIColor(hex: "4267D2"), range: NSRange(
                location: 23, length: keywords.count-23))
            
            
        } else {
            keywordsAttributedString = NSMutableAttributedString(string: "")
            
        }
        
        if (rule.senders?.count)! > 0 {
            if channel.count == 0 {
                senders = " sent by ".localized()
            } else {
                senders = " ,sent by ".localized()
                
            }
        } else {
            senders = ""
        }
        for (i, item) in (rule.senders?.enumerated())! {
            
            if i == (rule.senders?.count)! - 1 {
                senders += item.name ?? ""
            } else {
                senders += "\(item.name ?? ""), "
            }
        }
        
        if (rule.senders?.count)! > 0 {
            
            sendersAttributedString = NSMutableAttributedString(string: senders)
            
            
            sendersAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: Helper.hexStringToUIColor(hex: "4267D2"), range: NSRange(
                location: 9, length: senders.count-9))
            if Helper.getDeviceLang() == HubMeConstants.languages.en
            {
                sendersAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: Helper.hexStringToUIColor(hex: "4267D2"), range: NSRange(
                    location: 9, length: senders.count-9))
            }
            else
            {
                sendersAttributedString.addAttribute(NSAttributedStringKey.foregroundColor, value:Helper.hexStringToUIColor(hex: "4267D2"), range: NSRange(
                    location: 13, length: senders.count-13))
                
            }
            
        } else {
            sendersAttributedString = NSMutableAttributedString(string: "")
            
        }
        
    }
    
}

extension AddNewFlowViewController: UITextFieldDelegate, FlowRule, UITableViewDelegate, UITableViewDataSource, AddFlowPresenterView {
    
    func setFlowResult(response: LoginResponseModel?) {
        
        loading.stopAnimating()
        
        if response?.message?.lowercased() == HubMeConstants.Messages.success.lowercased().localized() || response?.message?.lowercased() == HubMeConstants.Messages.success.lowercased() {
            AddNewRuleViewController.flowRule = nil
            self.navigationController?.popViewController(animated: true)
            self.rulesList =  FlowRequestModel()
        } else {
            Helper.showFloatAlert(title: (response?.message)!, subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        
    }
    
    func updateFlow(response: LoginResponseModel?) {
        loading.stopAnimating()
        
        if response?.message?.lowercased().localized() == HubMeConstants.Messages.success.lowercased().localized() || response?.message?.lowercased() == HubMeConstants.Messages.success.lowercased()   {
            AddNewRuleViewController.flowRule = nil
            AddNewRuleViewController.editRule = nil
            self.navigationController?.popViewController(animated: true)
            self.rulesList =  FlowRequestModel()
        } else {
            Helper.showFloatAlert(title: (response?.message)!, subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        
    }
    
    func setFlowItem() {
        self.flownameTF.text = flowItem?.name
        rulesList?.name = flowItem?.name
        rulesList?.id = flowItem?.id
        
        for (_, item) in (flowItem?.rules)!.enumerated() {
            
            var selectedChannelsItems: [Channel] = []
            var selectedChannels: [Int] = []
            
            for (_, channel) in (item.channels?.enumerated())! {
                if channel.selected == true {
                    selectedChannelsItems.append(Channel(id: channel.id, username: channel.channelUsername))
                    selectedChannels.append(channel.id!)
                }
                
            }
            rulesList?.rules?.append(Rule(selectedChannels: selectedChannels, keywords: item.keywords!, senders: item.senders!, channels: selectedChannelsItems))
        }
        
        self.ruleTB.reloadData()
    }
    func setFlowDetails(response: FlowDetailsResponseModel?) {
        
        loading.stopAnimating()
        
        if response != nil {
            //            self.rulesList = response?.response.
            self.flownameTF.text = response?.response?.name
            
            rulesList?.name = response?.response?.name
            rulesList?.id = response?.response?.id
            
            for (_, item) in (response?.response?.rules)!.enumerated() {
                
                var selectedChannelsItems: [Channel] = []
                var selectedChannels: [Int] = []
                
                for (_, channel) in (item.channels?.enumerated())! {
                    if channel.selected == true {
                        selectedChannelsItems.append(Channel(id: channel.id, username: channel.channelUsername))
                        selectedChannels.append(channel.id!)
                    }
                }
                
                rulesList?.rules?.append(Rule(selectedChannels: selectedChannels, keywords: item.keywords ?? [], senders: item.senders ?? [], channels: selectedChannelsItems))
            }
            
            self.ruleTB.reloadData()
        }
    }
    
    func setRule(rule: Rule) {
        
        rule.selectedChannels = []
        
        for item in rule.channels! {
            rule.selectedChannels?.append(item.id!)
        }
        
        if AddNewRuleViewController.selectedRule == -1 {
            self.rulesList?.rules?.append(rule)
        } else {
            
            self.rulesList?.rules![AddNewRuleViewController.selectedRule!] = rule
        }
        
        self.ruleTB.reloadData()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        let maxLength = 25
        let currentString: NSString = flownameTF.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row != rulesList?.rules?.count {
            return true
        }
        return false
    }
 
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let add_rule_cell = ruleTB.dequeueReusableCell(withIdentifier: "add_rule_cell")
            
            return add_rule_cell!
        }
        else
        {
            let rule_cell = ruleTB.dequeueReusableCell(withIdentifier: "rule_cell")
            getFlowRule(rule: rulesList!.rules![indexPath.row-1])
            var mesText = NSMutableAttributedString()
            mesText = NSMutableAttributedString(string: "Messages")
            mesText.append(channelAttributedString)
            mesText.append(sendersAttributedString)
            mesText.append(keywordsAttributedString)
            
            rule_cell?.textLabel?.attributedText = mesText
            
            return rule_cell!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (rulesList?.rules?.count)! + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alertController = UIAlertController(title: "Delete Rule".localized(), message: "Are you Sure to delete rule".localized(), preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
            
            let DestructiveAction = UIAlertAction(title: "Cancel".localized(), style: UIAlertActionStyle.destructive) {
                (result : UIAlertAction) -> Void in
                print("Destructive")
            }
            
            // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
            
            let okAction = UIAlertAction(title: "OK".localized(), style: UIAlertActionStyle.default) {
                (result : UIAlertAction) -> Void in
                
                self.rulesList?.rules?.remove(at: indexPath.row)
                self.ruleTB.reloadData()
            }
            
            alertController.addAction(DestructiveAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let addRulesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewRuleViewController") as! AddNewRuleViewController
        AddNewRuleViewController.addFlowDelegate = self
 
        if indexPath.row == 0  // add new rule (Tap on button to add new one )
        {
            AddNewRuleViewController.editRule = nil
            AddNewRuleViewController.selectedRule = -1
            addRulesVC.screenTitle = "Create flow".localized()
            
        }
        else // edit rule
        {
            AddNewRuleViewController.selectedRule = indexPath.row-1
            AddNewRuleViewController.editRule = rulesList?.rules![indexPath.row-1]
            addRulesVC.screenTitle = "Edit rule".localized()
        }

        self.navigationController?.pushViewController(addRulesVC, animated: true)
        
    }
    
    func setError(error: NSError,url: String) {
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        loading.stopAnimating()
    }
    
    func goback(_ go: Bool) {
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
        self.dismissDialogViewController()
    }
    
}
