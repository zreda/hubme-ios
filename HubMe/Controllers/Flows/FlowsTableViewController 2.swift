//
//  FlowsViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/17/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
//import ActionButton

class FlowsTableViewController: UIViewController {

    @IBOutlet weak var noFlowLB: UILabel!
    @IBOutlet weak var flowsTB: UITableView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var addFlowBtn: UIBarButtonItem!
    fileprivate var flows: [FlowItem] = []
    fileprivate var allPositions: [PositionModel] = []

    fileprivate var userFlowPresenter: FlowListPresenter?
    fileprivate var userChannelsPresenter: UserChannelsPresenter?
    fileprivate var deletedFlowIndex: Int?
    var channelItem: UserChannelsItem?
    @IBOutlet weak var noFlowImg: UIImageView!
    @IBOutlet weak var noFlowText: UILabel!
    fileprivate let editBtn = UIButton.init(type: .custom)

    fileprivate var isReoderFlow: Bool = true

    override func viewDidLoad() {
        initView()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationItem.backBarButtonItem?.title = "Flows".localized()
        Helper.saveUserDefault(key: HubMeConstants.userDefault.isEdit, value: true)
        if flows.count > 0
        {
            loading.stopAnimating()
        }
        self.navigationItem.leftBarButtonItem = nil
        if channelItem != nil
        {
            let vc =  FeedsTableViewController.create()
            vc.type = .flow
            vc.selectedAccountItem = channelItem
            channelItem = nil
            self.navigationController?.pushViewController(vc, animated: true)
        }
        flows = []
        flowsTB.reloadData()
        loading.startAnimating()
        userChannelsPresenter = UserChannelsPresenter(self)
        userChannelsPresenter?.getUserChannels()
        addFlowBtn.isEnabled = false

    }

    @objc public static func create() -> FlowsTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! FlowsTableViewController
    }
    func initView() {
        flowsTB?.tableFooterView = UIView(frame: CGRect.zero)
        noFlowLB.isHidden = true
        noFlowImg.isHidden = true
        noFlowText.isHidden = true
    }
    
    func reorderFlowsBtn()
    {
        
        editBtn.setTitleColor(#colorLiteral(red: 0.6358308792, green: 0.635846138, blue: 0.6358379126, alpha: 1), for: UIControlState.normal)

        if Helper.getUserDefault(key: HubMeConstants.userDefault.isEdit) as! Bool
        {
            editBtn.setTitle("Edit".localized(), for: .normal)
            flowsTB.isEditing = false

        }
        else
        {
            editBtn.setTitle("Done".localized(), for: .normal)
            flowsTB.isEditing = true

        }
        //add function for button
        editBtn.addTarget(self, action: #selector(FlowsTableViewController.reoderFlowsBtnAction(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        editBtn.frame = CGRect(x: 0, y: 0, width: 44, height: 51)
        
        let leftButon = UIBarButtonItem(customView: editBtn)
        
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = leftButon
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "add_new_flow"{
            AddNewRuleViewController.flowRule = nil
            AddNewRuleViewController.editRule = nil
        }
    }

    @IBAction func reoderFlowsBtnAction(_ sender: Any) {
        
        if Helper.getUserDefault(key: HubMeConstants.userDefault.isEdit) as! Bool
            // begin reoder
        {
            Helper.saveUserDefault(key: HubMeConstants.userDefault.isEdit, value: false)
           editBtn.setTitle("Done".localized(), for: .normal)
           flowsTB.isEditing = true
        }
        else // end reoder
        {
            Helper.saveUserDefault(key: HubMeConstants.userDefault.isEdit, value: true)
            editBtn.setTitle("Edit".localized(), for: .normal)
            flowsTB.isEditing = false
        }
        
    }

}

extension FlowsTableViewController: UITableViewDelegate, UITableViewDataSource, FlowListPresenterView, ChannelsPresenterView {
   
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let flowCell = tableView.dequeueReusableCell(withIdentifier: "flow_cell", for: indexPath)
        flowCell.textLabel?.text = flows[indexPath.row].name
        return flowCell

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return flows.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 60.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc =  FeedsTableViewController.create()
        vc.type = .flow
        vc.selectedAccountItem = UserChannelsItem(id: flows[indexPath.row].id!, name: flows[indexPath.row].name!, iconUrl: "")

        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
//        print(indexPath.row)
        return true
    }


    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

//        // edit Action
        let editAction = UITableViewRowAction(style: .destructive, title: "Edit".localized(), handler: { (action, indexPath) in

            let addNewFlowVC = AddNewFlowViewController.create()
            addNewFlowVC.flowId = self.self.flows[indexPath.row].id
            addNewFlowVC.isAddFlow = false
            addNewFlowVC.flowItem = self.flows[indexPath.row]
            AddNewRuleViewController.flowRule = nil
            AddNewRuleViewController.editRule = nil
            self.navigationController?.pushViewController(addNewFlowVC, animated: true)

        })
        editAction.backgroundColor = Helper.hexStringToUIColor(hex: HubMeConstants.Colors.gold)

        // delete Action

        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete".localized(), handler: { (action, indexPath) in
            self.deletedFlowIndex = indexPath.row
            self.userFlowPresenter?.deleteFlow(flow_id: String(describing: self.flows[indexPath.row].id as! Int))
            self.flowsTB.reloadData()

        })
        deleteAction.backgroundColor = UIColor.red
        flowsTB?.isEditing = true

        return [deleteAction , editAction]
    
    }
    
    
     func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.flows[sourceIndexPath.row]
        flows.remove(at: sourceIndexPath.row)
        flows.insert(movedObject, at: destinationIndexPath.row)
        let request = ReorderFlowsModelRequest()
        self.allPositions = []
        for (i,item) in flows.enumerated()
        {
            allPositions.append(PositionModel(id: item.id ?? 0, position: i))
        }
        request.orderedUserFlows = allPositions
        userFlowPresenter?.reorderFlows(request: request)
        loading.startAnimating()
    }
    
     func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
     func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        if(self.isEditing) {
            
        }
    }
    
    
    // Override to support conditional rearranging of the table view.
     func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {

        return true
    }
    

    
    @available(iOS 11.0, *)

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        // edit Action

        let editAction = UIContextualAction.init(style: UIContextualAction.Style.normal, title: "Edit".localized(), handler: { (action, view, completion) in

            let addNewFlowVC = AddNewFlowViewController.create()
            addNewFlowVC.flowId = self.self.flows[indexPath.row].id
            addNewFlowVC.isAddFlow = false
            addNewFlowVC.flowItem = self.flows[indexPath.row]
            AddNewRuleViewController.flowRule = nil
            AddNewRuleViewController.editRule = nil
            self.navigationController?.pushViewController(addNewFlowVC, animated: true)

        })
        editAction.backgroundColor = Helper.hexStringToUIColor(hex: HubMeConstants.Colors.gold)

        // delete Action
        let deleteAction = UIContextualAction.init(style: UIContextualAction.Style.destructive, title: "Delete".localized(), handler: { (action, view, completion) in
            self.deletedFlowIndex = indexPath.row
            self.userFlowPresenter?.deleteFlow(flow_id: String(describing: self.flows[indexPath.row].id as! Int))
        })
        deleteAction.backgroundColor = UIColor.red

        self.flowsTB.reloadData()

        let config = UISwipeActionsConfiguration(actions: [deleteAction, editAction])

        config.performsFirstActionWithFullSwipe = false
        return config
    }

    func setUserChannelsResult(response: [UserChannelsItem]?) {

        if response?.count == 0 {
            addFlowBtn.isEnabled = false
            noFlowLB.isHidden = false
            noFlowText.isHidden = false
            noFlowImg.isHidden = false
            flows = []
            flowsTB.reloadData()
            loading.stopAnimating()
        } else {
            addFlowBtn.isEnabled = true
            userFlowPresenter = FlowListPresenter(self)
            userFlowPresenter?.getUserFlows()
        }

    }

    func deleteFlow(response: LoginResponseModel?) {

        //Remove object from array
        flows.remove(at: deletedFlowIndex!)
        Helper.showFloatAlert(title: "Flow deleted sucessfully".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)

        //Reload tableView
        self.flowsTB.reloadData()

        if flows.count == 0 {
            noFlowLB.isHidden = false
            noFlowImg.isHidden = false
            noFlowText.isHidden = false
        } else {
            noFlowLB.isHidden = true
            noFlowImg.isHidden = true
            noFlowText.isHidden = true
        
        }
        
        if flows.count > 1
        {
            reorderFlowsBtn()
        }
        else
        {
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    func setUFlowListResult(response: [FlowItem]?) {

        if response?.count == 0 {
            noFlowLB.isHidden = false
            noFlowImg.isHidden = false
            noFlowText.isHidden = false

        } else {
            noFlowLB.isHidden = true
            noFlowImg.isHidden = true
            noFlowText.isHidden = true
        }
        if (response?.count ?? 0) > 1
        {
            reorderFlowsBtn()
        }
        else
        {
            self.navigationItem.leftBarButtonItem = nil
        }
        flows = response!
        flowsTB.reloadData()
        loading.stopAnimating()

    }
    func setError(error: NSError) {
        
    }
    func setError(error: NSError,url:String) {
        if flows.count == 0 {
            noFlowLB.isHidden = false
            noFlowImg.isHidden = false
            noFlowText.isHidden = false
        } else {
            noFlowLB.isHidden = true
            noFlowImg.isHidden = true
            noFlowText.isHidden = true
        }
        if flows.count > 1
        {
            reorderFlowsBtn()
        }
        else
        {
            self.navigationItem.leftBarButtonItem = nil
        }
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        loading.stopAnimating()
    }
    
    func setSuccess(message: String?) {
        loading.stopAnimating()
        print("Flows reodered successfully")
    }
}
