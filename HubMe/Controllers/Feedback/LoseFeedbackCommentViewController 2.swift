//
//  LoseFeedbackCommentViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/27/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class LoseFeedbackCommentViewController: UIViewController {
    @IBOutlet weak var backgroundView: UIView!
    var delegate: SendFeedbackViewController?
    var replyDelegate:ReplyViewController?
    var flowDelegate:AddNewFlowViewController?
//    var feedbackDetails:FeedDetailsViewController?
    var feedbackDetails:FeedsDetailsTableViewController?

    var twitterDetails:TwitterViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.layer.cornerRadius = 10

        // Do any additional setup after loading the view.
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        self.delegate?.goback(false)
        self.replyDelegate?.goback(false)
        self.flowDelegate?.goback(false)
        self.feedbackDetails?.goback(false)
        self.twitterDetails?.goback(false)


    }
    @IBAction func confrimBtnAction(_ sender: Any) {
        self.delegate?.goback(true)
        self.replyDelegate?.goback(true)
        self.flowDelegate?.goback(true)
        self.feedbackDetails?.goback(true)
        self.twitterDetails?.goback(true)



    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
