//
//  LoseFeedbackCommentViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/27/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class LoseFeedbackCommentViewController: UIViewController {
    @IBOutlet weak var backgroundView: UIView!
    var delegate: SendFeedbackViewController?
    var replyDelegate:ReplyViewController?
    var flowDelegate:AddNewFlowViewController?
    //    var feedbackDetails:FeedDetailsViewController?
    var feedbackDetails:FeedsDetailsTableViewController?
    
    var twitterDetails:TwitterViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.layer.cornerRadius = 10
        
        // Do any additional setup after loading the view.
        self.replyDelegate?.tableView.isScrollEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.replyDelegate?.tableView.isScrollEnabled = true
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        
        self.delegate?.goback(false)
        self.replyDelegate?.goback(false)
        self.flowDelegate?.goback(false)
        self.feedbackDetails?.goback(false)
        self.twitterDetails?.goback(false)
        
        self.delegate?.dismissDialogViewController()
        self.replyDelegate?.dismissDialogViewController()
        self.feedbackDetails?.dismissDialogViewController()
        self.twitterDetails?.dismissDialogViewController()
        self.flowDelegate?.dismissDialogViewController()
      
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.discardChanges), object: nil, userInfo: ["back":false])
        
        
    }
    @IBAction func confrimBtnAction(_ sender: Any) {
        self.delegate?.goback(true)
        self.replyDelegate?.goback(true)
        self.flowDelegate?.goback(true)
        self.feedbackDetails?.goback(true)
        self.twitterDetails?.goback(true)
        
        if replyDelegate != nil
        {
            self.replyDelegate?.dismissDialogViewController()
        }
        else if flowDelegate != nil
        {
            self.replyDelegate?.dismissDialogViewController()
        }
        else if self.feedbackDetails != nil
        {
            self.feedbackDetails?.dismissDialogViewController()
            
        }
        else if self.twitterDetails != nil
        {
            self.twitterDetails?.dismissDialogViewController()
        }
        
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.discardChanges), object: nil, userInfo: ["back":true])
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
