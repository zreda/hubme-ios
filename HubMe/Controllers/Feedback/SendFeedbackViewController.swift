//
//  SendFeedbackViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/25/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController
import Cosmos

class SendFeedbackViewController: UIViewController , UITextViewDelegate  {
    
    @IBOutlet weak var feedbackText: UITextView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var rateView: CosmosView!
    fileprivate var userFeedbackPresenter:FeedbackPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.discardChanges(notification:)), name: Notification.Name(HubMeConstants.Notifications.discardChanges), object: nil)

        initUI()
    }
    
    func initUI()
    {
        
        feedbackText.delegate = self
        feedbackText.textColor = UIColor.lightGray
        feedbackText.delegate = self
       userFeedbackPresenter = FeedbackPresenter(self)
       userFeedbackPresenter?.getFeedback()
        let customBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action:  #selector(SendFeedbackViewController.backAction))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton
        rateView.settings.updateOnTouch = true
        
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
    }
    @objc func backAction(){
        
        if feedbackText.text != "" && feedbackText.textColor != UIColor.lightGray
        {
            let dialogViewController: LoseFeedbackCommentViewController = LoseFeedbackCommentViewController(nibName:"LoseFeedbackComment", bundle: nil)
            dialogViewController.delegate = self
            self.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
            self.view.endEditing(true)
            
        }
            
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc public static func create() -> SendFeedbackViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SendFeedbackViewController
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 1000
    }
    
    
    @IBAction func sendBtnAction(_ sender: Any) {
        
        if rateView.rating == 0.0
        {
            Helper.showFloatAlert(title: "rate_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if  !feedbackText.text.isEmpty && feedbackText.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if feedbackText.text.count < 3 && !feedbackText.text.isEmpty
            
        {
            Helper.showFloatAlert(title: "short_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if feedbackText.text.count > 1000 && !feedbackText.text.isEmpty
        {
            Helper.showFloatAlert(title: "long_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
            //        else if !Helper.isValidFeedback(text: feedbackText.text) && feedbackText.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""
        else if !Helper.isValidFeedback(text: feedbackText.text)
            
        {
            Helper.showFloatAlert(title: "no_valid_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
            
        else {
            userFeedbackPresenter?.sendFeedback(feedabck: FeedbackModel(commentText: feedbackText.text!,rating: Int(rateView.rating)))
            loading.startAnimating()
            
        }
    }
    
    
}

extension SendFeedbackViewController : FeedbackPresenterView
{
    func getFeedback(hasFeedback: FeedbackModel?) {
        if hasFeedback != nil
        {
            rateView.rating = Double(hasFeedback?.rating ?? 0)
        }
    }
    
    func stopLoading() {
        loading.stopAnimating()
    }
    func setSuccess(msg: String?, error: String?) {
        stopLoading()
        //        Helper.saveUserDefault(key: HubMeConstants.userDefault.hasComment, value: true)
        
        Helper.showFloatAlert(title: "feedback_success".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
        
    }
    
    
    func setError(error: NSError,url: String) {
        stopLoading()
        switch HubMeConstants.StatusCode(rawValue: error.code)!
        {
        case .StatusBadRequest:
            print("No feedback")
        default:
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
            
        }
        
    }
    
    func goback(_ go: Bool) {
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
        if ls_dialogViewController != nil
        {
            self.dismissDialogViewController()
        }
    }
    
    @objc func discardChanges(notification: Notification) {
          
        let go = notification.userInfo!["back"] as! Bool
        if go
        {
            self.navigationController?.popViewController(animated: true)
        }
        
//        if ls_dialogViewController != nil
//        {
            self.tabBarController?.dismissDialogViewController()
//        }
        
         }
}
