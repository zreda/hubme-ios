//
//  ViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var skipBtn: UIButton!

    @IBOutlet weak var nextBtn: UIButton!

    var tutorialPageViewController: TutorialPageViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        pageControl.addTarget(self, action: #selector(TutorialViewController.didChangePageControlValue), for: .valueChanged)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? TutorialPageViewController {
            self.tutorialPageViewController = tutorialPageViewController
        }
    }

    @IBAction func didTapNextButton(_ sender: UIButton) {

        if pageControl.currentPage == pageControl.numberOfPages - 1 {
           navigateTabHome()
        } else {
            tutorialPageViewController?.scrollToNextViewController()
        }
    }

    @IBAction func didTapSkipButton(_ sender: Any) {

        navigateTabHome()
    }

    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    @objc func didChangePageControlValue() {
        tutorialPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }

    func navigateTabHome() {
        Helper.saveUserDefault(key: HubMeConstants.userDefault.isSkip, value: true)
       let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
//        let tabBar  = mainSB.instantiateViewController(withIdentifier: "HomeTabBar") as! UITabBarController
        let loginVC = mainSB.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController

        self.present(loginVC, animated: true, completion: nil)

    }
}

extension TutorialViewController: TutorialPageViewControllerDelegate {

    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController,
                                    didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }

    func tutorialPageViewController(_ tutorialPageViewController: TutorialPageViewController,
                                    didUpdatePageIndex index: Int) {
        pageControl.currentPage = index

        if pageControl.currentPage == pageControl.numberOfPages - 1 {
            nextBtn.setTitle("Done".localized(), for: .normal)

         //   skipBtn.isHidden = true

        } else {
          //  skipBtn.isHidden = false
            nextBtn.setTitle(">", for: .normal)

        }

    }

}
