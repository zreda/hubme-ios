//
//  ComposeNewMessageViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SDWebImage
import WSTagsField
import SVProgressHUD

class ComposeNewMessageViewController: UITableViewController {

    //    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var channelImg: UIImageView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var toTagView: UIView!
    @IBOutlet weak var ccTagView: UIView!
    @IBOutlet weak var bccTagView: UIView!
    @IBOutlet weak var subjectTF: UITextField!
    @IBOutlet weak var messageContent: UITextView!
    fileprivate var signaturePresenter:SignaturePresenter?
    fileprivate var composeNewMessagePresenter:ComposeNewMessagePresenter?
    fileprivate var sendBtn:UIButton?
    //    fileprivate var mailSignature:String = ""
    fileprivate var request:ComposeMessageRequestModel =  ComposeMessageRequestModel()
    fileprivate var toTagField = WSTagsField()
    fileprivate var ccTagField = WSTagsField()
    fileprivate var bccTagField = WSTagsField()
    fileprivate var activeTextField =  WSTagsField()
    fileprivate var senderHeight:CGFloat = 60.0
    fileprivate var ccHeight:CGFloat = 60.0
    fileprivate var bccHeight:CGFloat = 60.0
    fileprivate var to : [Bcc] = []
    fileprivate var cc : [Bcc] = []
    fileprivate var bcc : [Bcc] = []
    fileprivate var senders:[SendersModel] = []
    fileprivate var autoComplete: AutoComplete?
    fileprivate var currentTag: WSTagsField?
    fileprivate var mySignarture :String?
    fileprivate var relogin:ReloginBaseController?
    
    var accountsItem:[UserChannelsItem]?
    weak var delegate: RefreshAccountsDelegate?
    var channelItem:UserChannelsItem?
    var groupedAccount:GroupedChannel?
    var type:userType?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        channelName.text = channelItem?.name
        if (channelItem?.icon?.hasPrefix("http"))! {

            channelImg.sd_setImage(with: URL(string: (channelItem?.icon)! ), placeholderImage: #imageLiteral(resourceName: "loading_image"))

        } else {
            channelImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(channelItem?.icon)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }

        if channelItem?.supportedActions?.contains(HubMeConstants.SupportedActions.signature.rawValue) ?? false
        {
            signaturePresenter = SignaturePresenter(self)
            signaturePresenter?.getSignature()
        }
        request.from = channelItem?.channelUsername
        composeNewMessagePresenter = ComposeNewMessagePresenter(self)
        NotificationCenter.default.addObserver(self, selector: #selector(ComposeNewMessageViewController.addNewSender),name: NSNotification.Name(rawValue: HubMeConstants.Notifications.addSenderList),object: nil)

        
    }
  
    override func viewWillDisappear(_ animated: Bool)
    {
        delegate?.refreshAccount(accountsItem: accountsItem, selectedAccountItem: channelItem)
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            SVProgressHUD.dismiss()
        })
    }

    @objc public static func create() -> ComposeNewMessageViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ComposeNewMessageViewController
    }
    
    deinit
    {
        
        NotificationCenter.default.removeObserver(self)
    }

    fileprivate  func initUI()
    {
        toTagField = WSTagsField()
        ccTagField = WSTagsField ()
        bccTagField = WSTagsField()
        
        sendBtn = UIButton.init(type: .custom)
        messageContent.delegate = self
        messageContent.textColor = UIColor.lightGray
        self.navigationItem.title = "New Message".localized()
        //set image for button
        sendBtn?.setImage(#imageLiteral(resourceName: "send-active"), for: UIControlState.normal)
        sendBtn?.setTitleColor(#colorLiteral(red: 0.9641233087, green: 0.5129789114, blue: 0, alpha: 1), for: UIControlState.normal)
        //add function for button
        sendBtn?.addTarget(self, action: #selector(ComposeNewMessageViewController.sendBtnAction(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        sendBtn?.frame = CGRect(x: 0, y: 0, width: 30, height: 51)

        let rightButton = UIBarButtonItem(customView: sendBtn!)

        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = rightButton

        initTags(field: toTagField)
        tagFieldEvents(tagField: toTagField)
        initTags(field: ccTagField)
        tagFieldEvents(tagField: ccTagField)
        initTags(field: bccTagField)
        tagFieldEvents(tagField: bccTagField)

        toTagField.textDelegate = self
        ccTagField.textDelegate = self
        bccTagField.textDelegate = self
    }

    fileprivate func initTags(field:WSTagsField)
    {
        if field == toTagField
        {
            field.frame = toTagView.bounds
            toTagView.addSubview(field)
//            toTagField.beginEditing()

        }
        else if field == ccTagField // cc
        {
            field.frame = ccTagView.bounds
            ccTagView.addSubview(field)
//            ccTagField.beginEditing()

        }
        else if field == bccTagField // cc
        {
            field.frame = bccTagView.bounds
            bccTagView.addSubview(field)
//            bccTagField.beginEditing()

        }

        field.cornerRadius = 2.0
        field.spaceBetweenLines = 5
        field.spaceBetweenTags = 5
        field.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        field.placeholder = ""
        field.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        field.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        field.selectedColor = #colorLiteral(red: 0.9641233087, green: 0.5129789114, blue: 0, alpha: 1)

        field.layoutMargins = UIEdgeInsets(top: 2, left: 6, bottom: 2, right: 6)
        field.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) //old padding
        field.fieldTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        field.returnKeyType = .done
        field.textDelegate = self
    }
    


    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendBtnAction(_ sender: Any) {
        request.bcc = []
        request.to = []
        request.cc = []
        request.subject = subjectTF.text
        if messageContent.textColor != UIColor.lightGray
        {
            request.messageText = messageContent.text
        }
        else
        {
            request.messageText = ""

        }
        if toTagField.tags.count == 0 && ccTagField.tags.count == 0 && bccTagField.tags.count == 0 && toTagField.text == "" && ccTagField.text == "" && bccTagField.text == ""
        {
            Helper.showFloatAlert(title: "Message has no senders".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }

        else
        {
            if toTagField.text != ""
            {
                if  !toTagField.text!.isEmpty && toTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || toTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }


                if !Helper.isValidEmail(mail_address: toTagField.text!.condensedWhitespace)
                {
                    Helper.showFloatAlert(title: "\(toTagField.text!.condensedWhitespace) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else
                {
                    self.to.append(Bcc(email: toTagField.text?.condensedWhitespace ?? "", name: toTagField.text!))
                    toTagField.addTag(toTagField.text ?? "")


                }
            }
            if ccTagField.text != ""
            {

                if  !ccTagField.text!.isEmpty && ccTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || ccTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }

                else if !Helper.isValidEmail(mail_address: ccTagField.text!.condensedWhitespace)
                {
                    Helper.showFloatAlert(title: "\(ccTagField.text!.condensedWhitespace) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else
                {
                    self.cc.append(Bcc(email: ccTagField.text?.condensedWhitespace ?? "", name: ccTagField.text!))
                    ccTagField.addTag(ccTagField.text ?? "")

                }
            }

            if bccTagField.text != ""
            {
                if  !bccTagField.text!.isEmpty && bccTagField.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" || bccTagField.text == ""
                {
                    Helper.showFloatAlert(title: "blank_feedback".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return
                }
                else if !Helper.isValidEmail(mail_address: bccTagField.text!.condensedWhitespace)
                {
                    Helper.showFloatAlert(title: "\(bccTagField.text!.condensedWhitespace) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    return

                }
                else
                {
                    self.bcc.append(Bcc(email: bccTagField.text?.condensedWhitespace ?? "", name: bccTagField.text!))
                    bccTagField.addTag(bccTagField.text ?? "")

                }
            }

            request.to = to
            request.cc = cc
            request.bcc = bcc
            self.tableView.isUserInteractionEnabled = false
            SVProgressHUD.show()
            composeNewMessagePresenter?.composeMessage(channelId: String(describing: channelItem?.id ?? 0), request: request)
            sendBtn?.isEnabled = false
        }

    }

    fileprivate func tagFieldEvents(tagField:WSTagsField) {

        tagField.beginEditing()
       
        tagField.onVerifyTag = {tag, text in

            if !Helper.isValidEmail(mail_address: text.condensedWhitespace)
            {
                Helper.showFloatAlert(title: "\(text) \("email_field_not_formmated".localized())", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                return false
            }
            return true
        }
        tagField.onDidAddTag = { tagField, tag in
            print("onDidAddTag")

            if tagField == self.toTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.to)
                {
                    self.to.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))
                }

            }
            else if tagField == self.ccTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.cc)
                {
                    self.cc.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))

                }
            }
            else if tagField == self.bccTagField
            {
                if !Helper.checkFound(str: tag.text,array: self.bcc)
                {
                    self.bcc.append(Bcc(email: tag.text.condensedWhitespace, name: tag.text))

                }
            }

        }

        tagField.onDidRemoveTag = {tagField, tag in
            print("onDidRemoveTag")

            if tagField == self.toTagField
            {
                let index = self.to.index{$0.name == tag.text}
                self.to.remove(at: index ?? 0)
            }
            else if tagField == self.ccTagField
            {
                let index = self.cc.index{$0.name == tag.text}
                self.cc.remove(at: index ?? 0)
            }
            else // bcc
            {
                let index = self.bcc.index{$0.name == tag.text}
                self.bcc.remove(at: index ?? 0)
            }
        }

        tagField.onDidChangeText = { _, text in
            print("onDidChangeText")

            if text?.trim() != "" && !(text?.trim().starts(with: ";") ?? false)!
            {
                self.currentTag = tagField
                self.composeNewMessagePresenter?.getSenders(sender: text?.trim() ?? "", channelType: "EMAIL")
            }

            if self.autoComplete != nil
            {
                for (_,view) in self.view.subviews.enumerated()
                {
                    if  view is AutoComplete
                    {
                        view.removeFromSuperview()
                        self.autoComplete = nil
                    }
                }
            }

        }

        tagField.onDidChangeHeightTo = { _, height in
            print("HeightTo \(height)")
            if tagField == self.toTagField
            {
                self.senderHeight = height + 10.0
                //                self.tableView.reloadRows(at: [IndexPath(row: 1, section: 0)], with: .none)
                //                self.tableView.reloadData()
            }
            else if tagField == self.ccTagField
            {
                self.ccHeight = height + 10.0
                //                self.tableView.reloadData()
                //                self.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)

            }
            else if tagField == self.bccTagField
            {
                self.bccHeight = height + 10.0
                //                self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .none)
                //                self.tableView.reloadData()

            }
            self.tableView.reloadData()

        }

        tagField.onDidSelectTagView = { _, tagView in
            print("Select \(tagView)")
        }

        tagField.onDidUnselectTagView = { _, tagView in
            print("Unselect \(tagView)")
        }
    }
    
  

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1
        {
            return 44.0
        }
        else if indexPath.row == 2 // to
        {
            return senderHeight

        }
        else if indexPath.row == 3 // cc
        {
            return ccHeight

        }
        else if indexPath.row == 4 // bcc
        {
            return bccHeight

        }
        else if indexPath.row == 5 // subject
        {
            return 60.0

        }
        else  // message
        {
            if mySignarture == ""
            {
                return 100.0
            }
            else
            {
                return  Helper.heightForText(text: mySignarture ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) < 60 ? 100 : Helper.heightForText(text: mySignarture ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) + 80
            }
        }


    }

    @objc private func addNewSender(notification: NSNotification){
        let sender = notification.object as! SendersModel
        print(sender.email ?? "")
        if currentTag == toTagField
        {
            to.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            toTagField.addTag(sender.name ?? "")

        }
        else if currentTag == ccTagField
        {
            cc.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            ccTagField.addTag(sender.name ?? "")
        }
        else if currentTag == bccTagField
        {
            bcc.append(Bcc(email: sender.email?.condensedWhitespace ?? "", name: sender.name ?? ""))
            bccTagField.addTag(sender.name ?? "")

        }
        if self.autoComplete != nil
        {
            for (_,view) in self.view.subviews.enumerated()
            {
                if  view is AutoComplete
                {
                    view.removeFromSuperview()
                    autoComplete = nil
                }
            }

        }

    }
    fileprivate func addSenders(view:WSTagsField , arr:[Bcc])
    {
        for (_,item) in  arr.enumerated()
        {
            view.addTag(item.name ?? "")
        }

    }

}

extension ComposeNewMessageViewController : SignaturePresenterView
{

    func setNewSignatureSuccess(response: String?)
    {
        let replaced = response?.replacingOccurrences(of: "\\n", with: "\n")
        mySignarture = "\n \n \n \n\(replaced ?? "")"
        messageContent.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        messageContent.text = "\n \n \n \n\(replaced ?? "")"
        self.tableView.reloadData()
    }

    func setError(error: NSError,url: String) {
        sendBtn?.isEnabled = true
        self.tableView.isUserInteractionEnabled = true

        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {

            switch type {

            case .some(.flow),.some(.search):

                Helper.showFloatAlert(title: "Account is expired".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                break
            case .some(.channel):

                relogin = ReloginBaseController()
                relogin?.controller = self
                relogin?.registerSocket()
                relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing: channelItem?.id as! Int), selectedAccount: channelItem ?? UserChannelsItem())
                break
            case .none:
                print("None")
            }
        }
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
        SVProgressHUD.dismiss()
    }


}

extension ComposeNewMessageViewController:ComposeNewMessagePresenterView
{
    func setErrorReponse(msg: String?) {
        sendBtn?.isEnabled = true
        self.tableView.isUserInteractionEnabled = true

        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        SVProgressHUD.dismiss()

    }

    func composeMessageSuccess(response: String) {
        Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        sendBtn?.isEnabled = false

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.dismiss(animated: true, completion: nil)
//            self.navigationController?.popViewController(animated: true)
            SVProgressHUD.dismiss()

        })

    }

    func getSendersSuccess(response: [SendersModel]?) {

        self.senders = response ?? []

        if senders.count > 0
        {
            if activeTextField == toTagField
            {
                let pos = 80 + to.count*12
                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.view.frame.size.height - 300))
                autoComplete?.results = self.senders

            }
            else if activeTextField == ccTagField
            {
                let pos = 80 + to.count*12 + cc.count*12

                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.view.frame.size.height))
                autoComplete?.results = self.senders


            }
            else
            {
                let pos = 100 + to.count*12 + cc.count*12 + bcc.count*12

                autoComplete = AutoComplete(frame: CGRect(x: 0, y:  CGFloat(pos) , width: self.view.frame.size.width, height: self.view.frame.size.height))
                autoComplete?.results = self.senders



            }
            print("Sender Height \(senderHeight)")

            if self.autoComplete != nil
            {
                if  !(self.view.subviews.last is AutoComplete)
                {
                    self.view.addSubview(autoComplete!)
                    self.view.setNeedsDisplay()
                }

            }
        }
        else
        {
            if self.autoComplete != nil
            {
                for (_,view) in self.view.subviews.enumerated()
                {
                    if  view is AutoComplete
                    {
                        view.removeFromSuperview()
                        autoComplete = nil
                    }
                }
            }
        }
        autoComplete?.tableView.reloadData()
    }



}

extension ComposeNewMessageViewController : UITextFieldDelegate , UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    // Assign the newly active text field to your activeTextField variable
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if self.autoComplete != nil
        {
            for (_,view) in self.view.subviews.enumerated()
            {
                if  view is AutoComplete
                {
                    view.removeFromSuperview()
                    autoComplete = nil
                }
            }
        }

        if textField == toTagField
        {
            activeTextField = toTagField
        }
        else if textField == ccTagField
        {
            activeTextField = ccTagField

        }
        else // bcc
        {
            activeTextField = bccTagField

        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if self.autoComplete != nil
        {
            for (_,view) in self.view.subviews.enumerated()
            {
                if  view is AutoComplete
                {
                    view.removeFromSuperview()
                    autoComplete = nil
                }
            }
        }

    }
}
