//
//  NewPostPlazzaViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/29/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import IQDropDownTextField

class ComposeNewPostPlazzaViewController: UITableViewController{

    @IBOutlet weak var accountLb: UILabel!
    @IBOutlet weak var plazzaTitleLb: UITextField!
    @IBOutlet weak var plazzaTypeDropDown: IQDropDownTextField!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    private var plazzaTypes:[String] = ["Discussion","Blog Post"]
    fileprivate var composeNewMessagePresenter:ComposeNewMessagePresenter?
    fileprivate var mySignarture :String?

    var channelItem:UserChannelsItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView()
    {
        plazzaTypeDropDown.isOptionalDropDown = false
        plazzaTypeDropDown.itemList = plazzaTypes
        composeNewMessagePresenter = ComposeNewMessagePresenter(self)
        accountLb.text = channelItem?.channelUsername


        messageText.delegate = self
        messageText.textColor = UIColor.lightGray

    }
    @objc public static func create() -> ComposeNewPostPlazzaViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ComposeNewPostPlazzaViewController
    }

    @IBAction func cancelBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func sendBtnAction(_ sender: Any) {

            if  plazzaTitleLb.text?.trim() == ""
            {
                Helper.showFloatAlert(title: "Please add a title for the post ".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            else if messageText.text.trim() == "" || messageText.textColor == UIColor.lightGray
            {
                Helper.showFloatAlert(title: "You can not post a blank message".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

            }
            else
            {
                loading.startAnimating()
                composeNewMessagePresenter?.composePlazzaMessage(channelId: String(describing: channelItem?.id ?? 0), request: PlazzaMessageRequest(plazzaPost: messageText.text.trimmingCharacters(in: .whitespacesAndNewlines), plazzaPostSubject: plazzaTitleLb.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "", plazzaPostType: plazzaTypeDropDown.selectedRow == 0 ? "discussion" : "post"))
                self.dismissKeyboard()

            }

    }
}
extension ComposeNewPostPlazzaViewController:ComposeNewMessagePresenterView
{
    func setErrorReponse(msg: String?) {
        sendBtn?.isEnabled = true
        loading.stopAnimating()
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
    }
    
    func composeMessageSuccess(response: String) {
        loading.stopAnimating()
        Helper.showFloatAlert(title: response, subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        sendBtn?.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.dismiss(animated: true, completion: nil)
            
        })
    }
}

extension ComposeNewPostPlazzaViewController : UITextFieldDelegate , UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
}
