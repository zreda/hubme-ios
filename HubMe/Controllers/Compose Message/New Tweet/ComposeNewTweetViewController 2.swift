//
//  ComposeNewTweetViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/7/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class ComposeNewTweetViewController: UITableViewController {
    @IBOutlet weak var channelImg: UIImageView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var messageContent: UITextView!
    @IBOutlet weak var numberOfChar: UILabel!
    fileprivate let sendBtn = UIButton.init(type: .custom)
    fileprivate var composeNewTweetPresenter:ComposeNewTweetPresenter?
    fileprivate var relogin:ReloginBaseController?
    weak var delegate: RefreshAccountsDelegate?
    
    var channelItem:UserChannelsItem?
    var accountsItem:[UserChannelsItem]?
    var groupedAccount:GroupedChannel?
    var type:userType?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        channelName.text = channelItem?.name
        if (channelItem?.icon?.hasPrefix("http"))! {
            
            channelImg.sd_setImage(with: URL(string: (channelItem?.icon)! ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            
        } else {
            channelImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(channelItem?.icon)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        delegate?.refreshAccount(accountsItem: accountsItem, selectedAccountItem: channelItem)
        //        SVProgressHUD.dismiss()
        self.dismissKeyboard()
    }
    @objc public static func create() -> ComposeNewTweetViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ComposeNewTweetViewController
    }
    
    func initUI()
    {
        
        self.navigationItem.title = "New Tweet".localized()
        //set image for button
        sendBtn.setImage(#imageLiteral(resourceName: "send-active"), for: UIControlState.normal)
        sendBtn.setTitleColor(#colorLiteral(red: 0.9641233087, green: 0.5129789114, blue: 0, alpha: 1), for: UIControlState.normal)
        //add function for button
        sendBtn.addTarget(self, action: #selector(ComposeNewTweetViewController.tweetBtnAction(_:)), for: UIControlEvents.touchUpInside)
        //set frame
        sendBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 51)
        
        let rightButton = UIBarButtonItem(customView: sendBtn)
        
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = rightButton
        messageContent.textColor = UIColor.lightGray
        messageContent.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        messageContent.layer.borderWidth = 1.0
        messageContent.delegate = self
        
    }
    
    @IBAction func tweetBtnAction(_ sender: Any) {
        
        if messageContent.textColor == UIColor.lightGray || messageContent.text == ""
        {
            Helper.showFloatAlert(title: "Add a message to be tweeted".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if  !messageContent.text.isEmpty && messageContent.text.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        {
            Helper.showFloatAlert(title: "Add a message to be tweeted".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            composeNewTweetPresenter = ComposeNewTweetPresenter(self)
            composeNewTweetPresenter?.createNewTweet(channelId:String(describing:channelItem?.id as! Int) , tweetText:messageContent.text!)
            self.dismissKeyboard()
            sendBtn.isEnabled = false
            
        }
        
        
    }
    
    
}

extension ComposeNewTweetViewController : UITextFieldDelegate , UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        numberOfChar.text =  "\(280 - (messageContent.text?.count ?? 0)!) character left"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let content = textView.text else { return true }
        let count = content.count + text.count - range.length
        return count <= 280
    }
    
}


extension ComposeNewTweetViewController:ComposeNewTweetPresenterView
{
    func setError(error: NSError, url: String) {
        
        if HubMeConstants.StatusCode(rawValue: error.code)! == .NotLogin // need to relogin
        {
            
            switch type {
                
            case .some(.flow):
                Helper.showFloatAlert(title: "Account is expired".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                break
            case .some(.channel):
                
                relogin = ReloginBaseController()
                relogin?.controller = self
                relogin?.registerSocket()
                
                relogin?.showReloginDialogue(accountName: channelItem?.channelUsername ?? "", channelName: channelItem?.name ?? "", channelId: String(describing: channelItem?.id as! Int), selectedAccount: channelItem ?? UserChannelsItem())
                break
            case .none:
                print("")
            }
        }
        else
        {
            self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        }
        sendBtn.isEnabled = true
    }
    func setErrorReponse(msg: String?) {
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        sendBtn.isEnabled = true
        
    }
    
    func composeMessageSuccess(response: String) {
        Helper.showFloatAlert(title: "New tweet sent successfully".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
        sendBtn.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            self.navigationController?.popViewController(animated: true)
        })
        
    }
    
    
}
