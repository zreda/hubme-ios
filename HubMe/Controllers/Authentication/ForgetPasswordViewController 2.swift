//
//  ForgetPasswordViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var mailTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var forgetPasswordPresenter: ForgetPasswordPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @objc public static func create() -> ForgetPasswordViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ForgetPasswordViewController
    }
    
    @IBAction func confirmPasswordBtnTapped(_ sender: Any) {
        
        if mailTF.text == ""
        {
            Helper.showFloatAlert(title: NSLocalizedString("email_field_empty", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if !Helper.isValidEmail(mail_address: mailTF.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else
        {
            loading.startAnimating()
            self.forgetPasswordPresenter = ForgetPasswordPresenter(self)
            self.forgetPasswordPresenter?.getForgetPasswordResult(mail: mailTF.text!)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension ForgetPasswordViewController : ForgetPasswordPresenterView
{
    func setSuccess() {
        loading.stopAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func stopLoading() {
        loading.stopAnimating()
    }
    
    func setError(error: NSError) {
        
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: "")
        loading.stopAnimating()
        
    }
    
}
