//
//  RegisterTableViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class RegisterTableViewController: UITableViewController , UITextFieldDelegate {
    
    @IBOutlet weak var fnameTF: DesignableUITextField!
    @IBOutlet weak var lnameTF: DesignableUITextField!
    @IBOutlet weak var phoneNumberTF: DesignableUITextField!
    
    @IBOutlet weak var codeTF: FPNTextField!
    @IBOutlet weak var mailTF: DesignableUITextField!
    
    @IBOutlet weak var passwordTF: DesignableUITextField!
    
    @IBOutlet weak var confirmPAsswordTF: DesignableUITextField!
    
    @IBOutlet weak var agreeBtn: CheckBox!
    
    @IBOutlet weak var agreeText: UILabel!
    @IBOutlet var showPasswordBtn: UIButton!
    @IBOutlet var showConfirmPassBtn: UIButton!
    fileprivate var registerPresenter: RegisterPresenter?
    fileprivate var passwordAppearance:PasswordAppearance = .hide
    fileprivate var confirmPwdAppearance:PasswordAppearance = .hide

    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTF.delegate = self
        codeTF.delegate = self
        codeTF.setFlag(for: FPNCountryCode.FR)
        codeTF.parentViewController = self
        agreeText.underlineText(startLoc: 15)

    }
    
    @objc public static func create() -> RegisterTableViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! RegisterTableViewController
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
    @IBAction func agreeLbTapped(_ sender: Any) {
        let privacyVC = TermsandAgreementViewController.create()
        privacyVC.file = "privacy"
        privacyVC.headerTitle = "Terms and conditions"
        self.navigationController?.pushViewController(privacyVC, animated: true)
    }
    
    
    @IBAction func loginBtnAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func showPwdBtnAction(_ sender: Any) {
         switch passwordAppearance {
         case .hide:
             passwordAppearance = .show
             showPasswordBtn.setImage(#imageLiteral(resourceName: "showPwd"), for: .normal)
             passwordTF.isSecureTextEntry = false
             break
         case .show:
             passwordAppearance = .hide
             showPasswordBtn.setImage(#imageLiteral(resourceName: "notShowPwd"), for: .normal)
             passwordTF.isSecureTextEntry = true
             
             break
             
         }
     }
    
    @IBAction func showConfirmPwdBtnAction(_ sender: Any) {
          switch confirmPwdAppearance {
          case .hide:
              confirmPwdAppearance = .show
              showConfirmPassBtn.setImage(#imageLiteral(resourceName: "showPwd"), for: .normal)
              confirmPAsswordTF.isSecureTextEntry = false
              break
          case .show:
              confirmPwdAppearance = .hide
              showConfirmPassBtn.setImage(#imageLiteral(resourceName: "notShowPwd"), for: .normal)
              confirmPAsswordTF.isSecureTextEntry = true
              
              break
              
          }
      }
    @IBAction func createAcctountBtnTapped(_ sender: Any) {
        
        if fnameTF.text == ""
        {
            Helper.showFloatAlert(title: "fname_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if !(fnameTF.text?.isAlphanumeric)!
        {
            Helper.showFloatAlert(title: "fname_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if lnameTF.text == ""
        {
            Helper.showFloatAlert(title: "lname_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if !(lnameTF.text?.isAlphanumericPlusSpace)!
        {
            Helper.showFloatAlert(title: "lname_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if phoneNumberTF.text == ""
        {
            Helper.showFloatAlert(title: "phone_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if (phoneNumberTF.text?.count)! < 6
        {
            Helper.showFloatAlert(title: "phone_short_msg".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            
        else if (!(phoneNumberTF.text?.isOnlyNumbers)!)
        {
            Helper.showFloatAlert(title: "phone_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if mailTF.text == ""
        {
            Helper.showFloatAlert(title: "mail_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if !Helper.isValidOrangeEmail(mail_address: mailTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if passwordTF.text == ""
        {
            Helper.showFloatAlert(title: "password_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            //        else if !(passwordTF.text?.isAlphanumeric)!
            //        {
            //            Helper.showFloatAlert(title: "password_not_formmtted".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            //        }
        else if (passwordTF.text?.count)! < 8
        {
            Helper.showFloatAlert(title: "password_short_msg".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
           
        else if !Helper.isValidPassword(password: passwordTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))

        {
            Helper.showFloatAlert(title: "password_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

        }
        else if confirmPAsswordTF.text == ""
        {
            Helper.showFloatAlert(title: "confirmPassword_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
            
        else if passwordTF.text != confirmPAsswordTF.text
        {
            Helper.showFloatAlert(title: "confirm_password_not_match".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else if !agreeBtn.isChecked
        {
            Helper.showFloatAlert(title: "please_accept_terms_to_continue".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }
        else
        {
            showActivity()
            registerPresenter = RegisterPresenter(self)
            registerPresenter?.getRegisterResult(auth: SignupRequestModel(email: mailTF.text!, firstName: fnameTF.text!, lastName: lnameTF.text!, password: passwordTF.text!, phoneNumber: phoneNumberTF.text!))
        }
        
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        if textField == codeTF
        {
            return updatedText.count == (codeTF.text?.count)!
        }
        else
        {
            return updatedText.count <= 14
            
        }
    }
}


extension RegisterTableViewController : RegisterPresenterView
{
    func stopLoading() {
        hideActivity()
    }
    func setError(error: NSError) {
        
        hideActivity()
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: "")
        
    }
    func setSuccess(msg: String?, error: String?) {
        
        hideActivity()
        if error != nil
        {
            Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else
        {
            self.navigationController?.popViewController(animated: true, completion: {
                
                Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)
                
                
            })
        }
    }
    
    
}

