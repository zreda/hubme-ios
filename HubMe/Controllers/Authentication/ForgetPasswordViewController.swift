//
//  ForgetPasswordViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var mailTF: DesignableUITextField!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var forgetPasswordPresenter: ForgetPasswordPresenter?
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LoginViewController.keyboardNotification(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil)
        
    }
    
    
    @objc public static func create() -> ForgetPasswordViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! ForgetPasswordViewController
    }
    
    @IBAction func confirmPasswordBtnTapped(_ sender: Any) {
        
        if mailTF.text == ""
        {
            Helper.showFloatAlert(title: NSLocalizedString("email_field_empty", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if !Helper.isValidEmail(mail_address: mailTF.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else
        {
            loading.startAnimating()
            self.forgetPasswordPresenter = ForgetPasswordPresenter(self)
            self.forgetPasswordPresenter?.getForgetPasswordResult(mail: mailTF.text!)
        }
        
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConst?.constant = 30.0
            } else {
                self.bottomConst?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension ForgetPasswordViewController : ForgetPasswordPresenterView
{
    func setSuccess() {
        loading.stopAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func stopLoading() {
        loading.stopAnimating()
    }
    
    func setError(error: NSError) {
        
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: "")
        loading.stopAnimating()
        
    }
    
}
