//
//  LoginViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/15/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SideMenuSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTF: DesignableUITextField!
 
    @IBOutlet weak var passwordTF: DesignableUITextField!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var loginPresenter: LoginPresenter?
    fileprivate var userData:SiginModelReponse?
    
    @IBOutlet weak var showPwdBtn: UIButton!
    fileprivate var passwordAppearance:PasswordAppearance = .hide

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
//        self.navigationController?.navigationBar.isHidden = true
        // test
//        usernameTF.text = "user009@orange.com"
//        passwordTF.text = "123456"
        // Do any additional setup after loading the view.
    }
    @IBAction func showPwdBtnAction(_ sender: Any) {
        switch passwordAppearance {
        case .hide:
            passwordAppearance = .show
            showPwdBtn.setImage(#imageLiteral(resourceName: "showPwd"), for: .normal)
            passwordTF.isSecureTextEntry = false
            break
        case .show:
            passwordAppearance = .hide
            showPwdBtn.setImage(#imageLiteral(resourceName: "notShowPwd"), for: .normal)
            passwordTF.isSecureTextEntry = true
            
            break
            
        }
    }
    
    @IBAction func signinBtnTapped(_ sender: Any) {
        
        if  usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            Helper.showFloatAlert(title: NSLocalizedString("email_field_empty", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if  !Helper.isValidEmail(mail_address:usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
            Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else if  passwordTF.text == "" {
            Helper.showFloatAlert(title: "password_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        }
        else {
            loading.startAnimating()
            self.loginPresenter = LoginPresenter(self)
            self.loginPresenter?.getLoginResult(auth: SigninRequestModel(grant_type: "password", username: usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: passwordTF.text!))
        }
        //            Crashlytics.sharedInstance().crash()
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension LoginViewController: LoginPresenterView {
    
    func setLoginResult(response: SiginModelReponse) {
        userData = response
        loginPresenter?.getUserInfoResult(auth: response.access_token ?? "")

    }
    func setUserInfoResult(rule: HubMeConstants.Authorities) {
        loading.stopAnimating()

        switch rule {
        case .admin:
            Helper.showFloatAlert(title: "invalid_username_password".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            break
        
        case .user :
            Helper.saveObjectDefault(key: HubMeConstants.userDefault.userData, value: userData ?? "")
            Helper.saveObjectDefault(key: HubMeConstants.userDefault.userName, value: usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let tabBarHome = mainSB.instantiateViewController(withIdentifier: "HomeTabBar")
            self.dismiss(animated: false, completion: nil)
            AppDelegate().registerSocket()
            appDelegate.window?.rootViewController = SideMenuController(contentViewController: tabBarHome ,menuViewController: ClientSideMenuViewController.create())
            break
        }
    }
    func setError(error: HubMeConstants.StatusCode,message :String,url:String) {
        
        self.handelError(errorCode: error, messge:message, url: url)
        loading.stopAnimating()
        
    }
}
