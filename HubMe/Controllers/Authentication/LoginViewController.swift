//
//  LoginViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/15/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SideMenuSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTF: DesignableUITextField!
    
    @IBOutlet weak var passwordTF: DesignableUITextField!
    
//    @IBOutlet weak var loading: UIActivityIndicatorView!
    fileprivate var loginPresenter: LoginPresenter?
    fileprivate var userData:SiginModelReponse?
    
    @IBOutlet weak var showPwdBtn: UIButton!
    fileprivate var passwordAppearance:PasswordAppearance = .hide
    fileprivate var manageVersion: ManageAppPresenter?
    
    @IBOutlet var bottomConst: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTF.delegate = self
        passwordTF.delegate = self
        usernameTF.returnKeyType = UIReturnKeyType.go
        passwordTF.returnKeyType = UIReturnKeyType.go
        manageVersion = ManageAppPresenter(self)
        manageVersion?.getAppVersion()
//        NotificationCenter.default.addObserver(self,
//              selector: #selector(LoginViewController.keyboardNotification(notification:)),
//              name: NSNotification.Name.UIKeyboardWillChangeFrame,
//              object: nil)

    }
    @IBAction func showPwdBtnAction(_ sender: Any) {
        switch passwordAppearance {
        case .hide:
            passwordAppearance = .show
            showPwdBtn.setImage(#imageLiteral(resourceName: "showPwd"), for: .normal)
            passwordTF.isSecureTextEntry = false
            break
        case .show:
            passwordAppearance = .hide
            showPwdBtn.setImage(#imageLiteral(resourceName: "notShowPwd"), for: .normal)
            passwordTF.isSecureTextEntry = true
            
            break
            
        }
    }
    
    @IBAction func signinBtnTapped(_ sender: Any) {
        
        login()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func login()
    {
        if  usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                Helper.showFloatAlert(title: NSLocalizedString("email_field_empty", comment: ""), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if  !Helper.isValidOrangeEmail(mail_address:usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines)) {
                Helper.showFloatAlert(title: "email_field_not_formmated".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if  passwordTF.text == "" {
                Helper.showFloatAlert(title: "password_field_empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else {
//                loading.startAnimating()
                self.loginPresenter = LoginPresenter(self)
                self.loginPresenter?.getLoginResult(auth: SigninRequestModel(grant_type: "password", username: usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: passwordTF.text!))
            }
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                self.bottomConst?.constant = 30.0
            } else {
                self.bottomConst?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                                       delay: TimeInterval(0),
                                       options: animationCurve,
                                       animations: { self.view.layoutIfNeeded() },
                                       completion: nil)
        }
    }
    
    
}
extension LoginViewController:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //textField code
        
        textField.resignFirstResponder()
        
       
        if textField == passwordTF && textField.textColor == UIColor.lightGray
        {
            passwordTF.text = ""
            passwordTF.textColor = UIColor.black
        }
        performAction()
        
        return true
    }
   
    func performAction() {
        //action events
        login()
        print("Return ")
    }
}
extension LoginViewController: LoginPresenterView {
    
    func setLoginResult(response: SiginModelReponse) {
        userData = response
        loginPresenter?.getUserInfoResult(auth: response.access_token ?? "")
        
    }
    func setUserInfoResult(rule: HubMeConstants.Authorities) {
//        loading.stopAnimating()
        
        switch rule {
        case .admin:
            Helper.showFloatAlert(title: "invalid_username_password".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            break
        case .user :
            Helper.saveObjectDefault(key: HubMeConstants.userDefault.userData, value: userData ?? "")
            Helper.saveObjectDefault(key: HubMeConstants.userDefault.userName, value: usernameTF.text!.trimmingCharacters(in: .whitespacesAndNewlines))
            loginPresenter?.getRabbitMQInfo()
            break
        }
    }
    func setError(error: HubMeConstants.StatusCode,message :String,url:String) {
        
        self.handelError(errorCode: error, messge:message, url: url)
//        loading.stopAnimating()
        
    }
    func setRabbitMQInfoResult(rabbitMQInfo: RabbitmqConfig) {
        //        rabbitMQInfo
        Helper.saveObjectDefault(key: HubMeConstants.userDefault.rabbitMQSecurity, value: rabbitMQInfo)
        let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let tabBarHome = mainSB.instantiateViewController(withIdentifier: "HomeTabBar") as? UITabBarController
        tabBarHome?.selectedIndex = 2
        self.dismiss(animated: false, completion: nil)
        appDelegate.window?.rootViewController = SideMenuController(contentViewController: tabBarHome! ,menuViewController: ClientSideMenuViewController.create())
        
        AppDelegate().registerSocket()
        
    }
}
extension LoginViewController:ManageAppPresenterView
{
    func setError(error: NSError,url: String)
    {
        
    }
    func setErrorReponse(msg:String?)
    {
        
    }
    func setAppVersion(response: AppVersionModel?)
    {
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        if versionNumber+"."+buildNumber !=  response?.latestVersion
        {

            if Int((versionNumber.components(separatedBy: "."))[0])! > Int((response?.minRequiredVersion?.components(separatedBy: "."))![0])! && (versionNumber+"."+buildNumber) != response?.latestVersion
            {
                let alert = UIAlertController(title: "New Version", message: "New version is availble \(response?.latestVersion!) please update your current version", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            else if  Int((versionNumber.components(separatedBy: "."))[0])! < Int((response?.minRequiredVersion?.components(separatedBy: "."))![0])! // check major minimum version
            {
                
                let alert = UIAlertController(title: "New Version", message: "You must upgrade your app to be able to enjoy using HubMe", preferredStyle: UIAlertControllerStyle.alert)
                self.present(alert, animated: true, completion: nil)
                self.view.isUserInteractionEnabled = false
            }
        }
        
        
        
    }
}
