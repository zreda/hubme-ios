//
//  TermsandAgreementViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class TermsandAgreementViewController: UIViewController , UIWebViewDelegate{
    
    @IBOutlet weak var termsAndAgreementView: UIWebView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    var file:String?
    var headerTitle: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = Bundle.main.url(forResource: file, withExtension: "html")
        
        let request = URLRequest(url: url!)
        
        termsAndAgreementView.loadRequest(request)
        
        termsAndAgreementView.delegate = self
        
        self.navigationItem.title = headerTitle
        // Do any additional setup after loading the view.
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if (webView.isLoading){
            return
        }
        loading.stopAnimating()
    }
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked{
            UIApplication.shared.open(request.url!, options: [:], completionHandler: nil)
            
            return false
        }
        return true
    }
    
    @objc public static func create() -> TermsandAgreementViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! TermsandAgreementViewController
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
