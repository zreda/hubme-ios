//
//  SettingsViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/4/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController

class SettingsViewController: UITableViewController {
    fileprivate var signaturePresenter:SignaturePresenter?
    @IBOutlet weak var userSignature: UILabel!
    @IBOutlet weak var deleteSignatureBtn: UIButton!
    @IBOutlet weak var emailSignature: UILabel!
    fileprivate var signatureText:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        signaturePresenter = SignaturePresenter(self)
        signaturePresenter?.getSignature()
        if signatureText != "" && signatureText != nil
        {
            emailSignature.text = "Update E-mail signture".localized()
        }
        else
        {
            emailSignature.text = "Create E-mail signture".localized()
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.addSignature(notification:)), name: Notification.Name(HubMeConstants.Notifications.updateSignature), object: nil)

    }
    override func viewWillDisappear(_ animated: Bool)
    {
//        if ls_dialogViewController != nil
//        {
//            self.tabBarController?.dismissDialogViewController(LSAnimationPattern.zoomInOut)
//        }
    }
    
    @objc public static func create() -> SettingsViewController {
        
        return UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! SettingsViewController
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 2 // add signature
        {
            // to show the dialog
            let dialogViewController: AddSignatureView = AddSignatureView(nibName:"SignatureView", bundle: nil)
            dialogViewController.signatureStr = signatureText ?? ""
            dialogViewController.delegate = self
            self.tabBarController?.presentDialogViewController(dialogViewController, animationPattern: LSAnimationPattern.zoomInOut, completion: { () -> Void in })
        }
        
        
        if let index = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: index, animated: true)
        }
    }
    
  
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 || indexPath.row == 1
        {
            return 63.0
        }
        else
        {
            return  Helper.heightForText(text: signatureText ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) < 60 ? 100 : Helper.heightForText(text: signatureText ?? "" , Font: UIFont.systemFont(ofSize: 14), Width: 200) + 80

        }
    }
    
     func addSignature(signatureText:String) {
        print(signatureText)
        
        signaturePresenter?.addSignature(signatureText: Signature(signatureText: signatureText))
        self.tabBarController?.dismissDialogViewController(LSAnimationPattern.zoomInOut)

    }
    
    @objc func addSignature(notification: Notification) {
          // Take Action on Notification
         let signatureText = notification.userInfo!["signature"] as! String
        
        signaturePresenter?.addSignature(signatureText: Signature(signatureText: signatureText))

      }

    @IBAction func deleteBtnAction(_ sender: Any) {
        signaturePresenter?.deleteSignature()
    }
    deinit {
           
           NotificationCenter.default.removeObserver(self)
           
       }
}

extension SettingsViewController : SignaturePresenterView
{
    func setNewSignatureSuccess(response: String?)
    {
        if response != "" {
            self.signatureText = response
            userSignature.text = response
            deleteSignatureBtn.isHidden = false
            self.tableView.reloadData()
            if response != ""
            {
                emailSignature.text = "Update E-mail signture".localized()
            }
            else
            {
                emailSignature.text = "Create E-mail signture".localized()

            }
        }
        else
        {
            userSignature.text = ""
            deleteSignatureBtn.isHidden = true
        }
    }
    
    func deleteSignature() {
        self.signatureText = ""
        userSignature.text = ""
        deleteSignatureBtn.isHidden = true
        if signatureText != ""
        {
            emailSignature.text = "Update E-mail signture".localized()
        }
        else
        {
            emailSignature.text = "Create E-mail signture".localized()
            
        }

    }
    func setError(error: NSError,url:String)
    {
        
//        self.tabBarController?.dismissDialogViewController(LSAnimationPattern.zoomInOut)
        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description,url:url)
    }
    
    func setErrorReponse(msg: String?)
    {
        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        hideActivity()
//        self.tabBarController?.dismissDialogViewController(LSAnimationPattern.zoomInOut)
    }
    
    func closeDilogue() {
        
        signaturePresenter?.getSignature()
        self.tabBarController?.dismissDialogViewController(LSAnimationPattern.zoomInOut)
    }
    
    
}



