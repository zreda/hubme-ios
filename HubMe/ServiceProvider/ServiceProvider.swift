//
//  ServiceProvider.swift
//  Intercom
//
//  Created by Zeinab Reda on 3/11/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import Alamofire
import SVProgressHUD

class ServiceProvider {
    let manager = Alamofire.SessionManager.default

    func sendUrl(showActivity: Bool, method: HTTPMethod, URLString: String, withQueryStringParameters parameters: [String: AnyObject]?, withHeaders headers: [String: String]?, paramEncoding: ParameterEncoding? = JSONEncoding.default, completionHandler completion:@escaping (_ :NSObject?, _ :NSError?,_:String?) -> Void) {
        
        manager.session.configuration.timeoutIntervalForRequest = 30
//        manager.session.configuration.httpMaximumConnectionsPerHost = 20
        if self.updateUserInterface() {
            manager.request(URLString, method: method, parameters: parameters, encoding: paramEncoding!, headers: headers).responseJSON { (response) in
                debugPrint("Request: \(String(describing: response.request))")
                debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
                if method == .post  || method == .put {
                    if let body = response.request?.httpBody
                    {
                        debugPrint("Body Paramters: \(NSString(data: body, encoding: String.Encoding.utf8.rawValue) ?? "")")
                    }
                }
                debugPrint("Response: \(String(describing: response.result.value))")
                
                debugPrint("Error: \(String(describing: response.error))")
                debugPrint("Status Code: \(String(describing: response.response?.statusCode))")
                
                
                switch(response.result) {
                    
                case .success(_):
                    
                    if response.response?.statusCode == HubMeConstants.StatusCode.StatusOK.rawValue {
                        if let data = response.result.value {
                            completion(data as? NSObject, nil, URLString)
                            
                        }
                    } else if response.response?.statusCode == HubMeConstants.StatusCode.UserNotAuthorized.rawValue {
                        
                        if let data = response.result.value {
                            
                            completion(data as? NSObject, nil , URLString)
                            
                        }
                        
                    } else if response.response?.statusCode == HubMeConstants.StatusCode.StatusBadRequest.rawValue {
                        
                        if let data = response.result.value {
                            completion(data as? NSObject, NSError(domain: (((response.result.value) as! [String:Any])["message"] as? String) ?? "Bad Request error, please try again", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        
                    } else if response.response?.statusCode == HubMeConstants.StatusCode.requestTimeout.rawValue {
                        //HANDLE TIMEOUT HERE
                        
                        completion(nil, NSError(domain: "Request Timeout , please try again", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        
                    } else  if response.response?.statusCode == HubMeConstants.StatusCode.tooManyResquests.rawValue {
                        completion(response.result.value as? NSObject, NSError(domain: "Too Many Requests", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        
                    }
                    else  if response.response?.statusCode == HubMeConstants.StatusCode.StatusNotfound.rawValue {
                        if URLString != HubMeConstants.Url.user_feedback_url
                        {
                            completion(nil, NSError(domain: "Server Error", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        else
                        {
                            completion(response.result.value as? NSObject, NSError(domain: "User has no feedback", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        
                    }
                    else  if response.response?.statusCode == HubMeConstants.StatusCode.UserForbidden.rawValue {
                        completion((response.result.value as! NSObject) , NSError(domain: "An error occurred while deleting ", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        
                    } else {
                        completion(nil, NSError(domain: "Server Error", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        
//                        Helper.showFloatAlert(title: "Server Error".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                        
                    }
                    
                    break
                    
                case .failure:
                    
                    if response.response?.statusCode == HubMeConstants.StatusCode.StatusBadRequest.rawValue  {
                        
                        completion(nil, NSError(domain: "Bad Request, please try again", code: HubMeConstants.StatusCode.StatusBadRequest.rawValue, userInfo: [:]), URLString)
                        
                    }
                   else if response.response?.statusCode == HubMeConstants.StatusCode.NotLogin.rawValue {
                        
                        completion(nil, NSError(domain: "You need Relogin, please try again", code: HubMeConstants.StatusCode.NotLogin.rawValue, userInfo: [:]), URLString)
                        
                    }
                    else if response.response?.statusCode == HubMeConstants.StatusCode.Failure.rawValue {
                        completion(nil, NSError(domain: "Failed..", code: HubMeConstants.StatusCode.Failure.rawValue, userInfo: [:]), URLString)

                        
                    }
                    else if response.response?.statusCode == HubMeConstants.StatusCode.UserNotAuthorized.rawValue {
                        
                        AuthenticationManager().refreshToken(completion: { (response, error,url) in
                            if error == nil {
                                var newHeader = headers
                                 let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
                                debugPrint("Old Access Token \(accessToken)")
                                debugPrint("New Access Token \(response?.access_token ?? "")")
                                newHeader?["Authorization"] = "\((Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? "") \(accessToken )"
                              completion(nil, NSError(domain: "", code: (error?.code) ?? 0, userInfo: [:]), url)

                            
                            } else {
                                
                                completion(nil, NSError(domain: "Bad Request error, please try again", code: (error?.code)!, userInfo: [:]), url)
                                
                            }
                        })
                        
                    } else if response.response?.statusCode == HubMeConstants.StatusCode.NotFound.rawValue || response.response?.statusCode == HubMeConstants.StatusCode.StatusOK.rawValue {
                        completion(nil, NSError(domain: "Server Error, please try again", code: (response.response?.statusCode) ?? 0, userInfo: [:]), URLString)
                        
                    } else if response.response?.statusCode == HubMeConstants.StatusCode.requestTimeout.rawValue {
                        //HANDLE TIMEOUT HERE
                        
                        completion(nil, NSError(domain: "Request Timeout, please try again", code: (response.response?.statusCode) ?? 0, userInfo: [:]), URLString)
                        
                    }
                    else  if response.response?.statusCode == HubMeConstants.StatusCode.NotFound.rawValue {
                        if URLString != HubMeConstants.Url.user_feedback_url ||  !URLString.contains("\(HubMeConstants().main_url)feed/sender/search?q=")
                        {
                            completion(nil, NSError(domain: "Server Error", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        
                        
                    }
                    else  if response.response?.statusCode == HubMeConstants.StatusCode.StatusNotfound.rawValue {
                        if URLString != HubMeConstants.Url.user_feedback_url || !URLString.contains("\(HubMeConstants().main_url)feed/sender/search?q=")
                        {
                            completion(nil, NSError(domain: "Server Error", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        else
                        {
                            completion(response.result.value as? NSObject, NSError(domain: "User has no feedback", code: (response.response?.statusCode)!, userInfo: [:]), URLString)
                        }
                        
                    }
                    else {
                        
                        if !URLString.contains("\(HubMeConstants().main_url)feed/sender/search?q=")
                        {
//                            Helper.showFloatAlert(title: "Server Error".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

                        }
//                        completion(nil, NSError(domain: "Server Error", code: (response.response?.statusCode) ?? -1, userInfo: [:]), URLString)

                    }
                    
                    break
                    
                }
                
            }
        }
        
    }
    
    func stopAllSessions() {
        manager.session.getAllTasks { tasks in
            
            if tasks.count > 1
            {
                tasks[0].cancel()
            }
//            tasks.forEach {
//                $0.cancel()
//            }
        }
    }
    func sendUrlString(showActivity: Bool, method: HTTPMethod, URLString: String, withQueryStringParameters parameters: [String: AnyObject]?, withHeaders headers: [String: String]?, paramEncoding: ParameterEncoding? = JSONEncoding.default, completionHandler completion:@escaping (_ :NSObject, _ :NSError?) -> Void) {
        //        if showActivity
        //        {
        //            SVProgressHUD.show()
        //        }
        
        manager.request(URLString, method: method, parameters: parameters, encoding: paramEncoding!, headers: headers).responseString { (response) in
            debugPrint("Request: \(String(describing: response.request))")
            debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            debugPrint("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            
            debugPrint("Response: \(String(describing: response.result.value))")
            
            debugPrint("Error: \(String(describing: response.error))")
            
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == HubMeConstants.StatusCode.StatusOK.rawValue {
                    
                    if let data = response.result.value {
                        completion(data as NSObject, nil)
                        
                    }
                } else if response.response?.statusCode == HubMeConstants.StatusCode.StatusBadRequest.rawValue {
                    Helper.showFloatAlert(title: "Bad Request error, please try again", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                } else if response.response?.statusCode == HubMeConstants.StatusCode.UserNotAuthorized.rawValue {
                    Helper.showFloatAlert(title: response.result.value!, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                    if let data = response.result.value {
                        completion(data as NSObject, NSError(domain: "", code: (response.response?.statusCode)!, userInfo: ["UserNotAuthorized":"401"]))
                        
                    }
                } else if response.response?.statusCode == HubMeConstants.StatusCode.UserForbidden.rawValue {
                    Helper.showFloatAlert(title: "User Forbidden", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                } else if response.response?.statusCode == HubMeConstants.StatusCode.StatusNotfound.rawValue {
                    if URLString != HubMeConstants.Url.user_feedback_url
                    {
                        Helper.showFloatAlert(title: "User Not Found", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    }
                    
                } else if response.response?.statusCode == HubMeConstants.StatusCode.Failure.rawValue {
                    Helper.showFloatAlert(title: "Service Failure", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                } else if response.response?.statusCode == HubMeConstants.StatusCode.Unavailable.rawValue {
                    Helper.showFloatAlert(title: "Service Unavailable", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                } else {
                    Helper.showFloatAlert(title: "Server Error , please try again", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                    
                }
                SVProgressHUD.dismiss()
                
                break
                
            case .failure(_):
                
                Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
                SVProgressHUD.dismiss()
                
                break
                
            }
            
        }
        
    }
    
    func sendMultiPartUrl(showActivity: Bool, method: HTTPMethod, URLString: String, withQueryStringParameters parameters: [String: AnyObject]?, withHeaders headers: [String: String]?, photos: [String: UIImage], completionHandler completion:@escaping (_ :NSObject, _ :NSError?) -> Void) {
        if showActivity {
            SVProgressHUD.show()
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in photos {
                
                multipartFormData.append(UIImageJPEGRepresentation(value, 0.5)!, withName: key, fileName: "img.jpeg", mimeType: "image/jpg")
                
            }
            
            for (key, value) in parameters! {
                
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                
            }
            
        }, to: URLString) {
            (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    debugPrint("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
                    debugPrint("Request: \(String(describing: response.request))")
                    debugPrint("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
                    //                    print("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
                    
                    debugPrint("Response: \(String(describing: response.result.value))")
                    
                    debugPrint("Error: \(String(describing: response.error))")
                    
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        
                        SVProgressHUD.dismiss()
                        completion(response.result.value as! NSObject, nil)
                        
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                debugPrint(encodingError)
                Helper.showAlert(type: HubMeConstants.AlertType.AlertError, title: "error".localized(), subTitle: ("please try again"), closeTitle: "close".localized())
                
            }
            
        }
        
    }
    
    
    func updateUserInterface() -> Bool {
        guard let status = Network.reachability?.status else { return false }
        switch status {
        case .unreachable:
            Helper.showFloatAlert(title: "no_internet".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            return false
        case .wifi:
            print("Connected")
        case .wwan:
            print("Connected")
            
        }
        print("Reachability Summary")
        print("Status:", status)
        print("HostName:", Network.reachability?.hostname ?? "nil")
        print("Reachable:", Network.reachability?.isReachable ?? "nil")
        print("Wifi:", Network.reachability?.isReachableViaWiFi ?? "nil")
        return true
    }
//    @objc func statusManager(_ notification: Notification) {
//        updateUserInterface()
//    }
    
}
