//
//  TextField.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/29/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

extension UITextField {
    func setBottomBorder(_color:UIColor? = #colorLiteral(red: 0.6571614146, green: 0.6571771502, blue: 0.6571686864, alpha: 1)) {
        self.borderStyle = .none
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = _color?.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
