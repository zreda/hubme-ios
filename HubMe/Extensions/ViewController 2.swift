//
//  ViewController.swift
//

class MyTapGesture: UITapGestureRecognizer {
    var index = Int()
    var eventIndex = Int()

}
//private let swizzling: (UIViewController.Type) -> Void = { viewController in
//
//    let originalSelector = #selector(viewController.viewWillDisappear(_:))
//    let swizzledSelector = #selector(viewController.proj_viewWillDisappear(animated:))
//
//    let originalMethod = class_getInstanceMethod(viewController, originalSelector)
//    let swizzledMethod = class_getInstanceMethod(viewController, swizzledSelector)
//
//    method_exchangeImplementations(originalMethod!, swizzledMethod!)
//
//}

private let swizzling: (UIViewController.Type) -> () = { viewController in
    
    let originalSelector = #selector(viewController.viewDidLoad)
    let swizzledSelector = #selector(viewController.viewDidLoad)
    
    let originalMethod = class_getInstanceMethod(viewController, originalSelector)
    let swizzledMethod = class_getInstanceMethod(viewController, swizzledSelector)
    
    let didAddMethod = class_addMethod(viewController, originalSelector, method_getImplementation(swizzledMethod!), method_getTypeEncoding(swizzledMethod!))
    
    if didAddMethod {
        class_replaceMethod(viewController, swizzledSelector, method_getImplementation(originalMethod!), method_getTypeEncoding(originalMethod!))
    } else {
        method_exchangeImplementations(originalMethod!, swizzledMethod!)
    }
}


import SVProgressHUD
extension UIViewController {
    
    func initialize() {
        // make sure this isn't a subclass
        guard self === UIViewController.self else { return }
                swizzling(self)
    }
    

    @objc func proj_viewWillDisappear(animated: Bool) {
        proj_viewWillDisappear(animated: animated)
        //        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized(), style: .plain, target: nil, action: nil)
        
        SVProgressHUD.dismiss()
        
    }
    func showActivity() {
        hideActivity()
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        actInd.center = view.center
        actInd.tag = 200
        actInd.color = #colorLiteral(red: 0.9479395747, green: 0.526617229, blue: 0, alpha: 1)
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        actInd.color = UIColor.orange
        view.addSubview(actInd)
        actInd.startAnimating()
    }
    
    func hideActivity() {
        if view.viewWithTag(200) != nil {
            let actInd: UIActivityIndicatorView = self.view.viewWithTag(200) as! UIActivityIndicatorView
            actInd.stopAnimating()
            actInd.color = #colorLiteral(red: 0.9479395747, green: 0.526617229, blue: 0, alpha: 1)
            view.viewWithTag(200)?.removeFromSuperview()
        }
    }
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func handelError(errorCode: HubMeConstants.StatusCode, messge: String,url:String) {
        
        switch errorCode {
            
        case .StatusOK :
            
            debugPrint("Error".localized())
            
        case .tooManyResquests :
            
            print("Too Many Requests".localized())
            
        case .StatusBadRequest:
            if url == HubMeConstants.Url.login_url &&  !self.isKind(of: LoginViewController.self)
            {
                Helper.goToLoginViewController(controller: self)

            }
            else if self.isKind(of: LoginViewController.self)
            {
                
                Helper.showFloatAlert(title: messge, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if self.isKind(of: LoginChannelViewController.self) {
                Helper.showFloatAlert(title: "This user already logged on this channel".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if self.isKind(of: SettingsViewController.self) || (self.isKind(of: ReplyViewController.self) && !url.contains("/meeting/action"))
            {
                debugPrint("No Action")
            }
            else if self.isKind(of: RegisterTableViewController.self) ||  self.isKind(of: EventDetailsViewController.self) || self.isKind(of: PeopleInvitedViewController.self) || self.isKind(of: FeedsDetailsTableViewController.self)
            {
                if self.isKind(of: PeopleInvitedViewController.self) || self.isKind(of: EventDetailsViewController.self) || self.isKind(of: FeedsDetailsTableViewController.self)
                {
                    Helper.showFloatAlert(title: "No Profile Found", subTitle: "", type: HubMeConstants.AlertType.AlertError)

                }
                else
                {
                    Helper.showFloatAlert(title: messge, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                
            }
            else if  url.contains("/meeting/action")
            {
                Helper.showFloatAlert(title: "You are already responded to this meeting".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

            }
           
        case .requestTimeout:
             if url.contains("/actions/tweet")
            {
                Helper.showFloatAlert(title: "You have already sent this Tweet".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else
             {
                Helper.showFloatAlert(title: "ErrorTimeOut".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
            
        case .ServerTimeout:
            Helper.showFloatAlert(title: "ErrorTimeOut".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        case .UserForbidden:
            
            Helper.showFloatAlert(title: "An error occurred while deleting".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        case .NotFound:
           debugPrint("Error".localized())
          
        case .Failure:
            Helper.showFloatAlert(title: "Failure".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

            debugPrint("Error".localized())

        case .UserNotAuthorized :
            
            Helper.showFloatAlert(title: "User Unauthorized".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
            
        case .StatusNone :
            
            if self.isKind(of: LoginViewController.self) {

                Helper.showFloatAlert(title: messge, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if self.isKind(of: LoginChannelViewController.self) {
                Helper.showFloatAlert(title: "This user already logged on this channel".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if self.isKind(of: SettingsViewController.self) ||  self.isKind(of: ReplyViewController.self)
            {
                debugPrint("No Action")
            }
            else if self.isKind(of: RegisterTableViewController.self)
            {
                Helper.showFloatAlert(title: messge, subTitle: "", type: HubMeConstants.AlertType.AlertError)
                
            }
            else if url == HubMeConstants.Url.login_url &&  !self.isKind(of: LoginViewController.self )
            {
                Helper.goToLoginViewController(controller: self)
                
            }
            return
        case .StatusNotfound :
            
            return

            
        default:
            if !url.contains("\(HubMeConstants().main_url)feed/sender/search?q=")
            {
//                 Helper.showFloatAlert(title: messge, subTitle: "", type: HubMeConstants.AlertType.AlertError)
            }
        }
        
    }
}

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}
extension UINavigationController {
    
    func popViewController(animated: Bool, completion: @escaping () -> ()) {
        popViewController(animated: animated)
        
        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}
