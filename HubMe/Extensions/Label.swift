//
//  Label.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

extension UILabel {
    func underlineText(startLoc:Int) {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(kCTUnderlineStyleAttributeName as NSAttributedStringKey, value: NSUnderlineStyle.styleSingle.rawValue, range: NSRange(location: startLoc, length: attributedString.length-startLoc))
            attributedText = attributedString

        }
    }
}
