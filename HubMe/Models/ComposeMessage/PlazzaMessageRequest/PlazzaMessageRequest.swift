//
//  File.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/29/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class PlazzaMessageRequest : NSObject, NSCoding, Mappable{
    
    var plazzaPost : String?
    var plazzaPostSubject : String?
    var plazzaPostType : String?

    
    init( plazzaPost : String ,plazzaPostSubject : String? ,plazzaPostType : String? ) {
        self.plazzaPost = plazzaPost
        self.plazzaPostSubject = plazzaPostSubject
        self.plazzaPostType = plazzaPostType

    }
    class func newInstance(map: Map) -> Mappable?{
        return PlazzaMessageRequest()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        plazzaPost <- map["plazzaPost"]
        plazzaPostSubject <- map["plazzaPostSubject"]
        plazzaPostType <- map["plazzaPostType"]

    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        plazzaPost = aDecoder.decodeObject(forKey: "plazzaPost") as? String
        plazzaPostSubject = aDecoder.decodeObject(forKey: "plazzaPostSubject") as? String
        plazzaPostType = aDecoder.decodeObject(forKey: "plazzaPostType") as? String

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if plazzaPost != nil{
            aCoder.encode(plazzaPost, forKey: "plazzaPost")
        }
        if plazzaPostSubject != nil{
            aCoder.encode(plazzaPostSubject, forKey: "plazzaPostSubject")
        }
        if plazzaPostType != nil{
            aCoder.encode(plazzaPostType, forKey: "plazzaPostType")
        }
        
    }
    
}
