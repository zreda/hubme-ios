//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ComposeMessageRequestModel : NSObject, NSCoding, Mappable{

	var bcc : [Bcc]?
	var cc : [Bcc]?
	var from : String?
	var messageText : String?
	var subject : String?
	var to : [Bcc]?


	class func newInstance(map: Map) -> Mappable?{
		return ComposeMessageRequestModel()
	}
    override init() {
        
    }
	required init?(map: Map){
        
    }

	func mapping(map: Map)
	{
		bcc <- map["bcc"]
		cc <- map["cc"]
		from <- map["from"]
		messageText <- map["messageText"]
		subject <- map["subject"]
		to <- map["to"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bcc = aDecoder.decodeObject(forKey: "bcc") as? [Bcc]
         cc = aDecoder.decodeObject(forKey: "cc") as? [Bcc]
         from = aDecoder.decodeObject(forKey: "from") as? String
         messageText = aDecoder.decodeObject(forKey: "messageText") as? String
         subject = aDecoder.decodeObject(forKey: "subject") as? String
         to = aDecoder.decodeObject(forKey: "to") as? [Bcc]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bcc != nil{
			aCoder.encode(bcc, forKey: "bcc")
		}
		if cc != nil{
			aCoder.encode(cc, forKey: "cc")
		}
		if from != nil{
			aCoder.encode(from, forKey: "from")
		}
		if messageText != nil{
			aCoder.encode(messageText, forKey: "messageText")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}
		if to != nil{
			aCoder.encode(to, forKey: "to")
		}

	}

}
