//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ReorderFlowsModelRequest : NSObject, NSCoding, Mappable{

	var orderedUserFlows : [PositionModel]?


	class func newInstance(map: Map) -> Mappable?{
		return ReorderFlowsModelRequest()
	}
	required init?(map: Map){}
	 override init(){}

	func mapping(map: Map)
	{
		orderedUserFlows <- map["orderedUserFlows"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         orderedUserFlows = aDecoder.decodeObject(forKey: "orderedUserFlows") as? [PositionModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if orderedUserFlows != nil{
			aCoder.encode(orderedUserFlows, forKey: "orderedUserFlows")
		}

	}

}
