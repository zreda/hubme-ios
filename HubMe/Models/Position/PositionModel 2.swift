//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class PositionModel : NSObject, NSCoding, Mappable{

	var id : Int?
	var position : Int?


	class func newInstance(map: Map) -> Mappable?{
		return PositionModel()
	}
    init( id : Int ,position : Int) {
        self.id = id
        self.position = position
    }
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		position <- map["position"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? Int
         position = aDecoder.decodeObject(forKey: "position") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if position != nil{
			aCoder.encode(position, forKey: "position")
		}

	}

}
