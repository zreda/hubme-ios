//
//	TimelineEventDTO.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class TimelineEventDTO : NSObject, NSCoding, Mappable{

	var accountName : String?
	var body : String?
	var channelName : String?
	var end : Int?
	var id : String?
	var isCancelled : Bool?
	var isRecurring : Bool?
	var location : String?
	var myResponse : String?
	var optionalAttendees : [OptionalAttendee]?
	var organizer : String?
	var recurrence : Recurrence?
	var requiredAttendees : [OptionalAttendee]?
	var start : Int?
	var subject : String?
	var userChannelId : AnyObject?


	class func newInstance(map: Map) -> Mappable?{
		return TimelineEventDTO()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		accountName <- map["accountName"]
		body <- map["body"]
		channelName <- map["channelName"]
		end <- map["end"]
		id <- map["id"]
		isCancelled <- map["isCancelled"]
		isRecurring <- map["isRecurring"]
		location <- map["location"]
		myResponse <- map["myResponse"]
		optionalAttendees <- map["optionalAttendees"]
		organizer <- map["organizer"]
		recurrence <- map["recurrence"]
		requiredAttendees <- map["requiredAttendees"]
		start <- map["start"]
		subject <- map["subject"]
		userChannelId <- map["userChannelId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountName = aDecoder.decodeObject(forKey: "accountName") as? String
         body = aDecoder.decodeObject(forKey: "body") as? String
         channelName = aDecoder.decodeObject(forKey: "channelName") as? String
         end = aDecoder.decodeObject(forKey: "end") as? Int
         id = aDecoder.decodeObject(forKey: "id") as? String
         isCancelled = aDecoder.decodeObject(forKey: "isCancelled") as? Bool
         isRecurring = aDecoder.decodeObject(forKey: "isRecurring") as? Bool
         location = aDecoder.decodeObject(forKey: "location") as? String
         myResponse = aDecoder.decodeObject(forKey: "myResponse") as? String
         optionalAttendees = aDecoder.decodeObject(forKey: "optionalAttendees") as? [OptionalAttendee]
         organizer = aDecoder.decodeObject(forKey: "organizer") as? String
         recurrence = aDecoder.decodeObject(forKey: "recurrence") as? Recurrence
         requiredAttendees = aDecoder.decodeObject(forKey: "requiredAttendees") as? [OptionalAttendee]
         start = aDecoder.decodeObject(forKey: "start") as? Int
         subject = aDecoder.decodeObject(forKey: "subject") as? String
         userChannelId = aDecoder.decodeObject(forKey: "userChannelId") as? AnyObject

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accountName != nil{
			aCoder.encode(accountName, forKey: "accountName")
		}
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if channelName != nil{
			aCoder.encode(channelName, forKey: "channelName")
		}
		if end != nil{
			aCoder.encode(end, forKey: "end")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isCancelled != nil{
			aCoder.encode(isCancelled, forKey: "isCancelled")
		}
		if isRecurring != nil{
			aCoder.encode(isRecurring, forKey: "isRecurring")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if myResponse != nil{
			aCoder.encode(myResponse, forKey: "myResponse")
		}
		if optionalAttendees != nil{
			aCoder.encode(optionalAttendees, forKey: "optionalAttendees")
		}
		if organizer != nil{
			aCoder.encode(organizer, forKey: "organizer")
		}
		if recurrence != nil{
			aCoder.encode(recurrence, forKey: "recurrence")
		}
		if requiredAttendees != nil{
			aCoder.encode(requiredAttendees, forKey: "requiredAttendees")
		}
		if start != nil{
			aCoder.encode(start, forKey: "start")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}
		if userChannelId != nil{
			aCoder.encode(userChannelId, forKey: "userChannelId")
		}

	}

}