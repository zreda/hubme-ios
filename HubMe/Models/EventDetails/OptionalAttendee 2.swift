//
//	OptionalAttendee.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class OptionalAttendee : NSObject, NSCoding, Mappable{

	var email : String?
	var name : String?
    var status :String?

    init( email : String , name : String ,status :String) {
        self.email = email
        self.name = name
        self.status = status
    }
	required init?(map: Map){}

	func mapping(map: Map)
	{
		email <- map["email"]
		name <- map["name"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObject(forKey: "email") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}

	}

}
