//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class EventDetailsResponse : NSObject, NSCoding, Mappable{

	var errorMessage : String?
	var timelineEventDTO : TimelineEventDTO?


	class func newInstance(map: Map) -> Mappable?{
		return EventDetailsResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		errorMessage <- map["errorMessage"]
		timelineEventDTO <- map["timelineEventDTO"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         errorMessage = aDecoder.decodeObject(forKey: "errorMessage") as? String
         timelineEventDTO = aDecoder.decodeObject(forKey: "timelineEventDTO") as? TimelineEventDTO

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "errorMessage")
		}
		if timelineEventDTO != nil{
			aCoder.encode(timelineEventDTO, forKey: "timelineEventDTO")
		}

	}

}
