//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FeedbackModel : NSObject, NSCoding, Mappable{

	var commentText : String?
	var id : Int?
	var rating : Int?


	class func newInstance(map: Map) -> Mappable?{
		return FeedbackModel()
	}
	required init?(map: Map){}
	private override init(){}
    
     init(commentText : String ,rating : Int) {
        self.commentText = commentText
        self.rating = rating
    }

	func mapping(map: Map)
	{
		commentText <- map["commentText"]
		id <- map["id"]
		rating <- map["rating"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         commentText = aDecoder.decodeObject(forKey: "commentText") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         rating = aDecoder.decodeObject(forKey: "rating") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if commentText != nil{
			aCoder.encode(commentText, forKey: "commentText")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}

	}

}
