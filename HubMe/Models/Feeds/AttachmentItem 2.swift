//
//  AttachmentItem.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/23/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class AttachmentItem: Mappable {

    var type: String?
    var url: String?

    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        type                 <- map["type"]
        url                  <- map["url"]

    }
}
