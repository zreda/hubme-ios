//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FeedDetailResponse : NSObject, NSCoding, Mappable{

	var messageId : String?
	var text : String?


	class func newInstance(map: Map) -> Mappable?{
		return FeedDetailResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		messageId <- map["messageId"]
		text <- map["text"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         messageId = aDecoder.decodeObject(forKey: "messageId") as? String
         text = aDecoder.decodeObject(forKey: "text") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if messageId != nil{
			aCoder.encode(messageId, forKey: "messageId")
		}
		if text != nil{
			aCoder.encode(text, forKey: "text")
		}

	}

}
