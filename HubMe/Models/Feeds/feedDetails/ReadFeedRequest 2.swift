//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ReadFeedRequest : NSObject, NSCoding, Mappable{

	var date : String?
	var messageId : String?
	var userChannelId : String?

    override init() {
        self.date = ""
        self.messageId  = ""
        self.userChannelId = ""
    }
    
    init( date : String , messageId : String ,userChannelId : String) {
        self.date = date
        self.messageId  = messageId
        self.userChannelId = userChannelId
    }

	class func newInstance(map: Map) -> Mappable?{
		return ReadFeedRequest()
	}
	required init?(map: Map){}

	func mapping(map: Map)
	{
		date <- map["date"]
		messageId <- map["messageId"]
		userChannelId <- map["userChannelId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         date = aDecoder.decodeObject(forKey: "date") as? String
         messageId = aDecoder.decodeObject(forKey: "messageId") as? String
         userChannelId = aDecoder.decodeObject(forKey: "userChannelId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if date != nil{
			aCoder.encode(date, forKey: "date")
		}
		if messageId != nil{
			aCoder.encode(messageId, forKey: "messageId")
		}
		if userChannelId != nil{
			aCoder.encode(userChannelId, forKey: "userChannelId")
		}

	}

}
