//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper


class FeedsResponse : NSObject, NSCoding, Mappable{
    
    var messages : [MessageItem]?
    var nextPageIndex : String?
    var error : String?

    
    
    class func newInstance(map: Map) -> Mappable?{
        return FeedsResponse()
    }
    required init?(map: Map){}
    private override init(){}
    
    func mapping(map: Map)
    {
        messages <- map["messages"]
        nextPageIndex <- map["nextPageIndex"]
        error <- map["error"]

        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        messages = aDecoder.decodeObject(forKey: "messages") as? [MessageItem]
        nextPageIndex = aDecoder.decodeObject(forKey: "nextPageIndex") as? String
        error = aDecoder.decodeObject(forKey: "error") as? String

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if messages != nil{
            aCoder.encode(messages, forKey: "messages")
        }
        if nextPageIndex != nil{
            aCoder.encode(nextPageIndex, forKey: "nextPageIndex")
        }
        if error != nil{
            aCoder.encode(error, forKey: "error")
        }
        
    }
    
}
