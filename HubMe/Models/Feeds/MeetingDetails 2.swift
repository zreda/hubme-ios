//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class MeetingDetails : NSObject, NSCoding, Mappable{

	var endDate : Int?
	var location : String?
	var recurrence : RecurrenceModel?
	var startDate : Int?


	class func newInstance(map: Map) -> Mappable?{
		return MeetingDetails()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		endDate <- map["endDate"]
		location <- map["location"]
		recurrence <- map["recurrence"]
		startDate <- map["startDate"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         endDate = aDecoder.decodeObject(forKey: "endDate") as? Int
         location = aDecoder.decodeObject(forKey: "location") as? String
         recurrence = aDecoder.decodeObject(forKey: "recurrence") as? RecurrenceModel
         startDate = aDecoder.decodeObject(forKey: "startDate") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if endDate != nil{
			aCoder.encode(endDate, forKey: "endDate")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if recurrence != nil{
			aCoder.encode(recurrence, forKey: "recurrence")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "startDate")
		}

	}

}
