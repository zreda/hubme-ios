//
//  MessageItem.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class MessageItem: Mappable {

    var userChannelId :String?
    var messageId:String?
    var date: String?
    var from: String?
    var snippet: String?
    var title: String?
    var desc: String?
    var channel: String?
    var options: OptionItem?
    var iconUrl: String?
    var isRead: Bool?
    var channelUsername:String?
    var channelPassword:String?
    var attachments: [AttachmentItem]?
    var supportedActions:[String]?
    var isExpanded:Bool = false


    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        userChannelId        <- map["userChannelId"]
        isRead               <- map["isRead"]
        messageId            <- map["messageId"]
        date                 <- map["date"]
        from                 <- map["from"]
        snippet              <- map["snippet"]
        title                <- map["title"]
        desc                 <- map["desc"]
        channel              <- map["channel"]
        options              <- map["options"]
        iconUrl              <- map["iconUrl"]
        channelUsername      <- map["channelUsername"]
        channelPassword      <- map["channelPassword"]
        attachments          <- map["attachments"]
        supportedActions     <- map["supportedActions"]
    }
}
