//
//  OptionItem.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class OptionItem: Mappable {

    var next: String?
    var numberOfUnreadMessages: String?
    var pageNumber: Int?
    var totalMailsNumber: Int?
    var isRead: Bool?
    var lastId: String?
    var cc: [Bcc]?
    var to: [Bcc]?
    var bcc: [Bcc]?
    var screenName: String?
    var mailId: String?
    var plazzaType: String?
    var plazzaIn: String?
    var plazzaTypeUrl: String?
    var accountAddress:Bcc?
    var fromAddress:Bcc?
    var messageType:String?
    var meetingDetails:MeetingDetails?

    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {

        next                                   <- map["next"]
        numberOfUnreadMessages                 <- map["numberOfUnreadMessages"]
        pageNumber                             <- map["pageNumber"]
        totalMailsNumber                       <- map["totalMailsNumber"]
        isRead                                 <- map["isRead"]
        lastId                                 <- map["lastId"]
        screenName                             <- map["screenName"]
        cc                                     <- map["cc"]
        bcc                                    <- map["bcc"]
        to                                     <- map["to"]
        accountAddress                         <- map["accountAddress"]
        fromAddress                            <- map["fromAddress"]
        mailId                                 <- map["mailId"]
        plazzaIn                               <- map["plazzaIn"]
        plazzaTypeUrl                          <- map["plazzaTypeUrl"]
        plazzaType                             <- map["plazzaType"]
        messageType                           <- map["messageType"]
        meetingDetails                        <- map["meetingDetails"]

    }
}
