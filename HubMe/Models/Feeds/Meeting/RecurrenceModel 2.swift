//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class RecurrenceModel : NSObject, NSCoding, Mappable{

	var dayOfMonth : AnyObject?
	var dayOfTheWeek : AnyObject?
	var dayOfTheWeekIndex : AnyObject?
	var daysOfWeek : AnyObject?
	var endDate : Int?
	var interval : Int?
	var month : AnyObject?
	var numberOfOccurrences : Int?
	var recurrenceAsString : String?
	var recurrencePattern : String?
	var startDate : Int?


	class func newInstance(map: Map) -> Mappable?{
		return RecurrenceModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		dayOfMonth <- map["dayOfMonth"]
		dayOfTheWeek <- map["dayOfTheWeek"]
		dayOfTheWeekIndex <- map["dayOfTheWeekIndex"]
		daysOfWeek <- map["daysOfWeek"]
		endDate <- map["endDate"]
		interval <- map["interval"]
		month <- map["month"]
		numberOfOccurrences <- map["numberOfOccurrences"]
		recurrenceAsString <- map["recurrenceAsString"]
		recurrencePattern <- map["recurrencePattern"]
		startDate <- map["startDate"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dayOfMonth = aDecoder.decodeObject(forKey: "dayOfMonth") as? AnyObject
         dayOfTheWeek = aDecoder.decodeObject(forKey: "dayOfTheWeek") as? AnyObject
         dayOfTheWeekIndex = aDecoder.decodeObject(forKey: "dayOfTheWeekIndex") as? AnyObject
         daysOfWeek = aDecoder.decodeObject(forKey: "daysOfWeek") as? AnyObject
         endDate = aDecoder.decodeObject(forKey: "endDate") as? Int
         interval = aDecoder.decodeObject(forKey: "interval") as? Int
         month = aDecoder.decodeObject(forKey: "month") as? AnyObject
         numberOfOccurrences = aDecoder.decodeObject(forKey: "numberOfOccurrences") as? Int
         recurrenceAsString = aDecoder.decodeObject(forKey: "recurrenceAsString") as? String
         recurrencePattern = aDecoder.decodeObject(forKey: "recurrencePattern") as? String
         startDate = aDecoder.decodeObject(forKey: "startDate") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dayOfMonth != nil{
			aCoder.encode(dayOfMonth, forKey: "dayOfMonth")
		}
		if dayOfTheWeek != nil{
			aCoder.encode(dayOfTheWeek, forKey: "dayOfTheWeek")
		}
		if dayOfTheWeekIndex != nil{
			aCoder.encode(dayOfTheWeekIndex, forKey: "dayOfTheWeekIndex")
		}
		if daysOfWeek != nil{
			aCoder.encode(daysOfWeek, forKey: "daysOfWeek")
		}
		if endDate != nil{
			aCoder.encode(endDate, forKey: "endDate")
		}
		if interval != nil{
			aCoder.encode(interval, forKey: "interval")
		}
		if month != nil{
			aCoder.encode(month, forKey: "month")
		}
		if numberOfOccurrences != nil{
			aCoder.encode(numberOfOccurrences, forKey: "numberOfOccurrences")
		}
		if recurrenceAsString != nil{
			aCoder.encode(recurrenceAsString, forKey: "recurrenceAsString")
		}
		if recurrencePattern != nil{
			aCoder.encode(recurrencePattern, forKey: "recurrencePattern")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "startDate")
		}

	}

}
