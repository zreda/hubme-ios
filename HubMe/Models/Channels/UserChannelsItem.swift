//
//  userChannelsItem.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/24/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class UserChannelsItem: NSObject, Mappable {
    var id: Int?
    var channelId: Int?
    var name: String?
    var icon: String?
    var date: String?
    var firstLoadConfig : FirstLoadConfig?
    var firstLoadStatus :String?
    var channelUsername:String?
    var channelPassword:String?
    var supportedActions:[String]?
    var authenticationType : String?
    var connectorTopicName : String?
    var domain : String?
    var label : String?
    var newTextMaxLength : Int?
    var readIconUrl : String?
    var topicName : String?
    var url : String?
    var subChannelId : String?
    var teamId : String?
    var subChannelName : String?
    var teamName : String?
    var isChecked:Bool?
    var accountName:String?



    override init() {
        self.id = 0
        self.name = ""
        self.icon = ""
        self.date = ""
        self.firstLoadStatus = ""
        self.accountName = ""


    }
    init(id: Int, name: String, iconUrl: String ) {
        self.id = id
        self.name = name
        self.icon = iconUrl
        self.date =  ""
        self.firstLoadStatus = ""
        self.accountName = ""


    }
    init(id: Int, name: String, iconUrl: String, firstLoadStatus:String) {
        self.id = id
        self.name = name
        self.icon = iconUrl
        self.date = ""
        self.firstLoadStatus = ""
    }
    
    init(id: Int, name: String, iconUrl: String, date: String , firstLoadStatus:String) {
        self.id = id
        self.name = name
        self.icon = iconUrl
        self.date = date
        self.firstLoadStatus = firstLoadStatus
    }
    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        id                    <- map["id"]
        name                  <- map["name"]
        icon                  <- map["icon"]
        date                  <- map["date"]
        firstLoadConfig       <- map["firstLoadConfig"]
        firstLoadStatus       <- map["firstLoadStatus"]
        channelUsername       <- map["channelUsername"]
        channelPassword       <- map["channelPassword"]
        supportedActions      <- map["supportedActions"]
        authenticationType    <- map["authenticationType"]
        connectorTopicName    <- map["connectorTopicName"]
        domain                <- map["domain"]
        firstLoadConfig       <- map["firstLoadConfig"]
        label                 <- map["label"]
        newTextMaxLength      <- map["newTextMaxLength"]
        readIconUrl           <- map["readIconUrl"]
        topicName             <- map["topicName"]
        url                   <- map["url"]
        channelId             <- map["channelId"]
        subChannelId          <- map["subChannelId"]
        teamId                <- map["teamId"]
        subChannelName        <- map["subChannelName"]
        teamName              <- map["teamName"]
        accountName           <- map["accountName"]


    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        channelId = aDecoder.decodeObject(forKey: "channelId") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        icon = aDecoder.decodeObject(forKey: "icon") as? String
        date = aDecoder.decodeObject(forKey: "date") as? String
        firstLoadConfig = aDecoder.decodeObject(forKey: "FirstLoadConfig") as? FirstLoadConfig
        firstLoadStatus = aDecoder.decodeObject(forKey: "firstLoadStatus") as? String
        channelUsername = aDecoder.decodeObject(forKey: "channelUsername") as? String
        channelPassword = aDecoder.decodeObject(forKey: "channelPassword") as? String
        supportedActions = aDecoder.decodeObject(forKey: "supportedActions") as? [String]
        authenticationType = aDecoder.decodeObject(forKey: "authenticationType") as? String
        connectorTopicName = aDecoder.decodeObject(forKey: "connectorTopicName") as? String
        domain = aDecoder.decodeObject(forKey: "domain") as? String
        label = aDecoder.decodeObject(forKey: "label") as? String
        newTextMaxLength = aDecoder.decodeObject(forKey: "newTextMaxLength") as? Int
        readIconUrl = aDecoder.decodeObject(forKey: "readIconUrl") as? String
        topicName = aDecoder.decodeObject(forKey: "topicName") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
        subChannelId = aDecoder.decodeObject(forKey: "subChannelId") as? String
        teamId = aDecoder.decodeObject(forKey: "teamId") as? String
        subChannelName = aDecoder.decodeObject(forKey: "subChannelName") as? String
        teamName = aDecoder.decodeObject(forKey: "teamName") as? String
        isChecked = aDecoder.decodeObject(forKey: "isChecked") as? Bool

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if channelId != nil{
            aCoder.encode(channelId, forKey: "channelId")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if icon != nil{
            aCoder.encode(icon, forKey: "icon")
        }
        if firstLoadConfig != nil{
            aCoder.encode(firstLoadConfig, forKey: "firstLoadConfig")
        }
        if firstLoadStatus != nil{
            aCoder.encode(firstLoadStatus, forKey: "firstLoadStatus")
        }
        if channelUsername != nil{
            aCoder.encode(channelUsername, forKey: "channelUsername")
        }
        if channelPassword != nil{
            aCoder.encode(channelPassword, forKey: "channelPassword")
        }
        if supportedActions != nil{
            aCoder.encode(supportedActions, forKey: "supportedActions")
        }
        if authenticationType != nil{
            aCoder.encode(authenticationType, forKey: "authenticationType")
        }
        if connectorTopicName != nil{
            aCoder.encode(connectorTopicName, forKey: "connectorTopicName")
        }
        if domain != nil{
            aCoder.encode(domain, forKey: "domain")
        }
        if label != nil{
            aCoder.encode(label, forKey: "label")
        }
        if readIconUrl != nil{
            aCoder.encode(readIconUrl, forKey: "readIconUrl")
        }
        
        if topicName != nil{
            aCoder.encode(topicName, forKey: "topicName")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if subChannelId != nil{
            aCoder.encode(subChannelId, forKey: "subChannelId")
        }
        if teamId != nil{
            aCoder.encode(teamId, forKey: "teamId")
        }
        if teamId != nil{
            aCoder.encode(teamId, forKey: "teamId")
        }
        if subChannelName != nil{
            aCoder.encode(subChannelName, forKey: "subChannelName")
        }
        if teamName != nil{
            aCoder.encode(teamName, forKey: "teamName")
        }
        if isChecked != nil{
            aCoder.encode(isChecked, forKey: "isChecked")
        }
        
    }
    
    
}

