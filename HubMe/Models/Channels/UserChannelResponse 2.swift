//
//  UserChannelResponse.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class UserChannelResponse: Mappable {
    var response: [UserChannelsItem]?
    var message: String?
    var status:String?
    var authenticationURL:String?
    var userChannelId:Int?


    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        response             <- map["response"]
        message              <- map["message"]
        status               <- map["status"]
        authenticationURL   <- map["authenticationURL"]
        userChannelId       <- map["userChannelId"]
    }
}
