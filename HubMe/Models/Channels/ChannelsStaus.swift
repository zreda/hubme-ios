//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class ChannelsStaus : NSObject, NSCoding, Mappable{

	var status : String?
	var userChannelId : String?


	class func newInstance(map: Map) -> Mappable?{
		return ChannelsStaus()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		status <- map["status"]
		userChannelId <- map["userChannelId"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         status = aDecoder.decodeObject(forKey: "status") as? String
         userChannelId = aDecoder.decodeObject(forKey: "userChannelId") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if userChannelId != nil{
			aCoder.encode(userChannelId, forKey: "userChannelId")
		}

	}

}
