//
//  GroupedChannel.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/28/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
enum SelectStatus : Int
{
    case selectAll = 0
    case deSelectAll = 1
    case multiple = 2
}
class GroupedChannel: NSObject {
    
    var groupName:String?
    var groupId:Int?
    var accountId:Int?
    var accounts:[UserChannelsItem]?
    var firstConfig:FirstLoadConfig?
    var isSelectAll:SelectStatus = .multiple

    
    override init()
    {
        groupName = ""
        accounts = []
    }
    
    init(groupId:Int , groupName:String,firstConfig:FirstLoadConfig,accounts:[UserChannelsItem])
    {
        self.groupId = groupId
        self.groupName = groupName
        self.firstConfig = firstConfig
        self.accounts = accounts
    }
    

    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        groupName = aDecoder.decodeObject(forKey: "groupName") as? String
        groupId = aDecoder.decodeObject(forKey: "groupId") as? Int
        accounts = aDecoder.decodeObject(forKey: "accounts") as? [UserChannelsItem]
        firstConfig = aDecoder.decodeObject(forKey: "firstConfig") as? FirstLoadConfig
        isSelectAll = aDecoder.decodeObject(forKey: "isSelectAll") as? SelectStatus ?? SelectStatus.multiple
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if groupName != nil{
            aCoder.encode(groupName, forKey: "groupName")
        }
        if groupId != nil{
            aCoder.encode(groupId, forKey: "groupId")
        }
        if accounts != nil{
            aCoder.encode(accounts, forKey: "accounts")
        }
        if firstConfig != nil{
            aCoder.encode(firstConfig, forKey: "firstConfig")
        }
        if isSelectAll != nil{
            aCoder.encode(isSelectAll, forKey: "isSelectAll")
        }
    }

}
