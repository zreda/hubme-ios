//
//  FlowItemResponse.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper

class FlowItemResponse: Mappable {
    var response: [FlowItem]?
    var message: String?

    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        response             <- map["response"]
        message              <- map["message"]
    }
}
