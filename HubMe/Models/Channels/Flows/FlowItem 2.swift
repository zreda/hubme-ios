//
//  FlowItem.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class FlowItem: NSObject, Mappable {

    var id: Int?
    var name: String?
    var channels: [Channel]?
    var rules: [Rule]?

    override init() {
        self.id = 0
        self.name = ""
    }
    init(id: Int, name: String) {
        self.id = id
        self.name = name

    }
    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        id                <- map["id"]
        name              <- map["name"]
        channels          <- map["channels"]
        rules             <- map["rules"]
    }
}
