//
//	Channel.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper

class Channel: NSObject, NSCoding, Mappable {

	var icon: String?
	var id: Int?
	var label: String?
	var name: String?
	var readIconUrl: String?
	var selected: Bool?
    var channelUsername:String?

	class func newInstance(map: Map) -> Mappable? {
		return Channel()
	}
	required init?(map: Map) {}

	 override init() {

    }

     init( id: Int?, name: String?) {
        self.id = id
        self.name = name
    }
    init( id: Int?, username: String?) {
        self.id = id
        self.channelUsername = username
    }
	func mapping(map: Map) {
		icon <- map["icon"]
		id <- map["id"]
		label <- map["label"]
		name <- map["name"]
		readIconUrl <- map["readIconUrl"]
		selected <- map["selected"]
        channelUsername <- map["channelUsername"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder) {
         icon = aDecoder.decodeObject(forKey: "icon") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         label = aDecoder.decodeObject(forKey: "label") as? String
         name = aDecoder.decodeObject(forKey: "name") as? String
         readIconUrl = aDecoder.decodeObject(forKey: "readIconUrl") as? String
         selected = aDecoder.decodeObject(forKey: "selected") as? Bool
        channelUsername = aDecoder.decodeObject(forKey: "channelUsername") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
		if icon != nil {
			aCoder.encode(icon, forKey: "icon")
		}
		if id != nil {
			aCoder.encode(id, forKey: "id")
		}
		if label != nil {
			aCoder.encode(label, forKey: "label")
		}
		if name != nil {
			aCoder.encode(name, forKey: "name")
		}
		if readIconUrl != nil {
			aCoder.encode(readIconUrl, forKey: "readIconUrl")
		}
		if selected != nil {
			aCoder.encode(selected, forKey: "selected")
		}
        if selected != nil {
            aCoder.encode(selected, forKey: "channelUsername")
        }
	}

}
