//
//	Rule.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper

class FlowRuleModel: NSObject, NSCoding, Mappable {

	var channels: [Channel]?
	var keywords: [String]?
	var senders: [SendersModel]?

	class func newInstance(map: Map) -> Mappable? {
		return Rule()
	}
	required init?(map: Map) {}
	private override init() {}

	func mapping(map: Map) {
		channels <- map["channels"]
		keywords <- map["keywords"]
		senders <- map["senders"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder) {
         channels = aDecoder.decodeObject(forKey: "channels") as? [Channel]
         keywords = aDecoder.decodeObject(forKey: "keywords") as? [String]
         senders = aDecoder.decodeObject(forKey: "senders") as? [SendersModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
		if channels != nil {
			aCoder.encode(channels, forKey: "channels")
		}
		if keywords != nil {
			aCoder.encode(keywords, forKey: "keywords")
		}
		if senders != nil {
			aCoder.encode(senders, forKey: "senders")
		}

	}

}
