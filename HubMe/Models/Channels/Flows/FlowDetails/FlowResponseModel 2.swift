//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper

class FlowResponseModel: NSObject, NSCoding, Mappable {

	var id: Int?
	var name: String?
	var rules: [FlowRuleModel]?

	class func newInstance(map: Map) -> Mappable? {
		return FlowResponseModel()
	}
	required init?(map: Map) {}
    override init() {

    }

	func mapping(map: Map) {
		id <- map["id"]
		name <- map["name"]
		rules <- map["rules"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder) {
         id = aDecoder.decodeObject(forKey: "id") as? Int
         name = aDecoder.decodeObject(forKey: "name") as? String
         rules = aDecoder.decodeObject(forKey: "rules") as? [FlowRuleModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
		if id != nil {
			aCoder.encode(id, forKey: "id")
		}
		if name != nil {
			aCoder.encode(name, forKey: "name")
		}
		if rules != nil {
			aCoder.encode(rules, forKey: "rules")
		}

	}

}
