//
//	Rule.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper

class Rule: NSObject, NSCoding, Mappable {

	var selectedChannels: [Int]?
	var keywords: [String]?
	var senders: [SendersModel]?
    var channels: [Channel]?

	class func newInstance(map: Map) -> Mappable? {
		return Rule()
	}
	required init?(map: Map) {

    }
    override init() {
        selectedChannels = []
        keywords = []
        senders = []
        channels = []

    }

    init( selectedChannels: [Int], keywords: [String], senders: [SendersModel], channels: [Channel]?) {
        self.selectedChannels = selectedChannels
        self.keywords = keywords
        self.senders = senders
        self.channels = channels

    }

	func mapping(map: Map) {
        channels <- map["channels"]
		selectedChannels <- map["channels"]
		keywords <- map["keywords"]
		senders <- map["senders"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder) {
         selectedChannels = aDecoder.decodeObject(forKey: "selectedChannels") as? [Int]
         channels = aDecoder.decodeObject(forKey: "channels") as? [Channel]
         keywords = aDecoder.decodeObject(forKey: "keywords") as? [String]
         senders = aDecoder.decodeObject(forKey: "senders") as? [SendersModel]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder) {
        if selectedChannels != nil {
            aCoder.encode(channels, forKey: "selectedChannels")
        }
		if channels != nil {
			aCoder.encode(channels, forKey: "channels")
		}
		if keywords != nil {
			aCoder.encode(keywords, forKey: "keywords")
		}
		if senders != nil {
			aCoder.encode(senders, forKey: "senders")
		}

	}

}
