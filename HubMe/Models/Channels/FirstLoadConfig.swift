//
//	FirstLoadConfig.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FirstLoadConfig : NSObject, NSCoding, Mappable{

	var criteria : String?
	var maxCount : Int?
	var minCount : Int?
	var periods : [Int]?
    var firstLoadFetchValue :Int?


	class func newInstance(map: Map) -> Mappable?{
		return FirstLoadConfig()
	}
	required init?(map: Map){}
    override init(){}

    init (criteria:String ,firstLoadFetchValue:Int )
    {
        self.criteria = criteria
        self.firstLoadFetchValue = firstLoadFetchValue
    }
    init (criteria:String ,periods:[Int] )
    {
        self.criteria = criteria
        self.periods = periods
    }
    
	func mapping(map: Map)
	{
		criteria <- map["criteria"]
		maxCount <- map["maxCount"]
		minCount <- map["minCount"]
		periods <- map["periods"]
        firstLoadFetchValue <- map["firstLoadFetchValue"]

		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         criteria = aDecoder.decodeObject(forKey: "criteria") as? String
         maxCount = aDecoder.decodeObject(forKey: "maxCount") as? Int
         minCount = aDecoder.decodeObject(forKey: "minCount") as? Int
         periods = aDecoder.decodeObject(forKey: "periods") as? [Int]
        firstLoadFetchValue = aDecoder.decodeObject(forKey: "firstLoadFetchValue") as? Int


	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if criteria != nil{
			aCoder.encode(criteria, forKey: "criteria")
		}
		if maxCount != nil{
			aCoder.encode(maxCount, forKey: "maxCount")
		}
		if minCount != nil{
			aCoder.encode(minCount, forKey: "minCount")
		}
		if periods != nil{
			aCoder.encode(periods, forKey: "periods")
		}
        if firstLoadFetchValue != nil{
            aCoder.encode(periods, forKey: "firstLoadFetchValue")
        }


	}

}
