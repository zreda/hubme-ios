//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class NotificationResponse : NSObject, NSCoding, Mappable{

	var accountName : String?
	var channelName : String?
	var iconUrl : String?
	var id : Int?
	var message : String?
	var messageDate : Int?
	var numberOfMsgs : String?
	var read : Bool?
    var open : Bool?
	var type : String?
	var userChannelId : String?
	var userName : String?


	class func newInstance(map: Map) -> Mappable?{
		return NotificationResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		accountName <- map["accountName"]
		channelName <- map["channelName"]
		iconUrl <- map["iconUrl"]
		id <- map["id"]
		message <- map["message"]
		messageDate <- map["messageDate"]
		numberOfMsgs <- map["numberOfMsgs"]
		read <- map["read"]
        open <- map["open"]
		type <- map["type"]
		userChannelId <- map["userChannelId"]
		userName <- map["userName"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountName = aDecoder.decodeObject(forKey: "accountName") as? String
         channelName = aDecoder.decodeObject(forKey: "channelName") as? String
         iconUrl = aDecoder.decodeObject(forKey: "iconUrl") as? String
         id = aDecoder.decodeObject(forKey: "id") as? Int
         message = aDecoder.decodeObject(forKey: "message") as? String
         messageDate = aDecoder.decodeObject(forKey: "messageDate") as? Int
         numberOfMsgs = aDecoder.decodeObject(forKey: "numberOfMsgs") as? String
         read = aDecoder.decodeObject(forKey: "read") as? Bool
         open = aDecoder.decodeObject(forKey: "open") as? Bool
         type = aDecoder.decodeObject(forKey: "type") as? String
         userChannelId = aDecoder.decodeObject(forKey: "userChannelId") as? String
         userName = aDecoder.decodeObject(forKey: "userName") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accountName != nil{
			aCoder.encode(accountName, forKey: "accountName")
		}
		if channelName != nil{
			aCoder.encode(channelName, forKey: "channelName")
		}
		if iconUrl != nil{
			aCoder.encode(iconUrl, forKey: "iconUrl")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if messageDate != nil{
			aCoder.encode(messageDate, forKey: "messageDate")
		}
		if numberOfMsgs != nil{
			aCoder.encode(numberOfMsgs, forKey: "numberOfMsgs")
		}
		if read != nil{
			aCoder.encode(read, forKey: "read")
		}
        if open != nil{
            aCoder.encode(open, forKey: "open")
        }
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userChannelId != nil{
			aCoder.encode(userChannelId, forKey: "userChannelId")
		}
		if userName != nil{
			aCoder.encode(userName, forKey: "userName")
		}

	}

}
