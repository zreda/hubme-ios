//
//  TimeLine.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/12/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class TimeLine: NSObject {

    var startTime:Double?
    var events:[TimeLineAppointments]?
    var sameDayAppointments :[TimeLineAppointments]?
    override init() {
        startTime = 0.0
        events = []
    }
    init(startTime:Double , events:[TimeLineAppointments]) {
        self.startTime = startTime
        self.events = events
    }
}
