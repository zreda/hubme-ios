//
//  LoginRequestModel.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/24/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

class LoginRequestModel: NSObject {

    var email: String?
    var password: String?
    var account: String?
    var channel_id: String?
    var channel_name:String?
    var authToken:String?
    var authTokenSecret:String?
    var serverAuthCode:String?
    var firstLoadConfig : FirstLoadConfig?
    var verificationCode :String?
    var teamName: String?
    var subChannelName: String?
    var expired:Bool = false

    override init() {
        self.email = ""
        self.password = ""
        self.account = ""
        self.channel_id = ""
        self.verificationCode = ""
        self.subChannelName = ""
        self.teamName = ""
        self.expired = false
    }
    
    init(expired:Bool,teamName: String ,subChannelName: String , firstLoadConfig:FirstLoadConfig,channel_id: String ) {
        self.teamName = teamName
        self.subChannelName = subChannelName
        self.firstLoadConfig = firstLoadConfig
        self.channel_id = channel_id
        self.expired = expired

    }
    
    init(expired:Bool,email: String, serverAuthCode: String?,channel_id: String , channel_name:String , firstLoadConfig:FirstLoadConfig?) {

        self.email = email
        self.serverAuthCode = serverAuthCode
        self.channel_name = channel_name
        self.channel_id = channel_id
        self.firstLoadConfig = firstLoadConfig
        self.expired = expired
    }
    
    init(expired:Bool,channel_id: String , channel_name:String , firstLoadConfig:FirstLoadConfig?) {
        
        self.channel_name = channel_name
        self.channel_id = channel_id
        self.firstLoadConfig = firstLoadConfig
        self.expired = expired
    }
    init(expired:Bool,authToken: String, authTokenSecret: String?,email: String, password: String?, channel_id: String ,channel_name:String , firstLoadConfig:FirstLoadConfig?) {

        self.authToken = authToken
        self.authTokenSecret = authTokenSecret
        self.channel_id = channel_id
        self.channel_name = channel_name
        self.email = email
        self.password = password
        self.firstLoadConfig = firstLoadConfig
        self.expired = expired
    }

    
    init(expired:Bool,email: String, password: String?,verificationCode:String?,account:String?, channel_id: String ,channel_name:String , firstLoadConfig:FirstLoadConfig?) {
        
        self.email = email
        self.password = password
        self.account = account
        self.channel_id = channel_id
        self.channel_name = channel_name
        self.firstLoadConfig = firstLoadConfig
        self.verificationCode = verificationCode
        self.expired = expired

    }
}
