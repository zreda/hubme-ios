//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class BadResponseModel : NSObject, NSCoding, Mappable{

	var error : String?
	var errorDescription : String?


	class func newInstance(map: Map) -> Mappable?{
		return BadResponseModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		error <- map["error"]
		errorDescription <- map["error_description"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         error = aDecoder.decodeObject(forKey: "error") as? String
         errorDescription = aDecoder.decodeObject(forKey: "error_description") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if error != nil{
			aCoder.encode(error, forKey: "error")
		}
		if errorDescription != nil{
			aCoder.encode(errorDescription, forKey: "error_description")
		}

	}

}
