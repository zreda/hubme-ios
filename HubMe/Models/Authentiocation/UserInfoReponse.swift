//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class UserInfoReponse : NSObject, NSCoding, Mappable{

	var authorities : [String]?
	var email : String?


	class func newInstance(map: Map) -> Mappable?{
		return UserInfoReponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		authorities <- map["authorities"]
		email <- map["email"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         authorities = aDecoder.decodeObject(forKey: "authorities") as? [String]
         email = aDecoder.decodeObject(forKey: "email") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if authorities != nil{
			aCoder.encode(authorities, forKey: "authorities")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}

	}

}
