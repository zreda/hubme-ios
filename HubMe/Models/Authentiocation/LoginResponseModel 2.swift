//
//  LoginResponseModel.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/22/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class LoginResponseModel: NSObject, Mappable {
    var message: String?
    var status: Int?

    override init() {

    }
    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        message                 <- map["message"]
        status                  <- map["status"]
    }
}
