//
//	RabbitmqConfig.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class RabbitmqConfig : NSObject, NSCoding, Mappable{

	var exchangeUrl : String?
	var exchangeUrlUser : String?
	var password : String?
	var url : String?
	var username : String?


	class func newInstance(map: Map) -> Mappable?{
		return RabbitmqConfig()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		exchangeUrl <- map["exchangeUrl"]
		exchangeUrlUser <- map["exchangeUrlUser"]
		password <- map["password"]
		url <- map["url"]
		username <- map["username"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         exchangeUrl = aDecoder.decodeObject(forKey: "exchangeUrl") as? String
         exchangeUrlUser = aDecoder.decodeObject(forKey: "exchangeUrlUser") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         url = aDecoder.decodeObject(forKey: "url") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if exchangeUrl != nil{
			aCoder.encode(exchangeUrl, forKey: "exchangeUrl")
		}
		if exchangeUrlUser != nil{
			aCoder.encode(exchangeUrlUser, forKey: "exchangeUrlUser")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if url != nil{
			aCoder.encode(url, forKey: "url")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}