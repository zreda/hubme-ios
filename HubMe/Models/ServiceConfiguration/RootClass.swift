//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct RabbitMQRConfigResponse : Codable {

	let message : String?
	let response : RabbitmqConfigData?


	enum CodingKeys: String, CodingKey {
		case message = "message"
		case response
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		response = try RabbitmqConfigData(from: decoder)
	}


}
