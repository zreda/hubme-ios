//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class RabbitmqConfigData : NSObject, NSCoding, Mappable{

	var rabbitmqConfig : RabbitmqConfig?


	class func newInstance(map: Map) -> Mappable?{
		return RabbitmqConfigData()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		rabbitmqConfig <- map["rabbitmqConfig"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         rabbitmqConfig = aDecoder.decodeObject(forKey: "rabbitmqConfig") as? RabbitmqConfig

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if rabbitmqConfig != nil{
			aCoder.encode(rabbitmqConfig, forKey: "rabbitmqConfig")
		}

	}

}
