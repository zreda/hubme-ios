//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class AppVersionResponse : NSObject, NSCoding, Mappable{

	var aNDROID : AppVersionModel?
	var iOS : AppVersionModel?


	class func newInstance(map: Map) -> Mappable?{
		return AppVersionResponse()
	}
	private override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
	{
		aNDROID <- map["ANDROID"]
		iOS <- map["IOS"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        aNDROID = aDecoder.decodeObject(forKey: "ANDROID") as? AppVersionModel
        iOS = aDecoder.decodeObject(forKey: "IOS") as? AppVersionModel

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if aNDROID != nil{
            aCoder.encode(aNDROID, forKey: "ANDROID")
		}
		if iOS != nil{
            aCoder.encode(iOS, forKey: "IOS")
		}

	}

}
