//
//	ANDROID.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class AppVersionModel : NSObject, NSCoding, Mappable{

	var latestVersion : String?
	var minRequiredVersion : String?


	class func newInstance(map: Map) -> Mappable?{
		return AppVersionModel()
	}
	private override init(){}
    required init?(map: Map){}

    func mapping(map: Map)
	{
		latestVersion <- map["latestVersion"]
		minRequiredVersion <- map["minRequiredVersion"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    required init(coder aDecoder: NSCoder)
	{
         latestVersion = aDecoder.decodeObject(forKey: "latestVersion") as? String
         minRequiredVersion = aDecoder.decodeObject(forKey: "minRequiredVersion") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if latestVersion != nil{
			aCoder.encodeConditionalObject(latestVersion, forKey: "latestVersion")
		}
		if minRequiredVersion != nil{
			aCoder.encodeConditionalObject(minRequiredVersion, forKey: "minRequiredVersion")
		}

	}
   
}
