//
//  File.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

class TimeLineEvent: NSObject {

    var startTime:Double?
    var events:[TimeLineAppointments]?
    
    override init() {
        startTime = 0.0
        events = []
    }
     init(startTime:Double , events:[TimeLineAppointments]) {
        self.startTime = startTime
        self.events = events
    }

}
