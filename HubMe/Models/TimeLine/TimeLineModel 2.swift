//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class TimeLineModel : NSObject, NSCoding, Mappable{

	var appointments : [TimeLineAppointments]?
	var errorMessage : String?


	class func newInstance(map: Map) -> Mappable?{
		return TimeLineModel()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		appointments <- map["appointments"]
		errorMessage <- map["errorMessage"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         appointments = aDecoder.decodeObject(forKey: "appointments") as? [TimeLineAppointments]
         errorMessage = aDecoder.decodeObject(forKey: "errorMessage") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if appointments != nil{
			aCoder.encode(appointments, forKey: "appointments")
		}
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "errorMessage")
		}

	}

}
