//
//    RootClass.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import ObjectMapper


class TimeLineRequestModel : NSObject, NSCoding, Mappable{
    
    var endDate : String?
    var startDate : String?
    var userChannelIds : [Int]?
    
    
    class func newInstance(map: Map) -> Mappable?{
        return TimeLineRequestModel()
    }
    required init?(map: Map){}
    private override init(){}
    
    init(startDate:String , endDate : String ,userChannelIds : [Int]) {
        self.startDate = startDate
        self.endDate = endDate
        self.userChannelIds = userChannelIds
        
    }
    init(fromDictionary dictionary: [String:Any]){
        endDate = dictionary["endDate"] as? String
        startDate = dictionary["startDate"] as? String
        userChannelIds = dictionary["userChannelIds"] as? [Int]
    }
    func mapping(map: Map)
    {
        endDate <- map["endDate"]
        startDate <- map["startDate"]
        userChannelIds <- map["userChannelIds"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        endDate = aDecoder.decodeObject(forKey: "endDate") as? String
        startDate = aDecoder.decodeObject(forKey: "startDate") as? String
        userChannelIds = aDecoder.decodeObject(forKey: "userChannelIds") as? [Int]
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if endDate != nil{
            aCoder.encode(endDate, forKey: "endDate")
        }
        if startDate != nil{
            aCoder.encode(startDate, forKey: "startDate")
        }
        if userChannelIds != nil{
            aCoder.encode(userChannelIds, forKey: "userChannelIds")
        }
        
    }
    
}
