//
//	Appointment.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class TimeLineAppointments : NSObject, NSCoding, Mappable{

	var accountName : String?
	var channelName : String?
	var end : Double?
	var id : String?
	var isCancelled : Bool?
	var isRecurring : Bool?
	var location : String?
	var myResponse : String?
	var organizer : String?
	var start : Double?
	var subject : String?
    var userChannelId:String?

	class func newInstance(map: Map) -> Mappable?{
		return TimeLineAppointments()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		accountName <- map["accountName"]
		channelName <- map["channelName"]
		end <- map["end"]
		id <- map["id"]
		isCancelled <- map["isCancelled"]
		isRecurring <- map["isRecurring"]
		location <- map["location"]
		myResponse <- map["myResponse"]
		organizer <- map["organizer"]
		start <- map["start"]
		subject <- map["subject"]
        userChannelId <- map["userChannelId"]

	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         accountName = aDecoder.decodeObject(forKey: "accountName") as? String
         channelName = aDecoder.decodeObject(forKey: "channelName") as? String
         end = aDecoder.decodeObject(forKey: "end") as? Double
         id = aDecoder.decodeObject(forKey: "id") as? String
         isCancelled = aDecoder.decodeObject(forKey: "isCancelled") as? Bool
         isRecurring = aDecoder.decodeObject(forKey: "isRecurring") as? Bool
         location = aDecoder.decodeObject(forKey: "location") as? String
         myResponse = aDecoder.decodeObject(forKey: "myResponse") as? String
         organizer = aDecoder.decodeObject(forKey: "organizer") as? String
         start = aDecoder.decodeObject(forKey: "start") as? Double
         subject = aDecoder.decodeObject(forKey: "subject") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if accountName != nil{
			aCoder.encode(accountName, forKey: "accountName")
		}
		if channelName != nil{
			aCoder.encode(channelName, forKey: "channelName")
		}
		if end != nil{
			aCoder.encode(end, forKey: "end")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if isCancelled != nil{
			aCoder.encode(isCancelled, forKey: "isCancelled")
		}
		if isRecurring != nil{
			aCoder.encode(isRecurring, forKey: "isRecurring")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if myResponse != nil{
			aCoder.encode(myResponse, forKey: "myResponse")
		}
		if organizer != nil{
			aCoder.encode(organizer, forKey: "organizer")
		}
		if start != nil{
			aCoder.encode(start, forKey: "start")
		}
		if subject != nil{
			aCoder.encode(subject, forKey: "subject")
		}

	}

}
