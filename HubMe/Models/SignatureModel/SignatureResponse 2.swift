//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class SignatureResponse : NSObject, NSCoding, Mappable{

	var message : String?
	var signature : Signature?


	class func newInstance(map: Map) -> Mappable?{
		return SignatureResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		message <- map["message"]
		signature <- map["signature"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         message = aDecoder.decodeObject(forKey: "message") as? String
         signature = aDecoder.decodeObject(forKey: "signature") as? Signature

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if signature != nil{
			aCoder.encode(signature, forKey: "signature")
		}

	}

}
