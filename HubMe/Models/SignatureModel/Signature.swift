//
//	Signature.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Signature : NSObject, NSCoding, Mappable{

	var id : String?
	var signatureText : String?
	var username : String?


	class func newInstance(map: Map) -> Mappable?{
		return Signature()
	}
	required init?(map: Map){}
    
     init(signatureText : String) {
        self.signatureText = signatureText
    }
	private override init(){}

	func mapping(map: Map)
	{
		id <- map["id"]
		signatureText <- map["signatureText"]
		username <- map["username"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         id = aDecoder.decodeObject(forKey: "id") as? String
         signatureText = aDecoder.decodeObject(forKey: "signatureText") as? String
         username = aDecoder.decodeObject(forKey: "username") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if signatureText != nil{
			aCoder.encode(signatureText, forKey: "signatureText")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
