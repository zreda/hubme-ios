//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class SignupRequestModel : NSObject, NSCoding, Mappable{

	var email : String?
	var firstName : String?
	var lastName : String?
	var password : String?
	var phoneNumber : String?


	class func newInstance(map: Map) -> Mappable?{
		return SignupRequestModel()
	}
	required init?(map: Map){}
	private override init(){}
    
    
     init( email : String , firstName : String , lastName : String ,password : String ,phoneNumber : String) {
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.password = password
        self.phoneNumber = phoneNumber
    }

	func mapping(map: Map)
	{
		email <- map["email"]
		firstName <- map["firstName"]
		lastName <- map["lastName"]
		password <- map["password"]
		phoneNumber <- map["phoneNumber"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         email = aDecoder.decodeObject(forKey: "email") as? String
         firstName = aDecoder.decodeObject(forKey: "firstName") as? String
         lastName = aDecoder.decodeObject(forKey: "lastName") as? String
         password = aDecoder.decodeObject(forKey: "password") as? String
         phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "firstName")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "lastName")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phoneNumber != nil{
			aCoder.encode(phoneNumber, forKey: "phoneNumber")
		}

	}

}
