//
//  SiginModelReponse.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import ObjectMapper

class SiginModelReponse: NSObject, NSCoding, Mappable {

    var access_token: String?
    var token_type: String?
    var refresh_token: String?
    var expires_in: Int?
    var scope: String?
    var message: String?
    var username: String?


    override init() {

    }
    
    init(username:String) {
        self.username = username
    }
    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        access_token                <- map["access_token"]
        token_type                  <- map["token_type"]
        refresh_token               <- map["refresh_token"]
        expires_in                  <- map["expires_in"]
        scope                       <- map["scope"]
        message                     <- map["message"]
        username                    <- map["username"]


    }

    required init(coder decoder: NSCoder) {

        self.access_token = decoder.decodeObject(forKey: "access_token") as? String ?? ""
        self.token_type = decoder.decodeObject(forKey: "token_type") as? String ?? ""
        self.refresh_token = decoder.decodeObject(forKey: "refresh_token") as? String ?? ""
        self.expires_in = decoder.decodeObject(forKey: "expires_in") as? Int ?? 0
        self.scope = decoder.decodeObject(forKey: "scope") as? String ?? ""
        self.username = decoder.decodeObject(forKey: "username") as? String ?? ""


    }

    func encode(with coder: NSCoder) {

        coder.encode(access_token, forKey: "access_token")
        coder.encode(token_type, forKey: "token_type")
        coder.encode(refresh_token, forKey: "refresh_token")
        coder.encode(expires_in, forKey: "expires_in")
        coder.encode(scope, forKey: "scope")

    }
}
