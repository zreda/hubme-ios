//
//  SigninRequestModel.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/8/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

class SigninRequestModel: NSObject {

    var grant_type: String?
    var username: String?
    var password: String?

    override init() {

        self.grant_type = ""
        self.username = ""
        self.password = ""
    }

    init(grant_type: String, username: String, password: String?) {

        self.username = username
        self.grant_type = grant_type
        self.password = password
    }
}
