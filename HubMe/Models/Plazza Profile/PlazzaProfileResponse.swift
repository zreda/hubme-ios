//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class PlazzaProfileResponse : NSObject, NSCoding, Mappable{

	var errorMessage : String?
	var profile : Profile?


	class func newInstance(map: Map) -> Mappable?{
		return PlazzaProfileResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		errorMessage <- map["errorMessage"]
		profile <- map["profile"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         errorMessage = aDecoder.decodeObject(forKey: "errorMessage") as? String
         profile = aDecoder.decodeObject(forKey: "profile") as? Profile

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if errorMessage != nil{
			aCoder.encode(errorMessage, forKey: "errorMessage")
		}
		if profile != nil{
			aCoder.encode(profile, forKey: "profile")
		}

	}

}
