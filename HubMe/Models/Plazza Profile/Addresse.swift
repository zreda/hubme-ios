//
//	Addresse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Addresse : NSObject, NSCoding, Mappable{

	var jiveLabel : String?
	var primary : Bool?
	var value : AddressDetails?


	class func newInstance(map: Map) -> Mappable?{
		return Addresse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		jiveLabel <- map["jive_label"]
		primary <- map["primary"]
		value <- map["value"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         jiveLabel = aDecoder.decodeObject(forKey: "jive_label") as? String
         primary = aDecoder.decodeObject(forKey: "primary") as? Bool
         value = aDecoder.decodeObject(forKey: "value") as? AddressDetails

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if jiveLabel != nil{
			aCoder.encode(jiveLabel, forKey: "jive_label")
		}
		if primary != nil{
			aCoder.encode(primary, forKey: "primary")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}
