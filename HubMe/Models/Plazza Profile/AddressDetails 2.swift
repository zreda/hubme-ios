//
//	Value.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class AddressDetails : NSObject, NSCoding, Mappable{

	var country : String?
	var formatted : String?
	var locality : String?
	var postalCode : String?
	var region : String?
	var streetAddress : String?


	class func newInstance(map: Map) -> Mappable?{
		return AddressDetails()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		country <- map["country"]
		formatted <- map["formatted"]
		locality <- map["locality"]
		postalCode <- map["postalCode"]
		region <- map["region"]
		streetAddress <- map["streetAddress"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         country = aDecoder.decodeObject(forKey: "country") as? String
         formatted = aDecoder.decodeObject(forKey: "formatted") as? String
         locality = aDecoder.decodeObject(forKey: "locality") as? String
         postalCode = aDecoder.decodeObject(forKey: "postalCode") as? String
         region = aDecoder.decodeObject(forKey: "region") as? String
         streetAddress = aDecoder.decodeObject(forKey: "streetAddress") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if country != nil{
			aCoder.encode(country, forKey: "country")
		}
		if formatted != nil{
			aCoder.encode(formatted, forKey: "formatted")
		}
		if locality != nil{
			aCoder.encode(locality, forKey: "locality")
		}
		if postalCode != nil{
			aCoder.encode(postalCode, forKey: "postalCode")
		}
		if region != nil{
			aCoder.encode(region, forKey: "region")
		}
		if streetAddress != nil{
			aCoder.encode(streetAddress, forKey: "streetAddress")
		}

	}

}
