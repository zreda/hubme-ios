//
//	Email.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Email : NSObject, NSCoding, Mappable{

	var jiveLabel : String?
	var primary : Bool?
	var type : String?
	var value : String?


	class func newInstance(map: Map) -> Mappable?{
		return Email()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		jiveLabel <- map["jive_label"]
		primary <- map["primary"]
		type <- map["type"]
		value <- map["value"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         jiveLabel = aDecoder.decodeObject(forKey: "jive_label") as? String
         primary = aDecoder.decodeObject(forKey: "primary") as? Bool
         type = aDecoder.decodeObject(forKey: "type") as? String
         value = aDecoder.decodeObject(forKey: "value") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if jiveLabel != nil{
			aCoder.encode(jiveLabel, forKey: "jive_label")
		}
		if primary != nil{
			aCoder.encode(primary, forKey: "primary")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if value != nil{
			aCoder.encode(value, forKey: "value")
		}

	}

}