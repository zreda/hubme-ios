//
//	Profile.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Profile : NSObject, NSCoding, Mappable{

	var addresses : [Addresse]?
	var avatar : String?
	var displayName : String?
	var emails : [Email]?
	var id : String?
	var phoneNumbers : [PhoneNumber]?
	var profileLink : String?
	var title : String?


	class func newInstance(map: Map) -> Mappable?{
		return Profile()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		addresses <- map["addresses"]
		avatar <- map["avatar"]
		displayName <- map["displayName"]
		emails <- map["emails"]
		id <- map["id"]
		phoneNumbers <- map["phoneNumbers"]
		profileLink <- map["profileLink"]
		title <- map["title"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         addresses = aDecoder.decodeObject(forKey: "addresses") as? [Addresse]
         avatar = aDecoder.decodeObject(forKey: "avatar") as? String
         displayName = aDecoder.decodeObject(forKey: "displayName") as? String
         emails = aDecoder.decodeObject(forKey: "emails") as? [Email]
         id = aDecoder.decodeObject(forKey: "id") as? String
         phoneNumbers = aDecoder.decodeObject(forKey: "phoneNumbers") as? [PhoneNumber]
         profileLink = aDecoder.decodeObject(forKey: "profileLink") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if addresses != nil{
			aCoder.encode(addresses, forKey: "addresses")
		}
		if avatar != nil{
			aCoder.encode(avatar, forKey: "avatar")
		}
		if displayName != nil{
			aCoder.encode(displayName, forKey: "displayName")
		}
		if emails != nil{
			aCoder.encode(emails, forKey: "emails")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if phoneNumbers != nil{
			aCoder.encode(phoneNumbers, forKey: "phoneNumbers")
		}
		if profileLink != nil{
			aCoder.encode(profileLink, forKey: "profileLink")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}
