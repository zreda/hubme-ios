//
//  TwitterApiKeyResponse.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import ObjectMapper

class TwitterApiKeyResponse: NSObject, NSCoding, Mappable {
    var consumer_key: String?
    var consumer_secret: String?

    override init() {

    }
    required init?(map: Map) {

    }

    // Mappable
    func mapping(map: Map) {
        consumer_key           <- map["appKey"]
        consumer_secret        <- map["appSecret"]

    }

    required init(coder decoder: NSCoder) {

        self.consumer_key = decoder.decodeObject(forKey: "consumer_key") as? String ?? ""
        self.consumer_secret = decoder.decodeObject(forKey: "consumer_secret") as? String ?? ""

    }

    func encode(with coder: NSCoder) {

        coder.encode(consumer_key, forKey: "consumer_key")
        coder.encode(consumer_secret, forKey: "consumer_secret")

    }
}
