//
//	Response.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class SendersModel : NSObject, NSCoding, Mappable{
    
    var channelIconUrl : String?
    var displayName : String?
    var email : String?
    var name : String?
    var senderName : String?
    
    
    override init() {
        self.channelIconUrl = ""
        self.name = ""
    }
    
    init( channelIconUrl : String ,name : String) {
        super.init()
        self.channelIconUrl = channelIconUrl
        self.name = name
    }
    
    class func newInstance(map: Map) -> Mappable?{
        return SendersModel()
    }
    required init?(map: Map){}
    
    func mapping(map: Map)
    {
        channelIconUrl <- map["channelIconUrl"]
        displayName <- map["displayName"]
        email <- map["email"]
        name <- map["name"]
        senderName <- map["senderName"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        channelIconUrl = aDecoder.decodeObject(forKey: "channelIconUrl") as? String
        displayName = aDecoder.decodeObject(forKey: "displayName") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        senderName = aDecoder.decodeObject(forKey: "senderName") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if channelIconUrl != nil{
            aCoder.encode(channelIconUrl, forKey: "channelIconUrl")
        }
        if displayName != nil{
            aCoder.encode(displayName, forKey: "displayName")
        }
        if email != nil{
            aCoder.encode(email, forKey: "email")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if senderName != nil{
            aCoder.encode(senderName, forKey: "senderName")
        }
        
    }
    
}
