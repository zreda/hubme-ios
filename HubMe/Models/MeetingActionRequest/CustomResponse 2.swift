//
//	CustomResponse.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class CustomResponse : NSObject, NSCoding, Mappable{

	var bcc : [Bcc]?
	var body : String?
	var cc : [Bcc]?
	var to : [Bcc]?


	class func newInstance(map: Map) -> Mappable?{
		return CustomResponse()
	}
	required init?(map: Map){}
    init( body : String , to : [Bcc], cc : [Bcc] ,bcc : [Bcc] ) {
        self.to = to
        self.cc = cc
        self.bcc = bcc
        self.body = body

    }
	 override init(){}

	func mapping(map: Map)
	{
		bcc <- map["bcc"]
		body <- map["body"]
		cc <- map["cc"]
		to <- map["to"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         bcc = aDecoder.decodeObject(forKey: "bcc") as? [Bcc]
         body = aDecoder.decodeObject(forKey: "body") as? String
         cc = aDecoder.decodeObject(forKey: "cc") as? [Bcc]
         to = aDecoder.decodeObject(forKey: "to") as? [Bcc]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if bcc != nil{
			aCoder.encode(bcc, forKey: "bcc")
		}
		if body != nil{
			aCoder.encode(body, forKey: "body")
		}
		if cc != nil{
			aCoder.encode(cc, forKey: "cc")
		}
		if to != nil{
			aCoder.encode(to, forKey: "to")
		}

	}

}
