//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class MeetingActionRequestModel : NSObject, NSCoding, Mappable{
    
    var action : String?
    var subAction : String?
    var customResponse : CustomResponse?
    
    class func newInstance(map: Map) -> Mappable?{
        return MeetingActionRequestModel()
    }
    override init()
    {
        self.action = ""
        self.subAction = ""
        
        self.customResponse = nil
    }
    init( action : String ,subAction : String)
    {
        self.action = action
        self.subAction = subAction
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map)
    {
        action <- map["action"]
        customResponse <- map["customResponse"]
        subAction <- map["subAction"]
        
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        action = aDecoder.decodeObject(forKey: "action") as? String
        customResponse = aDecoder.decodeObject(forKey: "customResponse") as? CustomResponse
        subAction = aDecoder.decodeObject(forKey: "subAction") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if action != nil{
            aCoder.encode(action, forKey: "action")
        }
        if customResponse != nil{
            aCoder.encode(customResponse, forKey: "customResponse")
        }
        if subAction != nil{
            aCoder.encode(subAction, forKey: "subAction")
        }
        
    }
    
}
