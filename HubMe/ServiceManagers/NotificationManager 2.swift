//
//  NotificationManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/17/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import Foundation
import Alamofire
import ObjectMapper

class NotificationManager: NSObject {
    
    func getAllNotifications (completion:  @escaping (_ :NotificationModel? , _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .get, URLString: HubMeConstants.Url.user_notifications_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                completion(NotificationModel(JSON: response as! [String: Any]), error, url)
                
            } else {
                completion(nil, error, url)
            }
        }
    }
    
    
    func readNotification(notificationId:String , completion:  @escaping (_ :BaseResponse? , _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: String(format: HubMeConstants.Url.read_notifications_url,notificationId), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if error == nil
            {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            }
            else {
                completion(nil, error, url)
            }
        }
    }
    
    func openAllNotification( completion:  @escaping (_ :BaseResponse? , _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: String(format: HubMeConstants.Url.open_all_notifications_url), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if error == nil
            {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            }
            else {
                completion(nil, error, url)
            }
        }
    }
}
