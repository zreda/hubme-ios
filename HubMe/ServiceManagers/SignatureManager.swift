//
//  SignatureManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/25/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class SignatureManager: NSObject {
    
    func getSignature (completion:  @escaping (_ :Signature? , _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .get, URLString: HubMeConstants.Url.user_signature_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                completion(Signature(JSON: response as! [String: Any]), error, url)
                
            } else {
                completion(nil, error, url)
            }
        }
    }
    
    
    func addSignature (signature:Signature,completion:  @escaping (_ :SignatureResponse? , _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        let params:[String:AnyObject] = ["signatureText":signature.signatureText as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.user_signature_url, withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                completion(SignatureResponse(JSON: response as! [String: Any]), error, url)
                
            } else {
                completion(nil, error, url)
            }
            
        }
    }
    
    func deleteSignature (completion:@escaping (_ :BaseResponse? , _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .delete, URLString: HubMeConstants.Url.user_signature_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
                
            } else {
                completion(nil, error, url)
            }
        }
    }
    
}
