//
//  ChannelManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/23/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class ChannelManager: NSObject {

    func getChannels(channelUrl: String, completion:  @escaping (_ :UserChannelResponse?, _ :NSError?, _:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: channelUrl, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in

            if response != nil {
                completion(UserChannelResponse(JSON: response as! [String: Any]), error,url)
            } else {
                completion(nil, error,url)

            }
        }
    }

    
    func getRegisteredChannels( completion:  @escaping (_ :UserChannelResponse?, _ :NSError?, _:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: HubMeConstants.Url.all_channels+"?filter=registered", withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if response != nil {
                completion(UserChannelResponse(JSON: response as! [String: Any]), error,url)
            } else {
                completion(nil, error,url)
                
            }
        }
    }
    
    func getChannelsStatus(completion:  @escaping (_ :[ChannelsStaus]?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: HubMeConstants.Url.channel_status_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in

            if response != nil {
                completion(Mapper<ChannelsStaus>().mapArray(JSONArray: response as! [[String : Any]]), nil,url)
            } else {
                completion(nil, error,url)

            }
        }
    }
    func loginChannel(auth: LoginRequestModel, completion:  @escaping (_ :SiginModelReponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        var params: [String: AnyObject] = [:]

        if auth.account != ""  // excahnge
        {
            params =  ["clientId":auth.email as AnyObject, "clientSecret":auth.password as AnyObject, "account":auth.account as AnyObject , "firstLoadConfig":auth.firstLoadConfig?.toJSON() as AnyObject]

        } else // plazza
        {
            params =  ["clientId":auth.email as AnyObject, "clientSecret":auth.password as AnyObject ,"verificationCode":auth.verificationCode as AnyObject, "firstLoadConfig":auth.firstLoadConfig?.toJSON() as AnyObject]
        }

        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: auth.expired ? HubMeConstants.Url.relogin_channel_url : HubMeConstants.Url.add_channel_url, auth.channel_id!), withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.applicationTypeKey:HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in

            if error == nil {

                completion(SiginModelReponse(JSON: response as! [String: Any]), error,url)

            } else if error != nil && response != nil {
                completion(SiginModelReponse(JSON: response as! [String: Any]), error,url)

            } else {
                completion(nil, error, url)

            }

        }
    }

    
    func verifyEmail(email: String, channel_id:String, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.register_verfify_channel_url,channel_id), withQueryStringParameters: ["email":email as AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.applicationTypeKey:HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                
                completion(BaseResponse(JSON: response as! [String: Any]), error,url)
                
            } else if error != nil && response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error,url)
                
            } else {
                completion(nil, error, url)
                
            }
            
        }
    }
    
    func getTwitterKeys(completion:  @escaping (_ :TwitterApiKeyResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: HubMeConstants.Url.twitter_cred_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.applicationTypeKey:HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in

            if response != nil {
                completion(TwitterApiKeyResponse(JSON: response as! [String: Any]), error,url)
            } else {
                completion(nil, error,url)

            }
        }
    }

    func loginSocialChannel(auth: LoginRequestModel , withQueryStringParameters parameters: [String: AnyObject]?, completion:  @escaping (_ :UserChannelResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

      

        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: auth.expired ? HubMeConstants.Url.relogin_channel_url :HubMeConstants.Url.add_channel_url, auth.channel_id!), withQueryStringParameters: parameters , withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.applicationTypeKey:HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in

            if response != nil {
                completion(UserChannelResponse(JSON: response as! [String: Any]), error,url)
            } else {
                completion(nil, error,url)

            }
        }
    }
    
    func deleteChannel(channelId: String, completion:  @escaping (_ :UserChannelResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .delete, URLString: String(format: HubMeConstants.Url.delete_user_channels, channelId), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil && error == nil {
                completion(UserChannelResponse(JSON: response as! [String: Any]), error,url)
            }
            else if error != nil && response != nil {
                completion(UserChannelResponse(JSON: response as! [String: Any]), error,url)
            }
            else {
                completion(nil, error,url)
                
            }
        }
    }
  

}
