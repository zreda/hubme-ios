//
//  FeedbackMAnager.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/26/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

class FeedbackManager: NSObject {
    
    func addFeedback(feedback:FeedbackModel, completion:  @escaping (_ :BaseResponse? ,  _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        
        let params: [String: AnyObject] =  ["commentText":feedback.commentText as AnyObject,"rating":feedback.rating as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.user_feedback_url, withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
                
            } else {
               
                    completion(nil, error, url)
                
                
            }
            
        }
    }
    
    
    func getFeedback (completion:  @escaping (_ :FeedbackModel? , _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .get, URLString: HubMeConstants.Url.user_feedback_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil {
                completion(FeedbackModel(JSON: response as! [String: Any]), error, url)
                
            } else {
                completion(nil, error, url)
            }
            
        }
    }
}
