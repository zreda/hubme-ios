//
//  AuthenticationManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/22/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Alamofire

class AuthenticationManager: NSObject {
    
    func sigin(auth: SigninRequestModel, completion:  @escaping (_:SiginModelReponse?,_:BadResponseModel? , _:NSError?,_:String?) -> Void) {
        
        let params: [String: AnyObject] =  ["grant_type":auth.grant_type as AnyObject, "username":auth.username as AnyObject, "password":auth.password as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.login_url, withQueryStringParameters: params, withHeaders: ["Authorization":"Basic bW9iaWxlOnNlY3JldA==", HubMeConstants.headers.applicationTypeKey:"application/x-www-form-urlencoded", HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if error == nil {
                
                completion(SiginModelReponse(JSON: response as! [String: Any]),nil, error,url)
                
            } else {
                
                if response != nil
                {
                    completion(nil,BadResponseModel(JSON: response as! [String: Any]), error,url)
                    
                }
                else
                {
                    completion(nil,nil, error,url)
                }
                
            }
            
        }
    }
    
    func userInfo(auth: String, completion:  @escaping (_:UserInfoReponse?, _:NSError?,_:String?) -> Void) {
        
        
        ServiceProvider().sendUrl(showActivity: false, method: .get, URLString: HubMeConstants.Url.userInfo_url, withQueryStringParameters: nil, withHeaders: ["Authorization":"Bearer \(auth)",HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if error == nil {
                
                completion(UserInfoReponse(JSON: response as! [String: Any]),nil,url)
                
            } else {
                
                completion(nil, error,url)
                
                
            }
            
        }
    }
    
    func refreshToken(completion: @escaping (_ :SiginModelReponse?, _ :NSError?,_:String?) -> Void) {
        
        let refresh_token = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.refresh_token ?? ""
        
        let params: [String: AnyObject] =  ["grant_type":"refresh_token" as AnyObject, "refresh_token": refresh_token as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.login_url, withQueryStringParameters: params, withHeaders: ["Authorization":"Basic bW9iaWxlOnNlY3JldA==", HubMeConstants.headers.applicationTypeKey:"application/x-www-form-urlencoded", HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if response != nil && error == nil {
                Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userData)
                Helper.saveObjectDefault(key: HubMeConstants.userDefault.userData, value: SiginModelReponse(JSON: response as! [String: Any]) ?? "")
                completion(SiginModelReponse(JSON: response as! [String: Any]), nil, url)
                
            }
            if response != nil && error != nil {
                Helper.removeKeyUserDefault(key: HubMeConstants.userDefault.userData)
                Helper.saveObjectDefault(key: HubMeConstants.userDefault.userData, value: response ?? "")
                completion(SiginModelReponse(JSON: response as! [String: Any]), error, url)
                
            }
            else {
                completion(nil, error, url)
                
            }
            
        }
    }
    
    func forgetPassword(mail: String, completion:  @escaping (_ :BaseResponse?, _ :NSError?) -> Void) {
        
        let params: [String: AnyObject] =  ["email":mail as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.forget_password_url, withQueryStringParameters: params, withHeaders: [HubMeConstants.headers.applicationTypeKey:  HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                
                completion(BaseResponse(JSON: response as! [String: Any]), error)
                
            } else {
                
                completion(nil, error)
            }
            
        }
    }
    
    func register(auth: SignupRequestModel, completion:  @escaping (_ :BaseResponse?, _ :NSError?) -> Void) {
        
        let params: [String: AnyObject] =  ["email":auth.email as AnyObject,"password":auth.password as AnyObject,"firstName":auth.firstName as AnyObject,"lastName":auth.lastName as AnyObject,"phoneNumber":auth.phoneNumber as AnyObject]
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.register_url, withQueryStringParameters: params, withHeaders: [HubMeConstants.headers.applicationTypeKey:  HubMeConstants.headers.applicationTypeValue, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                
                
                completion(BaseResponse(JSON: response as! [String: Any]), error)
                
            } else {
                
                completion(BaseResponse(JSON: response as! [String: Any]), error)
                
            }
            
        }
    }
    
    func getServiceConfig( completion: @escaping (_ :RabbitMQRConfigResponse?, _ :NSError?,_:String?) -> Void) {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.rabbitQServiceConfig_url, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                
                
                completion(RabbitMQRConfigResponse(JSON: response as! [String: Any]), error,url)
                
            } else {
                
                completion(RabbitMQRConfigResponse(JSON: response as! [String: Any]), error,url)
                
            }
            
        }
    }
    
    func getAppVersion( completion:  @escaping (_:AppVersionResponse?, _:NSError?,_:String?) -> Void) {
        
        ServiceProvider().sendUrl(showActivity: false, method: .get, URLString: HubMeConstants.Url.get_app_version_url, withQueryStringParameters: nil, withHeaders: [:]) { (response, error,url) in
            
            if error == nil
            {
                completion(AppVersionResponse(JSON: response as! [String: Any]),nil,url)
            }
            else
            {
                completion(nil, error,url)
            }
            
        }
    }
    
}
