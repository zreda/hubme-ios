//
//  FeedManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Alamofire

class FlowManager: NSObject {

    func getUserFlows(completion:  @escaping (_ :FlowItemResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: HubMeConstants.Url.user_flows, withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in

            if response != nil {
                completion(FlowItemResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)

            }
        }
    }

    func addUserFlows(requestParam: FlowRequestModel, completion:  @escaping (_ :LoginResponseModel?, _ :NSError?,_:String?) -> Void) {
        let param = requestParam.toJSON()
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: HubMeConstants.Url.user_flows, withQueryStringParameters: param as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in

            if response != nil {
                completion(LoginResponseModel(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)

            }
        }
    }

    func editUserFlows(requestParam: FlowRequestModel, completion:  @escaping (_ :LoginResponseModel?, _ :NSError?,_:String?) -> Void) {
        let param = requestParam.toJSON()
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .put, URLString: HubMeConstants.Url.user_flows, withQueryStringParameters: param as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in

            if response != nil {
                completion(LoginResponseModel(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)

            }
        }
    }

    func deleteUserFlows(id: String, completion:  @escaping (_ :LoginResponseModel?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .delete, URLString: String(format: HubMeConstants.Url.delete_user_flows, id), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in

            if response != nil {
                completion(LoginResponseModel(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)

            }
        }
    }
    func getUserFlowsDetails(id: Int, completion:  @escaping (_ :FlowDetailsResponseModel?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""

        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: String(format: HubMeConstants.Url.delete_user_flows, String(id)), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in

            if response != nil {
                completion(FlowDetailsResponseModel(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)

            }
        }
    }
    
    func subscribe(token:String , completion:  @escaping (_ :FeedsResponse?, _ :NSError?,_:String?) -> Void)
    {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        let param = ["token":token]
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: HubMeConstants.Url.subscribe, withQueryStringParameters: param as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(FeedsResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
        
    }
    
    func unsubscribe(token:String , completion:  @escaping (_ :FeedsResponse?, _ :NSError?,_:String?) -> Void)
    {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        let param = ["token":token]
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: HubMeConstants.Url.unsubscribe, withQueryStringParameters: param as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(FeedsResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
        
    }
    
    
    func searchSenders(query:String , completion:  @escaping (_ :SearchModelResponse?, _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        let url =  String(format: HubMeConstants.Url.search_senders_flow, query).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
       
        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: url ?? "", withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(SearchModelResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
        
    }
    
    
    func reorderFlows(request:ReorderFlowsModelRequest , completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void)
    {
        
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        
        ServiceProvider().sendUrl(showActivity: true, method: .put, URLString: HubMeConstants.Url.change_flow_positions_url, withQueryStringParameters: request.toJSON() as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
        
    }

}
