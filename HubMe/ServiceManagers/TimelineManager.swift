//
//  TimelineManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class TimelineManager: NSObject {

    func getTimeLineEvents(requestModel:TimeLineRequestModel, completion:  @escaping (_ :TimeLineReponse? , _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        let param = requestModel.toJSON()
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: String(format: HubMeConstants.Url.get_timeline_url), withQueryStringParameters: param as [String : AnyObject] , withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil
            {
                completion(TimeLineReponse(JSON: response as! [String: Any]), error, url)
            }
            else {
                completion(nil, error, url)
            }
        }
    }
    
    func getTimeLineEventsDetails(appointmentId:String,userChannelId:String, completion:  @escaping (_ :EventDetailsResponse? , _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: String(format: HubMeConstants.Url.get_timeline_details_url), withQueryStringParameters: ["appointmentId":appointmentId,"userChannelId":userChannelId] as [String : AnyObject] , withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil
            {
                completion(EventDetailsResponse(JSON:  response as! [String: Any]), error, url)
                
            }
            else {
                completion(nil, error, url)
            }
        }
    }
    
    func getPlazzaProfile(email:String, completion:  @escaping (_ :PlazzaProfileResponse? , _ :NSError?,_:String?) -> Void)
    {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: String(format: HubMeConstants.Url.get_plazza_profile_url), withQueryStringParameters: ["email":email] as [String : AnyObject] , withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if error == nil
            {
                completion(PlazzaProfileResponse(JSON:  response as! [String: Any]), error, url)
                
            }
            else {
                completion(nil, error, url)
            }
        }
    }
}
