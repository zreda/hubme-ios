//
//  FeedsManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import Alamofire
enum ReplyType :String
{
    case reply
    case replyAll
    case comment
    
}
class FeedsManager: NSObject {
    
    func getFeeds(type: userType ,url: String, id: String, itemsPerPage: String, pageIndex: String, completion:  @escaping (_ :FeedsResponse?, _ :NSError?,_:String?) -> Void) {
        let token_type = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as! SiginModelReponse).token_type
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        
        let service =  ServiceProvider()
        service.manager.session.getAllTasks { (tasks) in
            tasks.forEach {
                $0.cancel()
            }
            service.sendUrl(showActivity: true, method: .get, URLString: url, withQueryStringParameters: nil, withHeaders: ["Authorization":token_type!+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
                
                DispatchQueue.main.async {
                    if error == nil
                    {
                        completion(FeedsResponse(JSON: response as! [String: Any]), error, url)
                    }
                    else {
                        completion(nil, error, url)
                        
                    }
                }
            }
        }
        
    }
    
    func getFeedDetails(id: String,date:String,userChannelId:String, completion:  @escaping (_ :FeedDetailResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: String(format: HubMeConstants.Url.feed_details,userChannelId,date,id), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if response != nil {
                completion(FeedDetailResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    func readFeed(feed: ReadFeedRequest, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .put, URLString: String(format: HubMeConstants.Url.read_feed), withQueryStringParameters: feed.toJSON() as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    
    func replyFeed(msgId:String,date:String,userChannelId:String,request: ComposeMessageRequestModel,replyType:ReplyType, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        let params: [String: AnyObject] =  ["messageText":request.messageText as AnyObject , "to": request.to?.toJSON() as AnyObject , "cc": request.cc?.toJSON() as AnyObject , "bcc":request.bcc?.toJSON() as AnyObject]
        
        
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.reply_feed_url,userChannelId,date,msgId,replyType.rawValue), withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    
    func getSenders(senderName:String,channelType:String, completion:  @escaping (_ :SearchModelResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: String(format: HubMeConstants.Url.search_senders,senderName,channelType), withQueryStringParameters: nil, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"], paramEncoding: URLEncoding()) { (response, error,url) in
            
            if response != nil {
                completion(SearchModelResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    func sendMessage(channelId:String,request:ComposeMessageRequestModel, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        let params: [String: AnyObject] =  ["from":request.from as AnyObject, "subject":request.subject as AnyObject, "messageText":request.messageText as AnyObject , "to": request.to?.toJSON() as AnyObject , "cc": request.cc?.toJSON() as AnyObject , "bcc":request.bcc?.toJSON() as AnyObject]
        
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.compose_message_url,channelId), withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    func createNewTweet(channelId:String,body:String, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.compose_tweet_url,channelId), withQueryStringParameters: ["tweetText":body as AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    func sendPlazzaPost(channelId:String,request:PlazzaMessageRequest, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.compose_message_plazza_url,channelId), withQueryStringParameters: request.toJSON() as [String : AnyObject], withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
    
    
    func setMeetingAction(userChannelId:String,date:String,msgId:String,meetingRequestModel:MeetingActionRequestModel, completion:  @escaping (_ :BaseResponse?, _ :NSError?,_:String?) -> Void) {
        let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
        let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
        
        let params: [String: AnyObject] =  ["action":meetingRequestModel.action as AnyObject ,"subAction":meetingRequestModel.subAction as AnyObject, "customResponse":meetingRequestModel.customResponse?.toJSON() as AnyObject]
        
        
        
        ServiceProvider().sendUrl(showActivity: true, method: .post, URLString: String(format: HubMeConstants.Url.meeting_action_url,userChannelId,date,msgId), withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
            
            if response != nil {
                completion(BaseResponse(JSON: response as! [String: Any]), error, url)
            } else {
                completion(nil, error, url)
                
            }
        }
    }
}
