//
//  SearchManager.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/26/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

class SearchManager: NSObject {
        
    func search(searchText:String,itemsPerPage:String,pageIndex:String, completion:  @escaping (_ :FeedsResponse? ,  _ :NSError?,_:String?) -> Void) {
         
         let accessToken = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? ""
         let tokenType = (Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.token_type ?? ""
         
        let params: [String: AnyObject] =  ["query":searchText as AnyObject,"itemsPerPage":itemsPerPage as AnyObject ,"pageIndex":pageIndex as AnyObject]
         
         ServiceProvider().sendUrl(showActivity: false, method: .post, URLString: HubMeConstants.Url.search_url, withQueryStringParameters: params, withHeaders: ["Authorization":tokenType+" "+accessToken, HubMeConstants.headers.acceptLanguageKey:NSLocale.current.languageCode ?? "en"]) { (response, error,url) in
             
             if error == nil {
                 completion(FeedsResponse(JSON: response as! [String: Any]), error, url)
             } else {
                     completion(nil, error, url)
             }
             
         }
     }
}
