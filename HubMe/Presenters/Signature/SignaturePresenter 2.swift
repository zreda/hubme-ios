//
//  SignaturePresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/26/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation

@objc protocol SignaturePresenterView: NSObjectProtocol {
   @objc optional func setNewSignatureSuccess(response: String?)
   @objc optional func setError(error: NSError,url: String)
   @objc optional func setErrorReponse(msg:String?)
   @objc optional func closeDilogue()
   @objc optional func deleteSignature()

}

class SignaturePresenter {
    fileprivate let signatureService = SignatureManager()
    weak fileprivate var signatureView: SignaturePresenterView?
    
    init(_ view: SignaturePresenterView) {
        signatureView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        signatureView = nil
    }
    
    
    
    func addSignature(signatureText:Signature)
    {
        signatureService.addSignature(signature: signatureText) { (response, error,url) in
            
            if response != nil && error == nil {
                self.signatureView?.closeDilogue?()
            } else if response != nil && error != nil {
                self.signatureView?.setErrorReponse?(msg:response?.message)
            } else {
                self.signatureView?.setError?(error: error!,url: url!)
            }
            
        }
    }
    
    func deleteSignature()
    {
        signatureService.deleteSignature {(response, error,url) in
            
            if response != nil && error == nil {
                self.signatureView?.deleteSignature!()
            } else if response != nil && error != nil {
                self.signatureView?.setErrorReponse?(msg:response?.message)
            } else {
                self.signatureView?.setError?(error: error!,url: url!)
            }
            
        }
    }
    
    func getSignature()
    {
        signatureService.getSignature { (response, error,url) in
            if response != nil && error == nil {
                self.signatureView?.setNewSignatureSuccess?(response: response?.signatureText)
            } else if response != nil && error != nil {
//                self.signatureView?.setErrorReponse(msg:response?.message)
            } else {
                self.signatureView?.setError?(error: error!,url: url!)
            }
        }
    }
    
    
    
    
    
    
}
