//
//  FeedsPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
@objc protocol FeedsPresenterView: NSObjectProtocol {
    
    @objc optional func setFeedDetailsResult(response: FeedDetailResponse?)
    @objc optional func setFeedResult(response: FeedsResponse?)
    @objc optional func setMeetingActionResult(response: String?)

    @objc optional func setSuccess(response: String?)

    func setError(error: NSError,url:String)
    
}

class FeedsPresenter {
    fileprivate let feedService = FeedsManager()
    weak fileprivate var feedView: FeedsPresenterView?
    
    init(_ view: FeedsPresenterView) {
        feedView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        feedView = nil
    }
    
    public func getFeedResponse(type: userType, channelId: String, itemsPerPage: String, pageIndex: String) {
        
        var url: String = ""
        
        switch type {
        case .channel:
            
            url =   String(format: HubMeConstants.Url.user_feeds_channels, channelId, itemsPerPage,pageIndex)
            
        case .flow , .search:
            
            url =   String(format: HubMeConstants.Url.user_feeds_flows, channelId, itemsPerPage,pageIndex)
            
        }
        
        feedService.getFeeds(type: type , url: url, id: channelId, itemsPerPage: itemsPerPage, pageIndex: pageIndex) { (response, error,url) in
            
            if error == nil {
                self.feedView?.setFeedResult?(response: response)
            } else {
                self.feedView?.setError(error: error!,url: url!)
            }
        }
        
    }
    
    func setNewFeeds(oldFeeds: [MessageItem], newFeeds: [MessageItem]) -> [MessageItem] {
        
        var oldFeedsArray = oldFeeds + newFeeds
      
        oldFeedsArray = oldFeedsArray.unique{$0.messageId ?? ""}
        let newFeedsArray = newFeeds.unique{$0.messageId ?? ""}
        for (_, newFeed) in newFeedsArray.enumerated() {
            for (j, oldFeed) in oldFeedsArray.enumerated() {

                if (newFeed.isRead == true &&
                    (oldFeed.isRead == false || oldFeed.isRead == nil) ) &&
                    ( newFeed.date == oldFeed.date) &&  ( newFeed.messageId ?? "" == oldFeed.messageId ?? "")
                {
                    oldFeedsArray.remove(at: j)
                    oldFeedsArray.insert(newFeed, at: j)
                    oldFeedsArray[j] = newFeed
                    break
                }
            }
        }

        return oldFeedsArray
    }
    
    func isFound ( array: inout [MessageItem],newItem:MessageItem) -> Bool
    {
        for (_,oldItem) in array.enumerated()
        {

            if newItem.options?.mailId == oldItem.options?.mailId && (oldItem.options?.mailId != nil || newItem.options?.mailId !=  nil)
            {
                return true
            }
            if (newItem.messageId == oldItem.messageId && (oldItem.messageId != nil || newItem.messageId !=  nil))
            {
                return true
            }
        }
        return false
    }
    
    public func getFeedDetails(msgId: String ,date:String,userChannelId:String) {
        
        feedService.getFeedDetails(id: msgId,date: date,userChannelId: userChannelId) { (response, error,url) in
            
            if error == nil
            {
                self.feedView?.setFeedDetailsResult?(response: response)
                
            }
            else
            {
                self.feedView?.setError(error: error!,url: url!)
                
            }
        }
    }
    public func setMeetingAction(msgId: String ,date:String,userChannelId:String , request:MeetingActionRequestModel) {
        feedService.setMeetingAction(userChannelId: userChannelId, date: date, msgId: msgId, meetingRequestModel: request) { (response, error,url)  in
            if error == nil
            {
                self.feedView?.setMeetingActionResult?(response: response?.message)
            }
            else
            {
                self.feedView?.setError(error: error!,url: url!)
            }
        }
       
    }
    
    public func markFeedAsRead(feed: ReadFeedRequest) {
        
        feedService.readFeed(feed: feed, completion: { (response, error,url) in
            
            if error == nil
            {
                self.feedView?.setSuccess?(response: response?.message)
                
            }
            else
            {
                self.feedView?.setError(error: error!,url: url!)
                
            }
        })
    }
    
    
    
}
