//
//  ManageAppPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/1/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol ManageAppPresenterView: NSObjectProtocol {
    
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func setAppVersion(response: AppVersionModel?)
    
}

class ManageAppPresenter {
    fileprivate let appversionsService = AuthenticationManager()
    weak fileprivate var appVersion: ManageAppPresenterView?
    
    init(_ view: ManageAppPresenterView) {
        appVersion = view
    }
    
    init() {
    }
    func detachView() {
        appVersion = nil
    }
    
    func getAppVersion()
    {
        appversionsService.getAppVersion { (response, error, url) in
            if response != nil && error == nil
            {
                self.appVersion?.setAppVersion!(response: response?.iOS)
                
            }
            else {
                self.appVersion?.setError!(error: error!,url: url!)
            }
        }
        
    }
}
