//
//  NotificationCenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol NotificationPresenterView: NSObjectProtocol {
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func getUserNotifications(response: [NotificationResponse]?)
    @objc optional func readNotificationSuccess(response: String)
    
}

class NotificationPresenter {
    fileprivate let notificationService = NotificationManager()
    weak fileprivate var notificationView: NotificationPresenterView?
    
    init(_ view: NotificationPresenterView) {
        notificationView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        notificationView = nil
    }
    
    
    
    func getNotifications()
    {
        notificationService.getAllNotifications { (response, error, url) in
            
            if response != nil && error == nil {
                self.notificationView?.getUserNotifications?(response: response?.response ?? [])
            } else if response != nil && error != nil {
                self.notificationView?.setErrorReponse?(msg:response?.message)
            } else {
                self.notificationView?.setError?(error: error!,url: url!)
            }
        }
    }
    
    
    func readNotification(notificationId:String)
    {
        notificationService.readNotification(notificationId: notificationId) { (response, error, url) in
            if response != nil && error == nil
            {
                self.notificationView?.readNotificationSuccess?(response: response?.message ?? "")
                
            } else if response != nil && error != nil {
                self.notificationView?.setErrorReponse?(msg:response?.message)
            } else {
                self.notificationView?.setError?(error: error!,url: url!)
            }
        }
    }
    
    func openNotification()
    {
        notificationService.openAllNotification(completion: { (response, error, url) in
            if response != nil && error == nil
            {
                self.notificationView?.readNotificationSuccess?(response: response?.message ?? "")
                
            } else if response != nil && error != nil {
                self.notificationView?.setErrorReponse?(msg:response?.message)
            } else {
                self.notificationView?.setError?(error: error!,url: url!)
            }
        })
    }
}
