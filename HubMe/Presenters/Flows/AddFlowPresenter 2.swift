//
//  AddFlowPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

@objc protocol AddFlowPresenterView: NSObjectProtocol {

    @objc optional func setFlowResult(response: LoginResponseModel?)
    @objc optional func updateFlow(response: LoginResponseModel?)
    @objc optional func setFlowDetails(response: FlowDetailsResponseModel?)
    @objc optional func getSearchResults(response: SearchModelResponse)

    func setError(error: NSError,url: String)
}

class AddFlowPresenter {

    fileprivate let flowService = FlowManager()
    weak fileprivate var addFlowPresenterView: AddFlowPresenterView?

    init(_ view: AddFlowPresenterView) {
        addFlowPresenterView = view

    }

    init() {

    }

    func detachView() {
        addFlowPresenterView = nil
    }

    public func addUserFlow(flowRequest: FlowRequestModel) {
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")

        flowService.addUserFlows(requestParam: flowRequest, completion: { (response, error,url) in

            if response != nil {
                self.addFlowPresenterView?.setFlowResult?(response: response!)
            } else {
                self.addFlowPresenterView?.setError(error: error!,url: url!)
            }
        })

    }

    public func updateFlow(updatedFlow: FlowRequestModel) {
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")

        flowService.editUserFlows(requestParam: updatedFlow) { (response, error,url) in

            if response != nil {
                self.addFlowPresenterView?.updateFlow?(response: response!)
            } else {
                self.addFlowPresenterView?.setError(error: error!,url: url!)
            }
        }
    }

    public func getFlow(flowId: Int) {

        flowService.getUserFlowsDetails(id: flowId) { (response, error,url) in
            if response != nil {
                self.addFlowPresenterView?.setFlowDetails?(response: response!)
            } else {
                self.addFlowPresenterView?.setError(error: error!,url: url!)
            }
        }

    }
    
    
    public func searchSender(query: String) {
        
        flowService.searchSenders(query: query) { (response, error,url) in
            if response != nil {
                self.addFlowPresenterView?.getSearchResults?(response: response!)
            } else {
                self.addFlowPresenterView?.setError(error: error!,url: url!)
            }
        }
        
    }

}
