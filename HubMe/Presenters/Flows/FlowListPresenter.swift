//
//  FlowListPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/9/17.
//  Copyright © 2017 Orange. All rights reserved.
//

//import Foundation
//getUserFeeds
//class FlowListPresenter {
//    
//}

//
//  FlowListPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

@objc protocol FlowListPresenterView: NSObjectProtocol {

    @objc optional func setUFlowListResult(response: [FlowItem]?)
    @objc optional func deleteFlow(response: LoginResponseModel?)
    @objc optional func getFeedback(hasFeedback: FeedbackModel?)
    @objc optional func setSuccess(message: String?)

    func setError(error: NSError,url: String)
}

class FlowListPresenter {
    fileprivate let flowService = FlowManager()
    fileprivate let feedbackService = FeedbackManager()

    weak fileprivate var flowListPresenterView: FlowListPresenterView?

    init(_ view: FlowListPresenterView) {
        flowListPresenterView = view
    }

    init() {

    }

    func detachView() {
        flowListPresenterView = nil
    }

    public func getUserFlows() {

     
        flowService.getUserFlows { (response, error,url) in

            if error == nil {
                self.flowListPresenterView?.setUFlowListResult?(response: response?.response)
            } else {
                self.flowListPresenterView?.setError(error: error!,url: url!)
            }
        }

    }

    public func deleteFlow(flow_id: String) {

        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")

        flowService.deleteUserFlows(id: flow_id, completion: { (response, error,url) in

            if error == nil {
                self.flowListPresenterView?.deleteFlow?(response: response!)
            } else {
                self.flowListPresenterView?.setError(error: error!,url: url!)
            }

        })
    }
    
    public func getFeedback() {
        
        feedbackService.getFeedback { (result, error,url) in
            if error == nil {
                self.flowListPresenterView?.getFeedback?(hasFeedback: result!)
            } else {
                self.flowListPresenterView?.setError(error: error!,url: url!)
            }
        }
    }
    
    public func reorderFlows(request:ReorderFlowsModelRequest) {
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")

        flowService.reorderFlows(request: request) { (response, error,url) in
            
            if error == nil {
                self.flowListPresenterView?.setSuccess?(message: response?.message ?? "")
            } else {
                self.flowListPresenterView?.setError(error: error!,url: url!)
            }
            
        }
    }
}
