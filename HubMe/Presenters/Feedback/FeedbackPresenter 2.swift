//
//  FeedbackPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/26/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
protocol FeedbackPresenterView: NSObjectProtocol {
    func setSuccess(msg:String?,error:String?)
    func getFeedback(hasFeedback: FeedbackModel?)
    func stopLoading()
    func setError(error: NSError,url:String)

}

class FeedbackPresenter {
    weak fileprivate var feedbackPresenterView: FeedbackPresenterView?
    fileprivate let feedbackService = FeedbackManager()

    init(_ view: FeedbackPresenterView) {
        feedbackPresenterView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        feedbackPresenterView = nil
    }
    
  
    public func sendFeedback(feedabck:FeedbackModel) {
        
        
        feedbackService.addFeedback(feedback:feedabck) { (response, error, url) in
            
            if error == nil {
                
                self.feedbackPresenterView?.setSuccess(msg:(response?.message)!, error: nil)
                
            } else {
                if response != nil
                {
                    self.feedbackPresenterView?.setSuccess(msg:(response?.message)!, error: error?.localizedDescription)
                    
                }
                else
                {
                    self.feedbackPresenterView?.setError(error: error!,url: url!)
                    
                }
            }
        }
       
    }
    public func getFeedback() {
        
        feedbackService.getFeedback { (result, error, url) in
            if error == nil {
                self.feedbackPresenterView?.getFeedback(hasFeedback: result!)
            } else {
                self.feedbackPresenterView?.setError(error: error!,url: url!)
            }
        }
    }
}
