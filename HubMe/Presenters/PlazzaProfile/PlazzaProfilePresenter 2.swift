//
//  PlazzaProfilePresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/10/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol PlazzaProfilePresenterView: NSObjectProtocol {
    
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func getProfileDetails(response: Profile?)
    
}

class PlazzaProfilePresenter {
    fileprivate let timelineService = TimelineManager()
    weak fileprivate var timelineView: PlazzaProfilePresenterView?
    
    init(_ view: PlazzaProfilePresenterView) {
        timelineView = view
    }
    
    init() {
    }
    func detachView() {
        timelineView = nil
    }
    
    func getPlazzaProfile(email:String)
    {
        timelineService.getPlazzaProfile(email: email) { (response, error, url) in
            if response != nil && error == nil
            {
                self.timelineView?.getProfileDetails!(response: response?.profile)
                
            } else if response != nil && error != nil {
                self.timelineView?.setErrorReponse!(msg:response?.errorMessage ?? "")
            } else {
                self.timelineView?.setError!(error: error!,url: url!)
            }
        }
        
    }
}
