//
//  EventDetailsPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/4/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol EventDetailsPresenterView: NSObjectProtocol {
    
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func getEventDetails(response: TimelineEventDTO?)
    
}

class EventDetailsPresenter {
    fileprivate let timelineService = TimelineManager()
    weak fileprivate var timelineView: EventDetailsPresenterView?
    
    init(_ view: EventDetailsPresenterView) {
        timelineView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        timelineView = nil
    }
    
    func getEventDetails(appointmentId:String,userChannelId:String)
    {
        timelineService.getTimeLineEventsDetails(appointmentId: appointmentId, userChannelId: userChannelId) { (response, error, url) in
            if response != nil && error == nil
            {
                self.timelineView?.getEventDetails!(response: response?.timelineEventDTO)
                
            } else if response != nil && error != nil {
                self.timelineView?.setErrorReponse!(msg:response?.errorMessage ?? "")
            } else {
                self.timelineView?.setError!(error: error!,url: url!)
            }
        }
     
    }
}

