//
//  RegisterPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation

protocol RegisterPresenterView: NSObjectProtocol {
    
    func setSuccess(msg:String?,error:String?)
    func stopLoading()
    func setError(error: NSError)
    
}

class RegisterPresenter {
    fileprivate let RegisterService = AuthenticationManager()
    weak fileprivate var RegisterPresenterView: RegisterPresenterView?
    
    init(_ view: RegisterPresenterView) {
        RegisterPresenterView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        RegisterPresenterView = nil
    }
    
    public func getRegisterResult(auth: SignupRequestModel) {
        
        AuthenticationManager().register(auth: auth) {  (response, error) in
            
            if error == nil {
                
                self.RegisterPresenterView?.setSuccess(msg:(response?.message)!, error: nil)
                
            } else {
                
                if response != nil
                {
                    self.RegisterPresenterView?.setSuccess(msg:(response?.message)!, error: error?.localizedDescription)

                }
                else
                {
                    self.RegisterPresenterView?.setError(error: error!)
                    
                }
            }
            
        }
    }
    
}
