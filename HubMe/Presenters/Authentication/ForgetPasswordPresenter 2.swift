//
//  ForgetPasswordPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 6/18/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
protocol ForgetPasswordPresenterView: NSObjectProtocol {
    
    func setSuccess()
    func stopLoading()
    func setError(error: NSError)

}

class ForgetPasswordPresenter {
    fileprivate let ForgetPasswordService = AuthenticationManager()
    weak fileprivate var ForgetPasswordPresenterView: ForgetPasswordPresenterView?
    
    init(_ view: ForgetPasswordPresenterView) {
        ForgetPasswordPresenterView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        ForgetPasswordPresenterView = nil
    }
    
    public func getForgetPasswordResult(mail: String) {
        
        AuthenticationManager().forgetPassword(mail: mail) { (response, error) in
            
            if error == nil {
                
                Helper.showFloatAlert(title: (response?.message) ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertSuccess)

                self.ForgetPasswordPresenterView?.setSuccess()
                
            } else {
                if response != nil
                {
                    self.ForgetPasswordPresenterView?.stopLoading()

                    Helper.showFloatAlert(title: (response?.message) ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
                }
                else
                {
                    self.ForgetPasswordPresenterView?.setError(error: error!)

                }
            }
            
        }
    }
    
}
