//
//  LoginPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/15/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation

protocol LoginPresenterView: NSObjectProtocol {

    func setLoginResult(response: SiginModelReponse)
    func setUserInfoResult(rule:HubMeConstants.Authorities)
    func setRabbitMQInfoResult(rabbitMQInfo:RabbitmqConfig)

    func setError(error: HubMeConstants.StatusCode,message :String,url:String)
}

class LoginPresenter {
    fileprivate let loginService = AuthenticationManager()
    weak fileprivate var loginPresenterView: LoginPresenterView?

    init(_ view: LoginPresenterView) {
        loginPresenterView = view
    }

    init() {

    }

    func detachView() {
        loginPresenterView = nil
    }

    public func getLoginResult(auth: SigninRequestModel) {

        AuthenticationManager().sigin(auth: SigninRequestModel(grant_type: "password", username: auth.username!, password: auth.password!)) { (response,errorResponse ,error,url) in

            if response != nil {
                
               // Helper.saveObjectDefault(key: HubMeConstants.userDefault.userData, value: response!)
                self.loginPresenterView?.setLoginResult(response: response!)

            } else {
                if errorResponse != nil
                {
                    if errorResponse?.errorDescription! == "Bad credentials".localized()
                    {
                        
                        self.loginPresenterView?.setError(error: HubMeConstants.StatusCode(rawValue: (error?.code)!)!, message: "invalid_username_password".localized(), url: url!)

                    }
                    else
                    {
                        self.loginPresenterView?.setError(error: HubMeConstants.StatusCode(rawValue: (error?.code)!)!, message: (errorResponse?.errorDescription!)!, url: url!)

                    }

                }
                else
                {
                    self.loginPresenterView?.setError(error: HubMeConstants.StatusCode(rawValue: (error?.code)!)!, message: (error?.description)!, url: url!)

                }
            }

        }
    }
    
    public func getUserInfoResult(auth: String) {
        
        AuthenticationManager().userInfo(auth: auth, completion: { (response, error,url) in
            
            if error == nil
            {
                if response?.authorities != nil
                {
                    self.loginPresenterView?.setUserInfoResult(rule: HubMeConstants.Authorities(rawValue: ((response?.authorities?[0]))!)!)
                }
                else
                {
                    self.loginPresenterView?.setError(error: HubMeConstants.StatusCode.UserNotAuthorized, message: "User Unauthorized".localized(),url: url!)

                }
            }
            else
            {
                self.loginPresenterView?.setError(error: HubMeConstants.StatusCode(rawValue: (error?.code)!)!, message: (error?.description)!,url: url!)

            }
        })
    }
    
    
    public func getRabbitMQInfo() {
        AuthenticationManager().getServiceConfig(completion: { (response, error,url) in
            if error == nil
            {
                if response?.message?.lowercased() == "success".lowercased()
                    //.localized()
                {
                    self.loginPresenterView?.setRabbitMQInfoResult(rabbitMQInfo: (response?.response?.rabbitmqConfig)!)
                }
            }
            else
            {
                self.loginPresenterView?.setError(error: HubMeConstants.StatusCode(rawValue: (error?.code)!)!, message: (error?.description)!,url: url!)
                
            }
        })
    }
}
