//
//  ComposeNewTweetPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/7/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol ComposeNewTweetPresenterView: NSObjectProtocol {
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func composeMessageSuccess(response: String)
    
}

class ComposeNewTweetPresenter {
    fileprivate let feedService = FeedsManager()
    weak fileprivate var messageView: ComposeNewTweetPresenterView?
    
    init(_ view: ComposeNewTweetPresenterView) {
        messageView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        messageView = nil
    }
    
   
    
    func createNewTweet(channelId:String , tweetText:String)
    {
       
        feedService.createNewTweet(channelId: channelId, body: tweetText) { (response, error, url) in
            if response != nil && error == nil {
                self.messageView?.composeMessageSuccess?(response: response?.message ?? "")
            } else if response != nil && error != nil {
                self.messageView?.setErrorReponse?(msg:response?.message)
            } else {
                self.messageView?.setError?(error: error!,url: url!)
            }
        }
        
    }
}
