//
//  TimeLinePresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

//
//  NotificationCenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol TimeLinePresenterView: NSObjectProtocol {
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func getUserTimeLine(response: TimeLineModel?)
    
}

class TimeLinePresenter {
    fileprivate let timelineService = TimelineManager()
    weak fileprivate var timelineView: TimeLinePresenterView?
    
    init(_ view: TimeLinePresenterView) {
        timelineView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        timelineView = nil
    }
    
    func getTimeLine(requestModel:TimeLineRequestModel)
    {
        
        timelineService.getTimeLineEvents(requestModel: requestModel) { (response, error, url) in
            if response != nil && error == nil
            {
                self.timelineView?.getUserTimeLine?(response: response?.response ?? TimeLineModel())
                
            } else if response != nil && error != nil {
                self.timelineView?.setErrorReponse?(msg:response?.message)
            } else {
                self.timelineView?.setError?(error: error!,url: url!)
            }
        }
      
    }
}

