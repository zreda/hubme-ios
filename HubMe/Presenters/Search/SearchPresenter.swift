//
//  SearchPresneter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/26/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol SearchPresenterView: NSObjectProtocol {
    
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func getSearchResults(response: FeedsResponse?)
    
}

class SearchPresenter {
    fileprivate let searchervice = SearchManager()
    weak fileprivate var serachView: SearchPresenterView?
    
    init(_ view: SearchPresenterView) {
        serachView = view
    }
    
    init() {
    }
    func detachView() {
        serachView = nil
    }
    
    func getSearchResults(searchText:String,itemsPerPage:String,pageIndex:String)
    {
        searchervice.search(searchText: searchText, itemsPerPage: itemsPerPage, pageIndex: pageIndex) { (response, error,url) in
             if response != nil && error == nil
             {
                self.serachView?.getSearchResults?(response: response)
            }
             else{
        
                self.serachView?.setError!(error: error!,url: url!)
            }
        }
       
        
    }
}
