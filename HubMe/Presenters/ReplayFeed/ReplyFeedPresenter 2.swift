//
//  FeedsPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
@objc protocol ReplyFeedPresenterView: NSObjectProtocol {
    func setReplySuccess(response: String?)
    func setError(error: NSError,url:String)
    func setErrorReponse(msg:String?,url: String)
}

class ReplyFeedPresenter {
    fileprivate let feedService = FeedsManager()
    weak fileprivate var replyFeedView: ReplyFeedPresenterView?
    
    init(_ view: ReplyFeedPresenterView) {
        replyFeedView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        replyFeedView = nil
    }
    
  
    
   
    public func sendReply(msgId: String ,date:String,userChannelId:String , requestModel:ComposeMessageRequestModel , msgType:ReplyType) {
        
        feedService.replyFeed(msgId: msgId, date: date, userChannelId: userChannelId, request: requestModel, replyType: msgType) { (response, error,url) in
            
            if response != nil && error == nil {
                self.replyFeedView?.setReplySuccess(response: response?.message)
            } else if response != nil && error != nil {
                self.replyFeedView?.setErrorReponse(msg:response?.message,url: url!)
            } else {
                self.replyFeedView?.setError(error: error!,url: url!)
            }
        }
    }
    
    
    
    
    
}
