//
//  ComposeNewMessagePresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/24/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation

@objc protocol ComposeNewMessagePresenterView: NSObjectProtocol {
    @objc optional func getSendersSuccess(response: [SendersModel]?)
    @objc optional func setError(error: NSError,url: String)
    @objc optional func setErrorReponse(msg:String?)
    @objc optional func composeMessageSuccess(response: String)

}

class ComposeNewMessagePresenter {
    fileprivate let feedService = FeedsManager()
    weak fileprivate var messageView: ComposeNewMessagePresenterView?
    
    init(_ view: ComposeNewMessagePresenterView) {
        messageView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        messageView = nil
    }
    
    
    
    func getSenders(sender:String,channelType:String)
    {
        feedService.getSenders(senderName: sender, channelType: channelType) { (response, error, url) in
            
            if response != nil && error == nil {
                self.messageView?.getSendersSuccess?(response: response?.response)
            } else if response != nil && error != nil {
                self.messageView?.setErrorReponse?(msg:response?.message)
            } else {
                self.messageView?.setError?(error: error!,url: url!)
            }
        }
    }
  
    
    func composeMessage(channelId:String , request:ComposeMessageRequestModel)
    {
        feedService.sendMessage(channelId: channelId, request: request) { (response, error, url) in
            if response != nil && error == nil {
                self.messageView?.composeMessageSuccess?(response: response?.message ?? "")
            } else if response != nil && error != nil {
                self.messageView?.setErrorReponse?(msg:response?.message)
            } else {
                self.messageView?.setError?(error: error!,url: url!)
            }
        }
        
    }
    
    func composePlazzaMessage(channelId:String , request:PlazzaMessageRequest)
    {
        feedService.sendPlazzaPost(channelId: channelId, request: request) { (response, error, url) in
            if response != nil && error == nil {
                self.messageView?.composeMessageSuccess?(response: response?.message ?? "")
            } else if response != nil && error != nil {
                self.messageView?.setErrorReponse?(msg:response?.message)
            } else {
                self.messageView?.setError?(error: error!,url: url!)
            }
        }
        
    }
    
    
}
