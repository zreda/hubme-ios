//
//  LoginChannelPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/19/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

@objc  protocol LoginChannelPresenterView: NSObjectProtocol {
    
    @objc optional func setLoginSocialResult(response: String,authUrl:String)
    @objc optional func setLoginResult(response: String)
    @objc optional func confirmationResponse(response: String)

    func setError(error: NSError, url: String)
}

class LoginChannelPresenter {
    fileprivate let channelsService = ChannelManager()
    weak fileprivate var loginChannelPresenterView: LoginChannelPresenterView?
    
    init(_ view: LoginChannelPresenterView) {
        loginChannelPresenterView = view
    }
    
    init() {
        
    }
    
    func detachView() {
        loginChannelPresenterView = nil
    }
    
    public func getLoginResult(auth: LoginRequestModel) {
        
        channelsService.loginChannel(auth: auth) { (response, error,url) in
            
            if response != nil && error == nil {
                self.loginChannelPresenterView?.setLoginResult?(response: (response?.message)!)
            } else if response != nil && error != nil {
                self.loginChannelPresenterView?.setError(error: error!, url: url!)
            } else {
                self.loginChannelPresenterView?.setError(error: error!, url: url!)
            }
        }
    }
    
    public func getLoginSocial(auth: LoginRequestModel) {
        
        var param :[String:AnyObject] = [:]
        
        param = ["firstLoadConfig":auth.firstLoadConfig?.toJSON() as AnyObject]
        
        channelsService.loginSocialChannel(auth: auth, withQueryStringParameters: param) { (response, error,url) in
            
            if response != nil {
                self.loginChannelPresenterView?.setLoginSocialResult?(response: (response?.message)!, authUrl: response?.authenticationURL ?? "")
            } else {
                self.loginChannelPresenterView?.setError(error: error!, url: url!)
            }
        }
    }
    
    public func getLoginMessaging(auth: LoginRequestModel) {
        
        var param :[String:AnyObject] = [:]
        
        param = ["firstLoadConfig":auth.firstLoadConfig?.toJSON() as AnyObject,"subChannelName":auth.subChannelName as AnyObject ,"teamName":auth.teamName as AnyObject]
        
        channelsService.loginSocialChannel(auth: auth, withQueryStringParameters: param) { (response, error,url) in
            
            if response != nil {
                self.loginChannelPresenterView?.setLoginSocialResult?(response: (response?.message)!, authUrl: response?.authenticationURL ?? "")
            } else {
                self.loginChannelPresenterView?.setError(error: error!, url: url!)
            }
        }
    }
    
    public func verfiyPlazzaEmail(email: String , channelId:String) {

        channelsService.verifyEmail(email: email, channel_id: channelId) { (response, error, url) in
            if response != nil {
                self.loginChannelPresenterView?.confirmationResponse?(response: response?.message ?? "")
            } else {
                self.loginChannelPresenterView?.setError(error: error!, url: url!)
            }
        }
    }
    
}
