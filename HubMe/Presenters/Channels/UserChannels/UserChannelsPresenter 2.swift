//
//  ChannelsPresenter.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/25/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation

@objc protocol ChannelsPresenterView: NSObjectProtocol {

    @objc optional func setUserChannelsResult(response: [UserChannelsItem]?)
    @objc optional func getAllChannels(response: [UserChannelsItem]?)
    @objc optional func deleteChannel(response: String , error:Bool)
    @objc optional func setUserChannelsStatus(response: [ChannelsStaus]?)
    @objc optional func getTwitterKeys(response: TwitterApiKeyResponse?)

    func setError(error: NSError,url:String)
}

class UserChannelsPresenter {
    fileprivate let channelsService = ChannelManager()
    weak fileprivate var channelsPresenterView: ChannelsPresenterView?

    init(_ view: ChannelsPresenterView) {
        channelsPresenterView = view
    }

    init() {

    }

    func detachView() {
        channelsPresenterView = nil
    }
    public func getIndexOfChannel(userChannelList: [UserChannelsItem], id: Int) -> Int? {
        for (i, item)  in userChannelList.enumerated() {
           if item.id ==  id {
            return i
            }
        }
        return nil
    }
    public func getUserChannels() {

        channelsService.getChannels(channelUrl: HubMeConstants.Url.user_channels) { (response, error,url) in

            if error == nil {
                self.channelsPresenterView?.setUserChannelsResult?(response: response?.response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
        }
    }
    
    

    public func getFilteredUserChannels() {
        
        channelsService.getChannels(channelUrl: HubMeConstants.Url.user_channels_filtered) { (response, error,url) in
            
            if error == nil {
                self.channelsPresenterView?.setUserChannelsResult?(response: response?.response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
        }
    }
    public func getUserChannelsStatus() {

        channelsService.getChannelsStatus() { (response, error,url) in

            if error == nil {
                self.channelsPresenterView?.setUserChannelsStatus?(response: response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
        }
    }

    public func getAllChannels() {

        channelsService.getChannels(channelUrl: HubMeConstants.Url.all_channels) { (response, error,url) in

            if error == nil {
                self.channelsPresenterView?.getAllChannels?(response: response?.response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
        }
    }

    public func getRegisteredChannels() {
        
        channelsService.getRegisteredChannels {  (response, error,url) in
            
            if error == nil {
                self.channelsPresenterView?.getAllChannels?(response: response?.response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
            
        }
    }

    public func deleteChannel(channel_id: String) {
        
        channelsService.deleteChannel(channelId: channel_id) { (response, error,url) in
            
            if response != nil && error == nil{
                self.channelsPresenterView?.deleteChannel?(response: (response?.message!)!, error: false )
            }
                
            else if response != nil && error != nil
            {
                self.channelsPresenterView?.deleteChannel?(response: (response?.message!)!, error: true)

            }
            else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }
            
        }
        
    }

    public func getTwitterKeys() {
        channelsService.getTwitterKeys(completion: {(response, error,url) in

            if response != nil {
                self.channelsPresenterView?.getTwitterKeys?(response: response)
            } else {
                self.channelsPresenterView?.setError(error: error!,url: url!)
            }

        })
    }

}
