//
//  AppDelegate.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import IQKeyboardManagerSwift
import ObjectMapper
import Firebase
import Crashlytics
import SideMenuSwift
import Fabric
import StompClientLib

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var tabBarHome:UITabBarController?
    fileprivate var url = NSURL()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 0.0
        UITabBar.appearance().backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

//        SideMenuController.preferences.basic.menuWidth = 240
//        SideMenuController.preferences.basic.statusBarBehavior = .hideOnMenu
//        SideMenuController.preferences.basic.position = .above
//        SideMenuController.preferences.basic.direction = .left
//        SideMenuController.preferences.basic.enablePanGesture = true
//        SideMenuController.preferences.basic.supportedOrientations = .portrait
//        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                debugPrint(error)
            } catch {
                debugPrint(error)
            }
        } catch {
            debugPrint(error)
        }
        
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")
     
        //MARK :-  navigate to home tab bar when user is tap on skip or done
        let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
        tabBarHome = mainSB.instantiateViewController(withIdentifier: "HomeTabBar") as? UITabBarController
        tabBarHome?.selectedIndex = 2
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.notificationBadge)
        {
            Helper.saveUserDefault(key: HubMeConstants.userDefault.notificationBadge, value: 0)

        }
        if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.userData) {
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            
            //            appDelegate.window?.rootViewController = tabBarHome
            appDelegate.window?.rootViewController = SideMenuController(contentViewController: tabBarHome! ,menuViewController: ClientSideMenuViewController.create())
            
//            registerSocket()

        }
        else if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.isSkip)
        {
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            
            let loginVC = mainSB.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController
            appDelegate.window?.rootViewController = loginVC
        }
        
        appDelegate.window?.makeKeyAndVisible()
    
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        
        BITHockeyManager.shared().configure(withIdentifier: "357709d1d1684a0c9b44e0ed5acaa5c2")
        // Do some additional configuration if needed here
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Close".localized()


        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {

        debugPrint("app: \(app)")
        // print OAuth response URL
        debugPrint("url: \(url)")
        debugPrint("options: \(options)")
        if let sourceApplication = options[.sourceApplication] {
            if (String(describing: sourceApplication) == "com.apple.SafariViewService") || (String(describing: sourceApplication)) == "com.testApp.Incognito"
                {
                return true
            }
        }

        print("Received callback!")
        
        return true
    }

}
extension AppDelegate :StompClientLibDelegate
{
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        debugPrint("receiptId : \(receiptId)")
    }
    
   public func registerSocket(){
        let rabbitMQSec = Helper.getObjectDefault(key: HubMeConstants.userDefault.rabbitMQSecurity) as? RabbitmqConfig
    
        let baseURL = rabbitMQSec?.url
        url = NSURL(string: baseURL ?? "")!
        
    HubMeConstants.socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":rabbitMQSec?.username ?? "","passcode":rabbitMQSec?.password ?? "","host":"/","hear-beat": "1000,1000"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.notificationTopic
        debugPrint("Socket is Connected : \(topic)")
        debugPrint("Connect Time : \(Date())")
        HubMeConstants.socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
        HubMeConstants.socketClient.autoDisconnect(time: 50.0)
        //        // Reconnect after 4 sec
        HubMeConstants.socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        debugPrint("Socket is Disconnected")
        debugPrint("DisConnect Time : \(Date())")
        
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        debugPrint("Socket will is Disconnected")
        let topic = HubMeConstants.rabbirMQCredential.notificationTopic
        HubMeConstants.socketClient.unsubscribe(destination: topic)
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("JSON BODY : \(String(describing: jsonBody))")
        
        if let json = NotificationResponse(JSON:  jsonBody as! [String: Any]) 
        {
            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.refreshNotifications), object: json, userInfo: nil)
            
        }
        

        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, akaStringBody stringBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("String JSON BODY : \(String(describing: jsonBody))")
    
        
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        debugPrint("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        debugPrint("Server Ping")
    }
    
}
