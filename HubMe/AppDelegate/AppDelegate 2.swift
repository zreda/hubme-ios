//
//  AppDelegate.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/6/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
//import TwitterKit
import IQKeyboardManagerSwift
import ObjectMapper
//import GoogleSignIn
import Firebase
import Crashlytics
import SideMenuSwift
import Fabric
import BugfenderSDK
//import StompClientLib

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var tabBarHome:UITabBarController?
    fileprivate var url = NSURL()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        IQKeyboardManager.shared.enable = true
        SideMenuController.preferences.basic.menuWidth = 240
        SideMenuController.preferences.basic.statusBarBehavior = .hideOnMenu
        SideMenuController.preferences.basic.position = .above
        SideMenuController.preferences.basic.direction = .left
        SideMenuController.preferences.basic.enablePanGesture = true
        SideMenuController.preferences.basic.supportedOrientations = .portrait
        SideMenuController.preferences.basic.shouldRespectLanguageDirection = true
        do {
            Network.reachability = try Reachability(hostname: "www.google.com")
            do {
                try Network.reachability?.start()
            } catch let error as Network.Error {
                debugPrint(error)
            } catch {
                debugPrint(error)
            }
        } catch {
            debugPrint(error)
        }
        
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: "")
     
        //MARK :-  navigate to home tab bar when user is tap on skip or done
        let mainSB = UIStoryboard(name: HubMeConstants.StoryBoard.mainSB, bundle: nil)
        tabBarHome = mainSB.instantiateViewController(withIdentifier: "HomeTabBar") as! UITabBarController

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.notificationBadge)
        {
            Helper.saveUserDefault(key: HubMeConstants.userDefault.notificationBadge, value: 0)

        }
        if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.userData) {
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            
            //            appDelegate.window?.rootViewController = tabBarHome
            appDelegate.window?.rootViewController = SideMenuController(contentViewController: tabBarHome! ,menuViewController: ClientSideMenuViewController.create())
            
            registerSocket()

        }
        else if Helper.isKeyPresentInUserDefaults(key: HubMeConstants.userDefault.isSkip)
        {
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            
            let loginVC = mainSB.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController
            appDelegate.window?.rootViewController = loginVC
        }
        
        appDelegate.window?.makeKeyAndVisible()
    
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        
        BITHockeyManager.shared().configure(withIdentifier: "357709d1d1684a0c9b44e0ed5acaa5c2")
        // Do some additional configuration if needed here
        BITHockeyManager.shared().start()
        BITHockeyManager.shared().authenticator.authenticateInstallation()


        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey: Any] = [:]) -> Bool {

        
        Bugfender.activateLogger("Tac3P4js5iqjXsfEC7FDpkPA0ceG4HhV")
        Bugfender.enableCrashReporting()
        Bugfender.enableUIEventLogging()  // optional, log user interactions automatically

        
        debugPrint("app: \(app)")
        // print OAuth response URL
        debugPrint("url: \(url)")
        debugPrint("options: \(options)")
        if let sourceApplication = options[.sourceApplication] {
            if (String(describing: sourceApplication) == "com.apple.SafariViewService") || (String(describing: sourceApplication)) == "com.testApp.Incognito"
                {
                return true
            }
        }

        print("Received callback!")
        
        return true
    }

}
extension AppDelegate :StompClientLibDelegate
{
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
        debugPrint("receiptId : \(receiptId)")
    }
    
   public func registerSocket(){
        let baseURL = HubMeConstants.Url.rabbitmQ_url
        url = NSURL(string: baseURL)!
        
        HubMeConstants.socketClient.openSocketWithURLRequest(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate , connectionHeaders:["login":HubMeConstants.rabbirMQCredential.rabbitmQ_username,"passcode":HubMeConstants.rabbirMQCredential.rabbitmQ_password,"host":"/","hear-beat": "1000,1000"])
    }
    
    
    func stompClientDidConnect(client: StompClientLib!) {
        let topic = HubMeConstants.rabbirMQCredential.notificationTopic
        debugPrint("Socket is Connected : \(topic)")
        debugPrint("Connect Time : \(Date())")
        HubMeConstants.socketClient.subscribe(destination: topic)
        // Auto Disconnect after 3 sec
        HubMeConstants.socketClient.autoDisconnect(time: 50.0)
        //        // Reconnect after 4 sec
        HubMeConstants.socketClient.reconnect(request: NSURLRequest(url: url as URL) , delegate: self as StompClientLibDelegate, time: 1.0)
    }
    
    func stompClientDidDisconnect(client: StompClientLib!) {
        debugPrint("Socket is Disconnected")
        debugPrint("DisConnect Time : \(Date())")
        
    }
    
    func stompClientWillDisconnect(client: StompClientLib!, withError error: NSError) {
        debugPrint("Socket will is Disconnected")
        let topic = HubMeConstants.rabbirMQCredential.notificationTopic
        HubMeConstants.socketClient.unsubscribe(destination: topic)
        
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("JSON BODY : \(String(describing: jsonBody))")
        
        if let json = NotificationResponse(JSON:  jsonBody as! [String: Any]) 
        {
            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.refreshNotifications), object: json, userInfo: nil)
            
        }
        

        
    }
    
    func stompClientJSONBody(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: String?, withHeader header: [String : String]?, withDestination destination: String) {
        debugPrint("DESTIONATION : \(destination)")
        debugPrint("String JSON BODY : \(String(describing: jsonBody))")
    
        
    }
    
    func serverDidSendError(client: StompClientLib!, withErrorMessage description: String, detailedErrorMessage message: String?) {
        debugPrint("Error : \(String(describing: message))")
    }
    
    func serverDidSendPing() {
        debugPrint("Server Ping")
    }
    
}
