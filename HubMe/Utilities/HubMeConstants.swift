import UIKit
import StompClientLib

public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}
enum userType: String {
    case search = "search"
    case flow = "flow"
    case channel = "channel"
}

enum feedType: String {
    case message = "MESSAGE"
    case meeting = "MEETING"
}

enum meetingTypes: String {
    case message = "MESSAGE"
    case meeting = "MEETING"
}
enum  meetingSubActions : String {
    case responseBeforeSend = "EDIT_RESPONSE_BEFORE_SEND"
    case sendNow = "SEND_RESPONSE"
    case noReponse = "NO_RESPONSE"
}
enum  meetingActions : String {
    case accept = "ACCEPT"
    case maybe = "TENTATIVE"
    case decline = "DECLINE"
}
enum  attendeeState : String {
    case required = "Required Attendee"
    case optional = "Optional Attendee"
}
enum  meetingStatus : String {
    case accept = "accept"
    case organizer =  "organizer"
    case tentative = "tentative"
    case decline = "decline"
    case unknown = "unknown"
    case noResponse = "noresponsereceived"
}
enum RuleType
{
    case channels
    case senders
    case keywords
}

class HubMeConstants: NSObject {
    let twitterNumberPages = "100"
    let itemPerPage = "10"
    let flowItemPerPage = "50"
    //    let main_url: String = "http://hubme-dev.ole-lab.com/"       //    Dev URL
//    let main_url: String = "http://hubme-test.ole-lab.com/"    //    Test URL
        let main_url: String = "https://smartnomadworkspace-noprod.apps.fr01.paas.diod.orange.com/"        //    Staging URL
    static let rulesTabs: [String] = ["CHANNELS".localized(), "SENDERS".localized(), "KEYWORDS".localized()]
    
    static var allChannels:[GroupedChannel]  = []
    
    static var socketClient = StompClientLib()
    
    static  let periods :[Int] = [7,14,30,60,90]
    
    struct Url {
        
        static let login_url = HubMeConstants().main_url+"oauth/token"
        static let userInfo_url = HubMeConstants().main_url+"oauth/userInfo"
        
        static let forget_password_url = HubMeConstants().main_url+"user/resetPassword"
        static let register_url = HubMeConstants().main_url+"user"
        static let rabbitQServiceConfig_url = HubMeConstants().main_url+"serviceConfiguration/getServiceConfig"
        
        static let add_channel_url = HubMeConstants().main_url+"user-channel/%@/registration"
        static let relogin_channel_url = HubMeConstants().main_url+"user-channel/%@/relogin"
        static let register_verfify_channel_url = HubMeConstants().main_url+"user-channel/%@/registerWithVerification"
        
        static let channel_status_url = HubMeConstants().main_url+"user-channel/last-update"
        static let twitter_cred_url = HubMeConstants().main_url+"user-channel/twitterApiCred"
        
        static let user_channels = HubMeConstants().main_url+"user-channel"
        
        static let user_channels_filtered = HubMeConstants().main_url+"user-channel?filter=timeline"
        
        static let delete_user_channels = HubMeConstants().main_url+"user-channel/%@"
        
        static let all_channels = HubMeConstants().main_url+"channel"
        
        static let user_flows = HubMeConstants().main_url+"user-flows"
        static let delete_user_flows = HubMeConstants().main_url+"user-flows/%@"
        
        static let user_feeds_channels = HubMeConstants().main_url+"user-channel/%@/feeds?itemsPerPage=%@&pageIndex=%@"
        static let user_feeds_flows = HubMeConstants().main_url+"user-flows/%@/feeds?itemsPerPage=%@&pageIndex=%@"
        
        
        static let feed_details = HubMeConstants().main_url+"message-details/text/%@/%@/%@"
        static let read_feed = HubMeConstants().main_url+"message-details/read"
        
        static let subscribe = HubMeConstants().main_url+"user/push_notification/subscribe"
        static let unsubscribe = HubMeConstants().main_url+"user/push_notification/unsubscribe"
        static let user_feedback_url = HubMeConstants().main_url+"user/feedback"
        static let user_signature_url = HubMeConstants().main_url+"user/signature"
        static let reply_feed_url = HubMeConstants().main_url+"message-details/%@/%@/%@/%@"
        static let search_senders_flow = HubMeConstants().main_url+"feed/sender/search?q=%@"
        static let search_senders = HubMeConstants().main_url+"feed/sender/search?q=%@&channelType=%@"
        static let compose_message_url = HubMeConstants().main_url+"user-channel/%@/actions/compose"
        static let compose_tweet_url = HubMeConstants().main_url+"user-channel/%@/actions/tweet"
        static let compose_message_plazza_url = HubMeConstants().main_url+"user-channel/%@/actions/plazza-post"
        
        static let change_flow_positions_url = HubMeConstants().main_url+"user-flows/positions"
        //        static let rabbitmQ_url = "http://213.77.62.204:80/ws" // testing
        //        static let rabbitmQ_url = "http://213.77.62.203:80/ws" // dev
        static let rabbitmQ_url = "https://ws.hubme.ole-lab.com/ws" // staging
        static let user_notifications_url = HubMeConstants().main_url+"notification/notifications"
        static let read_notifications_url = HubMeConstants().main_url+"notification/read?notificationId=%@"
        static let open_all_notifications_url = HubMeConstants().main_url+"notification/open"
        static let meeting_action_url = HubMeConstants().main_url+"message-details/%@/%@/%@/meeting/action"
        static let get_timeline_url = HubMeConstants().main_url+"timeline/timelineEvents"
        static let get_timeline_details_url = HubMeConstants().main_url+"timeline/timelineEvent/details"
        static let get_plazza_profile_url = HubMeConstants().main_url+"timeline/attendee/profile"
        static let get_app_version_url = HubMeConstants().main_url+"hubme/mob/version/"
        static let search_url = HubMeConstants().main_url+"search/results"
    }
    
    struct rabbirMQCredential {
        static let  rabbitmQ_username = "guest"// testing & dev & staging
        //        static let rabbitmQ_password = "m6a_u32zF-P@XgUA" // testing
        static let rabbitmQ_password = "guest" //dev & staging
        static let topic = "/exchange/hubme/\((Helper.getObjectDefault(key: HubMeConstants.userDefault.userData) as? SiginModelReponse)?.access_token ?? "")"
        static let notificationTopic = "/exchange/hubme-user/\((Helper.getObjectDefault(key: HubMeConstants.userDefault.userName) as? String) ?? "")"
    }
    
    struct Channels {
        static let gmailChannel = "Gmail"
        static let twitterChannel = "Twitter"
        static let plazzaChannel = "Plazza"
        static let exchangeChannel = "Exchange"
        static let mattermostChannel = "MatterMost"
        static let slackChannel = "Slack"
        
    }
    
    struct MessageType {
        static let discussion = "discussion"
        static let task = "task"
    }
    enum ManageAccount:String {
        case add = "add"
        case manage = "manage"
    }
    struct ChannelType {
        static let email = "EMAIL"
        static let social = "SOCIAL"
    }
    struct FeedsLoadStatus {
        static let expired  = "EXPIRED"
    }
    
    struct NotificationType {
        static let new = "new"
        static let alert = "alert"
    }
    struct SocialKeys{
        static let kSocialAuthenticationPathGoogle: String = "GOOGLE"
    }
    
    struct Messages {
        static let success = "Success".lowercased()
        //        static let success = "Success".localized()
    }
    
    
    
    enum Authorities:String
    {
        case user = "ROLE_USER"
        case admin = "ROLE_ADMIN"
    }
    enum SupportedActions:String
    {
        case reply = "REPLY"
        case replyAll = "REPLY_ALL"
        case signature = "SIGNATURE"
        case comment = "COMMENT"
        
    }
    
    enum Optionality:String
    {
        case required = "(Required)"
        case optional = "(Optional)"
    }
    enum StatusCode: Int {
        case StatusNoConnection = -1
        case StatusNone = 0
        case StatusOK = 200
        case StatusBadRequest = 400
        case StatusNotfound = 404
        case UserNotAuthorized = 401
        case requestTimeout = -1001
        case ServerTimeout = 504
        case UserForbidden = 403
        case Failure = 500
        case NotFound = 502
        case tooManyResquests = 429
        case Unavailable = 503
        case Unsupported = 405
        case NotLogin = 406
        
        
    }
    
    struct languages {
        static let en = "en"
        static let fr = "fr"
    }
    struct headers {
        static let accessKey = "access_key"
        static let accessKeyValue = "RcWr19Ws52Cv2c20ASh221Sa"
        static let accessPasswordKey = "access_password"
        static let accessPasswordValue = "LKJKm52AS2h1h1hQAW1a454qrlMll"
        static let applicationTypeKey = "Content-Type"
        static let acceptLanguageKey = "Accept-Language"
        static let applicationTypeValue = "application/json"
        static let authorization = "bearer 76ac47cd-bc95-453e-9b63-977b40c46f40"
        
    }
    struct Notifications {
        static let refreshUserChannels = "RefreshUserChannels"
        static let socailChannelCancel = "socailChannelCancel"
        static let outhPeriodsChannel = "outhPeriodsChannel"
        static let sidemenu = "sidemenu"
        static let addNewChannel = "addNewChannel"
        static let addNewFlow = "addNewFlow"
        static let updateFeeds = "updateFeeds"
        static let HomeChannels = "HomeChannels"
        static let HomeFlow = "HomeFlows"
        static let refreshAccounts = "refreshAccounts"
        static let getChannelFeeds = "getChannelFeeds"
        static let CloseSafari = "closeSafari"
        static let addSenderList = "addSenderList"
        static let refreshNotifications = "refreshNotifications"
        static let updateSignature = "updateSignature"
        static let discardChanges = "discardChanges"
        
        
    }
    struct AlertType {
        static let AlertSuccess = 2
        static let AlertError = 1
        static let Alertinfo = 3
        static let AlertWarn = 4
    }
    
    struct userDefault {
        static let userName = "userName"
        static let userData = "userData"
        static let isSkip = "isSkip"
        static let consumer_key = "consumer_key"
        static let consumer_secret = "consumer_secret"
        static let fcm_token = "fcm_token"
        static let hasComment = "hasComment"
        static let selectedFlowId = "selectedFlowId"
        static let selectedFlowIndex = "selectedFlowIndex"
        static let isEdit = "isEdit"
        static let notificationBadge = "notificationBadge"
        static let channelsSyncPeriods = "channelsSyncPeriods"
        static let rabbitMQUserName = "rabbitMQUserName"
        static let rabbitMQPassword = "rabbitMQPassword"
        static let rabbitMQURL = "rabbitMQURL"
        static let rabbitMQSecurity = "rabbitMQSecurity"
        static let minRequiredVersion = "minRequiredVersion"
        static let latestVersion = "latestVersion"
    }
    
    struct StoryBoard {
        static let mainSB = "Main"
        static let wizeredSB = "Wizered"
    }
    struct Colors {
        static let gold = "FFD700"
    }
    
}
