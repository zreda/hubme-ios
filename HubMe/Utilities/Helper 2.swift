//
//  Helper.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/15/16.
//  Copyright © 2016 Alaa Taher. All rights reserved.
//

import UIKit
import SCLAlertView
import Foundation
import  JDropDownAlert
import Messages

public extension UIImage {
    func base64Encode() -> String? {
        guard let imageData = UIImagePNGRepresentation(self) else {
            return nil
        }

        let base64String = imageData.base64EncodedString()
        let fullBase64String = "data:image/png;base64,\(base64String))"

        return fullBase64String
    }
    
    
}


extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
}

extension String.CharacterView {
    /// This method makes it easier extract a substring by character index where a character is viewed as a human-readable character (grapheme cluster).
    internal func substring(start: Int, offsetBy: Int) -> String? {
        guard let substringStartIndex = self.index(startIndex, offsetBy: start, limitedBy: endIndex) else {
            return nil
        }

        guard let substringEndIndex = self.index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex) else {
            return nil
        }

        return String(self[substringStartIndex ..< substringEndIndex])
    }
}


extension UIAlertController {
    func changeFont(view: UIView, font: UIFont) {
        for item in view.subviews {
            if item is UICollectionView {
                let col = item as! UICollectionView
                for  row in col.subviews {
                    self.changeFont(view: row, font: font)
                }
            }
            if item is UILabel {
                let label = item as! UILabel
                label.font = font
            } else {
                self.changeFont(view: item, font: font)
            }

        }
    }
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //        let font =  UIFont(name: "GE SS", size: 16.0)!
        //        changeFont(view: self.view, font: font )
    }
}

class Helper: NSObject {

    static let userDef = UserDefaults.standard

    class func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    static func convertDataToJson(data: Data) ->Any {
        var jsonDic: Any?
        do {
            jsonDic = try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions())
        }
        return jsonDic as Any
    }

    static func getNetworkStatus() -> Bool {

        guard let reachability = Network.reachability?.isReachable
            else {
                return false
        }

        if (reachability == false) {
            Helper.showFloatAlert(title: "no_internet".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)

        }
        return reachability
    }

    // show custom alert
    static func showAlert(type: Int, title: String, subTitle: String, closeTitle: String) {
        if type == HubMeConstants.AlertType.AlertError {
            SCLAlertView().showError(title, subTitle: subTitle, closeButtonTitle: closeTitle)
        } else if type == HubMeConstants.AlertType.AlertSuccess {
            SCLAlertView().showSuccess(title, subTitle: subTitle, closeButtonTitle: closeTitle)
        } else if type == HubMeConstants.AlertType.Alertinfo {
            SCLAlertView().showInfo(title, subTitle: subTitle, closeButtonTitle: closeTitle)
        } else if type == HubMeConstants.AlertType.AlertWarn {
            SCLAlertView().showWarning(title, subTitle: subTitle, closeButtonTitle: closeTitle)

        }
    }

    static func showFloatAlert(title: String, subTitle: String, type: Int) {

        let alert = JDropDownAlert()

//        alert.alertWith(title)
//        alert.messageFont = UIFont.systemFont(ofSize: 8)
        alert.titleFont = UIFont.systemFont(ofSize: 14)  
        if type == HubMeConstants.AlertType.AlertError {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor.red)
        } else if type == HubMeConstants.AlertType.AlertSuccess {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor.green)

        } else if type == HubMeConstants.AlertType.Alertinfo {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor.blue)

        }
        alert.didTapBlock = {
            debugPrint("Top View Did Tapped")
        }
    }

    // insert and retreive object or primitave data type in user defualt

    static func saveUserDefault(key: String, value: Any) {
        userDef.set(value, forKey: key)
    }
    static func getUserDefault(key: String)->Any {
        return userDef.object(forKey: key) as Any
    }
    static func removeKeyUserDefault(key: String) {
        userDef.removeObject(forKey: key)
        userDef.synchronize()

    }

    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return userDef.object(forKey: key) != nil
    }

    static func saveObjectDefault(key: String, value: Any) {
        let userDataEncoded = NSKeyedArchiver.archivedData(withRootObject: value)
        userDef.set(userDataEncoded, forKey: key)
        userDef.synchronize()
    }

    static func getObjectDefault(key: String)->Any {

        if let decodedNSData = UserDefaults.standard.object(forKey: key) as? NSData,
            let Data = NSKeyedUnarchiver.unarchiveObject(with: decodedNSData as Data) {

            return Data
        }
        else
        {
            debugPrint("Failed")
            return ""
        }
    }

    static func checkFound(str:String, array:[Bcc]) -> Bool
    {
        for item in array
        {
            if item.email == str || item.name == str
            {
                return true
            }
        }
        return false
    }
    // convert UIColor from hexa color

    static func hexStringToUIColor (hex: String) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    static func heightForText(text: String,Font: UIFont,Width: CGFloat) -> CGFloat{
        
        let constrainedSize = CGSize.init(width:Width, height: CGFloat(MAXFLOAT))
        
        let attributesDictionary = NSDictionary.init(object: Font, forKey:NSAttributedStringKey.font as NSCopying)
        
        let mutablestring = NSAttributedString.init(string: text, attributes: attributesDictionary as? [NSAttributedStringKey : Any])
        
        var requiredHeight = mutablestring.boundingRect(with:constrainedSize, options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil)
        
        if requiredHeight.size.width > Width {
            requiredHeight = CGRect.init(x: 0, y: 0, width: Width, height: requiredHeight.height)
            
        }
        return requiredHeight.size.height + 8
    }
    
    // Animate Table View

    static func animateTable(table: UITableView) {
        table.reloadData()

        let cells = table.visibleCells
        let tableHeight: CGFloat = table.bounds.size.height

        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }

        var index = 0

        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)

            index += 1
        }
    }

    static func openUrl(urlStr: String) {

        if let url = URL(string: urlStr) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            debugPrint("Open \(urlStr): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                debugPrint("Open \(urlStr): \(success)")
            }
        }

    }

     static func convertStringToDictionary(_ text: String) -> [String: String]? {

        if let data = text.data(using: String.Encoding.utf8) {

            do {

                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]

            } catch let error as NSError {

                print(error)

            }

        }

        return nil

    }

    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                // print(error.localizedDescription)
            }
        }
        return nil
    }
    static func getDeviceLang() -> String
    {
        let arr = (Locale.preferredLanguages[0] as String).components(separatedBy: "-")
        return  arr.first!

    }

    static func getCurrentLang() -> String {
        if let lang =   Helper.getUserDefault(key: "lang") as? String {
            return lang
        }
        return "en"
    }

    static func getImageWithSize(urlStr: String, originalSize: String, updateSize: String) -> String {//"20x14"
        let flag = urlStr.replacingOccurrences(of: originalSize, with: updateSize, options: .literal, range: nil)
        return flag
    }

    static func isValidEmail(mail_address: String) -> Bool {
//        let emailRegex = "[A-Z0-9a-z._%+-àâçéèêëîïôûùüÿñæœ]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailRegex = "^[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@(orange)(\\.com)$"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: mail_address)

    }
    static func isValidPassword(password: String) -> Bool {
        //        let emailRegex = "[A-Z0-9a-z._%+-àâçéèêëîïôûùüÿñæœ]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let passwordRegex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\\w\\s]).{8,}$"
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: password)
        
    }

    static func verifyOnlyCharacterAndNumber(text: String) -> Bool {
        let emailRegex = "^[A-Za-z0-9 àâçéèêëîïôûùüÿñæœ]*$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)

    }
    static func verifyOnlyNumber(text: String) -> Bool {
        let emailRegex = "^[0-9 ]*$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)

    }

    static func verifyOnlyCharacter(text: String) -> Bool {
        let emailRegex = "^[A-Za-z _àâçéèêëîïôûùüÿñæœ]*$"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)

    }

    static func getStringUntill(myString: String, character: Character) -> String {
        var newString = ""
        for c in myString.characters {
            if c != character {
                newString.append(c)
            } else {
                break
            }
        }

        return newString

    }
     static func isValidAccount(account: String) -> Bool {
        
        let accountRegex = "^([a-zA-Z])+\\\\([A-Z0-9a-z._-àâçéèêëîïôûùüÿñæœ])+$"
        let texttest = NSPredicate(format: "SELF MATCHES %@", accountRegex)
        guard texttest.evaluate(with: account) else { return false }

        return true
    }
    
    
    static func isValidUserName(text: String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9-_@..àâçéèêëîïôûùüÿñæœ]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
        
    }
    
    static func isValidKeywords(text: String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9-\\s_;<=>?@.àâçéèêëîïôûùüÿñæœ]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
        
    }
    
    static func isValidSenders(text: String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9-_;<=>?@.àâçéèêëîïôûùüÿñæœ]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)

    }
    
    static func isValidFlowName(text: String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9-_; <=>?@.àâçéèêëîïôûùüÿñæœ]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
        
    }
    
    static func isValidFeedback(text: String) -> Bool {
        let emailRegex = "^[a-zA-Z0-9-_; <=>?@.àâçéèêëîïôûùüÿñæœ,\\-]*$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: text)
        
    }

    static func getDayOfWeek(_ today: String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        guard let todayDate = formatter.date(from: today) else { return nil }
//        let myCalendar = Calendar(identifier: .gregorian)
//        let weekDay = myCalendar.component(.weekday, from: todayDate)

        return todayDate.dayOfWeek()
    }
    static func pushToViewController(stroyBoardName: String, controllerIdentifier: String, navController: UINavigationController) {
        let accountStoryBoard = UIStoryboard(name: stroyBoardName, bundle: nil)
        let controller = accountStoryBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        navController.pushViewController(controller, animated: true)
    }

    static func presentViewController(stroyBoardName: String, controllerIdentifier: String, viewController: UIViewController) {
        let accountStoryBoard = UIStoryboard(name: stroyBoardName, bundle: nil)
        let controller = accountStoryBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        viewController.present(controller, animated: true, completion: nil)
    }
    

    static func goToLoginViewController(controller: UIViewController) {
        
        let loginVC = controller.storyboard?.instantiateViewController(withIdentifier: "AuthorizationNC") as! UINavigationController

        controller.navigationController?.present(loginVC, animated: false, completion: nil)

    }
    
    

    static func groupChannels(channels:[UserChannelsItem] ) -> [GroupedChannel]
    {
        var groupChannels : [GroupedChannel] = []
        
        for (_,item) in channels.enumerated()
        {
            if groupChannels.contains(where: { $0.groupName == item.name}) {
         
                let index = Helper.getIndexOf(channels:groupChannels,groupName:item.name ?? "") ?? 0
                groupChannels[index].accounts?.append(item)
            }
            else
            {
                groupChannels.append(GroupedChannel(groupId: item.channelId ?? 0,groupName: item.name ?? "",firstConfig:item.firstLoadConfig ?? FirstLoadConfig(), accounts: [item]))
            }
        }
        
        return groupChannels
        
    }
    
//    static func groupActiveChannels(channels:[UserChannelsItem] ) -> [GroupedChannel]
//    {
//        var groupChannels : [GroupedChannel] = []
//        
//        for (_,item) in channels.enumerated()
//        {
//            if groupChannels.contains(where: { $0.groupName == item.name}) {
//                
//                let index = Helper.getIndexOf(channels:groupChannels,groupName:item.name ?? "") ?? 0
//               if item.firstLoadStatus?.lowercased() != HubMeConstants.FeedsLoadStatus.expired.lowercased()
//               {
//                    groupChannels[index].accounts?.append(item)
//                }
//            }
//            else
//            {
//                groupChannels.append(GroupedChannel(groupId: item.channelId ?? 0,groupName: item.name ?? "",firstConfig:item.firstLoadConfig ?? FirstLoadConfig(), accounts: [item]))
//            }
//        }
//        
//        return groupChannels
//        
//    }
    
    static func getIndexOf(channels:[GroupedChannel],groupName:String) -> Int?
        
    {
        for (index,item) in channels.enumerated()
        {
            if item.groupName == groupName
            {
                return index
            }
        }
        return nil
        
    }
    static func convertMillSecondStringtoDate(str: String?) -> Date {

        return  Date(timeIntervalSince1970: (Double(str ?? "0.0")!/1000.0))

    }

    static func convertIntervaltoDate(interval: TimeInterval?) -> Date {

        return  Date(timeIntervalSinceNow: interval!)

    }

    static func convertDatetoString(date: Date, formate: String) -> String {
        let formatter = DateFormatter()
//        formatter.timeZone = TimeZone(abbreviation: "GMT")
//        formatter.locale = NSLocale.current

        // initially set the format based on your datepicker date
        formatter.dateFormat = formate
        // convert your string to date
        formatter.dateFormat = formate
        // again convert your date to string
        let myStringafd = formatter.string(from: date)

        return (myStringafd)

    }

    static func convertDateFormate(date : Date) -> String{
        // Day
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMMM"
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        return newDate + " " + day 
    }
    
    static func timeAgoSinceDate(date: Date, numericDates: Bool) -> String {
        let calendar = Calendar.current
        let now = Date()
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components: DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute, NSCalendar.Unit.hour, NSCalendar.Unit.day, NSCalendar.Unit.weekOfYear, NSCalendar.Unit.month, NSCalendar.Unit.year, NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())


        if (components.month! >= 1) {

            return Helper.convertDateFormate(date: date)
        } else if (components.weekOfYear! >= 2) {
            return String(components.weekOfYear!) + "weeks ago".localized()
        } else if (components.weekOfYear! >= 1) {
                return "1" + "week ago".localized()
        } else if (components.day! >= 2) {
            return String(components.day!) + "days ago".localized()
        } else if (components.day! >= 1) {
            if (numericDates) {
                return "1 "+"day ago".localized()
            } else {
                return "Yesterday".localized()
            }
        } else if (components.hour! >= 2) {
            return String(components.hour!) + "h ago".localized()
        } else if (components.hour! >= 1) {
            if (numericDates) {
                return "1 " + "h ago".localized()
            } else {
                return "1" + "h ago".localized()
            }
        } else if (components.minute! >= 2) {
            return  String(components.minute!) + "min ago".localized()
        } else if (components.minute! >= 1) {
            if (numericDates) {
                return "1 min ago".localized()
            } else {
                return "1" + "min ago".localized()
            }
        } else if (components.second! >= 0) {
//            return String(components.second!) + "sec ago".localized()
            return "a few seconds ago".localized()
        }
        else {
            return "a few seconds ago".localized()
        }
    }
    
    
    static func getSyncTime(of numberofDays:NSInteger)  -> String
    {
        if numberofDays < 7
        {
            return String(numberofDays) + ""
        }
        else if numberofDays/7 < 4
        {
            return String(numberofDays%7)
        }
        return ""
    }
    
    static func getColorFromStatus(status:meetingStatus) -> (UIColor,UIColor)
    {
        switch status {
        case .accept , .organizer:
            return (#colorLiteral(red: 0.7803921569, green: 0.9294117647, blue: 0.8470588235, alpha: 0.8),#colorLiteral(red: 0.737254902, green: 0.9176470588, blue: 0.8117647059, alpha: 1))
        case .decline :
            return (#colorLiteral(red: 1, green: 0.7137254902, blue: 0.7137254902, alpha: 0.5),#colorLiteral(red: 1, green: 0.5333333333, blue: 0.5333333333, alpha: 0.8))
        case .tentative :
            return (#colorLiteral(red: 0.9843137255, green: 0.8980392157, blue: 0.8274509804, alpha: 0.61),#colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 0.54))
        case .noResponse , .unknown:
            return (#colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1),#colorLiteral(red: 0.8235294118, green: 0.8235294118, blue: 0.8235294118, alpha: 0.8))
      
        }
    }
}
