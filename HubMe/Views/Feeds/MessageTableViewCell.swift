//
//  MessageTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/13/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import ReadMoreTextView

protocol NotSoGoodCellDelegate {
    func moreTapped(cell: MessageTableViewCell)
}


class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var senderImg: UIImageView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var messageText: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var showMoreBtn: UIButton!
    var delegate: NotSoGoodCellDelegate?
    var isExpanded: Bool = false

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
//    @IBAction func btnMoreTapped(_ sender: Any) {
//        
//        if sender is UIButton {
//            isExpanded = !isExpanded
//            messageText.numberOfLines = isExpanded ? 0 : 2
//            showMoreBtn.setTitle(isExpanded ? "Read less...".localized() : "Read more...".localized(), for: .normal)
//            
//            delegate?.moreTapped(cell: self)
//        }
//        
//    }

    func configureCell(feed: MessageItem) {
        isExpanded = false
    
        if (feed.iconUrl?.hasPrefix("http"))! {
            senderImg.sd_setImage(with: URL(string: feed.iconUrl!), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            
        } else {
            senderImg.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios/"+feed.iconUrl!) ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }

        senderName.text = feed.from
        date.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: feed.date), numericDates: false)
        messageText.text = feed.snippet ?? ""
        messageText.numberOfLines = feed.isExpanded ? 0 : 2
        showMoreBtn.setTitle(feed.isExpanded ? "Read less...".localized() : "Read more...".localized(), for: .normal)

        if  Helper.heightForText(text: messageText.text ?? "", Font: UIFont.systemFont(ofSize: 17.0), Width: 200) < 100.0
        {
            self.showMoreBtn.isHidden = true
        }
        else
        {
            self.showMoreBtn.isHidden = false
            
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
