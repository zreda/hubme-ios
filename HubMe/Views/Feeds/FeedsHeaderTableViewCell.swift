//
//  FeedsHeaderTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/28/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class FeedsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var channelIcon: UIImageView!
    @IBOutlet weak var currentAccountLb: UILabel!
    @IBOutlet weak var switchAccountBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
