//
//  RecipientTimeTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class RecipientTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var plazzaTypeImg: UIImageView!
    @IBOutlet weak var plazzaTypeWidthCons: NSLayoutConstraint!
    @IBOutlet weak var recipientTimeLb: UILabel!
    
    @IBOutlet weak var collapseBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
