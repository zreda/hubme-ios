//
//  MailTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/22/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class FeedTableViewCell: UITableViewCell {
    @IBOutlet weak var feed_logo: UIImageView!
    @IBOutlet weak var feed_from: UILabel!
    @IBOutlet weak var feed_subject: UILabel!
    @IBOutlet weak var feed_time: UILabel!
    @IBOutlet weak var feed_content: UILabel!

 
    func configureCell(feed: MessageItem) {
//        feed_logo.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+feed.iconUrl!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        feed_subject.text = feed.title
        feed_from.text = feed.from
        
        if feed.isRead == false || feed.isRead == nil
        {
            feed_subject.font = UIFont.boldSystemFont(ofSize: 15)
            feed_from.font = UIFont.boldSystemFont(ofSize: 17)
            feed_logo.image = #imageLiteral(resourceName: "pagination_orange")
        }
        else
        {
            feed_subject.font = UIFont(name:"Arial",size:15)
            feed_from.font = UIFont(name:"Arial",size:17)
            feed_logo.image = nil
        }
        
        feed_time.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: feed.date), numericDates: false)
        feed_content.text = feed.snippet ?? ""
    }

}
