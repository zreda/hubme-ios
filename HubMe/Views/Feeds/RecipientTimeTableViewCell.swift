//
//  RecipientTimeTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD

class RecipientTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var senderLb: UILabel!
    @IBOutlet weak var recipientTimeLb: UILabel!
    @IBOutlet weak var showMoreBtn: UIButton!
    @IBOutlet weak var numberOfRecipient: UILabel!
    @IBOutlet weak var firstRecipientLb: UILabel!
    
//    @IBOutlet weak var collapseBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension RecipientTimeTableViewCell : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
//        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
//        view?.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }
    func setError(error: NSError, url: String) {
//        self.tableView.isUserInteractionEnabled = true
//        SVProgressHUD.dismiss()
//        self.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        
    }
    func setErrorReponse(msg: String?) {
        SVProgressHUD.dismiss()
//        self.tableView.isUserInteractionEnabled = true

        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
        
    }
}
