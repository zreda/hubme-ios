//
//  FeedContentTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import WebKit
class FeedContentTableViewCell: UITableViewCell,  UIWebViewDelegate {

//    @IBOutlet weak var feedContentWebView: WKWebView!
    @IBOutlet weak var feedContentWebView: UIWebView!

    @IBOutlet weak var webViewHeightConst: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  

}
