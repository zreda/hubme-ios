//
//  RecipientParentTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD
class RecipientParentTableViewCell: UITableViewCell , UITableViewDataSource , UITableViewDelegate {

    @IBOutlet weak var tableView: SelfSizedTableView!

    var  toRecipients : [Bcc] = []
    var  ccRecipients : [Bcc] = []
    var  bccRecipients : [Bcc] = []
    fileprivate var plazzaProfilePresenter: PlazzaProfilePresenter?
    var view:FeedsDetailsTableViewController?
    override func awakeFromNib() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    @objc func getPlazzaProfile(sender:RecipeintBtn)
    {
        tableView.isUserInteractionEnabled = false
        plazzaProfilePresenter = PlazzaProfilePresenter(self)
        var email = ""
        if sender.section == 0
        {
            email =  toRecipients[sender.row].email ?? ""
        }
        else if sender.section == 1
        {
            email =  ccRecipients[sender.row].email ?? ""

        }
        else if sender.section == 2
        {
            email =  bccRecipients[sender.row].email ?? ""

        }
        plazzaProfilePresenter?.getPlazzaProfile(email: email)
        SVProgressHUD.show()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return toRecipients.count
        }
        else if section == 1
        {
            return ccRecipients.count
        }
        else
        {
            return bccRecipients.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "textCell")! as! RecipientTableViewCell
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                cell.receipientTypeLb.text =  "TO".localized()

            }
            else
            {
                 cell.receipientTypeLb.text = ""
            }
            cell.recipientEmailLb.text = toRecipients[indexPath.row].name ?? toRecipients[indexPath.row].email ?? ""
        }
        else if indexPath.section == 1
        {
            if indexPath.row == 0
            {
                cell.receipientTypeLb.text =  "CC".localized()
                
            }
            else
            {
                cell.receipientTypeLb.text = ""
            }
            cell.recipientEmailLb.text = ccRecipients[indexPath.row].name ?? ccRecipients[indexPath.row].email ?? ""
        }
        else if indexPath.section == 2
        {
            if indexPath.row == 0
            {
                cell.receipientTypeLb.text =  "BCC".localized()
                
            }
            else
            {
                cell.receipientTypeLb.text = ""
            }
            cell.recipientEmailLb.text = bccRecipients[indexPath.row].name ?? bccRecipients[indexPath.row].email ?? ""
        }
        cell.plazzaProfileBtn.row = indexPath.row
        cell.plazzaProfileBtn.section = indexPath.section

        cell.plazzaProfileBtn.addTarget(self, action: #selector(RecipientParentTableViewCell.getPlazzaProfile), for: .touchUpInside)

        cell.layoutIfNeeded()
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}
extension RecipientParentTableViewCell : PlazzaProfilePresenterView
{
    func getProfileDetails(response: Profile?) {
        self.tableView.isUserInteractionEnabled = true

        SVProgressHUD.dismiss()
        let plazzaProfileVC = PlazzaProfileViewController.create()
        plazzaProfileVC.profile = response
        view?.navigationController?.pushViewController(plazzaProfileVC, animated: true)
        
    }
    func setError(error: NSError, url: String) {
        self.tableView.isUserInteractionEnabled = true
        SVProgressHUD.dismiss()
        view?.handelError(errorCode: HubMeConstants.StatusCode(rawValue: error.code)!, messge: error.description, url: url)
        
    }
    func setErrorReponse(msg: String?) {
        SVProgressHUD.dismiss()
        self.tableView.isUserInteractionEnabled = true

        Helper.showFloatAlert(title: msg ?? "", subTitle: "", type: HubMeConstants.AlertType.AlertError)
        
        
    }
}
