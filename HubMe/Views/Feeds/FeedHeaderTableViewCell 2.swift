//
//  FeedHeaderTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class FeedHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var channelLogoImg: UIImageView!
    @IBOutlet weak var senderLb: UILabel!
    @IBOutlet weak var feedSubjectLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
