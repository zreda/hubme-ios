//
//  EmaiHeaderView.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/9/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit


 class EmaiHeaderView: UIView {
    @IBOutlet weak var sent: UILabel!
    @IBOutlet weak var receiverLb: UILabel!
    @IBOutlet weak var ccLb: UILabel!
    @IBOutlet weak var subject: UILabel!

}
