//
//  TwitterTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/4/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {
    @IBOutlet weak var feed_logo: UIImageView!
    @IBOutlet weak var feed_from: UILabel!
    @IBOutlet weak var feed_subject: UILabel!
    @IBOutlet weak var feed_time: UILabel!
    @IBOutlet weak var feed_content: UILabel!
    @IBOutlet weak var feed_photo: UIImageView!

    @IBOutlet weak var readIcon: UIImageView!
    @IBOutlet weak var tweetPhotoCons: NSLayoutConstraint!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func configureCell(feed: MessageItem) {
        if (feed.iconUrl?.hasPrefix("http"))! {
            feed_logo.sd_setImage(with: URL(string: feed.iconUrl!), placeholderImage: #imageLiteral(resourceName: "avatar_icon"))

        } else {
            feed_logo.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios/"+feed.iconUrl!) ), placeholderImage: #imageLiteral(resourceName: "avatar_icon"))
        }
        if feed.options?.screenName == "" || feed.options?.screenName ==  nil {
            feed_subject.text = (feed.title ?? "")!
        } else {
            feed_subject.text = "@"+(feed.options?.screenName ?? "")!

        }
        feed_from.text = feed.from
        feed_time.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: feed.date), numericDates: false)

        if feed.isRead == false || feed.isRead == nil
        {
            readIcon.image = #imageLiteral(resourceName: "unread")
            
        }
        else
        {
            readIcon.image = #imageLiteral(resourceName: "read")
            
        }
    
//        if feed.attachments?[0] != nil {
//
//            feed_photo.sd_setImage(with: URL(string: ((feed.attachments?[0].url!)!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
//            tweetPhotoCons.constant = 150
//            feed_photo.isHidden = false
//
//        } else {
//            tweetPhotoCons.constant = 0
//
//            feed_photo.isHidden = true
//
//        }

        feed_content.text = feed.snippet ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
