//
//  MeetingTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 11/20/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class MeetingTableViewCell: UITableViewCell {

    @IBOutlet weak var meeting_month: UILabel!
    @IBOutlet weak var meeting_day: UILabel!
    @IBOutlet weak var meeting_subject: UILabel!
    @IBOutlet weak var meeting_time: UILabel!
    @IBOutlet weak var meeting_location: UILabel!
    @IBOutlet weak var location_img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
