//
//  RecipientTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/25/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class RecipeintBtn: UIButton{
    
    var row: Int = 0
    var section: Int = 0
    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 0.1750856164) : .clear
        }
    }
    
}
class RecipientTableViewCell: UITableViewCell {

    @IBOutlet weak var receipientTypeLb: UILabel!
    @IBOutlet weak var recipientEmailLb: UILabel!
    @IBOutlet weak var plazzaProfileBtn: RecipeintBtn!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
