//
//  FullDayEventTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/22/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class FullDayEventTableViewCell: UITableViewCell {

    @IBOutlet var numOfDays: UILabel!
    @IBOutlet var fullDayView: CardView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
