//
//  PlazzaProfileTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class PlazzaProfileHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var plazzaImg: UIImageView!
    @IBOutlet weak var plazzaName: UILabel!
    @IBOutlet weak var plazzaTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
