//
//  PlazzaPorfileTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class PlazzaProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    var dataArray:[String] = []
    var sectionItem = ("",UIImage())
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
//        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension PlazzaProfileTableViewCell: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "section_cell")
            cell?.textLabel?.text = sectionItem.0
            cell?.imageView?.image = sectionItem.1

            return cell ?? UITableViewCell()
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "data_cell") as! PlazzaProfileDataTableViewCell
            cell.dataLb.text = dataArray[indexPath.row-1]
            if indexPath.row  == dataArray.count
            {
                cell.sepeatorView.isHidden = true
            }
            return cell
        }
        
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count + 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if sectionItem.0.lowercased() == "Plazza".lowercased()
        {
            UIApplication.shared.open(URL(string: dataArray[0])!)
        }
       self.tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
}
