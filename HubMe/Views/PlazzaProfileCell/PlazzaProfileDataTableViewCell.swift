//
//  PlazzaProfileDataTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class PlazzaProfileDataTableViewCell: UITableViewCell {
    @IBOutlet weak var dataLb: UILabel!
    @IBOutlet weak var sepeatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
