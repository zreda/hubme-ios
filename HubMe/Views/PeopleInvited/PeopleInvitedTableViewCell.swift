//
//  PeopleInvitedTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 7/2/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class PeopleInvitedTableViewCell: UITableViewCell {
    @IBOutlet weak var inviteeImg: UIImageView!
    @IBOutlet weak var inviteeName: UILabel!
    @IBOutlet weak var inviteeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
