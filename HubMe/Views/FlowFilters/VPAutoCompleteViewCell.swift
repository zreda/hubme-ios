//
//  VPAutoCompleteViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 8/14/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class VPAutoCompleteViewCell: UITableViewCell {

    @IBOutlet weak var channel_icon: UIImageView!
    @IBOutlet weak var sender_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
