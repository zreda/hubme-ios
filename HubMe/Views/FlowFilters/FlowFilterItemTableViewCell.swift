//
//  FlowFilterItemTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/26/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class FlowFilterItemTableViewCell: UITableViewCell {

    @IBOutlet weak var filterItemName: UILabel!
    @IBOutlet weak var deleteFilterBtn: UIButton!
    @IBOutlet weak var channel_icon: UIImageView!
    @IBOutlet weak var imgConst: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
