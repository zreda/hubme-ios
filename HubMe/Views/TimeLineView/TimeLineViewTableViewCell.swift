//
//  TimeLineOneViewTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class TimeLineViewTableViewCell: UITableViewCell {

    @IBOutlet weak var timeMode: UILabel!
    @IBOutlet weak var startTimeLb: UILabel!
    @IBOutlet weak var event1View: CardView!
    @IBOutlet weak var event1ChannelImg: UIImageView!
    @IBOutlet weak var event1Title: UILabel!
    @IBOutlet weak var event1Time: UILabel!
    @IBOutlet weak var event1Location: UILabel!
    @IBOutlet weak var event2View: CardView!
    @IBOutlet weak var event2Title: UILabel!
    @IBOutlet weak var event2Time: UILabel!
    @IBOutlet weak var event2Location: UILabel!

    @IBOutlet weak var event2ChannelImg: UIImageView!
    @IBOutlet weak var eventStatusView: CardView!
    @IBOutlet weak var event3ChannelImg: UIImageView!
    @IBOutlet weak var event3Time: UILabel!
    @IBOutlet weak var event3View: CardView!
    @IBOutlet weak var event3Title: UILabel!
    @IBOutlet weak var event3Location: UILabel!

    @IBOutlet weak var eventsMoreView: CardView!
    @IBOutlet weak var numExtraEvents: UILabel!
    @IBOutlet weak var extraEventsBtn: UIButton!
    
    @IBOutlet weak var eventChannelImg: UIImageView!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventView: CardView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
