//
//  CustomSectionHeader.swift
//  CustomSectionHeader
//
//  Created by Onur Tuna on 12.02.2019.
//  Copyright © 2019 onurtuna. All rights reserved.
//

import UIKit

protocol  SelectAllChannels{
    func selectAllChannels(selectAll:SelectStatus, section:Int)
}
class FilterViewHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var channelImg: UIImageView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var selectAllBtn: CheckBox!
    var delegate:SelectAllChannels?
    
    override func awakeFromNib() {
          selectAllBtn.checkedImage = #imageLiteral(resourceName: "group_10")
          selectAllBtn.uncheckedImage = #imageLiteral(resourceName: "rectangle")
    }
    @IBAction func selectAllBtnAction(_ sender: Any) {
        
//    !selectAllBtn.isChecked ? !selectAllBtn.isChecked  : nil
//        if selectAllBtn.isChecked
//        {
//            SelectStatus.deSelectAll
//        }
//        else
//        {
//            SelectStatus.SelectAll
//
//        }
        delegate?.selectAllChannels(selectAll: (selectAllBtn.isChecked ? SelectStatus.deSelectAll : SelectStatus.selectAll) , section: selectAllBtn.tag)
    }
}
