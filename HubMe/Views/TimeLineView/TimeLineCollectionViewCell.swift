//
//  TimeLineCollectionViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class TimeLineCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var eventChannelImg: UIImageView!
    @IBOutlet weak var eventView: CardView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var eventLocationImg: UIImageView!
    @IBOutlet weak var eventTime: UILabel!
}
