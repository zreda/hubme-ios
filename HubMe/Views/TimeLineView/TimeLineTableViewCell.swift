//
//  TimeLineTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class TimeLineTableViewCell: UITableViewCell {

    @IBOutlet weak var timeMode: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var eventStatusView: CardView!
    @IBOutlet weak var timeLineCollectionView: UICollectionView!
    var eventsObj :TimeLineEvent?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.timeLineCollectionView.delegate = self
        self.timeLineCollectionView.dataSource = self
        startDate.text = Helper.convertDatetoString(date: Date(timeIntervalSince1970: eventsObj?.startTime  ?? 0.0), formate: "HH:mm")
        timeMode.text = Helper.convertDatetoString(date: Date(timeIntervalSince1970: eventsObj?.startTime  ?? 0.0), formate: "a")
        eventStatusView.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsObj?.events?[0].myResponse ?? "")) ?? .unknown).1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension TimeLineTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return eventsObj?.events?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: TimeLineCollectionViewCell = timeLineCollectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! TimeLineCollectionViewCell

        cell.eventView.backgroundColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsObj?.events?[indexPath.row].myResponse ?? "")) ?? .unknown).0
        cell.eventView.borderColor = Helper.getColorFromStatus(status: meetingStatus(rawValue: (eventsObj?.events?[indexPath.row].myResponse)!) ?? .unknown).1
        cell.eventTime.text = Helper.convertDatetoString(date: Date(milliseconds: Int(eventsObj?.events?[indexPath.row].start ?? 0)), formate: "HH:mm a") + "-" +  Helper.convertDatetoString(date: Date(milliseconds: Int(eventsObj?.events?[indexPath.row].end ?? 0)), formate: "HH:mm a")
        if  eventsObj?.events?[indexPath.row].subject != ""
        {
            cell.eventTitle.text = eventsObj?.events?[indexPath.row].subject
        }
        else
            
        {
            cell.eventTitle.text = "No Subject".localized()

        }
        
        if eventsObj?.events?[indexPath.row].channelName?.lowercased() == HubMeConstants.Channels.exchangeChannel.lowercased()
        {
            cell.eventChannelImg.image = #imageLiteral(resourceName: "channel_exchange_color")
        }
        else {
            cell.eventChannelImg.image = #imageLiteral(resourceName: "channel_gmail_color")
        }
        
        if eventsObj?.events?[indexPath.row].location != nil && eventsObj?.events?[indexPath.row].location != ""
        {
//            cell.eventLocation.isHidden = false
//            cell.eventLocationImg.isHidden = false
            cell.eventLocation.text = eventsObj?.events?[indexPath.row].location
        }
        else
        {
            cell.eventLocation.text = "No Location".localized()

//            cell.eventLocation.isHidden = true
//            cell.eventLocationImg.isHidden = true
        }
        return cell
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 100, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        
    }
}


