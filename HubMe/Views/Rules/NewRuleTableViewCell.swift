//
//  NewRuleTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/23/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import Foundation

class NewRuleTableViewCell: UITableViewCell {

    @IBOutlet weak var ruleNumber: UILabel!
    @IBOutlet weak var addRuleBtn: RoundRectView!
}
