//
//  RuleTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 10/1/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class RuleTableViewCell: UITableViewCell {

    @IBOutlet weak var ruleImg: UIImageView!
    @IBOutlet weak var ruleTitle: UILabel!
    @IBOutlet weak var checkBtn: CheckBox!

    func configureCell(channel: UserChannelsItem) {

        ruleImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios/" +
            channel.icon!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))

        ruleTitle.text = channel.channelUsername

    }

}
