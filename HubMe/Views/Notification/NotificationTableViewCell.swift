//
//  NotificationTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/14/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var channel_icon: UIImageView!
    @IBOutlet weak var num_of_notification: UILabel!
    @IBOutlet weak var channel_name: UILabel!
    @IBOutlet weak var notification_time: UILabel!
    @IBOutlet weak var channel_account: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(notification:NotificationResponse)
    {
        channel_account.text = notification.accountName
        channel_name.text = notification.channelName
        if let date = notification.messageDate
        {
            notification_time.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: String(describing:date as! Int)), numericDates: false)
        }
        num_of_notification.text = notification.message
        
        if (notification.iconUrl?.hasPrefix("http")) ?? false {
            channel_icon.sd_setImage(with: URL(string: notification.iconUrl!), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            
        } else {
            channel_icon.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios/"+(notification.iconUrl! ))), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }
       
        if notification.read ?? false
        {
            self.contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)

        }
        else
        {
            self.contentView.backgroundColor  = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
