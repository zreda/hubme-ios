//
//  ChannelAccountTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/16/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class ChannelAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var accountImg: UIImageView!
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var accountMail: UILabel!
    @IBOutlet weak var checkBtn: CheckBox!
    @IBOutlet weak var removeBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if checkBtn != nil
        {
            checkBtn.checkedImage = #imageLiteral(resourceName: "mark_selected")
            checkBtn.uncheckedImage = #imageLiteral(resourceName: "mark_unselected")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


