//
//  ChannelHeaderTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/16/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class ChannelHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var channelImg: UIImageView!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var checkBtn: CheckBox!
    var delegate:SelectAllChannels?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBtn.checkedImage = #imageLiteral(resourceName: "mark_selected")
        checkBtn.uncheckedImage = #imageLiteral(resourceName: "mark_unselected")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectAllBtnAction(_ sender: Any) {
            
            delegate?.selectAllChannels(selectAll: (checkBtn.isChecked ? SelectStatus.deSelectAll : SelectStatus.selectAll) , section: checkBtn.tag)
    }

}
