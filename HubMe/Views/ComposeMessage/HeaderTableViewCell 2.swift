//
//  HeaderTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var channelImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
