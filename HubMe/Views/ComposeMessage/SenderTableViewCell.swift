//
//  SenderTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class SenderTableViewCell: UITableViewCell {

    @IBOutlet weak var toTagView: TLTagsControl!
    @IBOutlet weak var ccTagView: TLTagsControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
