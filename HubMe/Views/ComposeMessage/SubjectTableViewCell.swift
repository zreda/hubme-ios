//
//  SubjectTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {
    @IBOutlet weak var messageSubject: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
