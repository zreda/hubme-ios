//
//  AutoComplete.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/6/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


class AutoComplete: UIView,UITableViewDelegate, UITableViewDataSource {
    
    
    var tableView: UITableView = UITableView()
    
    //    var results: [String] = ["teste1","teste2","teste3","teste4"]
    var results: [SendersModel] = []
    var word: String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tableView.frame         =   CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.width, height: frame.size.height - 100)
        tableView.delegate      =   self
        tableView.dataSource    =   self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.addSubview(tableView)
        
//        tableView.backgroundColor = UIColor.white
        self.backgroundColor = UIColor.clear
        
    }
    
    
    func wordChanged(){
        print(results.count)
        tableView.reloadData()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.results.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        self.tableView = tableView ?? UITableView()
       
        if let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")
            {
                cell.textLabel?.text = self.results[indexPath.row].email
                cell.imageView?.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios"+(self.results[indexPath.row].channelIconUrl ?? "")!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
                return cell
            }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.results.count == 0{
            tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        }else{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: HubMeConstants.Notifications.addSenderList), object: results[indexPath.row])
            print("You selected cell #\(indexPath.row)!")
        }
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    
    
}
