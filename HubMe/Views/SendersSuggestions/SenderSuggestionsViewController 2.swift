//
//  SenderSuggestionsView.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/23/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class SenderSuggestionsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "SenderCell", bundle: nil), forCellReuseIdentifier: "sender_suggestion_cell")
    }
    
    
}
extension SenderSuggestionsViewController : UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sender_suggestion_cell") as! SenderSuggestionTableViewCell
        //        cell.senderName.text = "Zeinab"
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 5
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
