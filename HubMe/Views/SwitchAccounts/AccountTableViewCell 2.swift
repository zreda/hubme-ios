//
//  AccountTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/28/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var accountEmail: UILabel!
    @IBOutlet weak var accountName: UILabel!
    @IBOutlet weak var defaultImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
