//
//  ManageTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 3/1/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class ManageTableViewCell: UITableViewCell {
    @IBOutlet weak var accountImg: UIImageView!
    @IBOutlet weak var accountEmailLb: UILabel!
    @IBOutlet weak var accountNameLb: UILabel!
    @IBOutlet weak var defaultImg: UIImageView!
    @IBOutlet weak var deleteBtn: RoundedButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
