//
//  FeedCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/20/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SDWebImage
enum LoadStatus :String
{
    case Active = "ACTIVE"
    case New = "NEW"
}
class ChannelTableViewCell: UITableViewCell {

    @IBOutlet weak var channel_img: UIImageView!

    @IBOutlet weak var channel_name: UILabel!

    @IBOutlet weak var tickRead: UIImageView!

    @IBOutlet weak var msgReadingTime: UILabel!
    @IBOutlet weak var footerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(userChannel: UserChannelsItem) {

        channel_img.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" +
                        userChannel.icon!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        channel_name.text = userChannel.name
        
//        if userChannel.firstLoadStatus != LoadStatus.Active.rawValue
//        {
            if userChannel.date == nil {
                msgReadingTime.text = ""
                tickRead.isHidden = true

            }
            else if userChannel.date == "a few seconds ago".localized(){
                msgReadingTime.text = "a few seconds ago".localized()
                msgReadingTime.textColor = UIColor.gray
                tickRead.isHidden = false
            }
            else if Helper.verifyOnlyNumber(text: userChannel.date!) {
                msgReadingTime.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: userChannel.date), numericDates: false)
                msgReadingTime.textColor = UIColor.gray
                tickRead.isHidden = false
                
            }
            else if userChannel.date == "NEW_ACTIVITY" {
                msgReadingTime.text = "New activites".localized()
                msgReadingTime.textColor = UIColor.orange
                tickRead.isHidden = true
                
            }
            else if userChannel.date == "SYNCED"
            {
                msgReadingTime.text = ""
                tickRead.isHidden = true
            }
            else
            {
                msgReadingTime.text = userChannel.date
                msgReadingTime.textColor = UIColor.gray
                tickRead.isHidden = false
            }
//        }
//        else
//        {
//            msgReadingTime.text = "Synchronize"
//            msgReadingTime.textColor = UIColor.orange
//            tickRead.isHidden = false
//
//        }

    }

}
