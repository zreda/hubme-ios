//
//  SideMenuHeaderTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/27/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class SideMenuHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLb: UILabel!
    @IBOutlet weak var appVersionLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
