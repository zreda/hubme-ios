//
//  MeetingResponsesViewController.swift
//  HubMe
//
//  Created by Zeinab Reda on 4/18/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController

class MeetingResponsesViewController: UIViewController {
    var delegate: MeetingDetailsTableViewController?
    var action: meetingActions?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func respondWithCommentBtnAction(_ sender: Any) {
        self.delegate?.handelMeetingAction(action:action ?? meetingActions(rawValue: "")! , subAction: .responseBeforeSend)
    }
    
    @IBAction func respondWithoutCommentBtnAction(_ sender: Any) {
        self.delegate?.handelMeetingAction(action:action ?? meetingActions(rawValue: "")! , subAction: .sendNow)

    }
    
    @IBAction func noResponseBtnAction(_ sender: Any) {
        self.delegate?.handelMeetingAction(action:action ?? meetingActions(rawValue: "")! , subAction: .noReponse)

    }
}
