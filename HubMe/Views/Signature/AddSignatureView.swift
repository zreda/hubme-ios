//
//  AddSignatureView.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/20/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import LSDialogViewController

class AddSignatureView: UIViewController , UITextViewDelegate {
    var delegate: SettingsViewController?

    @IBOutlet weak var addSignatureTF: DesignableUITextField!
    @IBOutlet weak var signatureText: UITextView!
    @IBOutlet weak var numCharacterLeft: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    var signatureStr:String?
    
    
    override func viewDidLoad() {
        backgroundView.layer.cornerRadius = 10
        signatureText.delegate = self
        if signatureStr == nil
        {
            signatureText.textColor = UIColor.lightGray
        }
        signatureText.text = signatureStr ?? ""
        numCharacterLeft.text =  "\(1000 - (signatureText.text?.count ?? 0)!) character left"

    }
    @IBAction func confirmBtnAction(_ sender: Any)
    {
        if signatureText.textColor != UIColor.lightGray &&  signatureText.text != ""
        {

            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.updateSignature), object: nil, userInfo: ["signature":signatureText.text!])

//            delegate?.addSignature(signatureText: signatureText.text!)
        }
        else
        {
            Helper.showFloatAlert(title: "signature empty".localized(), subTitle: "", type: HubMeConstants.AlertType.AlertError)
        }

    }
    @IBAction func cancelBtnAction(_ sender: Any)
    {
            self.delegate?.tabBarController?.dismissDialogViewController()
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let content = textView.text else { return true }
        let count = content.count + text.count - range.length
        return count < 1000
    }
    
    func textViewDidChange(_ textView: UITextView) {
        numCharacterLeft.text =  "\(999 - (signatureText.text?.count ?? 0)!) character left"
    }
}


