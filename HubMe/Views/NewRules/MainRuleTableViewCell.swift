//
//  MainRuleTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/9/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class MainRuleTableViewCell: UITableViewCell {

    @IBOutlet weak var tableView: SelfSizedTableView!
    var ruleImg:UIImage?
    var ruleName:String?
    var isEnabled:Bool = true
    var isRuleOptional:String?
    var selectedChannelItems: [Channel] = []
    var selectedSenderItems: [SendersModel] = []
    var selectedKeywordItems: [String] = []

    var ruleType:RuleType?
    var view:UIViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100.0
        tableView.reloadData()
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
           super.setSelected(selected, animated: animated)

           // Configure the view for the selected state
       }

}

extension MainRuleTableViewCell : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "section_rule_cell") as! RuleSectionTableViewCell
            cell.ruleSectionName.text = ruleName
            cell.ruleSectionImg.image = ruleImg
            cell.requiredLb.text = isRuleOptional

            if selectedChannelItems.count == 0
            {
                cell.speratorView.isHidden = true
            }
            
            if isRuleOptional == HubMeConstants.Optionality.optional.rawValue.localized()
            {
               cell.isRequired.isHidden = true
            }
            else
            {
                 cell.isRequired.isHidden = false
            }
            
            if !isEnabled
            {
                cell.ruleSectionName.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.43)
                cell.requiredLb.textColor = #colorLiteral(red: 0.5254901961, green: 0.5254901961, blue: 0.5254901961, alpha: 0.43)
                cell.arrowImg.image = #imageLiteral(resourceName: "next")
                cell.isUserInteractionEnabled = isEnabled
                cell.speratorView.isHidden = true
            }
            else
            {
                cell.ruleSectionName.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
                cell.requiredLb.textColor = #colorLiteral(red: 0.5254901961, green: 0.5254901961, blue: 0.5254901961, alpha: 0.4605094178)

                 cell.arrowImg.image = #imageLiteral(resourceName: "rightArrow")
                cell.isUserInteractionEnabled = true

            }
            
            cell.layoutIfNeeded()

            return cell
        }
        else
        {

            let cell = tableView.dequeueReusableCell(withIdentifier: "user_account_cell") as! UserAccountTableViewCell

            switch ruleType {
            case .channels:
                cell.userNameLb.text = selectedChannelItems[indexPath.row - 1].channelUsername

            case .keywords:
                cell.userNameLb.text = selectedKeywordItems[indexPath.row - 1]
                cell.userImg.isHidden = true

            case .senders:
                cell.userNameLb.text = selectedSenderItems[indexPath.row - 1].email != "" ? selectedSenderItems[indexPath.row - 1].email : selectedSenderItems[indexPath.row - 1].name

            default:
                print("none")
               
            }
//            if ruleType.row == 1
//            {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "account_section_cell") as! AccountSectionTableViewCell
//                cell.layoutIfNeeded()
//                return cell

//            }
//            else
//            {
            cell.layoutIfNeeded()
            return cell
//            }

        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch ruleType {
        case .channels:
            return selectedChannelItems.count + 1
            
        case .keywords:
            return selectedKeywordItems.count + 1
            
        case .senders:
            return selectedSenderItems.count + 1
            
        default:
            return 0
        }
    }
}
extension MainRuleTableViewCell : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch ruleType {
        case .channels:
            let vc = AddNewChanneldViewController.create()
            vc.selectedChannels = AddNewRuleTableViewController.selectedChannels
            view?.navigationController?.pushViewController(vc , animated: true)
            break
        case .keywords:
            let vc = KeywordsViewController.create()
            vc.keywordsList = AddNewRuleTableViewController.selectedKeywords
            view?.navigationController?.pushViewController(vc, animated: true)
            break
        case .senders:
            let vc = SendersViewController.create()
            vc.sendersList = AddNewRuleTableViewController.selectedSenders
            view?.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            print("none")
        }

        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
