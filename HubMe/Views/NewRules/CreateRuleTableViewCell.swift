//
//  RuleTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/26/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class CreateRuleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ruleNumberLb: UILabel!
    @IBOutlet weak var tableView: SelfSizedTableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 100.0
        tableView.reloadData()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
extension CreateRuleTableViewCell : UITableViewDataSource , UITableViewDelegate
{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rule_title") as! RuleTitleTableViewCell
            cell.layoutIfNeeded()
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "user_account") as! AccountSectionTableViewCell
            cell.layoutIfNeeded()
            
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
}


