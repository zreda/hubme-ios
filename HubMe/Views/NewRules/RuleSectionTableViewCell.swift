//
//  RuleSectionTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/9/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class RuleSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var ruleSectionImg: UIImageView!
    @IBOutlet weak var ruleSectionName: UILabel!
    @IBOutlet weak var isRequired: UILabel!
    @IBOutlet weak var requiredLb: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var speratorView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
