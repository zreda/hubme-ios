//
//  NewRuleHeaderTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/12/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class NewRuleHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var ruleNumberTxt: UILabel!
    @IBOutlet weak var ruleDescTxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
