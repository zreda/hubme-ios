//
//  AccountSectionTableViewCell.swift
//  HubMe
//
//  Created by Ghonim on 1/9/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class AccountSectionTableViewCell: UITableViewCell {

    @IBOutlet weak var channelImg: RoundedImageView!
    @IBOutlet weak var channelName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
