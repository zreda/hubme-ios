//
//  TimelineFilterTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 5/22/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class TimelineFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userMail: UILabel!
    @IBOutlet weak var checkBtn: CheckBox!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkBtn.checkedImage = #imageLiteral(resourceName: "group_7")
        checkBtn.uncheckedImage = #imageLiteral(resourceName: "rectangleCopy5")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
