//
//  RoundRectView.swift
//  JAK
//
//  Created by Zeinab Reda on 5/23/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import UIKit

@IBDesignable class RoundRectView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0.0
    @IBInspectable var borderColor: UIColor = UIColor.white
    @IBInspectable var borderWidth: CGFloat = 0.5
    @IBInspectable var shadowColor: UIColor =  UIColor.white

    private var customBackgroundColor = UIColor.white
    override var backgroundColor: UIColor? {
        didSet {
            customBackgroundColor = backgroundColor!
            super.backgroundColor = UIColor.clear
        }
    }

    func setup() {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize.zero

        layer.shadowRadius = 5.0
        layer.shadowOpacity = 0.5
        super.backgroundColor = UIColor.clear
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    override func draw(_ rect: CGRect) {
        customBackgroundColor.setFill()
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius ).fill()
        let borderRect = UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, borderWidth/2, borderWidth/2, 0) )
        let borderPath = UIBezierPath(roundedRect: borderRect, cornerRadius: cornerRadius - borderWidth/2)
        borderColor.setStroke()
        borderPath.lineWidth = borderWidth
        borderPath.stroke()

        // whatever else you need drawn
    }
}
