//
//  UIView+Rounding.swift
//  Dealights
//
//  Created by Manar Magdy on 8/20/16.
//  Copyright © 2016 Manar Magdy. All rights reserved.
//

import UIKit

public extension UIView {

    /**
     This method provides rounded view.
     
     - parameter view: the view that we need to round
     
     */
    class func roundView(view: UIView) {

        view.layer.cornerRadius = 8
        view.layer.shadowOffset = CGSize.init(width: 0, height: 2)
        view.layer.shadowRadius = 2
        view.layer.shadowOpacity = 0.3
        view.clipsToBounds = true
    }

    /**
     This method provides rounded view with border
     
     - parameter view: the view that we need to round
     
     */
    class func roundViewWithBorder(view: UIView, borderColor: UIColor?) {

        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor?.cgColor
        UIView.roundView(view: view)
    }

    /**
     This method provides circular view with border
     
     - parameter view: the view that we want to circular
     
     */

    class func circularView(view: UIView) {

        view.layer.borderWidth = 1.0
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }

    /**
     This method provides circular view with border
     
     - parameter view: the view that we want to circular
     - parameter borderColor: the view border color
     
     */

    class func circularViewWithBorder(view: UIView, borderColor: UIColor) {

        view.layer.borderWidth = 1.0
        view.layer.masksToBounds = false
        view.layer.borderColor = borderColor.cgColor
        view.layer.cornerRadius = view.frame.size.width/2
        view.clipsToBounds = true
    }

    /**
     This method adds shadow effect to a specific view.
     
     - parameter view: the targeted view that we want to add shadow to
     - parameter shadowColor: the shadow color
     - parameter shadowOpacity: the shadow opacity
     - parameter shadowRadius: the shadow radius
     
     */
    static func addShadowEffect(view: UIView, shadowColor: UIColor, shadowOpacity: Float, shadowRadius: CGFloat) {

        view.layer.shadowColor = shadowColor.cgColor
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowOpacity = shadowOpacity
        view.layer.shadowRadius = shadowRadius
    }

}
