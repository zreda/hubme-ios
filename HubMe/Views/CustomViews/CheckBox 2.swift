//
//  CheckBox.swift
//  AlgazeeraPaint
//
//  Created by Zeinab Reda on 3/17/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import Foundation

import UIKit

extension UIButton {

    
    @objc dynamic var borderColor: UIColor? {
        get {
            if let cgColor = layer.borderColor {
                return UIColor(cgColor: cgColor)
            }
            return nil
        }
        set { layer.borderColor = newValue?.cgColor }
    }
    @objc dynamic var borderWidth: CGFloat {
        get { return layer.borderWidth }
        set { layer.borderWidth = newValue }
    }
    @objc dynamic var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }

//    @IBInspectable
//    public var checkedImage: UIImage = UIImage(named: "check")! as UIImage {
//
//        didSet {
//            self.checkedImage = self.checkedImage
//        }
//    }
//
//    @IBInspectable
//    public var uncheckedImage: UIImage {
//        get {
//            return self.checkedImage
//        }
//        set {
//            self.checkedImage = UIImage(named: "uncheck")! as UIImage
//        }
//    }
    

}
class CheckBox: UIButton {
    // Images
    open var checkedImage = UIImage(named: "check")! as UIImage
    open var uncheckedImage = UIImage(named: "uncheck")! as UIImage
    open var index: IndexPath = IndexPath()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
        clipsToBounds = true

    }
    // Bool property
    var isChecked: Bool = false {
        didSet {
            if isChecked == true {
                self.setImage(checkedImage, for: .normal)
            } else {
                self.setImage(uncheckedImage, for: .normal)
            }
        }
    }

    override func awakeFromNib() {
      //  self.addTarget(self, action: Selector(("buttonClicked")), for: UIControlEvents.touchUpInside)
        self.addTarget(self, action: #selector(buttonClicked), for: UIControlEvents.touchUpInside)
        self.isChecked = false
    }

    @objc func buttonClicked(sender: UIButton) {
        if sender == self {
            isChecked = !isChecked
        }
    }
}
