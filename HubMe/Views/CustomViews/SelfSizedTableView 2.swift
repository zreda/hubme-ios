//
//  SelfSizedTableView.swift
//  Venue App
//
//  Created by Jahid Hasan Polash on 4/6/18.
//  Copyright © 2018 InfancyIT. All rights reserved.
//  Reference: https://medium.com/@dushyant_db/swift-4-recipe-self-sizing-table-view-2635ac3df8ab

import UIKit

class SelfSizedTableView: UITableView {

    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
}
