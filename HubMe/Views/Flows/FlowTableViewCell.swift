//
//  FlowTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 12/20/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class FlowTableViewCell: UITableViewCell {
    @IBOutlet weak var flowName: UILabel!
    @IBOutlet weak var flowActivity: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
