//
//  RuleTitleTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/24/20.
//  Copyright © 2020 Orange. All rights reserved.
//

import UIKit

class RuleTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var ruleTitleLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
