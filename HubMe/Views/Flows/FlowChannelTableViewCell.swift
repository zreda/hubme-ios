//
//  FlowChannelTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 1/22/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class FlowChannelTableViewCell: UITableViewCell {

    @IBOutlet weak var flowName: UILabel!

    @IBOutlet weak var channel1: UIImageView!
    @IBOutlet weak var channel2: UIImageView!
    @IBOutlet weak var channel3: UIImageView!
    @IBOutlet weak var channel4: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
