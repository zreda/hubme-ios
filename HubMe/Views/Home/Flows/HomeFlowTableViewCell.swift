//
//  HomeFlowTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/19/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class HomeFlowTableViewCell: UITableViewCell  {
    
    @IBOutlet weak var showMoreBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var numOfChannels:Int = 0
    @IBOutlet weak var footerView: UIView!
    var flows: [FlowItem] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        collectionView.allowsMultipleSelection = false
        collectionView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getFlows(notification:)), name: Notification.Name(HubMeConstants.Notifications.HomeFlow), object: nil)
    }
    
    override func layoutSubviews() {
        if Helper.getUserDefault(key: HubMeConstants.userDefault.selectedFlowId) as! String == "" ||  Helper.getUserDefault(key: HubMeConstants.userDefault.selectedFlowId) == nil

        {
            self.collectionView.scrollToItem(at:IndexPath(item: 0, section: 0), at: .centeredVertically, animated: true)
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func getFlows(notification: Notification) {
        // Take Action on Notification
        self.flows = notification.object as! [FlowItem]
        self.numOfChannels = notification.userInfo!["Channels"] as! Int
        self.collectionView.reloadData()
    }
    
    @objc func addNewFlowBtnAction(_sender:Any)
    {
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.addNewFlow), object: nil, userInfo: nil)
    }
    
    @objc func selectFlowAction(_sender:UIButton)
    {
        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value:String(describing: flows[_sender.tag].id as! Int))
//        Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowIndex, value:_sender.index)

        if collectionView != nil
        {
            collectionView.reloadData()
        }
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.updateFeeds), object: _sender.tag, userInfo: nil)
    }
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
}
extension HomeFlowTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if flows.count > 0
        {
            return flows.count + 1
        }
        else
        {
            return 2
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0
        {
            if let cell: HomeFlowCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? HomeFlowCollectionViewCell
            
            {
//                cell.addFlowBtn.titleLabel?.numberOfLines = 1
//                cell.addFlowBtn.titleLabel?.lineBreakMode = .byTruncatingTail

                if numOfChannels > 0
                {
//                    cell.addFlowBtn.setTitle("", for: .normal)
                    cell.addFlowBtn.setImage(#imageLiteral(resourceName: "addChannel"), for: .normal)
                    cell.addFlowBtn.addTarget(self, action: #selector(HomeFlowTableViewCell.addNewFlowBtnAction), for: .touchUpInside)
                    cell.addFlowBtn.isUserInteractionEnabled = true
                    cell.addFlowBtn.setBackgroundImage(nil, for: .normal)
                }
                else
                {
                    cell.addFlowBtn.setImage(nil, for: .normal)
                    cell.addFlowBtn.isUserInteractionEnabled = false
                    cell.addFlowBtn.setBackgroundImage(#imageLiteral(resourceName: "addFlowEmpty"), for: .normal)
                }
                return cell
            }
            
        }
        else
        {
            
            if let cell: HomeFlowCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionFlowCell", for: indexPath) as? HomeFlowCollectionViewCell
            {
                
                if flows.count > 0
                {
//                    cell.addFlowBtn.setTitle(flows[indexPath.row - 1].name, for: .normal)
                    cell.flowTitle.text = flows[indexPath.row - 1].name
                    cell.addFlowBtn.tag = indexPath.row - 1
                    cell.addFlowBtn.addTarget(self, action: #selector(HomeFlowTableViewCell.selectFlowAction), for: .touchUpInside)
                    if Helper.getUserDefault(key: HubMeConstants.userDefault.selectedFlowId) as? String == "" || Helper.getUserDefault(key: HubMeConstants.userDefault.selectedFlowId) as? String == nil // set first flow select by default
                    {
                        if indexPath.row  == 1
                        {
                            Helper.saveUserDefault(key: HubMeConstants.userDefault.selectedFlowId, value: String(describing: flows[indexPath.row - 1].id as! Int))
                            cell.flowSelected = true
                            cell.addFlowBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
                            cell.flowTitle.textColor = #colorLiteral(red: 0.9937841296, green: 0.9938072562, blue: 0.9937947392, alpha: 1)
//                            cell.addFlowBtn.setTitleColor(#colorLiteral(red: 0.9937841296, green: 0.9938072562, blue: 0.9937947392, alpha: 1), for: .normal)
                            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.updateFeeds), object: indexPath.row-1, userInfo: nil)
                        }
                        else
                        {
                            cell.flowSelected = false
                            cell.addFlowBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                            cell.flowTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

//                            cell.addFlowBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                        }
                        
                    }
                    else
                    {
                        if String(describing: flows[indexPath.row - 1].id as! Int) == Helper.getUserDefault(key: HubMeConstants.userDefault.selectedFlowId) as? String
                        {
                            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.updateFeeds), object: indexPath.row - 1, userInfo: nil)
                            
                            cell.flowSelected = true
                            cell.addFlowBtn.backgroundColor = #colorLiteral(red: 1, green: 0.4745098039, blue: 0, alpha: 1)
                            cell.flowTitle.textColor = #colorLiteral(red: 0.9937841296, green: 0.9938072562, blue: 0.9937947392, alpha: 1)
//                            cell.addFlowBtn.setTitleColor(#colorLiteral(red: 0.9937841296, green: 0.9938072562, blue: 0.9937947392, alpha: 1), for: .normal)
                        }
                        else
                        {
                            cell.flowSelected = false
                            cell.addFlowBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                            cell.flowTitle.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

//                            cell.addFlowBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
                        }
                        
                    }
                    return cell
                    
                }
                else
                {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noFlowsCell", for: indexPath) as! EmptyHomeFlowCollectionViewCell
                    
                    if numOfChannels > 0
                    {
                        cell.noFlows.text = "Add Flows to start".localized()
                    }
                    else
                    {
                        cell.noFlows.text = "Add channel first to add a flow".localized()
                    }
                    return cell
                }
            }
            
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if indexPath.row == 0
        {
            return CGSize(width: 100, height: 100)
        }
        else
        {
            if flows.count > 0
            {
                return CGSize(width: 80, height: 60)
            }
            else
            {
                return CGSize(width: 250, height: 60)
                
            }
        }
        
    }
    
    
}


