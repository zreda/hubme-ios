//
//  HomeFlowCollectionViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/19/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class HomeFlowCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var addFlowBtn: RoundedButton!
    @IBOutlet weak var flowTitle: UILabel!
    var flowSelected:Bool = false
    
}
