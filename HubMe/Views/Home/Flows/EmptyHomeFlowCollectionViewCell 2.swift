//
//  EmptyHomeFlowCollectionViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/27/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class EmptyHomeFlowCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var noFlows: UILabel!
}
