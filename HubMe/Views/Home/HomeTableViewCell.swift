//
//  HomeTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 9/28/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var num_of_channels: UILabel!
    @IBOutlet weak var activity_date: UILabel!
    @IBOutlet weak var channel1Img: UIImageView!
    @IBOutlet weak var channel2Img: UIImageView!
    @IBOutlet weak var channel3Img: UIImageView!
}
