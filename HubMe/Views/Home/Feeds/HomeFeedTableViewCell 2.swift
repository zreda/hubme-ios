
//
//  HomeFeedTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/21/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class HomeFeedTableViewCell: UITableViewCell {
    @IBOutlet weak var feed_logo: UIImageView!
    @IBOutlet weak var feed_from: UILabel!
    @IBOutlet weak var feed_subject: UILabel!
    @IBOutlet weak var feed_time: UILabel!
    @IBOutlet weak var feed_content: UILabel!
    @IBOutlet weak var feedBg: CardView!
    @IBOutlet weak var readBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureCell(feed: MessageItem) {
        
        
        if (feed.iconUrl?.hasPrefix("http"))! {
            feed_logo.sd_setImage(with: URL(string: feed.iconUrl!), placeholderImage: #imageLiteral(resourceName: "loading_image"))
            
        } else {
            feed_logo.sd_setImage(with: URL(string: (HubMeConstants().main_url+"ios/"+feed.iconUrl!) ), placeholderImage: #imageLiteral(resourceName: "loading_image"))
        }
        feed_subject.text = feed.title
        
        feed_from.text = feed.from
        
        if feed.isRead == false || feed.isRead == nil
        {
            readBtn.imageView?.image = #imageLiteral(resourceName: "unread")

        }
        else
        {
            readBtn.imageView?.image = #imageLiteral(resourceName: "read")

        }
        
        feed_time.text = Helper.timeAgoSinceDate(date: Helper.convertMillSecondStringtoDate(str: feed.date), numericDates: false)
        feed_content.text = feed.snippet ?? ""
    }

}
