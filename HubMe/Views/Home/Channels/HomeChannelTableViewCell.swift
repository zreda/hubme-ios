//
//  HomeChannelTableViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/19/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit
import SDWebImage

class HomeChannelTableViewCell: UITableViewCell  {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var showMoreBtn: UIButton!
    var registeredChannels:[GroupedChannel] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.allowsMultipleSelection = false
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.getRegisteredChannels(notification:)), name: Notification.Name(HubMeConstants.Notifications.HomeChannels), object: nil)
        
    }
    
    @objc func getRegisteredChannels(notification: Notification) {
        // Take Action on Notification
        self.registeredChannels = notification.object as! [GroupedChannel]
        self.collectionView.reloadData()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    @objc func addNewChannelBtnAction(_sender:Any)
    {
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.addNewChannel), object: nil, userInfo: nil)
        
    }
    
    @objc func getChannelFeedsBtnAction(_sender:UIButton)
    {
        
        NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.getChannelFeeds), object: _sender.tag, userInfo: ["accountsItem":registeredChannels[_sender.tag]])
    }
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
    }
}

extension HomeChannelTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if registeredChannels.count > 0
        {
            return registeredChannels.count + 1
        }
        else
        {
            return 2
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0
        {
            if let cell: HomeChannelCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? HomeChannelCollectionViewCell
            {
                cell.channelBtn.addTarget(self, action: #selector(HomeChannelTableViewCell.addNewChannelBtnAction), for: .touchUpInside)
                cell.channelBtn.setBackgroundImage(#imageLiteral(resourceName: "button_bg"), for: .normal)
                cell.channelBtn.setImage(#imageLiteral(resourceName: "addChannel"), for: .normal)
                return cell
            }
            
        }
        else
        {
            if registeredChannels.count > 0
            {
                if let cell: HomeChannelCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionChannelCell", for: indexPath) as? HomeChannelCollectionViewCell
                {
                    cell.channelBtn.tag = indexPath.row - 1
                    cell.channelBtn.setBackgroundImage(#imageLiteral(resourceName: "button_bg"), for: .normal)
                    cell.channelImg.sd_setImage(with: URL(string: (HubMeConstants().main_url + "/ios" + registeredChannels[indexPath.row - 1].accounts![0].icon!)), placeholderImage: #imageLiteral(resourceName: "loading_image"))
                    cell.channelName.text = registeredChannels[indexPath.row - 1].accounts![0].name
                    cell.channelBtn.addTarget(self, action: #selector(HomeChannelTableViewCell.getChannelFeedsBtnAction), for: .touchUpInside)
                    return cell
                }
            }
            else
            {
                return collectionView.dequeueReusableCell(withReuseIdentifier: "noChannelCell", for: indexPath)
                
            }
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        if indexPath.row == 0
        {
            return CGSize(width: 75, height: 100)
        }
        else
        {
            if registeredChannels.count > 0
            {
                return CGSize(width: 65, height: 100)
            }
            else
            {
                return CGSize(width: 230, height: 100)
                
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if registeredChannels.count > 0
        {
            NotificationCenter.default.post(name: Notification.Name(HubMeConstants.Notifications.getChannelFeeds), object: indexPath.row, userInfo: ["accountsItem":registeredChannels[indexPath.row]])
        }
        
    }
}


