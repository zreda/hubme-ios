//
//  HomeChannelCollectionViewCell.swift
//  HubMe
//
//  Created by Zeinab Reda on 2/19/19.
//  Copyright © 2019 Orange. All rights reserved.
//

import UIKit

class HomeChannelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var channelBtn: UIButton!
    @IBOutlet weak var channelName: UILabel!
    @IBOutlet weak var channelImg: UIImageView!
}
